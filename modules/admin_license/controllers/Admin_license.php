<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_license extends Admin_Controller  {
function __construct()
{
    parent::__construct();
    $this->load->model('admin_license_model', '', TRUE);
    $this->load->model('files_model', '', TRUE);
    $this->data['title'] = 'Лицензии';
    $this->logs_data_type = 'Лицензии';
}

function index()
{
    $this->data['licenses'] = $this->admin_license_model->get_license_list();
    $this->data['title'] = 'Все лицензии';
    $this->data['users_list'] = $this->load->view('license_list_view',$this->data,TRUE);
    $this->load->view('admin/base_node_dashboard_view');
}

function add_license_page()
{
    $this->data['title'] = 'Добавить лицензию';
    $this->load->view('add_license_view',$this->data);
}

/**
* Добавление лицензии
* @param  
* @return вывод результатов сохранения
*/
function add_license()
{
    $this->view_type = 2;
    $additional_data = array('title' => $this->input->post('title'),
                            'text' => $this->input->post('text'));
    $result = $this->admin_license_model->add_license($additional_data);
    $this->data['result'] = $result['success'];
    if($this->data['result'])
    {
	$config['upload_path'] = './uploads/files';
	$config['allowed_types'] = 'pdf|jpg|png';
	$config['max_size']	= '24000';
	$config['max_width']  = '2000';
	$config['max_height']  = '1500';
	$this->load->library('upload', $config);
	if ( ! $this->upload->do_upload('image'))
	{
	    $this->data['result'] =  $this->upload->display_errors();
	}	
	else
	{
	    $data = array('upload_data' => $this->upload->data());
	    $this->data['result'] = $this->files_model->add_file($result['insert_id'],'license','/uploads/files/'.$data['upload_data']['file_name']);
	}
	//логирование
	$this->admin_logs_model->add($result['insert_id'], $this->logs_data_type,$this->input->post('title'),'/admin_license/edit_license_page/'.$result['insert_id'],'Добавление');
    }
    echo $this->load->view('admin/json_result_view',$this->data);
}

function edit_license_page($license_id = NULL)
{
    $this->data['license'] = $this->admin_license_model->get_license_by_id($license_id);
    $this->data['title'] = $this->data['license'][0]->title;
    $this->data['users_list'] = $this->load->view('edit_license_page_view',$this->data,TRUE);
    $this->load->view('admin/base_node_dashboard_view');
}

/**
* Изменение лицензии
* @param  $license_id айди лицензии
* @return вывод результатов сохранения
*/
function edit_license($license_id = NULL)
{
    $this->view_type = 2;
    $additional_data = array('title' => $this->input->post('title'),
                            'text' => $this->input->post('text'));
    $result = $this->admin_license_model->edit_license($license_id,$additional_data);
    $this->data['result'] = $result;
    if($this->data['result'])
    {
	$config['upload_path'] = './uploads/files';
	$config['allowed_types'] = 'pdf|jpg|png';
	$config['max_size']	= '24000';
	$config['max_width']  = '2000';
	$config['max_height']  = '1500';
	$this->load->library('upload', $config);
	if ( ! $this->upload->do_upload('image'))
	{
	    $this->data['result'] =  $this->upload->display_errors();
	}	
	else
	{
	    $this->files_model->clear_all_parent_files($license_id,'license');
	    $data = array('upload_data' => $this->upload->data());
	    $this->data['result'] = $this->files_model->add_file($license_id,'license','/uploads/files/'.$data['upload_data']['file_name']);
	}
	//логирование
	$this->admin_logs_model->add($license_id, $this->logs_data_type,$this->input->post('title'),'/admin_license/edit_license_page/'.$license_id,'Редактирование');
    }
    echo $this->load->view('admin/json_result_view',$this->data);
}

/**
* Удаление лицензии
* @param  $license_id айди лицензии
* @return вывод результатов сохранения
*/
function delete_license($license_id = NULL)
{
    $this->view_type = 2;
    $this->data['license'] = $this->admin_license_model->get_license_by_id($license_id);
    $this->admin_license_model->delete_license($license_id);
    $this->files_model->clear_all_parent_files($license_id,'license');
    //логирование
    $this->admin_logs_model->add($license_id, $this->logs_data_type,$this->data['license'][0]->title,'/admin_license/edit_license_page/'.$license_id,'Удаление');
}

}