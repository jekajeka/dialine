<?
class Admin_license_model extends CI_Model {

private $db_table = 'license';

function Admin_license_model()
{
    parent::__construct();
}

function add_license($additional_data = array())
{
    $insert_result = array();
    $insert_result['success'] = $this->db->insert($this->db_table, $additional_data);
    $insert_result['insert_id'] = $this->db->insert_id();
    return $insert_result;
}

function get_license_list()
{
    $this->db->select('*')->from($this->db_table)->order_by("title", "ASC");
    $data = $this->db->get();
    return $data->result();
}

function get_license_by_id($license_id = NULL)
{
    $this->db->select(array('title',$this->db_table.'.id','path','text'));
    $this->db->from($this->db_table);
    $this->db->join('files','files.parent_id = '.$license_id.' AND files.parent_type="license"','LEFT');
    $this->db->where($this->db_table.'.id', $license_id);    
    return $this->db->get()->result();
}

function edit_license($id = NULL,$additional_data = array())
{
    $this->db->where('id', $id);
    return $this->db->update($this->db_table, $additional_data);
}

function delete_license($id = NULL)
{
    $this->db->where('id', $id);
    $this->db->delete($this->db_table);
}

}