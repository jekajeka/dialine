<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vacancy extends Frontend_Controller  {
function __construct()
{
    parent::__construct();
    $this->load->model('admin_vacancy/admin_vacancy_model', '', TRUE);
    $this->load->model('info/frontend_info_model', '', TRUE);
    $this->data['title'] = 'Вакансии/Диалайн, Волгоград';
    $this->config->load('dialine_config');
}


function index()
{
    $this->data['vacancy'] = $this->admin_vacancy_model->get_moderated_vacancy();
    $this->data['info_data'] = $this->frontend_info_model->get_info_data($this->config->item('vacancy_page_id'));
    $this->data['vrachi_vacancy'] = array();
    $this->data['medpersonal'] = array();
    $this->data['other'] = array();
    foreach($this->data['vacancy'] as $vacancy)
    {
        if($vacancy->vacancy_type == 'Врачи')
            $this->data['vrachi_vacancy'][] = $vacancy;
        if($vacancy->vacancy_type == 'Средний медицинский персонал')
            $this->data['medpersonal'][] = $vacancy;
        if($vacancy->vacancy_type == 'Прочие вакансии')
            $this->data['other'][] = $vacancy;
    }
    $this->getSeo('complex_page', 8);
    $this->load->view('vacancy_page_view',$this->data);
}

function send_vacancy()
{
    $this->view_type = 2;
    $config['upload_path'] = './uploads/vacancy';
    $config['allowed_types'] = 'doc|docx|pdf';
    $config['max_size']	= '10000';
    $config['max_width']  = '3000';
    $config['max_height']  = '2000';
    $this->load->library('upload', $config);
    if ( ! $this->upload->do_upload('file'))
    {
        $this->data['result'] =  $this->upload->display_errors();
    }	
    else
    {	    
	$data = array('upload_data' => $this->upload->data());
        $link = $_SERVER["DOCUMENT_ROOT"].'/uploads/vacancy/'.$data['upload_data']['file_name'];
    }
    
    $this->load->library('email');
    $this->email->from('notify@dialine.org', 'Клиника Диалайн');
    $this->email->to('hr_otdel@dialine.org');
    $message = 'Имя: '.$this->input->post('name',TRUE).'
Телефон:  '.$this->input->post('phone',TRUE).'
Ссылка на резюме:  '.$this->input->post('resume_link',TRUE).'
Список вакансий: '.$this->input->post('vacancy_list',TRUE);
    $this->email->attach($link);
    $this->email->subject('Новое резюме (диалайн.рф)');
    $this->email->message($message);
    $this->email->send();
    //echo $message;
}


}
