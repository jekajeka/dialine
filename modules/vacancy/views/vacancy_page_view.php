<div class="row row_title_image">
    <div class="container">
        <h1>Вакансии</h1>
    </div>
</div>
<div class="row row_breadcrumbs">
    <div class="container">
        <ul>
            <li><a href="/">Главная</a></li> | <li><a href="/info">О компании</a></li> | <li><a href="/vacancy">Вакансии</a></li>
        </ul>
    </div>
</div>
<div class="row row_price">
    <div class="container">
        
        <div class="price_list_wrap" style="float: left;font-size: 1.33rem;">
            <div style="float: left;font-family: 'Franklin Gothic Book'" >
                <?=$info_data[0]->text?>
            </div>
            <div class="structured_data_wrap structured_data_wrap_vacancy">
                <?if(count($vrachi_vacancy)){?>
                <div class="structured_data_content_wrap">
                    <a class="structured_data_title">
                        <h4>Врачи</h4><span class="structured_data_title_qty"><?=count($vrachi_vacancy)?></span>
                    </a>
                    <div class="structured_data_table_wrap">
                        <div class="structured_data_table_wrap_order">
                            <?foreach($vrachi_vacancy as $single_vacancy){?>
                            <div class="structured_data_table_line">
                                <div class="structured_data_table_line_border">
                                    <h4><a><?=$single_vacancy->title?></a></h4>
                                    <div class="structured_data_table_line_price"><?if($single_vacancy->price) echo $single_vacancy->price.' р.'?></div>
                                    <div class="vacancy_description">
                                        <?=$single_vacancy->text?>
                                    </div>
                                </div>
                                <a id="vacancy_<?=$single_vacancy->id?>" class="structured_data_table_link">Откликнуться</a>
                            </div>
                            <?}?>
                        </div>
                    </div>
                </div>
                <?}?>
                <?if(count($medpersonal)){?>
                <div class="structured_data_content_wrap">
                    <a class="structured_data_title">
                        <h4>Средний медицинский персонал</h4><span class="structured_data_title_qty"><?=count($medpersonal)?></span>
                    </a>
                    <div class="structured_data_table_wrap">
                        <div class="structured_data_table_wrap_order">
                            <?foreach($medpersonal as $single_vacancy){?>
                            <div class="structured_data_table_line">
                                <div class="structured_data_table_line_border">
                                    <h4><a><?=$single_vacancy->title?></a></h4>
                                    <div class="structured_data_table_line_price"><?if($single_vacancy->price) echo $single_vacancy->price.' р.'?></div>
                                    <div class="vacancy_description">
                                        <?=$single_vacancy->text?>
                                    </div>
                                </div>
                                <a id="vacancy_<?=$single_vacancy->id?>" class="structured_data_table_link">Откликнуться</a>
                            </div>
                            <?}?>
                        </div>
                    </div>
                </div>
                <?}?>
                <?if(count($other)){?>
                <div class="structured_data_content_wrap">
                    <a class="structured_data_title">
                        <h4>Прочие вакансии</h4><span class="structured_data_title_qty"><?=count($other)?></span>
                    </a>
                    <div class="structured_data_table_wrap">
                        <div class="structured_data_table_wrap_order">
                            <?foreach($other as $single_vacancy){?>
                            <div class="structured_data_table_line">
                                <div class="structured_data_table_line_border">
                                    <h4><a><?=$single_vacancy->title?></a></h4>
                                    <div class="structured_data_table_line_price"><?if($single_vacancy->price) echo $single_vacancy->price.' р.'?></div>
                                    <div class="vacancy_description">
                                        <?=$single_vacancy->text?>
                                    </div>
                                </div>
                                <a id="vacancy_<?=$single_vacancy->id?>" class="structured_data_table_link">Откликнуться</a>
                            </div>
                            <?}?>
                        </div>
                    </div>
                </div>
                <?}?>
            </div>
        </div>
        <div class="service_result_wrap_wrap">
            <div class="service_result_wrap_inner">
                <div class="service_result_wrap">
                    <div class="service_result_calc_wrap box_shadow">
                        <div class="service_result_total">Вы собираетесь откликнуться на вакансию</div>
                        <div class="service_result_data_wrap"></div>
                        <form id="send_vacancy_form">
                            <input type="hidden" id="vacancy_list" name="vacancy_list">
                            <div class="total_data">
                                <input name="name" id="vacancy_name" class="vacancy_input" type="text" placeholder="Имя*" />
                                <input name="phone" id="vacancy_phone" class="vacancy_input" type="text" placeholder="Телефон*" />
                                <input name="resume_link" id="resume_link" class="vacancy_input" type="text" placeholder="Ссылка на резюме (при наличии)" />
                                <a class="vacancy_file">
                                    Прикрепить резюме
                                    <input  type="file" name="file" />
                                </a>
                                <div class="vacancy_agree">
                                    <input type="checkbox"  id="vacancy_form_agree"><label>Оставляя свои персональные данные, Вы даете добровольное согласие на их обработку. <a class="open-popup-link" target="_blank" href="#test-popup">Подробнее</a></label>
                                </div>
                            </div>
                        </form>
                        <a id="send_vacancy" class="green_btn">Откликнуться</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>