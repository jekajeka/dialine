<div class="row row_breadcrumbs">
    <div class="container">
        <ul>
            <li><a href="">Главная</a></li> | <li><a href="/dolg">Оплата оказанных услуг</a></li>
        </ul>
    </div>
</div>
<div class="row row_title_h1">
    <div class="container">
        <h1>Оплата оказанных услуг</h1>
    </div>
</div>
<div class="row help_row">
    <div class="container">
        <div class="dolg_text">
            <div style="font-size: 2.5rem;width: 100%;text-align: center;">
                <p>
                    <img src="/frontend/images/dolg.png" width="200" />
                </>
                <p>
                    Сервис временно недоступен по техническим причинам. Приносим извинения за неудобства. Оплатить услуги можно в клинике, где они были оказаны.
                </p>
            </div>
        </div>
    </div>
</div>