
    <div class="row row_breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="">Главная</a></li> | <li><a href="/dolg">Оплата оказанных услуг</a></li>
            </ul>
        </div>
    </div>
    <div class="row row_title_h1">
        <div class="container">
            <h1>Оплата оказанных услуг</h1>
        </div>
    </div>
    <div class="row help_row">
        <div class="container">
            <div class="dolg_text">
                <p>
                    В данном разделе вы можете оплатить уже оказанные клиниками ДИАЛАЙН услуги, по которым имеется задолженность
                </p>
            </div>
            <div class="dolg_form_wrap">
                <form method="post" id="dolg-form" action="https://money.yandex.ru/eshop.xml">
                    <div class="input">
                        <label for="CustName"><span>ФИО<dfn class="indeed">*</dfn></span></label>
                        <input class="inp" name="CustName" id="CustName" type="text">
                    </div>
                    <div class="input">
                        <label for="date"><span>Дата рождения<dfn class="indeed">*</dfn></span></label>
                        <input class="inp hasDatepicker" name="date" id="date" type="text">
                    </div>
                    <div class="input">
                        <label for="CustEMail"><span>Email<dfn class="indeed">*</dfn></span></label>
                        <input class="inp" value="" name="CustEMail" id="CustEMail" type="text">
                    </div>
                    <div class="input">
                        <label for="cps_phone"><span>Телефон<dfn class="indeed">*</dfn></span></label>
                        <input class="inp" value="" name="cps_phone" id="cps_phone" type="text">
                    </div>
                    <div class="input">
                        <label for="clinics"><span>Медицинский центр<dfn class="indeed">*</dfn></span></label>
                        <select id="clinics" name="clinics" class="selectBox clinic_dolg" style="width: 335px">
                            <option value="">Выберите медицинский центр</option>
                            <?foreach($clinics as $clinic){?>
                            <option value="<?=$clinic->title?>"><?=$clinic->title?></option>
                            <?}?>
                        </select>
                    </div>
                    <div class="input">
                        <label for="sum"><span>Сумма платежа<dfn class="indeed">*</dfn></span></label>
                        <input class="inp" value="" name="sum" id="sum" type="text">
                    </div>
                    <div class="question">
                        <div class="checkbox">
                            <label for="PC">Со счета в Яндекс.Деньгах</label>
                            <input name="paymentType" value="PC" id="PC" checked="checked" type="radio">
                        </div>
                        <div class="checkbox">
                            <label for="AC">С банковской карты</label>
                            <input name="paymentType" value="AC" id="AC" type="radio">
                        </div>
                        <div class="checkbox">
                            <label for="WM">Со счета WebMoney</label>
                            <input name="paymentType" value="WM" id="WM" type="radio">
                        </div>
                        <div class="checkbox">
                            <label for="GP">По коду через терминал</label>
                            <input name="paymentType" value="GP" id="GP" type="radio">
                        </div>
                        <div class="checkbox">
                            <label for="AB">Оплата через Альфа-Клик</label>
                            <input name="paymentType" value="AB" id="AB" type="radio">
                        </div>
                        <div class="checkbox">
                            <label for="PB">Оплата через интернет-банк Промсвязьбанка</label>
                            <input name="paymentType" value="PB" id="PB" type="radio">
                        </div>
                        <div class="checkbox">
                            <label for="QW">Оплата через QIWI Wallet</label>
                            <input name="paymentType" value="QW" id="QW" type="radio">
                        </div>
                    </div>
                    <div class="input check">
                        <input name="check" id="check" value="1" type="checkbox">
                            <label for="check">Я подтверждаю свое согласие на обработку и передачу третьим лицам указанных на форме персональных данных в соответствии с требованиями Федерального закона No. 152-Ф3 от 27.07.06 "О персональных данных".</label>
                    </div>
                    <div class="input">
                        <input id="send_pay_form" class="green_btn box_shadow" style="border: none;margin-top: 20px" class="submit-form" name="submit" value="Оплатить" type="submit">
                            <p class="small">Возврат безналичных средств на банковскую карту, осуществляется путем личной подачи письменного заявления в  ООО «ММЦ «ДИАЛАЙН»</p>
                    </div>
                    
                </form>
                <form method="post" id="dolg-form-send" action="https://money.yandex.ru/eshop.xml">
                    <input name="shopSuccessURL" value="https://dialine_main.dev/dolg" type="hidden">
                    <input name="shopFailURL" value="https://dialine_main.dev/dolg" type="hidden">
                    <input name="cps_email" value="" type="hidden">
                    <input name="orderDetails" value="" type="hidden">
                    <input name="scid" value="36257" type="hidden">
                    <input name="ShopID" value="102859" type="hidden">
                    <input name="paymentType" value="" type="hidden">
                    <input name="sum" value="" type="hidden">
                    <input name="CustomerNumber" id="CustomerNumber" value="" type="hidden">
                    <input id="send_pay_yandex_form"  style="display: none" class="submit-form" name="submit" value="Оплатить" type="submit" />
                </form>
            </div>
        </div>
    </div>