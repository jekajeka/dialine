<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dolg extends Frontend_Controller  {
function __construct()
{
    parent::__construct();
    $this->data['title'] = 'Оплата оказанных услуг/Диалайн, Волгоград';
    $this->load->model('info/frontend_info_model', '', TRUE);
    $this->load->model('clinic/frontend_clinics_model','', TRUE);
}


function index()
{
    $this->data['clinics'] = $this->frontend_clinics_model->get_clinics_list();
    //$this->load->view('dolg_view.php',$this->data); //временно отключили до возврата онлайн касс
    $this->load->view('dolg_denied_view.php',$this->data);
}

function send_data()
{
    $this->view_type = 2;
    $this->load->library('email');
    $this->email->from('notify@dialine.org', 'Клиника Диалайн');
    $this->email->to('abramova.i@dialine.org ');
    $message = 'Имя: '.$this->input->post('name').'
Телефон:  '.$this->input->post('phone');
    if($this->input->post('order'))
        $message .= '
'.$this->input->post('order');
$message = '
ФИО: '.$this->input->post('CustName').'
Дата рождения:'.$this->input->post('date').'
EMail:'.$this->input->post('CustEMail').'
Телефон:'.$this->input->post('cps_phone').'
Клиника:'.$this->input->post('clinics').'
Сумма:'.$this->input->post('sum').'
Тип оплаты:'.$this->input->post('paymentType');
    $this->email->subject('Уведомление об оплате на сайте (диалайн.рф)');
    $this->email->message($message);
    $this->email->send();
    echo $message;
}


}?>