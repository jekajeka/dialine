<?
class Admin_seo_model extends CI_Model
{
	private $enterprise_query_data_list = '';
	private $db_table = 'seo_meta_tags';

	function __construct()
	{
		parent::__construct();
	}

	function add_seo_entity_table($page)
	{
		$data['title'] = '';
		$data['description'] = '';
		$data['keywords'] = '';
		$data['comment'] = '';
		$data['entity_table'] = $page;
		$data['entity_id'] = 0;
		return $this->db->insert($this->db_table, $data);
	}

	function get_all_static_seo()
	{
		$data = $this->db->get_where($this->db_table, ['entity_id' => 0]);
		return $data->result();
	}

	function get_seo_for_page($page = NULL)
	{
		$data = $this->db->get_where($this->db_table, ['entity_table' => $page], 1);
		return $data->result();   
	}

	
	function edit_seo_for_page($page, $data){
		$this->db->where('entity_table', $page);
		$this->db->where('entity_id', 0);
		return $this->db->update($this->db_table, $data);
	}


// 	function delete_clinic($id = NULL)
// 	{
// 		$this->db->where('id', $id);
// 		$this->db->delete($this->db_table);
// 	}
}
