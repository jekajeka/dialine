<div class="">
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
			<h2><?=$title?></h2>
			<div class="clearfix"></div>
			</div>
			<div class="x_content">
			<br />
			<form id="add_static_seo_page" data-parsley-validate class="form-horizontal form-label-left">
				<div class="item form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="page">Entity_table (типа название можно считать) <span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					<input id="page" class="form-control col-md-7 col-xs-12"   name="page" placeholder="" required="required" type="text">
					</div>
				</div>
				<div class="item form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="comment">Комментарий (описание)</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					<input id="comment" class="form-control col-md-7 col-xs-12"   name="comment" placeholder="Комментарий" type="text">
					</div>
				</div>
				<?if(isset($seo)):?>
					<?$this->load->view('admin/seo', $seo);?>
				<?endif;?>
				<div class="ln_solid"></div>
				<div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="add_user_final">
					<button type="submit" class="btn btn-success">Добавить</button>
					</div>
				</div>
			</form>  
			</div>
		</div>
		</div>
	</div>
</div>
<script src="/admin_js/admin_seo/add_static_seo.js"></script>
