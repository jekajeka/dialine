<div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?=$title?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a title="Удалить клинику" href="/admin_clinics/delete_clinic/<?//=$clinic[0]->id?>" class="add-link"><i class="fa fa-trash"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <input type="hidden" id="id" value="<?=$seoData[0]->entity_table?>">
                    <form id="edit_static_seo_page" data-parsley-validate class="form-horizontal form-label-left">
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="comment">Комментарий (описание)</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
							<input id="comment" class="form-control col-md-7 col-xs-12" value="<?=$seoData[0]->comment?>"  name="comment" placeholder="Комментарий" type="text">
							</div>
						</div>
						<?if(isset($seo)):?>
							<?$this->load->view('admin/seo', $seo);?>
						<?endif;?>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="add_user_final">
                          <button type="submit" class="btn btn-success">Сохранить</button>
                        </div>
                      </div>
                    </form>  
                  </div>
                </div>
              </div>
            </div>
</div>
<script src="/admin_js/admin_seo/edit_static_seo.js"></script>
