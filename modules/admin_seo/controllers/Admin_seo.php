<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_seo extends Admin_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('admin_seo_model', '', TRUE);
		$this->data['title'] = 'Редактирование сео-тегов разных страниц';
	}

	function add_seo(){
		$this->view_type = 2;
		$data = array(
			'page' => $this->input->post('page')
		);
		if($this->admin_seo_model->add_seo_entity_table($data['page'])){
			$data = $this->readSeoFromForm();
			$this->data['result'] = $this->admin_seo_model->edit_seo_for_page($this->input->post('page'), $data);
		}
		echo $this->load->view('admin/json_result_view',$this->data);
	}



	function edit_static_page($page)
	{
		//$this->data['seos'] = $this->admin_seo_model->get_seo_for_page($page);
 		$this->data['seoData'] = $this->admin_seo_model->get_seo_for_page($page);
		$this->data['title'] = 'Изменить сео теги';
		$this->getSeo($page, 0);
		$this->load->view('edit_static_page_seo_view',$this->data);
	}
	
	function edit_static_seo($page){
		$this->view_type = 2;
		$data = $this->readSeoFromForm();
		$data['comment'] = $this->input->post('comment');
		$this->data['result'] = $this->admin_seo_model->edit_seo_for_page($page, $data);
		echo $this->load->view('admin/json_result_view', $this->data);
	}
	
	function add_static_page_seo(){
		$this->data['title'] = 'Новая статическая страница мета тегов';
		$this->data['page'] = '';
		$seo = [
			'title' => '',
			'description' => '',
			'keywords' => ''
		];
		$this->data['seo'] = $seo;
		$this->load->view('add_static_page_view',$this->data);
	}

	function index()
	{
		$this->data['pages'] = $this->admin_seo_model->get_all_static_seo();
		$this->data['title'] = 'Все страницы';
		$this->data['users_list'] = $this->load->view('static_seo_list_view',$this->data,TRUE);
		$this->load->view('admin/base_node_dashboard_view');
	}

	function edit_clinic($id = NULL)
	{
		$this->load->model('images_model', '', TRUE);
		$this->view_type = 2;
		$additional_data = array('title' => $this->input->post('title'),
								'path' => $this->input->post('path'),
								'subtitle' => $this->input->post('subtitle'),
					'address' => $this->input->post('address'),
					'phones' => $this->input->post('phones'),
								'work_time' => $this->input->post('work_time'),
								'text' => $this->input->post('text'),
					'year' => $this->input->post('year'),
					'district' => $this->input->post('district'),
								'coordinates' => $this->input->post('coordinates'),
				);
		$result = $this->admin_clinics_model->edit_clinic($id,$additional_data);
		$this->saveSeo('clinics', $id);
		$this->data['result'] = $result;
		if($this->data['result'])
		{
			$config['upload_path'] = './uploads/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '10000';
			$config['max_width']  = '2000';
			$config['max_height']  = '1500';
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('image'))
			{
				//$this->data['result'] =  $this->upload->display_errors();
			}	
			else
			{
				$this->images_model->clear_all_parent_images($id,'clinics');
				$data = array('upload_data' => $this->upload->data());
				$this->data['result'] = $this->images_model->add_image($id,'clinics','/uploads/'.$data['upload_data']['file_name']);
			}
		}
		echo $this->load->view('admin/json_result_view',$this->data);
	}

	function delete_static_seo_page($page)
	{
		$this->view_type = 2;
		$this->admin_clinics_model->delete_clinic($id);
		$this->images_model->clear_all_parent_images($id,'clinics');
	}


}
