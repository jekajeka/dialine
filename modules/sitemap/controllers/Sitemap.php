<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Sitemap extends Frontend_Controller  {
function __construct()
{
    parent::__construct();
    $this->load->model('news/news_frontend_model', '', TRUE);
    $this->load->model('clinic/frontend_clinics_model', '', TRUE);
    $this->load->model('doctors/frontend_doctors_model', '', TRUE);
    $this->load->model('admin_gallery/admin_gallery_model', '', TRUE);
    $this->load->model('info/frontend_info_model', '', TRUE);
    $this->load->model('admin_reviews/admin_reviews_model', '', TRUE);
    $this->load->model('services/services_model', '', TRUE);
    $this->load->helper('url');
    $this->title = 'Карта сайта/Диалайн';
}

function index()
{
    $this->view_type = 2;
    $this->data['site_url'] = site_url();
    $this->data['articles'] = $this->news_frontend_model->get_news_by_type(2);
    $this->data['clinics'] = $this->frontend_clinics_model->get_clinics_list();
    $this->data['doctors'] = $this->frontend_doctors_model->get_doctors();
    $this->data['doctors_rubrics'] = $this->frontend_doctors_model->get_rubrics();
    $this->data['galleries'] = $this->admin_gallery_model->get_gallery_list();
    $this->data['medical_programs'] = $this->frontend_info_model->get_all_medical_programs();
    $this->data['news'] = $this->news_frontend_model->get_all_news();
    $this->data['service_description'] = $this->news_frontend_model->get_news_by_type(10);
    $this->data['reviews'] = $this->admin_reviews_model->get_moderated_reviews();
    $this->data['services'] = $this->services_model->get_overlay_services();
    $this->data['simple_services'] = $this->services_model->get_simple_services_by_type();
    //$this->output->enable_profiler(TRUE);
    $this->load->view('sitemap_view',$this->data);
}


}
