<?
class Admin_logs_model extends MY_Model {
function Admin_logs_model()
{
    parent::__construct();
    $this->db_table = 'logs';
}

function add($record_id, $data_type="�� ����������",$record_title = "�� ����������",$link="�� ����������", $action_type='�� ����������')
{
    $added_data = array('operator' => $this->ion_auth->get_current_user(),
                        'time' => date("Y-m-d H:i:s"),
                        'record_id' => $record_id,
                        'data_type' => $data_type,
                        'record_title' => $record_title,
                        'link' => $link,
                        'action_type'=>$action_type);
    $insert_result = array();
    $insert_result['success'] = $this->db->insert($this->db_table, $added_data);
    $insert_result['insert_id'] = $this->db->insert_id();
    return $insert_result;
}

/**
* ��������� ���� ������� �����
* @param integer $id
* @return ������ �������� ������� �����
*/
function get_all()
{
    $this->db->select('*');
    $this->db->from($this->db_table);
    $this->db->order_by('id', 'DESC');
    return $this->db->get()->result();
}

}