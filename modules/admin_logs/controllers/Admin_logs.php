<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_logs extends Admin_Controller  {
function __construct()
{
    parent::__construct();
    $this->data['title'] = 'Редактирование клиник';
}

function index()
{
    $this->data['logs'] = $this->admin_logs_model->get_all();
    $this->data['title'] = 'Все логи';
    $this->data['users_list'] = $this->load->view('logs_list_view',$this->data,TRUE);
    $this->load->view('admin/base_node_dashboard_view');
}

}?>