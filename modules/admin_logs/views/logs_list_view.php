<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?=$this->data['title']?></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a href="/admin_clinics/add_clinic_page" class="add-link"><i class="fa fa-plus"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
                      <thead>
                        <tr>
                          <th></th>
                          <th>id</th>
                          <th>Номер записи</th>
                          <th>Тип записи</th>
                          <th>Пользователь</th>
                          <th>Название записи</th>
                          <th>Время</th>
                          <th>Тип действия</th>
                          <th>Запись</th>
                        </tr>
                      </thead>


                      <tbody>
                        <?foreach($logs as $log ){?>
                        <tr>
                          <td></td>
                          <td><?=$log->id?></td>
                          <td><?=$log->record_id?></td>
                          <td><?=$log->data_type?></td>
                          <td><?=$log->operator?></td>
                          <td><?=$log->record_title?></td>
                          <td><?=$log->time?></td>
                          <td><?=$log->action_type?></td>
                          <td><a target="_blank" href="<?=$log->link?>"><?=$log->link?></a></td>
                        </tr>
                        <?}?>
                      </tbody>
                    </table>
                  </div>
                </div>
</div>
              