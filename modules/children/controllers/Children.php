<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Children extends Frontend_Controller  {
function __construct()
{
    parent::__construct();
    $this->load->model('frontend_children_model', '', TRUE);
    $this->load->model('clinic/frontend_clinics_model', '', TRUE);
    $this->load->model('doctors/frontend_doctors_model', '', TRUE);
    $this->load->model('admin_gallery/admin_gallery_model', '', TRUE);
    $this->load->model('info/frontend_info_model', '', TRUE);

    $this->load->model('services/services_model');

    $this->title = 'Для детей/Диалайн';
}

function index()
{
//    $this->data['title'] = 'Детские поликлиники "Диалайн" в Волгограде и Волжском';
    $this->data['directions'] = $this->frontend_children_model->get_kids_direction();

    $this->data['rubrics'] = $this->frontend_children_model->get_rubrics();

    $this->data['clinics'] = $this->frontend_children_model->get_clinics_list();

    $this->data['banners'] = $this->admin_gallery_model->get_all_child_banners(false);

    $this->data['about'] = $this->frontend_children_model->get_info_data(17);
    $this->data['images_gallery'] = $this->frontend_children_model->get_images_gallery(17);

    $this->data['komplex_program_advantages'] = $this->frontend_info_model->get_komplex_program_advantages();

    $this->data['medical_programs'] = $this->frontend_info_model->get_all_medical_programs('kids');

    $this->getSeo('complex_page',17);
    $this->load->view('children_page_view',$this->data);
}

function doctors()
{
    $this->data['doctors'] = $this->frontend_doctors_model->get_doctors('kids');
    $this->data['rubrics'] = $this->frontend_doctors_model->get_rubrics();
    $this->data['clinics'] = $this->frontend_clinics_model->get_clinics_list();
    $this->data['about'] = $this->frontend_children_model->get_info_data(18);
//    $this->data['title'] = $this->data['about'][0]->title;
    $this->getSeo('complex_page', 18);
    $this->load->view('all_kids_doctors_page_view',$this->data);
}

function doctor_text_search()
{
    $this->view_type = 2;
    $this->data['rubrics'] = $this->frontend_children_model->get_rubrics();
    $this->data['doctors'] = $this->frontend_children_model->search_doctors_by_text($this->input->post('search_text'));
    $this->load->view('all_doctors_list_view',$this->data);
}
function doctor_text_search_4_main()
{
    $this->view_type = 2;
    $this->data['doctors'] = $this->frontend_children_model->search_doctors_by_text($this->input->post('search_text'),' doctors.id ');
    $this->load->view('simple_doctors_list_view',$this->data);
}

function doctor_text_search_speciality($speciality_id = NULL)
{
    $this->view_type = 2;
    $this->data['rubrics'] = $this->frontend_children_model->get_rubrics();
    $this->data['doctors'] = $this->frontend_children_model->search_doctors_by_text_n_speciality($this->input->post('search_text'),$speciality_id);
    $this->load->view('all_doctors_list_view',$this->data);
}

function search_doctor_complicated()
{
    $this->view_type = 2;
    $this->data['rubrics'] = $this->frontend_children_model->get_rubrics();
    $this->data['doctors'] = $this->frontend_children_model->search_doctor_complicated(2, $this->input->post('speciality'),$this->input->post('clinic'));
    $this->load->view('all_doctors_list_view',$this->data);
}

//отбор клиник в селектбокс по изменению состояния возраста и/или специализации врачей
function filter_clinics()
{
    $this->view_type = 2;
    $this->data['clinics'] = $this->frontend_children_model->get_selected_clinics($this->input->post('age'),$this->input->post('speciality'));
    $this->load->view('selected_clinics_view',$this->data);
}

function search_doctor_complicated_on_top()
{
    $this->view_type = 2;
    if(($this->input->post('age')=='all')&&($this->input->post('speciality')=='all')&&($this->input->post('clinic')=='all'))
    {
        $this->data['rubrics'] = $this->main_page_model->get_rubrics();
        $this->load->view('directions_list_overflow_view',$this->data);
    }
    else
    {
        $this->data['doctors'] = $this->frontend_children_model->search_doctor_complicated($this->input->post('age'),$this->input->post('speciality'),$this->input->post('clinic'),1);
        $this->load->view('simple_doctors_list_view',$this->data);
    }
}

function specialisation($specialisation = NULL)
{
    if($specialisation)
    {
        $this->data['specialisation'] = $this->frontend_children_model->get_specialisation($specialisation);

        //если в базе нет такой специализации - увести на страницу 404
        if(!empty($this->data['specialisation']))
        {
            $this->data['title'] = $this->data['specialisation'][0]->title.'/Диалайн, Волгоград';
            $this->data['doctors'] = $this->frontend_children_model->get_doctors_by_specialisation_text($specialisation);

            $dicId_arr = [];
//
//            foreach ($this->data['doctors'] as $doctor)
//                $dicId_arr[] = $doctor->id;

            $this->data['clinics'] = $this->frontend_clinics_model->get_clinics_list();

            $this->data['service'] = $this->services_model->get_service($specialisation);
            $this->data['service_types'] = $this->frontend_children_model->get_med_service_type($this->data['service'][0]->id);

            $this->data['subservices'] = $this->frontend_children_model->get_services($this->data['service'][0]->id,$dicId_arr);

            foreach ($this->data['service_types'] as $key => $service_type) {
                foreach ($this->data['subservices'] as $single_service) {
                    if (($single_service->service_type == $service_type->service_type) && ($single_service->rubric == $service_type->rubric)) {

                        $this->data['service_types'][$key]->services[] = $single_service;

                    }
                }
                if(!empty($this->data['service_types'][$key]->services))
                $service_type->services_qty = count($this->data['service_types'][$key]->services);
                else
                    $service_type->services_qty = 0;
            }

            $this->getSeo('kids_doctors_specializations', $this->data['specialisation'][0]->id);
            $this->load->view('children_direction_page_view',$this->data);
        }
        else
            show_404('page');
    }
    
}

function single_doctors_page($specialisation = '',$translite_name = '')
{
    $this->data['doctor'] = $this->frontend_children_model->get_doctor($translite_name);
    //если в базе нет такого врача, или он снят с публикации - увести на страницу 404
    if(!empty($this->data['doctor']))
    {
        if($this->data['doctor'][0]->published)
            $published = 1;
        if($this->data['doctor'][0]->published == '0')
            $published = 0;
        if($this->data['doctor'][0]->published == NULL)
            $published = 1;
        //echo $published;
        if($published)
        {
            $this->data['title'] = $this->data['doctor'][0]->name;
            $this->data['services'] = $this->frontend_children_model->get_doctor_services($this->data['doctor'][0]->id);
            $this->data['clinics'] = $this->frontend_children_model->get_doctor_clinics($this->data['doctor'][0]->id);
            $this->data['rubrics'] = $this->frontend_children_model->get_doctor_rubrics($this->data['doctor'][0]->id);
            $doctor_direction_list = $this->frontend_children_model->get_doctor_directions($this->data['doctor'][0]->id);
            $this->data['other_doctors'] = $this->frontend_children_model->get_other_doctors($doctor_direction_list[0]->direction,$this->data['doctor'][0]->id);
            $this->data['breadcrumbs_segment'] = '<li><a href="/info/diskontnye_karty/">Дисконтная программа</a></li>';
            $this->getSeo('doctors', $this->data['doctor'][0]->id);
            $this->load->view('single_doctor_view',$this->data);
        }
        else
            show_404('page');
    }
    else
        show_404('page');
}


}
