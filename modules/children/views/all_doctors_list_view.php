<!--<script src="/frontend/js/jquery.pajinate.min.js"></script>-->
<?
//$rub_line_active_title = "active";
//$rub_line_active_body = "style=\"display: block\"";
//
//if (($_SERVER['REQUEST_URI'] == '/doctors') || ($_SERVER['REQUEST_URI'] == '/doctors/') ||
//    ($_SERVER['REQUEST_URI'] == '/kids') || ($_SERVER['REQUEST_URI'] == '/kids/')) {
//    $rub_line_active_title = "";
//    $rub_line_active_body = "";
//}
foreach ($rubrics as $rubric) {
    $has_doctors = 0;
    foreach ($doctors as $doctor) {
        if (($doctor->medical_direction == $rubric->id) && (($doctor->published == 1) || ($doctor->published == NULL)))
            $has_doctors = 1;
    }
    if ($has_doctors) {
        ?>
        <div class="single_service_wrap_wide">
            <div class="switch_text_wrap">

                <div class="switch_text active" id="specialist_switch">
                    <div class="specialists_carousel">
                        <div class="swiper-wrapper">
                            <? foreach ($doctors as $doc) {
                                if (($doc->published == 1) || ($doc->published == NULL)) {
                                    ?>
                                    <div class="swiper-slide">
                                        <a style="width:270px" class="single_scrubs_wrap"
                                           href="/kids/<?= $doc->direct_path ?>/<?= $doc->translite ?>">
                                            <div class="single_scrubs_img"><img src="<?= $doc->path ?>"/></div>
                                            <h4><?= $doc->name ?></h4>
                                            <div class="single_scrubs_text">
                                                <?= $doc->specialisation ?>
                                            </div>
                                            <div class="single_scrubs_border"></div>
                                        </a>
                                    </div>
                                <? }
                            } ?>
                        </div>
                    </div>
                    <a class="specialists_carousel_link specialists_carousel_link_left"></a>
                    <a class="specialists_carousel_link specialists_carousel_link_right"></a>
                </div>

            </div>
        </div>

        <script>

            var specialists_carousel_Swiper = new Swiper('.specialists_carousel',{
                loop:false,
                grabCursor: true,
                onlyExternal : false,
                paginationClickable: false,
                slidesPerView : 'auto'
            })

            $('.specialists_carousel_link_left').on('click', function(e){
                e.preventDefault()
                specialists_carousel_Swiper.swipePrev()
            })
            $('.specialists_carousel_link_right').on('click', function(e){
                e.preventDefault()
                specialists_carousel_Swiper.swipeNext()
            })

        </script>

    <? }
} ?>