<div class="row row_title_image_kids">
    <div class="container">
        <h1>Детские <?= $specialisation[0]->title ?> в Волгограде и Волжском</h1>
    </div>
</div>
<div class="row row_breadcrumbs">
    <div class="container">
        <ul>
            <li><a href="/">Главная</a></li>
            |
            <li><a href="/kids">Детские поликлиники Диалайн</a></li>
            |
            <li><?= $specialisation[0]->title ?></li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="container">
        <!--        <div class="single_service_wrap">-->

        <?php if (!empty($specialisation[0]->kids_text)) { ?>
            <div class="switch_text_wrap">
                <input type="hidden" id="serviceId" value="34">
                <div class="switch_text switch_text_hidden active rolled_up">

                    <?= $specialisation[0]->kids_text ?>

                    <a style="color: #fff;margin: 0 auto;display: block;width: 158px;" id="order_service_btn"
                       class="green_btn box_shadow">Записаться на прием</a>

                </div>
            </div>
            <div class="open_switch_text_wrap">
                <a class="open_switch_text"></a>
            </div>
            <!--        </div>-->
            <div class="child_icon_bird_top"></div>
        <?php } ?>
        <div class="child_icon_bee"></div>
    </div>
</div>

<div class="row row_speciality_rule">
    <div class="container">
        <div class="text_search_speciality_wrap">
            <input type="text" id="search_child_doctor_speciality" speciality="<?= $specialisation[0]->id ?>"
                   placeholder="Начните поиск, например, хирург">
        </div>
        <div class="text_search_speciality_long_wrap">
            <!--            <select id="age_page">-->
            <!--                <option value="all">Все возрасты</option>-->
            <!--                <option value="2">Детям</option>-->
            <!--                <option value="1">Взрослым</option>-->
            <!--            </select>-->
            <input type="hidden" id="speciality_page" value="<?= $specialisation[0]->id ?>">
            <select id="clinic_page">
                <option value="all">Все клиники</option>
                <? foreach ($clinics as $clinic) { ?>
                    <option value="<?= $clinic->id ?>"><?= $clinic->title ?> <?= $clinic->district ?></option>
                <? } ?>
            </select>
            <a id="search_child_doctor_complicated" class="green_btn">Подобрать</a>
        </div>
    </div>
</div>
<div class="row specialists_row children_page">
    <div class="container">
        <div class="single_service_wrap_wide">
            <div class="switch_text_wrap">

                <div class="switch_text active" id="specialist_switch">
                    <div class="specialists_carousel">
                        <div class="swiper-wrapper">
                            <? foreach ($doctors as $doc) {
                                if (($doc->published == 1) || ($doc->published == NULL)) {
                                    ?>
                                    <div class="swiper-slide">
                                        <a style="width:270px" class="single_scrubs_wrap"
                                           href="/kids/<?= $doc->direct_path ?>/<?= $doc->translite ?>">
                                            <div class="single_scrubs_img"><img src="<?= $doc->path ?>"/></div>
                                            <h4><?= $doc->name ?></h4>
                                            <div class="single_scrubs_text">
                                                <?= $doc->specialisation ?>
                                            </div>
                                            <div class="single_scrubs_border"></div>
                                        </a>
                                    </div>
                                <? }
                            } ?>
                        </div>
                    </div>
                    <a class="specialists_carousel_link specialists_carousel_link_left"></a>
                    <a class="specialists_carousel_link specialists_carousel_link_right"></a>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="row row_speciality_rule children_page">
    <div class="container">

        <div class="switch_text active" id="specialist_switch">
            <h2>Перечень услуг</h2>
            <!--            <div class="searh_service_container">-->
            <!--                <input style="width:240px" id="search_service_analyze" placeholder="Поиск..." type="text" />-->
            <!--                <a id="search_service_go" class="green_btn">Найти</a>-->
            <!--            </div>-->
            <div class="structured_data_wrap">
                <?php $i=0;
                foreach ($service_types as $key => $service_type) {
                    if ($service_type->services_qty) {
                        $activityDataTitle = "";
                        $activityDataTable = "";
                        if ($i == 0) {
                            $activityDataTitle = "active";
                            $activityDataTable = 'style="display:block"';
                        }
                        ?>
                        <div class="structured_data_content_wrap">
                            <a class="structured_data_title <?= $activityDataTitle ?>">
                                <h4><?= $service_type->service_type ?></h4><span
                                        class="structured_data_title_qty"><?= $service_type->services_qty ?></span>
                            </a>
                            <div class="structured_data_table_wrap" <?= $activityDataTable ?>>
                                <div class="structured_data_table_wrap_order">
                                    <?

                                    if(!empty($service_type->services)){
                                    foreach ($service_type->services as $single_service) {
                                        if (($single_service->service_type == $service_type->service_type) && ($single_service->rubric == $service_type->rubric)) {
                                            ?>
                                            <div class="structured_data_table_line">
                                                <div class="structured_data_table_line_border">
                                                    <h4>
                                                        <? if (!empty($single_service->link)) {
                                                            ?>
                                                            <a title="<?= $single_service->title ?>"
                                                               href="<?= $single_service->link ?>"><?= $single_service->title ?> - <?= $single_service->mz_service_id ?></a>
                                                            <?
                                                        } else { ?>
                                                            <?= $single_service->title ?>
                                                        <? } ?>
                                                    </h4>
                                                    <div class="structured_data_table_line_price"><?= $single_service->price ?>
                                                        р.
                                                    </div>
                                                </div>
                                                <? if (strpos($service_type->service_type, 'не требующие записи') == NULL) { ?>
                                                    <a data-service="<?= $single_service->title ?> за <?= $single_service->price ?> р."
                                                       class="subservice_from_kids structured_data_table_link">Записаться</a>
                                                <? } ?>
                                            </div>
                                        <? }
                                    }} ?>
                                </div>
                            </div>
                        </div>
                    <? $i++; }
                } ?>
            </div>
        </div>
        <div class="child_icon_bird_bottom" style="top:8px;"></div>
        <div class="child_icon_racoon"></div>
    </div>
</div>

<!--<div class="overlay_form_wrapper" id="send_service_subservice">-->
<!--    <div class="overlay_form">-->
<!--        <a class="close_overlay_form"></a>-->
<!--        <div class="overlay_form_inner">-->
<!--            <div class="overlay_form_title">Записаться на прием</div>-->
<!--            <input type="hidden" id="page_title" value="--><?//=$subservice[0]->title?><!--" />-->
<!--            <input type="hidden" id="service_subservice_purpose" value="" />-->
<!--            <input type="text" id="subservice_service_name" placeholder="Имя" />-->
<!--            <input type="text" id="subservice_service_phone" placeholder="Телефон" />-->
<!--            <div class="wrap_agree_checkbox">-->
<!--                <input type="checkbox"   id="subservice_service_agree"><label>Оставляя свои персональные данные, Вы даете добровольное согласие на их обработку.-->
<!--                    <a target="_blank" href="/uploads/files/Politika_obrabotki_i_zashiti_personalnih_dannih1.pdf">Подробнее</a></label>-->
<!--            </div>-->
<!--            <a class="send_overlay_form" id="send_service_subservice_btn">Отправить</a>-->
<!--            <p>*С Вами свяжутся в ближайшее время и запишут на удобное время к выбранному специалисту</p>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

<div class="overlay_form_wrapper" id="send_service_subservice_kids">
    <div class="overlay_form">
        <a class="close_overlay_form"></a>
        <div class="overlay_form_inner">
            <div class="overlay_form_title">Записаться на прием</div>
            <input type="hidden" id="page_title" value="<?=$service[0]->title?>" />
            <input type="hidden" id="service_subservice_purpose" value="" />
            <input type="text" id="subservice_service_name" placeholder="Имя" />
            <input type="text" id="subservice_service_phone" placeholder="Телефон" />
            <div class="wrap_agree_checkbox">
                <input type="checkbox"   id="subservice_service_agree"><label>Оставляя свои персональные данные, Вы даете добровольное согласие на их обработку.
                    <a target="_blank" href="/uploads/files/Politika_obrabotki_i_zashiti_personalnih_dannih1.pdf">Подробнее</a></label>
            </div>
            <a class="send_overlay_form" id="send_service_subservice_btn_kids">Отправить</a>
            <p>*С Вами свяжутся в ближайшее время и запишут на удобное время к выбранному специалисту</p>
        </div>
    </div>
</div>

<style>
    .bukashka {
        position: relative;
    }

    .bukashka .child_icon_beetle {
        display: block;
    }
</style>

