<? foreach ($medical_programs as $doc) { ?>

    <div class="swiper-slide">
        <a class="single_scrubs_wrap" href="/komplex_programm/<?= $doc->path ?>">

            <div class="single_scrubs_img"
                 style="background: url('<?= $doc->preview_path; ?>') center;background-size: cover;">
            </div>

            <h4><?= $doc->title ?></h4>
            <div class="single_scrubs_overlay">
                <div class="single_scrubs_overlay_title"><?= $doc->title ?></div>
                <div class="single_scrubs_overlay_text">
                    <?= mb_strimwidth($doc->target, 0, 120, "...");?>
                </div>
                <div class="single_scrubs_overlay_price"><?= $doc->cost ?> р.</div>
            </div>
        </a>
    </div>

<? } ?>