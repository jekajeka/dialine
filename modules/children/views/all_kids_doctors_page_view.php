<div class="row row_title_image">
    <div class="container">
        <h1><?= $about[0]->title; ?></h1>
    </div>
</div>
<div class="row row_breadcrumbs">
    <div class="container">
        <ul>
            <li><a href="/">Главная</a></li>
            |
            <li><a href="/kids">Детские поликлиники Диалайн</a></li>
            |
            <li><a href="/kids/doctors">Детские врачи</a></li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="container">
        <!--        <div class="single_service_wrap">-->
        <div class="switch_text_wrap">
            <input type="hidden" id="serviceId" value="34">
            <div class="switch_text switch_text_hidden active rolled_up">

                <?= $about[0]->text ?>

                <a style="color: #fff;margin: 0 auto;display: block;width: 158px;" id="order_service_btn_kids" class="green_btn box_shadow">Записаться на прием</a>

            </div>
        </div>
        <div class="open_switch_text_wrap">
            <a class="open_switch_text"></a>
        </div>
        <!--        </div>-->
        <div class="child_icon_bee"></div>
        <div class="child_icon_bird_top"></div>
    </div>
</div>

<div class="row row_speciality_rule">
    <div class="container">
        <div class="text_search_speciality_wrap">
            <input type="text" id="search_doctor" placeholder="Начните поиск, например, хирург">
        </div>
        <div class="text_search_speciality_long_wrap">
<!--            <select id="age_page">-->
<!--                <option value="all">Все возрасты</option>-->
<!--                <option value="2" --><?php //($isKids) ? print 'selected="selected"' : ''?><!--Детям</option>-->
<!--                <option value="1">Взрослым</option>-->
<!--            </select>-->
            <input type="hidden" id="age_page" value="2">
            <select id="speciality_page">
                <option value="all">Все направления</option>
                <? foreach ($rubrics as $rubric) { ?>
                    <option value="<?= $rubric->id ?>"><?= $rubric->title ?></option>
                <? } ?>
            </select>
            <select id="clinic_page">
                <option value="all">Все клиники</option>
                <? foreach ($clinics as $clinic) { ?>
                    <option value="<?= $clinic->id ?>"><?= $clinic->title ?> <?= $clinic->district ?></option>
                <? } ?>
            </select>
            <a id="search_doctor_complicated" class="green_btn">Подобрать</a>
        </div>
<!--        <div style="top:-35px" class="child_icon_bee"></div>-->
<!--        <div class="child_icon_bird_top"></div>-->
    </div>
</div>
<div class="row specialists_row">
    <div class="container">
        <?= $this->load->view('all_kids_doctors_list_view', $this->data) ?>

    </div>
</div>
<div class="row">
    <div class="container">

        <div class="child_icon_racoon"></div>

    </div>
</div>

<style>
    .bukashka{
        position:relative;
    }
    .bukashka .child_icon_beetle{
        display:block;
    }
</style>