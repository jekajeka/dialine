
<?php $i=0;

$count = ceil(count($directions)/4);

?>

<ul>
<?php foreach($rubrics as $rubric){?>
<?php if(in_array($rubric->id, $directions)){ ?>
    <?php if($i == $count) {
       echo '</ul><ul>';
       $i=0;
    }?>
    <li><a title="<?=$rubric->title?>" href="/kids/<?=$rubric->path?>"><?=$rubric->title?></a></li>
<?php $i++;}
}
?>