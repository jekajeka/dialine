<script>
    $(document).ready(function() {
        $(".fancybox").fancybox({
            openEffect	: 'none',
            closeEffect	: 'none',
            loop     : true,
            arrows : true,
        });
    });
</script>

<div class="row super_slider">
    <div class="swiper-wrapper">
        <? foreach ($banners as $banner) { ?>
            <a title='<?= $banner->title ?>' href="<?= $banner->link ?>" class="swiper-slide" style="background-image:url(<?= $banner->path ?>); background-repeat: no-repeat;background-position: center;background-size: cover">
                <div class="container">
                    <div class="swiper_table">
                        <div class="swiper_table_child">
                            <div class="green_field">
                                <div class="title"><?= $banner->title ?></div>
                                <?= $banner->text ?>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        <? } ?>
    </div>
    <div class="switches"></div>
</div>

<div class="row row_breadcrumbs">
    <div class="container">
        <ul>
            <li><a href="/">Главная</a></li>
            |
            <li><a href="/kids">Детские поликлиники Диалайн</a></li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="container">
        <h1><?= $about[0]->title ?></h1>
    </div>
</div>

<div class="row">
    <div class="container">
        <!--        <div class="single_service_wrap">-->
        <div class="switch_text_wrap">
            <input type="hidden" id="serviceId" value="34">
            <div class="switch_text switch_text_hidden active rolled_up">

                <?= $about[0]->text ?>

                <div class="switch_text_gallery" >

            <?php if($images_gallery) {
                foreach ($images_gallery as $images){?>
                <a class="fancybox" rel="gallery1" href="<?=$images->path ?>" title="" data-fancybox="group">
                    <div class="switch_text_gallery_img"
                         style="background: url('<?= $images->path; ?>') center;background-size: cover;">
                    </div>
<!--                    <img src="--><?//=$images->path ?><!--" alt="" />-->
                </a>
                <?php } ?>
            <?php } ?>
                </div>

                <a style="color: #fff;margin: 0 auto;display: block;width: 158px;" id="order_service_btn_kids"
                   class="green_btn box_shadow">Записаться на прием</a>

            </div>
        </div>
        <div class="open_switch_text_wrap">
            <a class="open_switch_text"></a>
        </div>
        <!--        </div>-->
        <div class="child_icon_bee"></div>
        <div class="child_icon_bird_top"></div>
    </div>
</div>

<div class="row row_title" id="find_clinic_block">
    <div class="container"><a title="Наши преимущества" href="/komplex_programm/kids">Наши преимущества</a></div>
</div>
<div class="row children_page">
    <div class="container">

        <div class="complex_teasers_wrap">
            <div class="complex_teaser" style="background: #fff url(/uploads/children_page/complex_economy.png) no-repeat 10% 15%;background-size: 20%;">
                <div class="complex_teaser_title">Экономия времени</div>
                <p>Возможность консультаций дистанционно и вызов врача на дом, отсутствие очередей</p>
            </div>
            <div class="complex_teaser" style="background: #fff url(/uploads/children_page/complex_economy_money1.png) no-repeat 10% 15%;background-size: 20%;">
                <div class="complex_teaser_title">Экономия средств</div>
                <p>Комплексные и годовые программы для детей от 0  до 18 лет</p>
            </div>
            <div class="complex_teaser" style="background: #fff url(/uploads/children_page/complex_individual.png) no-repeat 10% 15%;background-size: 20%;">
                <div class="complex_teaser_title">Лечение без слез и боли</div>
                <p>Использование современных технологий для снижения болевых ощущений</p>
            </div>
            <div class="complex_teaser" style="background: #fff url(/uploads/children_page/complex_confidence.png) no-repeat 10% 15%;background-size: 20%;">
                <div class="complex_teaser_title">Комфорт</div>
                <p>Клиники оборудованы увлекательными детскими зонами</p>
            </div>
            <div class="complex_teaser" style="background: #fff url(/uploads/children_page/complex_trust.png) no-repeat 10% 15%;background-size: 20%;">
                <div class="complex_teaser_title">Доверие</div>
                <p>Врачи высшей категории, кандидаты и доктора медицинских наук</p>
            </div>
<!--            --><?//foreach($komplex_program_advantages as $advantage){?>
<!--                <div class="complex_teaser" style="background: #fff url(--><?//=$advantage->path?><!--) no-repeat 10% 15%;background-size: 15%;">
                    <div class="complex_teaser_title">*/<?//=$advantage->title?><!--</div>-->
<!--                    <p>--><?//=$advantage->text?><!--</p>-->
<!--                </div>-->
<!--            --><?//}?>
        </div>

    </div>
</div>

<div class="row row_title" id="find_clinic_block">
    <div class="container"><a title="Детские врачи" href="/kids/doctors">Детские врачи</a></div>
</div>
<div class="row">
    <div class="container">

        <div class="doctor_kids_direction">
            <?=$this->load->view('directions_kids_list_view',$this->data,true);?>
        </div>

    </div>
</div>

<div class="row row_title" id="find_clinic_block">
    <div class="container"><a title="Комплексные программы" href="/komplex_programm/kids">Комплексные программы</a></div>
</div>
<div class="row children_page">
    <div class="container">
        <div class="switch_text active" id="komplex_kids_switch">
            <div class="wrap_komplex_kids_carousel">
                <div class="komplex_kids_carousel">
                    <div class="swiper-wrapper">
                        <? echo $this->load->view('complex_program_kids_list', $this->data, TRUE) ?>
                    </div>
                </div>
                <a class="komplex_kids_carousel_link komplex_kids_carousel_link_left"></a>
                <a class="komplex_kids_carousel_link komplex_kids_carousel_link_right"></a>
            </div>
        </div>


    </div>
</div>

<div class="row row_title" id="find_clinic_block">
    <div class="container"><a title="Все клиники Диалайн" href="/clinic">Найти ближайшую к Вам клинику</a></div>
</div>
<div class="row">
    <div class="container">
        <div class="map_operate_wrap">
            <div class="addresses_wrap">
                <? foreach ($clinics as $key => $clinic) { ?>
                    <div class="single_address_wrap">
                        <div class="single_addr_title" data-trg="<?= $key ?>"
                             title='Расположение <?= $clinic->title ?> на карте'><?= $clinic->title ?> <?= $clinic->district ?></div>
                        <a class="full_clinic_link" title='<?= $clinic->title ?>' href="/clinic/<?= $clinic->path ?>">Подробнее...</a>
                        <div class="single_addr_data_wrap">
                            <?
                            $w_times = explode("\n", $clinic->work_time);
                            ?>
                            <div class="time_data_wrap">
                                <? foreach ($w_times as $w_tm) { ?>
                                    <div class="time_data"><?= $w_tm ?></div>
                                <? } ?>
                            </div>
                        </div>
                    </div>
                <? } ?>
            </div>
        </div>
        <div class="" id="map"></div>
        <div class="child_icon_racoon"></div>
        <div class="child_icon_bird_bottom"></div>
    </div>
</div>

<style>
    .bukashka{
        position:relative;
    }
    .bukashka .child_icon_beetle{
        display:block;
    }
</style>

<!--<div class="row row_speciality_rule">-->
<!--    <div class="container">-->
<!--        <div class="text_search_speciality_wrap">-->
<!--            <input type="text" id="search_doctor" placeholder="Начните поиск, например, хирург">-->
<!--        </div>-->
<!--        <div class="text_search_speciality_long_wrap">-->
<!--            <select id="age_page">-->
<!--                <option value="all">Все возрасты</option>-->
<!--                <option value="2">Детям</option>-->
<!--                <option value="1">Взрослым</option>-->
<!--            </select>-->
<!--            <select id="speciality_page">-->
<!--                <option value="all">Все направления</option>-->
<!--                --><? // foreach ($rubrics as $rubric) { ?>
<!--                    <option value="--><? //= $rubric->id ?><!--">--><? //= $rubric->title ?><!--</option>-->
<!--                --><? // } ?>
<!--            </select>-->
<!--            <select id="clinic_page">-->
<!--                <option value="all">Все клиники</option>-->
<!--                --><? // foreach ($clinics as $clinic) { ?>
<!--                    <option value="--><? //= $clinic->id ?><!--">--><? //= $clinic->title ?><!-- --><? //= $clinic->district ?><!--</option>-->
<!--                --><? // } ?>
<!--            </select>-->
<!--            <a id="search_doctor_complicated" class="green_btn">Подобрать</a>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--<div class="row specialists_row">-->
<!--    <div class="container">-->
<!--        --><? //= $this->load->view('all_doctors_list_view', $this->data) ?>
<!--    </div>-->
<!--</div>-->


<script>
    function init() {
        myMap = new ymaps.Map("map", {
            center: [48.758625, 44.818625],
            zoom: 16
        });
        var myPlacemark = new Array;
        visible = new ymaps.GeoObjectCollection();
        group_volgograd = new ymaps.GeoObjectCollection({
            properties: {
                id: 'group-volgograd',
                name: 'Волгоград'
            }
        })
        group_voljsky = new ymaps.GeoObjectCollection({
            properties: {
                id: 'group_voljsky',
                name: 'Волжский'
            }
        })
        group_mikhailovka = new ymaps.GeoObjectCollection({
            properties: {
                id: 'group_mikhailovka',
                name: 'Михайловка'
            }
        })
        <?foreach($clinics as $key=>$clinic) {?>
        myPlacemark[<?=$key?>] = new ymaps.Placemark([<?=$clinic->coordinates?>], {
            balloonContent: '<?=$clinic->title?><br/><a href="/clinic/<?=$clinic->path?>">Подробнее...</a>',
            iconCaption: '<?=$clinic->title?>'
        }, {
            preset: 'islands#hospitalIcon',
//            iconColor: '#00abb2'
            iconLayout: 'default#image',
            iconImageOffset: [-14, -50], // позиция иконки
            iconImageHref: '/frontend/images/kids_pict/racoon.png',
            iconImageSize: [45, 50]
        });
        <?
        if(strpos($clinic->address, 'Волгоград')){?>
        group_volgograd.add(myPlacemark[<?=$key?>]);
        <?}
        if(strpos($clinic->address, 'Волжский')){?>
        group_voljsky.add(myPlacemark[<?=$key?>]);
        <?}
        if(strpos($clinic->address, 'Михайловка')){?>
        group_mikhailovka.add(myPlacemark[<?=$key?>]);
        <?}
        }?>
        $('.single_addr_title').click(function (e) {
            e.preventDefault();
            var opts = Object;
            opts.duration = 2000;
            myMap.panTo(myPlacemark[$(this).attr('data-trg')].geometry.getBounds(), opts);
        });
        $('.selector_sity a').click(function (e) {
            if ($(this).data('city') == "Волжский") {
                myMap.setBounds(group_voljsky.getBounds(), {
                    checkZoomRange: true, callback: function () {
                        if (map.getZoom() > 10) map.setZoom(10);
                    }
                });
            }
            if ($(this).data('city') == "Михайловка" && group_mikhailovka.getBounds())
                myMap.setBounds(group_mikhailovka.getBounds(), {
                    checkZoomRange: true, callback: function () {
                        if (map.getZoom() > 10) map.setZoom(10);
                    }
                });
            else
                myMap.setBounds(group_volgograd.getBounds(), {
                    checkZoomRange: true, callback: function () {
                        if (map.getZoom() > 10) map.setZoom(10);
                    }
                });
        })
        visible.add(group_volgograd).add(group_voljsky).add(group_mikhailovka);
        myMap.geoObjects.add(visible);
        storage = ymaps.geoQuery(myPlacemark);
        if (Cookies.get('city') == "Волжский")
            myMap.setBounds(group_voljsky.getBounds(), {
                checkZoomRange: true, callback: function () {
                    if (map.getZoom() > 10) map.setZoom(10);
                }
            });
        else if (Cookies.get('city') == "Михайловка" && group_mikhailovka.getBounds())
            myMap.setBounds(group_mikhailovka.getBounds(), {
                checkZoomRange: true, callback: function () {
                    if (map.getZoom() > 10) map.setZoom(10);
                }
            });
        else
            myMap.setBounds(group_volgograd.getBounds(), {
                checkZoomRange: true, callback: function () {
                    if (map.getZoom() > 10) map.setZoom(10);
                }
            });

        myMap.behaviors.disable('scrollZoom');


    }

    $(document).ready(function () {
        ymaps.ready(init);


    });
</script>