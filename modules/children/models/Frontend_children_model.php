<?

class Frontend_children_model extends CI_Model
{
    private $db_table = 'doctors';

    function __construct()
    {
        parent::__construct();
    }

    function get_doctor($translite_name = '')
    {

        $data = $this->db->get_where($this->db_table, array('translite' => $translite_name));
        $doc = $data->result();
        $query = "SELECT name,outer_link, specialisation, " . $this->db_table . ".text,images.path,medical_direction," . $this->db_table . ".id,medical_directions.title as med_direct,medical_directions.path as direction_path,translite, published.published
                FROM " . $this->db_table . "
                LEFT JOIN images ON
                images.parent_id = " . $this->db_table . ".id AND images.parent_type = 'doctors'
                INNER JOIN doctor_direction ON
                doctor_direction.doctor_id=doctors.id
                INNER JOIN medical_directions ON
                medical_directions.id = doctor_direction.direction_id
                LEFT JOIN published ON
                published.type = 'doctors' AND published.record_id = " . $this->db_table . ".id 
                WHERE " . $this->db_table . ".translite = '$translite_name'";
        $data = $this->db->query($query);
        return $data->result();
    }

    function get_images_gallery($id){

        $query = "select * from images where parent_type like 'children_pic%' and parent_id = " . $id;
        $data = $this->db->query($query);
        return $data->result();

    }

    function get_doctor_directions($id = NULL)
    {
        $query = "SELECT group_concat(doctor_direction.direction_id) as direction FROM doctor_direction
            WHERE
            doctor_direction.doctor_id = $id
            ";
        $data = $this->db->query($query);
        return $data->result();
    }

    function get_other_doctors($direction = NULL, $doctor_id = NULL)
    {
        $query = "SELECT name, specialisation, " . $this->db_table . ".text,images.path,medical_directions.path as direct_path,translite,published.published
                FROM " . $this->db_table . "
                LEFT JOIN images ON
                images.parent_id = " . $this->db_table . ".id
                AND images.parent_type = 'doctors'
                INNER JOIN doctor_direction ON
                doctor_direction.doctor_id=doctors.id
                INNER JOIN medical_directions ON
                medical_directions.id = doctor_direction.direction_id
                LEFT JOIN published ON
                published.type = 'doctors' AND published.record_id = " . $this->db_table . ".id 
                WHERE
                doctor_direction.direction_id IN ($direction)
                AND " . $this->db_table . ".id<>$doctor_id
                GROUP BY doctors.id
                ORDER BY name ASC";
        $data = $this->db->query($query);
        return $data->result();
    }

    function get_kids_direction()
    {
        $query = "SELECT medical_directions.id
            FROM " . $this->db_table . "
            
            INNER JOIN doctor_direction ON
            doctor_direction.doctor_id=doctors.id
            
            INNER JOIN medical_directions ON
            medical_directions.id = doctor_direction.direction_id
            
            LEFT JOIN published ON
            published.type = 'doctors' AND published.record_id = " . $this->db_table . ".id
            
            LEFT JOIN published pb2 ON
            pb2.type = 'medical_directions' AND pb2.record_id = medical_directions.id
            
            WHERE (" . $this->db_table . ".age=0 or " . $this->db_table . ".age=2 ) and 
            (published.type = 'doctors' AND published.record_id = " . $this->db_table . ".id and published.published = 1) and
            (pb2.type = 'medical_directions' AND pb2.record_id = medical_directions.id and (pb2.published = 1 or pb2.published IS NULL))
            
            GROUP BY doctor_direction.direction_id
            
            ORDER BY title ASC";

//        print $query;
//        exit;

        $data = $this->db->query($query);

        $direction_arr = [];

        foreach ($data->result_array() as $res)
            $direction_arr[] = $res['id'];

        return $direction_arr;
    }

    function get_clinics_list()
    {
        $this->db->distinct();
        $this->db->select(array('clinics.*', 'published.published'));
        $this->db->from('clinics');
        $this->db->join('published', "published.type = 'clinics' AND published.record_id = clinics.id ", 'left');
        $this->db->join('clinic_doctor', "clinic_doctor.clinic_id = clinics.id ", 'left');
        $this->db->join('doctors', "clinic_doctor.doctor_id = doctors.id ", 'left');
        $this->db->join('published public_doc', "public_doc.type = 'doctors' AND public_doc.record_id = doctors.id ", 'left');
        $this->db->where('(doctors.age=0 or doctors.age=2) and public_doc.published = 1');

        return $this->db->get()->result();
    }

    function search_doctors_by_text($search_text = '', $group_by_parameter = NULL)
    {
        $group_by_parameter_text = '';
        if ($group_by_parameter)
            $group_by_parameter_text = ' GROUP BY ' . $group_by_parameter;
        if ($search_text == '') {
            return $this->get_doctors();
        } else {
            $sql_search_string = str_replace(' ', '%', $search_text);
            $query = "SELECT " . $this->db_table . ".id, name, specialisation, " . $this->db_table . ".text,images.path,medical_directions.path as direct_path,translite,doctor_direction.direction_id as medical_direction,doctor_direction.direction_id as medical_direction2,published.published
                FROM " . $this->db_table . "
                LEFT JOIN images ON
                images.parent_id = " . $this->db_table . ".id
                AND images.parent_type = 'doctors'
                INNER JOIN doctor_direction ON
                doctor_direction.doctor_id=doctors.id
                INNER JOIN medical_directions ON
                medical_directions.id = doctor_direction.direction_id
                LEFT JOIN published ON
                published.type = 'doctors' AND published.record_id = " . $this->db_table . ".id 
                WHERE
                " . $this->db_table . ".name LIKE '%$sql_search_string%'
                OR " . $this->db_table . ".specialisation LIKE '%$sql_search_string%'" . $group_by_parameter_text . ' ORDER BY name ASC';
            $data = $this->db->query($query);
            return $data->result();
        }
    }

    function search_doctors_by_text_n_speciality($search_text = '', $speciality_id = NULL)
    {
        $dop = '';
        if ($search_text != '') {
            $sql_search_string = str_replace(' ', '%', $search_text);
            $dop = "WHERE $this->db_table.name LIKE '%$sql_search_string%'
                OR " . $this->db_table . ".specialisation LIKE '%$sql_search_string%'
                ";
        }
        $query = "SELECT DISTINCT " . $this->db_table . ".id, name, specialisation, " . $this->db_table . ".text,images.path,medical_directions.path as direct_path,translite,doctor_direction.direction_id as medical_direction,published.published
                FROM " . $this->db_table . "
                LEFT JOIN images ON
                images.parent_id = " . $this->db_table . ".id
                AND images.parent_type = 'doctors'
                INNER JOIN doctor_direction ON
                doctor_direction.doctor_id=doctors.id
                INNER JOIN medical_directions ON
                medical_directions.id = doctor_direction.direction_id
                AND medical_directions.id=$speciality_id
                LEFT JOIN published ON
                published.type = 'doctors' AND published.record_id = " . $this->db_table . ".id 
                $dop
                GROUP BY " . $this->db_table . ".id
                ORDER BY name ASC";
        $data = $this->db->query($query);
        return $data->result();
    }


    function search_doctor_complicated($age = 'all', $speciality = 'all', $clinic = 'all', $distinct = NULL)
    {
        $speciality_string = '';
        $age_string = '';
        $clinic_string = '';
        $where = '';
        $group = '';
        if ($distinct)
            $group = ' GROUP BY  doctors.id';
        if ($speciality != 'all')
            $speciality_string = " AND doctor_direction.direction_id=$speciality";
        if ($age != 'all')
            $age_string = "( age=$age OR age=0 )";
        if ($clinic != 'all')
            $clinic_string = "INNER JOIN clinic_doctor
        ON clinic_id=$clinic
        AND clinic_doctor.doctor_id=" . $this->db_table . ".id";
        if ($age != 'all')
            $where = ' WHERE ';
        $query = "SELECT DISTINCT " . $this->db_table . ".id, name, specialisation, " . $this->db_table . ".text,images.path,medical_directions.path as direct_path,translite,doctor_direction.direction_id as medical_direction,published.published
                FROM " . $this->db_table . "
                LEFT JOIN images ON
                images.parent_id = " . $this->db_table . ".id
                AND images.parent_type = 'doctors'
                INNER JOIN doctor_direction ON
                doctor_direction.doctor_id=doctors.id
                INNER JOIN medical_directions ON
                medical_directions.id = doctor_direction.direction_id $speciality_string
                LEFT JOIN published ON
                published.type = 'doctors' AND published.record_id = " . $this->db_table . ".id 
                $clinic_string
                $where $age_string
                $group
                ORDER BY name ASC";
        $data = $this->db->query($query);
        return $data->result();
    }

//выборка клиник с наличием врачей указанной специализации и возраста пациентов
    function get_selected_clinics($age = "all", $speciality = 'all')
    {
        $this->db->distinct('clinics.id');
        $this->db->select(array('clinics.title', 'clinics.district', 'published.published', 'clinics.id'));
        $this->db->from('clinics');
        $this->db->join('clinic_doctor', "clinics.id = clinic_doctor.clinic_id", 'INNER');
        $this->db->join('doctors', "doctors.id = clinic_doctor.doctor_id", 'INNER');
        $this->db->join('doctor_direction', "doctor_direction.doctor_id = doctors.id", 'INNER');
        $this->db->join('published', "published.type='clinics' AND published.record_id=clinics.id", 'LEFT');
        $this->db->where('(published.published=1 OR published.published IS NULL)');
        if ($speciality != 'all')
            $this->db->where("doctor_direction.direction_id = $speciality");
        if ($age != 'all')
            $this->db->where(" ( age=$age OR age=0 )");
        $this->db->order_by("clinics.title", "ASC");
        return $this->db->get()->result();
    }

    function get_doctors_by_specialisation_text($direction = NULL)
    {
        $query = "SELECT " . $this->db_table . ".id,name,specialisation, " . $this->db_table . ".text,
        images.path,medical_directions.path as direct_path,translite,published.published
                FROM " . $this->db_table . "
                LEFT JOIN images ON
                images.parent_id = " . $this->db_table . ".id
                AND images.parent_type = 'doctors'
                INNER JOIN doctor_direction ON
                doctor_direction.doctor_id=doctors.id
                INNER JOIN medical_directions ON
                medical_directions.id = doctor_direction.direction_id
                LEFT JOIN published ON
                published.type = 'doctors' AND published.record_id = " . $this->db_table . ".id 
                WHERE
                (age = 0 or age = 2) and
                medical_directions.path = '$direction'
                ORDER BY name ASC";
        $data = $this->db->query($query);
        return $data->result();
    }

    function get_specialisation($link = NULL)
    {
        $this->db->select('medical_directions.*');
        $this->db->from('medical_directions');
        $this->db->join('published', "published.type = 'medical_directions' AND published.record_id = medical_directions.id ", 'left');
        $this->db->where('(published.published=1 OR published.published IS NULL)');
        $this->db->where(array('path' => $link));
        $this->db->order_by("title", "asc");
        return $this->db->get()->result();
    }

    function get_rubrics()
    {
        $this->db->select('medical_directions.title,medical_directions.id,medical_directions.path,medical_directions.service_path');
        $this->db->from('medical_directions');
        $this->db->join('published', "published.type = 'medical_directions' AND published.record_id = medical_directions.id ", 'left');
        $this->db->where('(published.published=1 OR published.published IS NULL)');
        $this->db->where('medical_directions.id<>' . ANALYZE_ID);
        $this->db->order_by("title", "asc");
        return $this->db->get()->result();
    }

    function get_doctor_rubrics($doc_id)
    {
        $query = "SELECT count( * ) as services_qty , service_type
                FROM services,service_doctor
                WHERE
                `service_doctor`.`service_id`=med_id
                AND `service_doctor`.`doctor_id`=$doc_id
                GROUP BY `service_type`
                ORDER BY `services`.`id` ASC";
        $data = $this->db->query($query);
        return $data->result();
    }

    function get_doctor_services($doc_id = NULL)
    {
        $query = "SELECT services.id,med_id,title, services.price, rubric,service_type,service_doctor.price as unic_price,service_doctor.id as sd_id
                FROM services,service_doctor
                WHERE
                service_doctor.service_id=med_id
                AND service_doctor.doctor_id=$doc_id
                ORDER BY services.id ASC";
        $data = $this->db->query($query);
        return $data->result();
    }

//Выборка клиник где оказывает врач услуги на странице врача
    function get_doctor_clinics($doc_id = NULL)
    {
        $this->db->select(array('clinics.id', 'title', 'district', 'path', 'published.published'));
        $this->db->from('clinics');
        $this->db->join('clinic_doctor', 'clinic_doctor.clinic_id=clinics.id AND clinic_doctor.doctor_id=' . $doc_id, 'INNER');
        $this->db->join('published', "published.type='clinics' AND published.record_id=clinics.id", 'LEFT');
        $this->db->where('(published.published=1 OR published.published IS NULL)');
        $this->db->order_by("clinics.title", "ASC");
        return $this->db->get()->result();
    }

//Вероятно стоит уничтожить потом
    function get_clinics()
    {
        $data = $this->db->get('clinics');
        return $data->result();
    }

    function get_services($medical_direction_id = NULL, $doctors_id)
    {
//    $query = "SELECT
//        services.id,services.title,services.price,services.rubric,services.service_type,services.link
//        FROM
//        services
//        LEFT JOIN
//        service_link
//        ON service_link.med_id=services.med_id
//        WHERE services.rubric = $medical_direction_id
//        ORDER BY services.id ASC";
        $doctors_id = implode(',', $doctors_id);

        $query = "SELECT services.id,services.title,services.price,services.rubric,services.service_type,services.link,services.mz_service_id 
        FROM service_kids 
        LEFT JOIN services on service_kids.service_id=services.med_id
        LEFT JOIN service_link ON service_link.med_id=services.med_id 
        WHERE services.rubric = $medical_direction_id
        GROUP BY services.id 
        ORDER BY services.id ASC";

//    print $query;exit; and service_doctor.doctor_id in ($doctors_id)

        $data = $this->db->query($query);
        return $data->result();
        //$data = $this->db->get_where('services',array('rubric' => $medical_direction_id));
        //return $data->result();
    }

    function get_med_service_type($medical_direction_id = NULL)
    {
        $query = "SELECT count( * ) as services_qty , medical_directions.service_rubric_title,services.*
                FROM services,medical_directions
                WHERE services.rubric=medical_directions.id
                AND medical_directions.id = $medical_direction_id
                GROUP BY `service_type`
                ORDER BY `services`.`id`,`services`.`rubric` ASC";
        $data = $this->db->query($query);
        return $data->result();
    }

    function get_info_data($id = NULL)
    {
        $data = $this->db->get_where('complex_page', array('id' => $id));
        return $data->result();
    }

}