<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends Frontend_Controller  {
	function __construct()
	{
		parent::__construct();
		$this->load->model('news_frontend_model', '', TRUE);
		$this->title = 'Новости';
		$this->data['active_rubric'] = 'news';
		$this->data['rubrics_list'] = '<li><a href="/news">Новости</a></li>
					<li><a href="/actions">Акции</a></li>
					<li><a href="/novelties">Новинки</a></li>
					<li><a href="/grafics">Графики работ</a></li>
					<li><a href="/smi">СМИ о нас</a></li>';
	}

	public function index()
	{
		$this->data['title'] = 'Все новости';
		$this->data['active_rubric'] = 'news';
		$this->data['news_objects'] = $this->news_frontend_model->get_all_news();
		$this->getSeo('news', 0);
		$this->load->view('all_news_page_view',$this->data);
	}

	public function actions()
	{
		$this->data['title'] = 'Все акции';
		$this->data['active_rubric'] = 'actions';
		$this->data['news_objects'] = $this->news_frontend_model->get_news_by_type(9);
		$this->getSeo('actions', 0);
		$this->load->view('all_news_page_view',$this->data);
	}

	public function grafics()
	{
		$this->data['title'] = 'Графики работ';
		$this->data['active_rubric'] = 'grafics';
		$this->data['news_objects'] = $this->news_frontend_model->get_news_by_type(7);
		$this->getSeo('grafics', 0);
		$this->load->view('all_news_page_view',$this->data);
	}

	public function novelties()
	{
		$this->data['title'] = 'Новинки';
		$this->data['active_rubric'] = 'novelties';
		$this->data['news_objects'] = $this->news_frontend_model->get_news_by_type(8);
		$this->getSeo('novelties', 0);
		$this->load->view('all_news_page_view',$this->data);
	}
	
	public function social_projects()
	{
		$this->data['title'] = 'Социальные проекты';
		$this->data['active_rubric'] = 'social_projects';
		$this->data['news_objects'] = $this->news_frontend_model->get_news_by_type(14);
		$this->getSeo('social_projects', 0);
		$this->load->view('all_news_page_view',$this->data);
	}

	function view($news_code = NULL)
	{
		if($news_code)
		{
		$this->data['news'] = $this->news_frontend_model->get_single_news($news_code);
		if(!empty($this->data['news']))
		{
			$this->data['breadcrumbs_segment'] = '<li><a href="/news">Новости</a></li> | ';
			$this->data['title'] = empty($this->data['news'][0]->meta_title) ? $this->data['news'][0]->title : $this->data['news'][0]->meta_title;
			$this->data['description'] = $this->data['news'][0]->meta_description;
			$this->getSeo('news', $this->data['news'][0]->id);
			$this->load->view('news_page_view',$this->data);
		}
		else
		{
			show_404('page');
		}
		
		}
	}

	function view_service_article($news_code = NULL)
	{
		if($news_code)
		{
			$this->data['breadcrumbs_segment'] = '';
			$this->data['news'] = $this->news_frontend_model->get_single_news($news_code);
			$this->data['title'] = $this->data['news'][0]->title;
			$this->getSeo('news', $this->data['news'][0]->id);
			$this->load->view('news_page_view',$this->data);
		}
	}


	public function smi()
	{
		$this->data['title'] = 'СМИ о нас';
		$this->data['active_rubric'] = 'smi';
		$this->data['news_objects'] = $this->news_frontend_model->get_news_by_type(11);
		$this->getSeo('smi', 0);
		$this->load->view('all_news_page_view',$this->data);
	}

	public function filter($type=null,$year='all',$month='all')
	{
		switch($type){
		case 'news':
		$this->data['title'] = 'Новости';
		$type_id = 1;
		break;
		case 'actions':
		$this->data['title'] = 'Акции';
		$type_id = 9;
		break;
		case 'grafics':
		$this->data['title'] = 'Графики';
		$type_id = 7;
		break;
		case 'novelties':
		$this->data['title'] = 'Новинки';
		$type_id = 8;
		break;
		case 'smi':
		$this->data['title'] = 'СМИ о нас';
		$type_id = 11;
		break;
		default:
		$this->data['title'] = 'Новости';
		}
		$this->data['active_rubric'] = $type;
		$this->data['news_objects'] = $this->news_frontend_model->get_news_by_type_and_date($type_id,$year,$month);
		$this->load->view('all_news_page_view',$this->data);
	}


}
