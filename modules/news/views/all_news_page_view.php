
    <section>
    <div class="row row_breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="/">Главная</a></li> | <li><a href=""><?=$title?></a></li>
            </ul>
        </div>
    </div>
    <div class="row row_title_h1">
        <div class="container">
            <h1><?=$title?></h1>
        </div>
    </div>
    <div class="row row_speciality_rule row_speciality_rule_news">
        <div class="container">
            <ul class="news_type">
                <?=str_replace('href="/'.$active_rubric,' class="active" href="/'.$active_rubric,$rubrics_list)?>
            </ul>
            <div class="news_select_wrap"">
                <input type="hidden" id="news_type" value="<?=$active_rubric?>">
                <select class="news_select" id="select_year"  style="width: 90px">
                    <option  value="all">Год</option>
                    <option value="2017">2017</option>
                    <option  value="2016">2016</option>
                    <option  value="2015">2015</option>
                    <option  value="2014">2014</option>
                    <option  value="2013">2013</option>
                    <option  value="2012">2012</option>
                    <option  value="2011">2011</option>
                    <option  value="2010">2010</option>
                </select>
                <select class="news_select" id="select_month"  style="width: 120px">
                    <option value="all">Месяц</option>
                    <option value="1">Январь</option>
                    <option value="2">Февраль</option>
                    <option value="3">Март</option>
                    <option value="4">Апрель</option>
                    <option value="5">Май</option>
                    <option value="6">Июнь</option>
                    <option value="7">Июль</option>
                    <option value="8">Август</option>
                    <option value="9">Сентябрь</option>
                    <option value="10">Октябрь</option>
                    <option value="11">Ноябрь</option>
                    <option value="12">Декабрь</option>
                </select>
                <a id="search_news">Искать</a>
            </div>
        </div>
    </div>
    <div class="row news_on_page_all">
        <div class="container">
            <?foreach($this->data['news_objects'] as $news) {?>
            <a class="single_news_item_page" href="/news/<?=$news->path?>.new" title="<?=$news->title?>" >
                <h4><?=$news->title?></h4>
                <div class="img_news_wrap">
                    <img width="170px" src="<?=$news->image_path?>" />
                    <span><?=date("d.m.Y", strtotime($news->news_date_pub));?></span>
                </div>
                <div class="single_news_item_page_text">
                    <p>
                        <?=$news->preview_text?>
                    </p>
                    <span class="single_news_item_page_link">Подробнее</span>
                </div>
            </a>
            <?}?>
        </div>
    </div>