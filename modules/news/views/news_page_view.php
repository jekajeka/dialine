
    <div class="row row_breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="">Главная</a></li> | <?=$breadcrumbs_segment?> <li><a href=""><?=$this->data['news'][0]->title?></a></li>
            </ul>
        </div>
    </div>
    <div class="row row_title_h1">
        <div class="container">
            <h1><?=$this->data['news'][0]->title?></h1>
        </div>
    </div>
    <div class="row single_news_page">
        <div class="container">
            <div class="pretext_news">
                <p>
                    <?=$this->data['news'][0]->subtitle?>
                </p>
            </div>
            <div class="single_news_text">
                <div class="single_news_img">
                    <img src="<?=$this->data['news'][0]->path?>" />
                </div>
                <?=$this->data['news'][0]->full_text?>
            </div>
        </div>
    </div>
    