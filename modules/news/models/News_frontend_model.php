<?
class News_frontend_model extends CI_Model {
private $enterprise_query_data_list = '';

function News_frontend_model()
{
    parent::__construct();
}    

function get_single_news($path = NULL)
{
 $query = " SELECT news.id, news.title, news.subtitle, news.full_text, news.date_pub AS news_date_pub, images.path,
			news.meta_title, news.meta_description
            FROM news
            LEFT JOIN images ON news.id = images.parent_id
            AND images.parent_type = 'news'
            WHERE news.path = '$path' ";
    $data = $this->db->query($query);
    return $data->result();   
}

function get_all_news()
{
 $query = " SELECT news.title, news.preview_text, news.date_pub AS news_date_pub,news.path, images.path as image_path
            FROM news, images, headings_node
            WHERE news.id = images.parent_id
            AND images.parent_type = 'news'
            AND news.id = headings_node.node_id
            AND ((headings_node.headings_id <> 2) AND (headings_node.headings_id <> 10))
            GROUP BY news.id
            ORDER BY news.date_pub DESC";
    $data = $this->db->query($query);
    return $data->result();   
}

function get_news_by_type($type = NULL)
{
 $query = " SELECT news.title, news.preview_text, news.date_pub AS news_date_pub,news.path, images.path as image_path
            FROM news, images, headings_node
            WHERE news.id = images.parent_id
            AND images.parent_type = 'news'
            AND news.id = headings_node.node_id
            AND headings_node.headings_id = $type
            ORDER BY news.date_pub DESC";
    $data = $this->db->query($query);
    return $data->result();   
}

function get_news_on_main()
{
    $query = " SELECT news.title, news.preview_text, DATE_FORMAT(news.date_pub, '%d.%m.%Y') AS news_date_pub,news.path, images.path as image_path
            FROM news, images, headings_node
            WHERE news.id = images.parent_id
            AND images.parent_type = 'news'
            AND news.id = headings_node.node_id
            AND ((headings_node.headings_id <> 2) AND (headings_node.headings_id <> 10))
            ORDER BY news.date_pub DESC
            LIMIT 4";
    $data = $this->db->query($query);
    return $data->result();
}

function get_news_by_search_text($search)
{
    $search = str_replace(' ','%',$search);
    $query = "SELECT news.*
            FROM news
            WHERE title LIKE '%$search%'
            OR
            subtitle LIKE '%$search%'
            OR
            full_text LIKE '%$search%'
            ORDER BY date_pub DESC";
    $data = $this->db->query($query);
    return $data->result();
}

function get_news_by_type_and_date($type,$year = 'all',$month = 'all')
{
    $year_string = '';
    $month_string = '';
    if($year!='all')
        $year_string = " AND  DATE_FORMAT(news.date_pub, '%Y')= $year ";
    if($month!='all')
        $month_string = " AND  DATE_FORMAT(news.date_pub, '%m')= $month ";
    $query =  "SELECT news.title, news.preview_text, DATE_FORMAT(news.date_pub, '%d.%m.%Y') AS news_date_pub,news.path, images.path as image_path
            FROM news, images, headings_node
            WHERE news.id = images.parent_id
            AND images.parent_type = 'news'
            AND news.id = headings_node.node_id
            AND headings_node.headings_id = $type
            $year_string
            $month_string
            ORDER BY news.date_pub DESC";
    $data = $this->db->query($query);
    return $data->result();
}



}
