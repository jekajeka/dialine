<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_page extends Frontend_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('news/news_frontend_model', '', TRUE);
        $this->load->model('info/frontend_info_model', '', TRUE);
        $this->load->model('admin_gallery/admin_gallery_model', '', TRUE);
        $this->load->model('admin_reviews/admin_reviews_model', '', TRUE);
        $this->load->model('admin_video/video_model', '', TRUE);
        $this->load->model('clinic/frontend_clinics_model', '', TRUE);
        $this->title = 'Диалайн';
    }


    public function index()
    {
        $this->output->cache(60);
        $this->data['title'] = 'Диалайн';
        $this->data['clinics'] = $this->frontend_clinics_model->get_clinics_list();
        $this->data['reviews'] = $this->admin_reviews_model->get_reviews_count_from(5, 0);
        $this->data['banners'] = $this->admin_gallery_model->get_all_banners(false,false);
        $this->data['video'] = $this->video_model->get_node(1);
        $this->data['news_objects'] = $this->news_frontend_model->get_news_on_main();
        $config['image_library'] = 'GD2'; // выбираем библиотеку
        $config['create_thumb'] = TRUE; // ставим флаг создания эскиза
        $config['maintain_ratio'] = TRUE; // сохранять пропорции
        $config['width'] = 270; // и задаем размеры
        $config['height'] = 150;
        foreach ($this->data['news_objects'] as $key => $clinic) {
            $config['source_image'] = substr($clinic->image_path, 1);
            $config['new_image'] = 'uploads/thumb/';
            $this->load->library('image_lib'); // загружаем библиотеку
            $this->image_lib->initialize($config);
            if (!$this->image_lib->resize()) {
            } else
                $this->data['news_objects'][$key]->image_path = '/uploads/thumb/' . basename($clinic->image_path, '.jpg') . '_thumb.jpg';
        }
        $this->getSeo('main_page', 0);
        $this->load->view('main_page_body_view', $this->data);
    }


}
