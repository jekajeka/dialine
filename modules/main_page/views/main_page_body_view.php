<div class="row super_slider">
        <div class="swiper-wrapper">
                <?foreach($banners as $banner){?>
                <a title='<?=$banner->title?>' href="<?=$banner->link?>" class="swiper-slide" style="background-image:url(<?=$banner->path?>); background-repeat: no-repeat;background-position: center;background-size: cover">
                    <div class="container">
                        <div class="swiper_table">
                            <div class="swiper_table_child">
                                <div class="green_field">
                                    <div class="title"><?=$banner->title?></div>
                                    <?=$banner->text?>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <?}?>
        </div>
        <div class="switches"></div>
</div>
<div class="row row_title">
        <div class="container main_page_h1"><h1>СЕТЬ МНОГОПРОФИЛЬНЫХ КЛИНИК ДИАЛАЙН</h1><span> ЭТО:</span></div>
</div>
<div class="row row_advantages">
        <div class="container">
            <div class="advantage_wrap advantage_wrap_1">
                <div class="advatage_icon advatage_icon_1"></div>
                <h2 class="advantage_title">Опытные<br/>специалисты</h2>
                <div class="advantage_overlay">
                    <div class="advantage_overlay_title">Опытные специалисты</div>
                    <div class="advantage_overlay_text">
                        <p>
                            Мы  тщательно подходим к подбору персонала. Наши врачи регулярно проходят обучение, сдают аттестацию, повышают  квалификацию.
                        </p>
                    </div>
                </div>
            </div>
            <div class="advantage_wrap advantage_wrap_2">
                <div class="advatage_icon advatage_icon_2"></div>
                <h2 class="advantage_title">Комфортные условия</h2>
                <div class="advantage_overlay">
                    <div class="advantage_overlay_title">Комфортные условия</div>
                    <div class="advantage_overlay_text">
                        <p>
                            Работаем без выходных, собственный колл-центр, территориальная доступность, обслуживание на дому.
                        </p>
                    </div>
                </div>
            </div>
            <div class="advantage_wrap advantage_wrap_3" >
                <div class="advatage_icon advatage_icon_3" ></div>
                <h2 class="advantage_title">Многопрофильность</h2>
                <div class="advantage_overlay">
                    <div class="advantage_overlay_title">Многопрофильность</div>
                    <div class="advantage_overlay_text">
                        <p>
                            Максимальное количество услуг в рамках одной клиники для экономии времени пациента.
                        </p>
                    </div>
                </div>
            </div>
            <div class="advantage_wrap advantage_wrap_4">
                <div class="advatage_icon advatage_icon_4"></div>
                <h2 class="advantage_title">Современное оборудование</h2>
                <div class="advantage_overlay">
                    <div class="advantage_overlay_title">Современное оборудование</div>
                    <div class="advantage_overlay_text">
                        <p>
                            Минихолтеры, высокоточные аппараты УЗИ, ультратонкое видеоэндоскопическое оборудование, рентген и многое другое, что позволяет выявить заболевание на ранней стадии.
                        </p>
                    </div>
                </div>
            </div>
            <div class="other_adv_wrap">
                <div class="advantage_wrap advantage_wrap_5">
                        <div class="advatage_icon advatage_icon_5"></div>
                        <h2 class="advantage_title">Собственная хирургия и лаборатория </h2>
                        <div class="advantage_overlay">
                            <div class="advantage_overlay_title">Собственная хирургия и лаборатория </div>
                            <div class="advantage_overlay_text">
                                <p>
                                    позволяют контролировать полный цикл диагностики и лечения.
                                </p>
                            </div>
                        </div>
                </div>
                <div class="advantage_wrap advantage_wrap_6">
                        <div class="advatage_icon advatage_icon_6"></div>
                        <h2 class="advantage_title">Единая инфосистема</h2>
                        <div class="advantage_overlay">
                            <div class="advantage_overlay_title">Единая инфосистема</div>
                            <div class="advantage_overlay_text">
                                <p>
                                    позволяет быстро найти  электронную карту пациента, а врачу оперативно проследить историю болезней и назначений из любой клиники Диалайн. Вам не придется расшифровывать почерк врача – все назначения в печатном виде.
                                </p>
                            </div>
                        </div>
                </div>
                <div class="advantage_wrap advantage_wrap_7">
                        <div class="advatage_icon advatage_icon_7"></div>
                        <h2 class="advantage_title">Стабильность</h2>
                        <div class="advantage_overlay">
                            <div class="advantage_overlay_title">Стабильность</div>
                            <div class="advantage_overlay_text">
                                <p>
                                     Диалайн существует с 2001 г., трудоустраивает более 700 человек, ежегодно открывает новые клиники.
                                </p>
                            </div>
                        </div>
                </div>
                <div class="advantage_wrap advantage_wrap_8">
                        <div class="advatage_icon advatage_icon_8"></div>
                        <h2 class="advantage_title">Гибкие условия</h2>
                        <div class="advantage_overlay">
                            <div class="advantage_overlay_title">Гибкие условия</div>
                            <div class="advantage_overlay_text">
                                <p>
                                    работаем по программам ОМС и ДМС, предлагаем систему скидок для постоянных клиентов, работаем с корпоративными клиентами на индивидуальных условиях по профосмотрам, вакцинации, выезжаем на территорию предприятия.
                                </p>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <div class="container more_advantages">
            <a class="more_advantages_btn">Ещё</a>
        </div>
    </div>
    <div class="row row_title" id="find_clinic_block">
        <div class="container"><a title="Все клиники Диалайн" href="/clinic">Найти ближайшую к Вам клинику</a></div>
    </div>
    <div class="row">
        <div class="container">
            <div class="map_operate_wrap">
                <div class="addresses_wrap">
                <?foreach($clinics as $key=>$clinic){?>
                    <div class="single_address_wrap">
                        <div class="single_addr_title" data-trg="<?=$key?>" title='Расположение <?=$clinic->title?> на карте'><?=$clinic->title?> <?=$clinic->district?></div>
                        <a class="full_clinic_link" title='<?=$clinic->title?>' href="/clinic/<?=$clinic->path?>">Подробнее...</a>
                        <div class="single_addr_data_wrap">
                                <?
                                $w_times = explode ("\n",$clinic->work_time);
                                ?>
                                <div class="time_data_wrap">
                                <?foreach($w_times as $w_tm){?>
                                        <div class="time_data"><?=$w_tm?></div>
                                <?}?>
                                </div>
                        </div>
                    </div>
                <?}?>
                </div>
            </div>
            <div class="" id="map"></div>
        </div>
    </div>
    <div class="row plancs">
        <div class="container">
            <!--noindex-->
            <div class="planc_wrap open_overlay_form_wrapper">
                <div class="plank_text_wrap ">
                    <a class="plank_title">Запись на прием</a>
                    <div class="plank_text">
                        Заполните форму обратной связи, и наши операторы свяжутся с Вами
                    </div>
                    <a class="plank_link plank_link_order "   href="">Записаться</a>
                    <div class="plank_overlay"></div>
                </div>
                <div class="plank_img_wrap plank_img_wrap_1"></div>
            </div>
            <!--/noindex-->
            <a class="planc_wrap" href="/dolg" title="Оплата оказанных услуг">
                <div class="plank_text_wrap" >
                    <span class="plank_title">Оплата услуг</span>
                    <div class="plank_text">
                        Здесь Вы можете оплатить уже оказанные клиниками ДИАЛАЙН услуги, по которым имеется задолженность
                    </div>
                    <span class="plank_link plank_link_pay" >Перейти к оплате</span>
                    <div class="plank_overlay"></div>
                </div>
                <div class="plank_img_wrap plank_img_wrap_2"></div>
            </a>
        </div>
    </div>
    <div class="row voicers_wrap">
        <div class="container">
                <a class="voicers" target="_blank" href="https://docs.google.com/forms/d/e/1FAIpQLSex7ZHmq_RTNS1RjrEuR6aLQwUJehHPsk3bcRdecieAjv17_A/viewform?embedded=true">
                        <img src="/frontend/img/voice3.png" alt="Голосование" />
                        <span class="voicers_title">Оцените качество оказания услуг сети многопрофильных клиник ДИАЛАЙН</span>
                        <span class="voicers_link">Голосовать</span>
                </a>
                <a class="voicers" target="_blank" href="http://oblzdrav.volgograd.ru/other/otsenka-kachestva/">
                        <img src="/frontend/img/voice1.png" alt="Голосование" />
                        <span class="voicers_title">Независимая оценка качества оказания услуг медицинскими организациями</span>
                        <span class="voicers_link">Голосовать</span>
                </a>
                <a class="voicers" target="_blank" href="https://www.rosminzdrav.ru/polls/9-anketa-dlya-otsenki-kachestva-okazaniya-uslug-meditsinskimi-organizatsiyami-v-ambulatornyh-usloviyah?region_code=VGG">
                        <img src="/frontend/img/voice2.png" alt="Голосование" />
                        <span class="voicers_title">Независимая оценка качества оказания услуг медицинскими организациями</span>
                        <span class="voicers_link">Голосовать</span>
                </a>
        </div>
    </div>
    <div class="row row_video">
        <div class="container">
            <div class="block6">
                <div class="video_text_wrap">
                    <div class="video_title"><?=$video[0]->title?></div>
                    <div class="video_text">
                        <?=$video[0]->content?>
                    </div>
                </div>
            </div>
            <div class="block6">
                <div class="video_vrap">
                    <iframe width="600" height="290" src="<?=$video[0]->video_link?>" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div class="container all_video_link">
            <!--noindex-->
            <a target="_blank" rel="nofollow" href="https://www.youtube.com/channel/UCARtFzDwn-05UVzFnOUR4Xg">Смотреть больше видео о клинике Диалайн</a>
            <!--/noindex-->
        </div>
    </div>
    <div class="row row_reviews">
        <div class="container">
            <div class="leave_review_wrap">
                <div class="leave_review_title">Написать главному врачу</div>
                <div class="review_form">
                    <input type="text" id="review_name" placeholder="Ваше имя" />
                    <input type="text" id="review_phone" placeholder="Телефон" />
                    <select id="review_clinic">
                        <option value="all">Выберите клинику</option>
                        <?foreach($clinics as $clinic){?>
                        <option value="<?=$clinic->id?>"><?=$clinic->title?></option>
                        <?}?>
                    </select>
                    <textarea id="review_text" placeholder="Ваш отзыв"></textarea>
                    <div class="wrap_agree_checkbox">
                        <input type="checkbox"   id="leave_review_agree"><label>Оставляя свои персональные данные, Вы даете добровольное согласие на их обработку. <a target="_blank" href="/uploads/files/Politika_obrabotki_i_zashiti_personalnih_dannih1.pdf">Подробнее</a></label>
                    </div>
                    <a class="big_green_btn">Отправить</a>
                </div>
            </div>
            <div class="reviews_title">Последние отзывы</div>
            <div class="reviews_container">
                <div class="swiper-wrapper">
                        <?foreach($reviews as $review){?>
                        <div class="swiper-slide">
                            <div class="single_review_wrap">
                                <div class="single_review_title_wrap">
                                    <?=$review->name?>
                                    <span><?=date("d.m.Y", strtotime($review->date_review))?></span>
                                </div>
                                <div class="addr_review"><?=$review->title?><br/> <?=$review->district?></div>
                                <div class="text_review">
                                    <p>
                                        <?=mb_substr(strip_tags($review->text),0,220).'...'?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <?}?>
                </div>
                <div class="reviews_control">
                    <a class="reviews_left"></a>
                    <a class="reviews_right"></a>
                </div>
                <a class="reviews_all_link" href="/reviews">Все отзывы</a>
            </div>
        </div>
    </div>
    <div class="row row_title">
        <div class="container">Последние новости</div>
    </div>
    <div class="row row_news">
        <div class="container">
                <?foreach($news_objects as $news){?>
            <div class="news_item_wrap">
                <a class="news_img_link" title="<?=$news->title?>" href="/news/<?=$news->path?>.new"><img alt="<?=$news->title?>" src="<?=$news->image_path?>" /></a>
                <div class="news_date"><?=$news->news_date_pub?></div>
                <div class="news-title"><?=$news->title?></div>
                <p>
                    <?=$news->preview_text?>
                </p>
                <a class="news_full_link" title="<?=$news->title?>" href="/news/<?=$news->path?>.new">Подробнее</a>
            </div>
            <?}?>            
        </div>
        <div class="container wrap_big_link">
            <a href="/news" class="big_green_btn">Все новости</a>
        </div>
    </div>
    <script>
    function init () {
        myMap = new ymaps.Map ("map", {
                                    center: [48.758625,44.818625],
                                    zoom: 16
                                });
        var myPlacemark = new Array;
        visible = new ymaps.GeoObjectCollection();
        group_volgograd = new ymaps.GeoObjectCollection({
                properties: {
                    id: 'group-volgograd',
                    name: 'Волгоград'
                }
            })
        group_voljsky = new ymaps.GeoObjectCollection({
                properties: {
                    id: 'group_voljsky',
                    name: 'Волжский'
                }
            })
        group_mikhailovka = new ymaps.GeoObjectCollection({
                properties: {
                    id: 'group_mikhailovka',
                    name: 'Михайловка'
                }
            })
        <?foreach($clinics as $key=>$clinic) {?>
                myPlacemark[<?=$key?>] = new ymaps.Placemark([<?=$clinic->coordinates?>], {
                balloonContent: '<?=$clinic->title?><br/><a href="/clinic/<?=$clinic->path?>">Подробнее...</a>',
                iconCaption: '<?=$clinic->title?>'
        }, {
            preset: 'islands#hospitalIcon',
	    iconColor: '#00abb2'
        });
        <?
        if(strpos($clinic->address,'Волгоград')){?>
                group_volgograd.add(myPlacemark[<?=$key?>]);
        <?}
        if(strpos($clinic->address,'Волжский')){?>
                group_voljsky.add(myPlacemark[<?=$key?>]);
        <?}
        if(strpos($clinic->address,'Михайловка')){?>
                group_mikhailovka.add(myPlacemark[<?=$key?>]);
        <?}
        }?>
        $('.single_addr_title').click(function(e){
                e.preventDefault();
                var opts = Object;
                opts.duration = 2000;
                myMap.panTo(myPlacemark[$(this).attr('data-trg')].geometry.getBounds(),opts);
                });
        $('.selector_sity a').click(function(e){
                if($(this).data('city') == "Волжский")
                    myMap.setBounds(group_voljsky.getBounds(),{checkZoomRange:true, callback:function(){ if(map.getZoom() > 10) map.setZoom(10); } });
                if($(this).data('city') == "Михайловка")
                        myMap.setBounds(group_mikhailovka.getBounds(),{checkZoomRange:true, callback:function(){ if(map.getZoom() > 10) map.setZoom(10); } });
                else
                        myMap.setBounds(group_volgograd.getBounds(),{checkZoomRange:true, callback:function(){ if(map.getZoom() > 10) map.setZoom(10); } });
                })
        visible.add(group_volgograd).add(group_voljsky).add(group_mikhailovka);
        myMap.geoObjects.add(visible);
        storage = ymaps.geoQuery(myPlacemark);
        if(Cookies.get('city')== "Волжский")
                myMap.setBounds(group_voljsky.getBounds(),{checkZoomRange:true, callback:function(){ if(map.getZoom() > 10) map.setZoom(10); } });
        else if(Cookies.get('city')== "Михайловка")
                myMap.setBounds(group_mikhailovka.getBounds(),{checkZoomRange:true, callback:function(){ if(map.getZoom() > 10) map.setZoom(10); } });
        else
                myMap.setBounds(group_volgograd.getBounds(),{checkZoomRange:true, callback:function(){ if(map.getZoom() > 10) map.setZoom(10); } });
    
myMap.behaviors.disable('scrollZoom');


    
}

$(document).ready(function(){
    ymaps.ready(init);
    
    
});
</script>