<!DOCTYPE html>
<html lang="ru">
<head>
<title><?=isset($meta_title) && !empty($meta_title) ? $meta_title : $title;?></title>
<?=$this->load->view('frontend/header_view',$this->data,TRUE);?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<link href="/frontend/css/select2.min.css" rel="stylesheet" />
<?
if(get_cookie('blind_see')) {?>
    <link href="/frontend/css/dialine_blind.css" rel="stylesheet" />
    <script src="/frontend/js/blind.js"></script>
<?} else {
    $main_css = 'frontend/css/dialine.css';?>
    <link href="/<?=$main_css.'?v='.filemtime($main_css)?>" rel="stylesheet" />
<?}?>

<script src="/frontend/js/jquery-1.10.1.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>

<script>
var __cs = __cs || [];
__cs.push(["setCsAccount", "0yxP0X858v474vs1PPcuMepV4xVZClxM"]);
__cs.push(["setDynamicalReplacement", true]);
</script>
<script async src="//app.comagic.ru/static/cs.min.js"></script>
</head>

<body>
    <!--noindex-->
    <noscript><div><img src="https://mc.yandex.ru/watch/17755831" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!--/noindex-->

    <!-- /Yandex.Metrika counter -->
    <!--<style>
        #page-preloader {
        position: fixed;
        left: 0;
        top: 0;
        right: 0;
        bottom: 0;
        background: #fff;
        z-index: 9999;
        }
    
        #page-preloader .spinner {
        width: 222px;
        height: 181px;
        position: absolute;
        left: 50%;
        top: 50%;
        background: url('/frontend/images/preload.gif') no-repeat 50% 50%;
        margin: -90px 0 0 -111px;
        }
    </style>
        <div id="page-preloader"><span class="spinner"></span></div>-->
    <?=$this->load->view('frontend/main_menu_view',$this->data,TRUE);?>
    <section>
        <?=$content?>
        <?=$this->load->view('frontend/footer_view','',TRUE);?>
    </section>
    <?=$this->load->view('frontend/overflow_search_view','',TRUE);?>
    <?=$this->load->view('frontend/overlay_form_wrapper','',TRUE);?>
    <?=$this->load->view('frontend/mobile_menu','',TRUE);?>

<!-- styles -->

<!--noindex-->
<link rel="stylesheet" media="screen" href="/frontend/css/idangerous.swiper.css" />
<link rel="stylesheet" href="/frontend/css/jquery.mCustomScrollbar.css" />
<link href="/frontend/css/magnific-popup.css" rel="stylesheet" />
<? $portrait_css = 'frontend/css/dialine_portrait.css';?>
<link href="/<?=$portrait_css.'?v='.filemtime($portrait_css)?>" rel="stylesheet" />

<!-- scripts -->
<? $main_js = 'frontend/js/main_main.js';?>
<script src="/<?=$main_js.'?v='.filemtime($main_js)?>"></script>
<!--<script src="//cdn.callibri.ru/callibri.js"></script>-->
<script src="/frontend/js/jquery.scrollTo.min.js"></script>
<script src="/frontend/js/jquery.magnific-popup.min.js"></script>
<script src="/frontend/js/jquery.mobile.custom.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="/frontend/js/idangerous.swiper-2.1.min.js"></script>
<script src="/frontend/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>-->
<script src="/frontend/js/select2.min.js"></script>
<script src="/frontend/js/js.cookie.min.js"></script>
<script src="/frontend/js/search_articles.min.js"></script>
<script src="/frontend/js/search_rules.min.js"></script>
<script src="/frontend/js/jquery.mask.min.js"></script>

<script src="/frontend/js/jquery.autocolumnlist.min.js"></script>

<!-- Facebook Conversion Code for Просмотры Диалайн -->
<script>(function() {
var _fbq = window._fbq || (window._fbq = []);
if (!_fbq.loaded) {
var fbds = document.createElement('script');
fbds.async = true;
fbds.src = 'https://connect.facebook.net/en_US/fbds.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6030000227959', {'value':'0.00','currency':'RUB'}]);
</script>

<!-- Yandex.Metrika counter -->
<script>
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter17755831 = new Ya.Metrika({
                    id:17755831,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>

<!-- GoogleAnalytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-87931745-1', 'auto');
  ga('send', 'pageview');
</script>
<!--/noindex-->
</body>
</html>
