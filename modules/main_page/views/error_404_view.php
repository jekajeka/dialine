<div class="row news_on_page_all">
    <div class="container">
        <div style="font-size: 200px;width: 100%;text-align: center;color: #0abab5">404</div>
        <div style="font-size: 2rem;width: 100%;text-align: center;">
            <p>
                Приносим свои извинения. Страница, на которую Вы перешли была удалена или перемещена, и теперь доступна по другому адресу. Перейдите на <a title="Перейти на главную страницу сайта" style="color: #0abab5" href="/">главную страницу сайта</a>, или воспользуйтесь главным меню.
                <br/>Желаем Вам приятного путешествия по нашей клинике!
            </p>
        </div>
    </div>
</div>