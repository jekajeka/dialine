<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clinic extends Frontend_Controller  {
    function __construct()
    {
	parent::__construct();
	$this->load->model('frontend_clinics_model', '', TRUE);
	$this->load->model('clinic/frontend_clinics_model', '', TRUE);
	$this->data['clinics'] = $this->frontend_clinics_model->get_clinics_list();
	$this->data['title'] = 'Клиники/Диалайн';
    }

    
    public function index()
    {
	$this->data['title'] = 'Все клиники/Диалайн, Волгоград';
	$this->data['clinics'] = $this->frontend_clinics_model->get_clinics_list();
	$config['image_library'] = 'GD2'; // выбираем библиотеку
	$config['create_thumb'] = TRUE; // ставим флаг создания эскиза
	$config['maintain_ratio'] = TRUE; // сохранять пропорции
	$config['width'] = 100; // и задаем размеры
	$config['height'] = 100;
	foreach($this->data['clinics'] as $key=>$clinic)
	{
	    $config['source_image'] = substr($clinic->image_path,1); 
	    $config['new_image'] = 'uploads/thumb/';
	    $this->load->library('image_lib'); // загружаем библиотеку
	    $this->image_lib->initialize($config);
	    if ( ! $this->image_lib->resize())
	    {
		//echo $config['source_image'];
		//echo $this->image_lib->display_errors();
		$this->data['clinics'][$key]->image_path = $clinic->image_path;
	    }
	    else
		$this->data['clinics'][$key]->image_path = '/uploads/thumb/'.basename($clinic->image_path,'.jpg').'_thumb.jpg';
	}
	$this->getSeo('clinic', 0);
	$this->load->view('priem_grazhdan_view',$this->data);
    }
    
    /**
     * Вывод страницы клиники
     * @param string $path сегмент адреса клиники
     * @return html страница клиники
     */
    function page($path = '')
    {
	$this->data['clinic'] = $this->frontend_clinics_model->get_clinic($path);
        //если в базе нет такой клиники, или она снята с публикации - увести на страницу 404
        if($this->data['clinic'][0]->published)
            $published = 1;
        if($this->data['clinic'][0]->published == '0')
            $published = 0;
        if($this->data['clinic'][0]->published == NULL)
            $published = 1;
        if(!empty($this->data['clinic'])&&$published)
        {
            $this->data['title'] = $this->data['clinic'][0]->title.'/Диалайн, Волгоград';
            $this->data['rubrics'] = $this->frontend_clinics_model->get_med_rubrics($this->data['clinic'][0]->id);
            $this->data['service_types'] = $this->frontend_clinics_model->get_med_service_type($this->data['clinic'][0]->id);
            $this->data['services'] = $this->frontend_clinics_model->get_services($this->data['clinic'][0]->id);
            $this->data['other_clinics'] = $this->frontend_clinics_model->get_other_clinics($path);
            $this->getSeo('clinics', $this->data['clinic'][0]->id);
            $this->load->view('single_clinic_view',$this->data);
        }
        else
            show_404('page');
    }
    
    

}
