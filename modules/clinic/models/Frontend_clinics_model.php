<?
class Frontend_clinics_model extends CI_Model {
private $db_table = 'clinics';

function Frontend_clinics_model()
{
    parent::__construct();
}

    function get_clinic($path = '')
    {
        $query = "SELECT ".$this->db_table.".title, ".$this->db_table.".subtitle,".$this->db_table.".address,".$this->db_table.".coordinates, ".$this->db_table.".text,images.path as image_path,".$this->db_table.".id,".$this->db_table.".path,".$this->db_table.".phones,".$this->db_table.".work_time, published.published
            FROM ".$this->db_table."
            LEFT JOIN images ON
            images.parent_id = ".$this->db_table.".id
            AND images.parent_type = 'clinics'
            LEFT JOIN published ON
            published.type = 'clinics' AND published.record_id = ".$this->db_table.".id 
            WHERE ".$this->db_table.".path = '$path'";
        $data = $this->db->query($query);
        return $data->result();
    }

function get_other_clinics($path = '')
{
    $query = "SELECT ".$this->db_table.".title, ".$this->db_table.".subtitle,".$this->db_table.".address,".$this->db_table.".coordinates, ".$this->db_table.".text,".$this->db_table.".id,".$this->db_table.".path,".$this->db_table.".phones,".$this->db_table.".work_time
                FROM ".$this->db_table."
                WHERE ".$this->db_table.".path <> '$path'";
    $data = $this->db->query($query);
    return $data->result();   
}

function get_med_rubrics($clinic_id)
{
    $query = "SELECT count( * ) as services_qty , medical_directions.service_rubric_title,rubric
                FROM services,medical_directions,service_clinic
                WHERE services.rubric=medical_directions.id
                AND service_clinic.clinic_id = $clinic_id
                AND service_clinic.service_id = services.med_id
                GROUP BY rubric
                ORDER BY `services`.`rubric` ASC";
    $data = $this->db->query($query);
    return $data->result();
}

function get_med_service_type($clinic_id)
{
    $query = "SELECT service_type, rubric
                FROM `services`,service_clinic
                WHERE
                service_clinic.clinic_id = $clinic_id
                AND service_clinic.service_id = services.med_id
                GROUP BY service_type,rubric
                ORDER BY `services`.`rubric` ASC";
    $data = $this->db->query($query);
    return $data->result();
}

function get_services($clinic_id)
{
    $query = "SELECT services.id,services.title,services.price,services.rubric,service_type,services.link FROM services,service_clinic
            WHERE
            service_clinic.clinic_id = $clinic_id
            AND service_clinic.service_id = services.med_id
            ORDER BY services.title ASC";
    $data = $this->db->query($query);
    return $data->result();
}


    /*èçâëå÷åíèå ñïèñêà âñåõ ÎÏÓÁËÈÊÎÂÀÍÍÛÕ êëèíèê
     *$order_by - ïîëå ñîðòèðîâêè äàííûõ
     *$order_direction íàïðàâëåíèå ñîðòèðîâêè äàííûõ
     *return ñïèñîê âñåõ êëèíèê
     */
    function get_clinics_list($order_by = 'title',$order_direction = 'ASC')
    {
        $this->db->select(array($this->db_table.'.*','published.published','images.path as image_path'));
        $this->db->from($this->db_table);
        $this->db->join('published', "published.type = 'clinics' AND published.record_id = ".$this->db_table.".id ",'left');
        $this->db->join('images', "images.parent_id = ".$this->db_table.".id AND images.parent_type = 'clinics' ",'left');
        $this->db->where('(published.published=1 OR published.published IS NULL)');
        $this->db->order_by($order_by,$order_direction);
        return $this->db->get()->result();
    }

    /*èçâëå÷åíèå ñïèñêà âñåõ  êëèíèê
     *$order_by - ïîëå ñîðòèðîâêè äàííûõ
     *$order_direction íàïðàâëåíèå ñîðòèðîâêè äàííûõ
     *return ñïèñîê âñåõ êëèíèê
     */
    function get_all_clinics_list($order_by = 'title',$order_direction = 'ASC')
    {
        $this->db->select(array($this->db_table.'.*','images.path as image_path'));
        $this->db->from($this->db_table);
        $this->db->join('images', "images.parent_id = ".$this->db_table.".id AND images.parent_type = 'clinics' ",'left');
        $this->db->order_by($order_by,$order_direction);
        return $this->db->get()->result();
    }

    /*èçâëå÷åíèå ñïèñêà âñåõ êëèíèê íà ïîèñêå
     *$text ñòðîêà ïîèñêà
     *return ñïèñîê âñåõ êëèíèê
     */
    function get_clinics_by_text($text = '')
    {
        $this->db->select(array($this->db_table.'.*'));
        $this->db->from($this->db_table);
        $this->db->join('published', "published.type = 'clinics' AND published.record_id = ".$this->db_table.".id ",'left');
        $this->db->where('(published.published=1 OR published.published IS NULL)');
        $this->db->where("(title LIKE '%$text%' OR subtitle LIKE '%$text%' OR address  LIKE '%$text%' OR district  LIKE '%$text%') OR text LIKE '%$text%'");
        $this->db->group_by($this->db_table.'.id');
        return $this->db->get()->result();
    }


}