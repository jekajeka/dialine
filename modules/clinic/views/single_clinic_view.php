    <div class="row row_breadcrumbs">
        <div class="container">
            <ul>
                <li><a title="СЕТЬ МНОГОПРОФИЛЬНЫХ КЛИНИК ДИАЛАЙН" href="/">Главная</a></li> | <li><a href="/clinic">Клиники</a></li> | <li><a href="/clinic/<?=$clinic[0]->path?>"><?=$clinic[0]->title?></a></li>
            </ul>
        </div>
    </div>
    <div class="row row_single_service">
        <div class="container">
            <div class="single_service_left_list_wrap">
                <div class="single_service_left_list box_shadow">
                    <h3>Клиники</h3>
                    <ul>
                        <?foreach($other_clinics as $other_clinic){?>
                        <li><a  href="/clinic/<?=$other_clinic->path?>"><?=$other_clinic->title?></a></li>
                        <?}?>
                    </ul>
                </div>
            </div>
            <div class="single_service_wrap" >
                <h1 ><?=$clinic[0]->title?></h1>
                <div class="switch_text_wrap clinic_wrap active">
                    <div class="clinic_image" style="float: right"><img src="<?=$clinic[0]->image_path?>" alt="<?=$clinic[0]->title?>"></div>
                    <div class="doctor_text_data" itemscope itemtype="http://schema.org/LocalBusiness">
                        <dl>
                            <dt class="org-name-hidden"><span itemprop="name"><?=$clinic[0]->title?></span></dt>
                            <dt>Адрес:</dt>
                            <dd itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="streetAddress"><?=$clinic[0]->address?></span></dd>
                            <dt>Телефоны:</dt>
                            <dd itemprop="telephone"><?=nl2br($clinic[0]->phones)?></dd>
                            <dt>Режим работы:</dt>
                            <dd itemprop="openingHours" datetime="Mo-Su"><?=nl2br($clinic[0]->work_time)?></dd>
                        </dl>
                    </div>
                    <?=$clinic[0]->text?>
                    <div class="price_list_wrap">
                        <div class="structured_data_wrap">
                            <?foreach($rubrics as $rubric){?>
                            <div class="structured_data_content_wrap">
                                <a class="structured_data_title">
                                    <h4><?=$rubric->service_rubric_title?></h4><span class="structured_data_title_qty"><?=$rubric->services_qty?></span>
                                </a>
                                <div class="structured_data_table_wrap">
                                    <?foreach($service_types as $service_type){
                                        if($service_type->rubric == $rubric->rubric){?>
                                    <h3><?=$service_type->service_type?></h3>
                                    <div class="structured_data_table_wrap_order">
                                        <?foreach($services as $service){
                                        if(($service->service_type == $service_type->service_type)&&($service->rubric == $service_type->rubric)){?>
                                        <div class="structured_data_table_line">
                                            <div class="structured_data_table_line_border">
                                                <h4><?=$service->title?></h4>
                                                <div class="structured_data_table_line_price"><?=$service->price?> р.</div>
                                            </div>
                                            <a data-service="<?=$service->title?> за <?=$service->price?> р." class="clinic_service_link structured_data_table_link">Записаться</a>
                                        </div>
                                        <?}}?>
                                    </div>
                                    <?}}?>
                                </div>
                            </div>
                            <?}?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="overlay_form_wrapper" id="send_clinic_service">
    <div class="overlay_form">
        <a class="close_overlay_form"></a>
        <div class="overlay_form_inner">
            <div class="overlay_form_title">Записаться на прием</div>
            <input type="hidden" id="clinic_service_purpose" value="" />
            <input type="hidden" id="clinic_name" value="<?=$clinic[0]->title?>" />
            <input type="text" id="clinic_service_name" placeholder="Имя" />
            <input type="text" id="clinic_service_phone" placeholder="Телефон" />
            <div class="wrap_agree_checkbox">
                <input type="checkbox"   id="clinic_service_agree"><label>Оставляя свои персональные данные, Вы даете добровольное согласие на их обработку. <a target="_blank" href="/uploads/files/Politika_obrabotki_i_zashiti_personalnih_dannih1.pdf">Подробнее</a></label>
            </div>
            <a class="send_overlay_form" id="clinic_service_send">Отправить</a>
            <p>*С Вами свяжутся в ближайшее время и запишут на удобное время к выбранному специалисту</p>
        </div>
    </div>
</div>