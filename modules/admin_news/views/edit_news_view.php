<script src="/vendors/select2/dist/js/select2.full.min.js"></script>
<link href="/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
<div class="">
            
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Изменить новость</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a title="Удалить новость" href="" class="add-link"><i class="fa fa-trash"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <input type="hidden" id="id" name="id" value="<?=$news[0]->id?>">
                    <form id="edit_news" data-parsley-validate class="form-horizontal form-label-left">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Заголовок <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="title" class="form-control col-md-7 col-xs-12" value="<?=$news[0]->title?>"   name="title" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="headings">Тип материала <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="headings" class="form-control" placeholder="Выберите тип материала" id="headings">
                            <option value="0">Выберите тип материала</option>
                            <?foreach($news_type as $n_type){?>
                            <?
                            $active_heading = '';
                            if($n_type->id == $news[0]->headings_id) $active_heading = 'selected'?>
                            <option <?=$active_heading?> value="<?=$n_type->id?>"><?=$n_type->title?></option>
                            <?}?>
                          </select>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">Главное изображение</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <img src="<?=$news[0]->image_path?>">
                        </div>
                      </div>
                     <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >&nbsp;</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input name="image" type="file" id="image">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="date_pub">Дата публикации</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="date_pub" class="form-control has-feedback-left" value="<?=$news[0]->news_date_pub?>" id="date_pub" placeholder="Дата публикации" aria-describedby="inputSuccess2Status">
                          <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                          <span id="inputSuccess2Status" class="sr-only">(success)</span>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="path">Адрес новости<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="path" class="form-control col-md-7 col-xs-12" value="<?=$news[0]->news_path?>" data-validate-words="1" name="path" placeholder="Только цифры" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="subtitle">Подзаголовок</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea class="form-control" name="subtitle"  placeholder="" rows="3" style="width: 100%; height: 135px;resize: none" id="subtitle"><?=$news[0]->subtitle?></textarea>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Краткий текст</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea name="preview_text" class="form-control"  placeholder="" rows="3" style="width: 100%; height: 135px;resize: none" id="preview_text"><?=$news[0]->preview_text?></textarea>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Полный текст</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea name="full_text" class="form-control" placeholder="" rows="3" style="width: 100%; height: 135px;resize: none" id="full_text"><?=$news[0]->full_text?></textarea>
                        </div>
                      </div>
					  <?if(isset($seo)):?>
						<?$this->load->view('admin/seo', $seo);?>
					  <?endif;?>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="add_user_final">
                          <button type="submit" class="btn btn-success">Сохранить</button>
                        </div>
                      </div>

                    </form>
                    
                  </div>
                </div>
              </div>
            </div>      
</div>
<script src="/admin_js/admin_news/admin_news.js"></script>
