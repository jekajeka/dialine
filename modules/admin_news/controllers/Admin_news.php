<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_news extends Admin_Controller  {
function __construct()
{
    parent::__construct();
    $this->load->model('news_model', '', TRUE);
    $this->load->model('headings_model', '', TRUE);
    $this->title = 'Новости';
    $this->data['title'] = 'Редактирование новостей';
    $this->logs_data_type = 'Новости';
}

public function index()
{
    $this->data['title'] = 'Все новости';
    //$this->data['users_objects'] = $this->base_node_model->get_all_users();
    $this->data['news_objects'] = $this->news_model->get_all_news();
    $this->data['users_list'] = $this->load->view('base_node_list_view',$this->data,TRUE);
    $this->load->view('admin/base_node_dashboard_view');
}

public function add_news_page()
{
    $this->data['title'] = 'Добавить новость';
    $this->data['news_type'] = $this->headings_model->select_headings_by_type(1);
    $this->load->view('add_news_view',$this->data);
}

/**
* Добавление новости
* @param  
* @return вывод результатов сохранения
*/
public function add_news()
{
    $this->load->model('images_model', '', TRUE);
    $this->view_type = 2;
    $date_time = $this->input->post('date_pub');
    if(empty($date_time))
	$date_pub = date("Y-m-d");
    else
	$date_pub = date("Y-m-d", strtotime($this->input->post('date_pub'))).' 00.00.00';
    $additional_data = array('title' => $this->input->post('title'),
			    'preview_text' => $this->input->post('preview_text'),
			    'full_text' => $this->input->post('full_text'),
			    'path' => $this->input->post('path'),
			    'subtitle' => $this->input->post('subtitle'),
			    'date_pub'=>$date_pub
#			    ,
#			    'meta_title' => $this->input->post('meta_title'),
#			    'meta_description' => $this->input->post('meta_description')
			);

	$meta_title = $this->input->post('meta_title');
	$meta_description = $this->input->post('meta_description');
	if (!empty($meta_title)) {
		$additional_data["meta_title"] = $meta_title;
	}
	if (!empty($meta_description)) {
		$additional_data["meta_description"] = $meta_description;
	}
			
    $result = $this->news_model->add_news($additional_data);
    $this->data['result'] = $result['success'];
//     if($this->data['result'] && !empty($this->input->post('image')))
//     {
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '10000';
		$config['max_width']  = '2000';
		$config['max_height']  = '1500';
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload('image'))
		{
			$this->data['result'] =  $this->upload->display_errors();
		}	
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$this->data['result'] = $this->images_model->add_image($result['insert_id'],'news','/uploads/'.$data['upload_data']['file_name']);
		}
//     }
    $this->headings_model->create_node_headings($result['insert_id'],$this->input->post('headings'));
    //логирование
    $this->admin_logs_model->add($result['insert_id'], $this->logs_data_type,$this->input->post('title'),'/admin_news/edit_news_page/'.$result['insert_id'],'Добавление');
    echo $this->load->view('admin/json_result_view',$this->data);
}

function edit_news_page($news_id)
{
    $this->data['news'] = $this->news_model->get_single_news($news_id);
    $this->data['news_type'] = $this->headings_model->select_headings_by_type(1);
    $this->data['news'][0]->news_date_pub = date("m/d/Y", strtotime($this->data['news'][0]->news_date_pub));
    $this->data['title'] = 'Редактировать новость '.$this->data['news'][0]->title;
    $this->getSeo('news', $news_id);
    $this->load->view('edit_news_view',$this->data);
}

/**
* Редактирование новости
* @param  $news_id айдишник новости
* @return вывод результатов сохранения
*/
public function edit_news($news_id)
{
    $this->load->model('images_model', '', TRUE);
    $this->view_type = 2;
    $date_time = $this->input->post('date_pub');
    if(empty($date_time))
	$date_pub = date("Y-m-d");
    else
	$date_pub = date("Y-m-d", strtotime($this->input->post('date_pub'))).' 00.00.00';
	
	$meta_title = $this->input->post('meta_title');
	$meta_description = $this->input->post('meta_description');
	
    $additional_data = array('title' => $this->input->post('title'),
			    'preview_text' => $this->input->post('preview_text'),
			    'full_text' => $this->input->post('full_text'),
			    'path' => $this->input->post('path'),
			    'subtitle' => $this->input->post('subtitle'),
			    'date_pub'=>$date_pub
			);
	$this->saveSeo('news', $news_id);
    $result = $this->news_model->edit_news($news_id,$additional_data);
    $this->data['result'] = $result;
//     if($this->data['result'] && !empty($this->input->post('image')))
//     {
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '10000';
		$config['max_width']  = '2000';
		$config['max_height']  = '1500';
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload('image'))
		{
			$this->data['result'] =  $this->upload->display_errors();
		}	
		else
		{
			$this->images_model->clear_all_parent_images($news_id,'news');
			$data = array('upload_data' => $this->upload->data());
			$this->data['result'] = $this->images_model->add_image($news_id,'news','/uploads/'.$data['upload_data']['file_name']);
		}
//     }
    $headings_type = 1;
    $this->headings_model->clear_all_node_headings($news_id,$headings_type);
    $this->headings_model->create_node_headings($news_id,$this->input->post('headings'));
    //логирование
    $this->admin_logs_model->add($news_id, $this->logs_data_type,$this->input->post('title'),'/admin_news/edit_news_page/'.$news_id,'Редактирование');
    echo $this->load->view('admin/json_result_view',$this->data);
}

/**
* Удаление новости
* @param  $id айдишник новости
* @return вывод результатов сохранения
*/
function delete_news($id = NULL)
{
    $this->load->model('images_model', '', TRUE);
    $this->data['news'] = $this->news_model->get_single_news($id);
    $this->view_type = 2;
    $headings_type = 1;
    $this->images_model->clear_all_parent_images($id,'news');
    $result = $this->news_model->delete_news($id);
    //логирование
    $this->admin_logs_model->add($id, $this->logs_data_type,$this->data['news'][0]->title,'/admin_news/edit_news_page/'.$id,'Удаление');
    $this->headings_model->clear_all_node_headings($id,$headings_type);
}

public function articles()
{
    $this->data['title'] = 'Все полезные статьи';
    $this->data['news_objects'] = $this->news_model->get_all_news_type(2);
    $this->data['users_list'] = $this->load->view('base_node_list_view',$this->data,TRUE);
    $this->load->view('admin/base_node_dashboard_view');
}

public function services()
{
    $this->data['title'] = 'Все статьи об услугах';
    $this->data['news_objects'] = $this->news_model->get_all_news_type(10);
    $this->data['users_list'] = $this->load->view('base_node_list_view',$this->data,TRUE);
    $this->load->view('admin/base_node_dashboard_view');
}


}
