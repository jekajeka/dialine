<?
class News_model extends CI_Model {
private $enterprise_query_data_list = '';

function News_model()
{
    parent::__construct();
}    


function add_news($news_descript = array())
{
    $insert_result = array();
    $insert_result['success'] = $this->db->insert('news', $news_descript);
    $insert_result['insert_id'] = $this->db->insert_id();
    return $insert_result;
}
function get_all_news()
{
 $query = "SELECT
                news.*,headings.title as headings_title
                FROM news
                LEFT JOIN headings_node
                ON headings_node.node_id=news.id
                LEFT JOIN
                headings
                ON headings.id=headings_node.headings_id
                GROUP BY news.id
                ORDER BY id ASC";
    $data = $this->db->query($query);
    return $data->result();   
}

function get_single_news($id = NULL)
{
 $query = " SELECT news.id, news.title, news.subtitle,headings_node.headings_id, news.full_text, news.date_pub AS news_date_pub, news.preview_text, images.path as image_path,news.path as news_path,images.id as image_id, news.meta_title, news.meta_description
            FROM news
            LEFT JOIN images
            ON news.id = images.parent_id
            AND images.parent_type = 'news'
            LEFT JOIN headings_node ON headings_node.node_id=$id
            WHERE news.id = $id ";
    $data = $this->db->query($query);
    return $data->result();   
}

function edit_news($id = NULL,$additional_data = array())
{
    $this->db->where('id', $id);
    return $this->db->update('news', $additional_data);
}

function delete_news($id = NULL)
{
    $this->db->where('id', $id);
    $this->db->delete('news');
}

function get_all_news_type($type = NULL)
{
    if($type)
    {
        $query = "SELECT
                news.id,news.title,news.date_pub 
                FROM news,headings_node
                WHERE
                headings_node.node_id = news.id
                AND headings_node.headings_id = $type
                ORDER BY news.id ASC";
        $query = "SELECT news.*,headings.title as headings_title
                FROM news
                INNER JOIN headings_node
                ON headings_node.node_id=news.id AND headings_node.headings_id = $type
                INNER JOIN
                headings
                ON headings.id=headings_node.headings_id
                GROUP BY news.id";
    $data = $this->db->query($query);
    return $data->result();   
    }
}


}
