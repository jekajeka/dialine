<?
class Admin_law_model extends CI_Model {

private $db_table = 'law_info';

function Admin_license_model()
{
    parent::__construct();
}

function add_law($additional_data = array())
{
    $insert_result = array();
    $insert_result['success'] = $this->db->insert($this->db_table, $additional_data);
    $insert_result['insert_id'] = $this->db->insert_id();
    return $insert_result;
}

function get_law_list()
{
    $this->db->select('*')->from($this->db_table)->order_by("title", "ASC");
    $data = $this->db->get();
    return $data->result();
}

function get_law_by_id($law_id = NULL)
{
    $this->db->select(array('title','law_info.id','path'));
    $this->db->from($this->db_table);
    $this->db->join('files','files.parent_id = '.$law_id.' AND files.parent_type="law"','LEFT');
    $this->db->where('law_info.id', $law_id);    
    return $this->db->get()->result();
}

function edit_law($id = NULL,$additional_data = array())
{
    $this->db->where('id', $id);
    return $this->db->update($this->db_table, $additional_data);
}

function delete_law($id = NULL)
{
    $this->db->where('id', $id);
    $this->db->delete($this->db_table);
}

}