<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_law extends Admin_Controller  {

function __construct()
{
    parent::__construct();
    $this->load->model('admin_law_model', '', TRUE);
    $this->load->model('files_model', '', TRUE);
    $this->data['title'] = 'Правовая информация';
    $this->logs_data_type = 'Правовая информация';
    
}

function index()
{
    $this->data['laws'] = $this->admin_law_model->get_law_list();
    $this->data['title'] = 'Вся правовая информация';
    $this->data['users_list'] = $this->load->view('law_list_view',$this->data,TRUE);
    $this->load->view('admin/base_node_dashboard_view');
}

function add_law_page()
{
    $this->data['title'] = 'Добавить правовую информацию';
    $this->load->view('add_law_view',$this->data);
}

/**
* Добавление правовой информации
* @param  
* @return вывод результатов сохранения
*/
function add_law()
{
    $this->view_type = 2;
    $additional_data = array('title' => $this->input->post('title')
			);
    $result = $this->admin_law_model->add_law($additional_data);
    $this->data['result'] = $result['success'];
    if($this->data['result'])
    {
	$config['upload_path'] = './uploads/files';
	$config['allowed_types'] = 'doc|pdf|docx|rtf';
	$config['max_size']	= '100000';
	$config['max_width']  = '2000';
	$config['max_height']  = '1500';
	$this->load->library('upload', $config);
	if ( ! $this->upload->do_upload('image'))
	{
	    $this->data['result'] =  $this->upload->display_errors();
	}	
	else
	{
	    $data = array('upload_data' => $this->upload->data());
	    $this->data['result'] = $this->files_model->add_file($result['insert_id'],'law','/uploads/files/'.$data['upload_data']['file_name']);
	}
	//логирование
	$this->admin_logs_model->add($result['insert_id'], $this->logs_data_type,$this->input->post('title'),'/admin_law/edit_law_page/'.$result['insert_id'],'Добавление');
    }
    echo $this->load->view('admin/json_result_view',$this->data);
}

function edit_law_page($law_id = NULL)
{
    $this->data['law'] = $this->admin_law_model->get_law_by_id($law_id);
    $this->data['title'] = $this->data['law'][0]->title;
    $this->data['users_list'] = $this->load->view('edit_law_page_view',$this->data,TRUE);
    $this->load->view('admin/base_node_dashboard_view');
}

/**
* Редактирование правовой информации
* @param  $id идентификатор правовой информации
* @return вывод результатов сохранения
*/
function edit_law($id = NULL)
{
    $this->view_type = 2;
    $additional_data = array('title' => $this->input->post('title'));
    $result = $this->admin_law_model->edit_law($id,$additional_data);
    $this->data['result'] = $result;
   if($this->data['result'])
    {
	$config['upload_path'] = './uploads/files';
	$config['allowed_types'] = 'doc|pdf|docx|rtf';
	$config['max_size']	= '100000';
	$config['max_width']  = '2000';
	$config['max_height']  = '1500';
	$this->load->library('upload', $config);
	if ( ! $this->upload->do_upload('image'))
	{
	    $this->data['result'] =  $this->upload->display_errors();
	}	
	else
	{
	    $this->files_model->clear_all_parent_files($id,'law');
	    $data = array('upload_data' => $this->upload->data());
	    $this->data['result'] = $this->files_model->add_file($id,'law','/uploads/files/'.$data['upload_data']['file_name']);
	}
	//логирование
	$this->admin_logs_model->add($id, $this->logs_data_type,$this->input->post('title'),'/admin_law/edit_law_page/'.$id,'Редактирование');
    }
    echo $this->load->view('admin/json_result_view',$this->data);
}

/**
* Удаление правовой информации
* @param  $id идентификатор правовой информации
* @return вывод результатов сохранения
*/
function delete_law($id = NULL)
{
    $this->view_type = 2;
    $this->data['law'] = $this->admin_law_model->get_law_by_id($id);
    $this->admin_law_model->delete_law($id);
    //логирование
    $this->admin_logs_model->add($id, $this->logs_data_type,$this->data['law'][0]->title,'/admin_law/edit_law_page/'.$id,'Удаление');
    $this->files_model->clear_all_parent_files($id,'law');
}

}