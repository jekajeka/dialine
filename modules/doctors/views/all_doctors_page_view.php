<div class="row row_title_image">
    <div class="container">
        <h1><?= $title ?></h1>
    </div>
</div>
<div class="row row_breadcrumbs">
    <div class="container">
        <ul>
            <li><a href="/">Главная</a></li>
            |
            <li><a href="/doctors">Врачи</a></li>
        </ul>
    </div>
</div>
<div class="row row_speciality_rule">
    <div class="container">
        <div class="text_search_speciality_wrap">
            <input type="text" id="search_doctor" placeholder="Начните поиск, например, хирург">
        </div>
        <div class="text_search_speciality_long_wrap">
            <select id="age_page">
                <option value="all">Все возрасты</option>
                <option value="2" <?php ($isKids) ? print 'selected="selected"' : ''?>>Детям</option>
                <option value="1">Взрослым</option>
            </select>
            <select id="speciality_page">
                <option value="all">Все направления</option>
                <? foreach ($rubrics as $rubric) { ?>
                    <option value="<?= $rubric->id ?>"><?= $rubric->title ?></option>
                <? } ?>
            </select>
            <select id="clinic_page">
                <option value="all">Все клиники</option>
                <? foreach ($clinics as $clinic) { ?>
                    <option value="<?= $clinic->id ?>"><?= $clinic->title ?> <?= $clinic->district ?></option>
                <? } ?>
            </select>
            <a id="search_doctor_complicated" class="green_btn">Подобрать</a>
        </div>
    </div>
</div>
<div class="row specialists_row">
    <div class="container">
        <?= $this->load->view('all_doctors_list_view', $this->data) ?>
    </div>
</div>