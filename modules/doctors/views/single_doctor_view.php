<div class="row row_breadcrumbs">
    <div class="container">
        <ul>
            <?php
            if(strpos($_SERVER["REQUEST_URI"], '/kids/') !== false) { ?>
                <li><a href="/">Главная</a></li> | <li><a href="/kids">Детские поликлиники Диалайн</a></li> | <li><a href="/kids/<?=$doctor[0]->direction_path?>"><?=$doctor[0]->med_direct?></a></li> | <li><a href="/kids/<?=$doctor[0]->direction_path?>/<?=$doctor[0]->translite?>"><?=$doctor[0]->name?></a></li>
            <? } else { ?>
                <li><a href="/">Главная</a></li> | <li><a href="/doctors">Врачи</a></li> | <li><a href="/doctors/<?=$doctor[0]->direction_path?>"><?=$doctor[0]->med_direct?></a></li> | <li><a href="/doctors/<?=$doctor[0]->direction_path?>/<?=$doctor[0]->translite?>"><?=$doctor[0]->name?></a></li>
            <? } ?>
        </ul>
    </div>
</div>
<div class="row row_single_doctor">
    <div class="container">
        <div class="doctor_main_data_wrap">
            <div class="image_n_sign_wrap">
                <img src="<?=$doctor[0]->path?>" />
                <?if(!empty($doctor[0]->outer_link)){?>
                <a href="<?=$doctor[0]->outer_link?>" class="green_btn order_doctor box_shadow">Записаться на прием</a>
                <?}else{?>
                    <?php
                    if(strpos($_SERVER["REQUEST_URI"], '/kids/') !== false) { ?>
                        <a class="green_btn order_doctor order_doctor_overlay_win_kids box_shadow">Записаться на прием</a>
                    <? } else { ?>
                        <a class="green_btn order_doctor order_doctor_overlay_win box_shadow">Записаться на прием</a>
                    <? } ?>
                <?}?>
                <?php
                if(strpos($_SERVER["REQUEST_URI"], '/kids/') !== false) { ?>
                    <div class="child_icon_bird_top" style="bottom:30px"></div>
                <?php } ?>
            </div>
            <div class="doctor_text_data_wrap">
                <h1><?=$doctor[0]->name?></h1>
                <div class="doctor_text_data">
                    <dl>
                        <dt>Специализация, категория:</dt>
                        <dd><?=$doctor[0]->specialisation?></dd>
                        <dd><?=$doctor[0]->text?></dd>
                    </dl>
                </div>
            </div>
            <div class="structured_data_wrap">
                <h2>Клиники приема:</h2>
                <div class="structured_data_content_wrap">
                    <?foreach($clinics as $clinic) {?>
                    <p>
                        <a style="color: #000" href="/clinic/<?=$clinic->path?>"><?=$clinic->title?> <span><?=$clinic->district?></span></a>;
                    </p>
                    <?}?>
                </div>
                <?php
                if(strpos($_SERVER["REQUEST_URI"], '/kids/') !== false) { ?>
                    <div class="child_icon_bird_bottom" style="top:-10px;"></div>
                <?php } ?>
            </div>
            <div class="structured_data_wrap">
                <h2>Услуги оказываемые данным специалистом:</h2>
                <?foreach($rubrics as $rubric){?>
                <div class="structured_data_content_wrap">
                    <a class="structured_data_title">
                        <h4><?=$rubric->service_type?></h4><span class="structured_data_title_qty"><?=$rubric->services_qty?></span>
                    </a>
                    <div class="structured_data_table_wrap">
                        <?foreach($services as $service){
                        if($service->service_type==$rubric->service_type){?>
                        <div class="structured_data_table_line">
                            <div class="structured_data_table_line_border">
                                <h4><?=$service->title?></h4>
                                <div class="structured_data_table_line_price"><?=$service->unic_price?> р.</div>
                            </div>
                            <?php
                            if(strpos($_SERVER["REQUEST_URI"], '/kids/') !== false) { ?>
                                <a data-service="<?=$service->title?> за <?=$service->unic_price?> р." class="kids_doctor_service_link structured_data_table_link">Записаться</a>
                            <? } else { ?>
                                <a data-service="<?=$service->title?> за <?=$service->unic_price?> р." class="doctor_service_link structured_data_table_link">Записаться</a>
                            <? } ?>

                        </div>
                        <?}}?>
                    </div>
                </div>
                <?}?>
            </div>
            <div class="back_to_specials_wrap">
                <? if(strpos($_SERVER["REQUEST_URI"], '/kids/') !== false) { ?>
                    <a href="/kids/<?=$doctor[0]->direction_path?>" class="green_btn box_shadow back_to_specials_btn">Назад ко всем специалистам</a>
                <? } else { ?>
                    <a href="/doctors/<?=$doctor[0]->direction_path?>" class="green_btn box_shadow back_to_specials_btn">Назад ко всем специалистам</a>
                <? } ?>
            </div>
        </div>
        <div class="doctors_additional_panel">
            <h5>Другие врачи этой специальности</h5>
            <?foreach($other_doctors as $doc) {
                if(($doc->published==1)||($doc->published == NULL)){?>
            <a class="single_scrubs_wrap" href="/doctors/<?=$doc->direct_path?>/<?=$doc->translite?>">
                <div class="single_scrubs_img"><img src="<?=$doc->path?>" /></div>
                <h4><?=$doc->name?></h4>
                <div class="single_scrubs_text">
                    <?=$doc->specialisation?>
                </div>
                <div class="single_scrubs_border"></div>
            </a>
            <?}}?>
        </div>

        <?php
        if(strpos($_SERVER["REQUEST_URI"], '/kids/') !== false) { ?>
            <div class="child_icon_bee"></div>
            <div class="child_icon_racoon"></div>
        <?php } ?>
    </div>
</div>
<div class="overlay_form_wrapper" id="send_doctor_service_wrap">
    <div class="overlay_form">
        <a class="close_overlay_form"></a>
        <div class="overlay_form_inner">
            <div class="overlay_form_title">Записаться на прием</div>
            <input type="hidden" id="doctor_service_purpose" value="" />
            <input type="hidden" id="doctor_name" value="<?=$doctor[0]->name?>" />
            <input type="text" id="doctor_service_name" placeholder="Имя" />
            <input type="text" id="doctor_service_phone" placeholder="Телефон" />
            <div class="wrap_agree_checkbox">
                <input type="checkbox" id="doctor_service_agree"><label>Оставляя свои персональные данные, Вы даете добровольное согласие на их обработку.
                    <a target="_blank" href="/uploads/files/Politika_obrabotki_i_zashiti_personalnih_dannih1.pdf">Подробнее</a></label>
            </div>
            <a class="send_overlay_form" id="send_doctor_service">Отправить</a>
            <p>*С Вами свяжутся в ближайшее время и запишут на удобное время к выбранному специалисту</p>
        </div>
    </div>
</div>

<div class="overlay_form_wrapper" id="send_kids_doctor_service_wrap">
    <div class="overlay_form">
        <a class="close_overlay_form"></a>
        <div class="overlay_form_inner">
            <div class="overlay_form_title">Записаться на прием</div>
            <input type="hidden" id="doctor_service_purpose" value="" />
            <input type="hidden" id="doctor_name" value="<?=$doctor[0]->name?>" />
            <input type="text" id="doctor_service_name" placeholder="Имя" />
            <input type="text" id="doctor_service_phone" placeholder="Телефон" />
            <div class="wrap_agree_checkbox">
                <input type="checkbox" id="doctor_service_agree"><label>Оставляя свои персональные данные, Вы даете добровольное согласие на их обработку.
                    <a target="_blank" href="/uploads/files/Politika_obrabotki_i_zashiti_personalnih_dannih1.pdf">Подробнее</a></label>
            </div>
            <a class="send_overlay_form" id="send_kids_doctor_service">Отправить</a>
            <p>*С Вами свяжутся в ближайшее время и запишут на удобное время к выбранному специалисту</p>
        </div>
    </div>
</div>

<?php
if(strpos($_SERVER["REQUEST_URI"], '/kids/') !== false) { ?>
    <style>
        .bukashka{
            position:relative;
        }
        .bukashka .child_icon_beetle{
            display:block;
        }
    </style>
<?php } ?>
