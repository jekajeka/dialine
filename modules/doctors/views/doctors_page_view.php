
    <div class="row row_title_image">
        <div class="container">
            <h1><?=$specialisation[0]->title?></h1>
        </div>
    </div>
    <div class="row row_breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="/">Главная</a></li> | <li><a href="/doctors">Врачи</a></li> | <li><a href="/doctors/<?=$specialisation[0]->path?>"><?=$specialisation[0]->title?></a></li>
            </ul>
        </div>
    </div>
    <div class="row row_speciality_rule">
        <div class="container">
            <div class="text_search_speciality_wrap">
                <input type="text" id="search_doctor_speciality" speciality="<?=$specialisation[0]->id?>" placeholder="Начните поиск, например, хирург">
            </div>
            <div class="text_search_speciality_long_wrap">
                <select id="age_page">
                    <option value="all">Все возрасты</option>
                    <option value="2">Детям</option>
                    <option value="1">Взрослым</option>
                </select>
                <input type="hidden" id="speciality_page" value="<?=$specialisation[0]->id?>">
                <select id="clinic_page">
                    <option value="all">Все клиники</option>
                    <?foreach($clinics as $clinic){?>
                    <option value="<?=$clinic->id?>"><?=$clinic->title?> <?=$clinic->district?></option>
                    <?}?>
                </select>
                <a id="search_doctor_complicated" class="green_btn">Подобрать</a>
            </div>
        </div>
    </div>
    <div class="row specialists_row">
        <div class="container">
            <div class="toggle_wrapper">
                <a class="toggle_title active"><h2><?=$specialisation[0]->title?></h2></a>
                <div class="toggle_data paginate_scrubs " style="display: block">
                    <ul class="paginate_scrubs_ul">
                        <?foreach($doctors as $doc){
                            if(($doc->published==1)||($doc->published == NULL)){?>
                        <li>
                            <a class="single_scrubs_wrap" href="/doctors/<?=$specialisation[0]->path?>/<?=$doc->translite?>">
                                <div class="single_scrubs_img"><img src="<?=$doc->path?>" /></div>
                                <h4><?=$doc->name?></h4>
                                <div class="single_scrubs_text">
                                    <?=$doc->specialisation?>
                                </div>
                                <div class="single_scrubs_border"></div>
                            </a>
                        </li>
                        <?}}?>
                    </ul>
                    <div class="paginate_scrubs_navigation"></div>
                </div>
            </div>
        </div>
    </div>