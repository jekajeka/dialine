<script src="/frontend/js/jquery.pajinate.min.js"></script>
<?
$rub_line_active_title="active";
$rub_line_active_body="style=\"display: block\"";

if(($_SERVER['REQUEST_URI']=='/doctors')||($_SERVER['REQUEST_URI']=='/doctors/'))
{
    $rub_line_active_title="";
    $rub_line_active_body="";
}
foreach($rubrics as $rubric){
    $has_doctors = 0;
    foreach($doctors as $doctor){
        if(($doctor->medical_direction == $rubric->id)&&(($doctor->published==1)||($doctor->published == NULL)))
            $has_doctors  = 1;
    }
        if($has_doctors){?>
<div class="toggle_wrapper">
    <a class="toggle_title <?=$rub_line_active_title?>"><h2><?=$rubric->title?></h2></a>
    <div class="toggle_data paginate_scrubs" id="medicals_<?=$rubric->id?>" <?=$rub_line_active_body?>>
        <ul class="paginate_scrubs_ul">
        <?foreach($doctors as $doctor){
            $directions = explode(',',$doctor->medical_direction);
            if(in_array($rubric->id,$directions)){
		if(($doctor->published==1)||($doctor->published == NULL)){?>
            <li style="list-style: none;display: block">
                <a class="single_scrubs_wrap" href="/doctors/<?=$doctor->direct_path?>/<?=$doctor->translite?>">
                    <div class="single_scrubs_img"><img src="<?=$doctor->path?>" /></div>
                    <h4><?=$doctor->name?></h4>
                    <div class="single_scrubs_text">
                        <?=$doctor->specialisation?>
                    </div>
                    <div class="single_scrubs_border"></div>
                </a>
            </li>
            <?}}}?>
        </ul>
        <div class="paginate_scrubs_navigation"></div>
    </div>
<script>
$('.toggle_data.paginate_scrubs#medicals_'+<?=$rubric->id?>).pajinate({
	items_per_page : 8,
	item_container_id : '.paginate_scrubs_ul',
	nav_panel_id : '.paginate_scrubs_navigation'
    });
</script>
</div>
<?}}?>