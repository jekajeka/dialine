<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctors extends Frontend_Controller  {
function __construct()
{
    parent::__construct();
    $this->load->model('frontend_doctors_model', '', TRUE);
    $this->load->model('clinic/frontend_clinics_model', '', TRUE);
    $this->title = 'Врачи/Диалайн';
}

function index()
{
    $this->data['title'] = 'Врачи';
    $this->data['doctors'] = $this->frontend_doctors_model->get_doctors();
    $this->data['rubrics'] = $this->frontend_doctors_model->get_rubrics();
    $this->data['clinics'] = $this->frontend_clinics_model->get_clinics_list();
    $this->data['isKids'] = false;
    $this->getSeo('doctors', 0);
    $this->load->view('all_doctors_page_view',$this->data);
}

function doctor_text_search()
{
    $this->view_type = 2;
    $this->data['rubrics'] = $this->frontend_doctors_model->get_rubrics();
    $this->data['doctors'] = $this->frontend_doctors_model->search_doctors_by_text($this->input->post('search_text'));
    $this->load->view('all_doctors_list_view',$this->data);
}
function doctor_text_search_4_main()
{
    $this->view_type = 2;
    $this->data['doctors'] = $this->frontend_doctors_model->search_doctors_by_text($this->input->post('search_text'),' doctors.id ');
    $this->load->view('simple_doctors_list_view',$this->data);
}

function doctor_text_search_speciality($speciality_id = NULL)
{
    $this->view_type = 2;
    $this->data['rubrics'] = $this->frontend_doctors_model->get_rubrics();
    $this->data['doctors'] = $this->frontend_doctors_model->search_doctors_by_text_n_speciality($this->input->post('search_text'),$speciality_id);
    $this->load->view('all_doctors_list_view',$this->data);
}

function search_doctor_complicated()
{
    $this->view_type = 2;
    $this->data['rubrics'] = $this->frontend_doctors_model->get_rubrics();
    $this->data['doctors'] = $this->frontend_doctors_model->search_doctor_complicated($this->input->post('age'),$this->input->post('speciality'),$this->input->post('clinic'));
    $this->load->view('all_doctors_list_view',$this->data);
}

//отбор клиник в селектбокс по изменению состояния возраста и/или специализации врачей
function filter_clinics()
{
    $this->view_type = 2;
    $this->data['clinics'] = $this->frontend_doctors_model->get_selected_clinics($this->input->post('age'),$this->input->post('speciality'));
    $this->load->view('selected_clinics_view',$this->data);
}

function search_doctor_complicated_on_top()
{
    $this->view_type = 2;
    if(($this->input->post('age')=='all')&&($this->input->post('speciality')=='all')&&($this->input->post('clinic')=='all'))
    {
        $this->data['rubrics'] = $this->main_page_model->get_rubrics();
        $this->load->view('directions_list_overflow_view',$this->data);
    }
    else
    {
        $this->data['doctors'] = $this->frontend_doctors_model->search_doctor_complicated($this->input->post('age'),$this->input->post('speciality'),$this->input->post('clinic'),1);
        $this->load->view('simple_doctors_list_view',$this->data);
    }
}

function specialisation($specialisation = NULL)
{
    if($specialisation)
    {
        $this->data['specialisation'] = $this->frontend_doctors_model->get_specialisation($specialisation);
        //если в базе нет такой специализации - увести на страницу 404
        if(!empty($this->data['specialisation']))
        {
            $this->data['title'] = $this->data['specialisation'][0]->title.'/Диалайн, Волгоград';
            $this->data['doctors'] = $this->frontend_doctors_model->get_doctors_by_specialisation_text($specialisation);
            $this->data['clinics'] = $this->frontend_clinics_model->get_clinics_list();
            $this->getSeo('doctors_specializations', $this->data['specialisation'][0]->id);
            $this->load->view('doctors_page_view',$this->data);
        }
        else
            show_404('page');
    }
    
}

function single_doctors_page($specialisation = '',$translite_name = '')
{
    $this->data['doctor'] = $this->frontend_doctors_model->get_doctor($translite_name);
    //если в базе нет такого врача, или он снят с публикации - увести на страницу 404
    if(!empty($this->data['doctor']))
    {
        if($this->data['doctor'][0]->published)
            $published = 1;
        if($this->data['doctor'][0]->published == '0')
            $published = 0;
        if($this->data['doctor'][0]->published == NULL)
            $published = 1;
        //echo $published;
        if($published)
        {
            $this->data['title'] = $this->data['doctor'][0]->name;
            $this->data['services'] = $this->frontend_doctors_model->get_doctor_services($this->data['doctor'][0]->id);
            $this->data['clinics'] = $this->frontend_doctors_model->get_doctor_clinics($this->data['doctor'][0]->id);
            $this->data['rubrics'] = $this->frontend_doctors_model->get_doctor_rubrics($this->data['doctor'][0]->id);
            $doctor_direction_list = $this->frontend_doctors_model->get_doctor_directions($this->data['doctor'][0]->id);
            $kids = 0;
            if(strpos($_SERVER["REQUEST_URI"], '/kids/') !== false) $kids = 1;
            $this->data['other_doctors'] = $this->frontend_doctors_model->get_other_doctors($doctor_direction_list[0]->direction,$this->data['doctor'][0]->id,$kids);
            $this->data['breadcrumbs_segment'] = '<li><a href="/info/diskontnye_karty/">Дисконтная программа</a></li>';
            $this->getSeo('doctors', $this->data['doctor'][0]->id);
            $this->load->view('single_doctor_view',$this->data);
        }
        else
            show_404('page');
    }
    else
        show_404('page');
}


}
