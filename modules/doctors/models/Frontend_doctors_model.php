<?
class Frontend_doctors_model extends CI_Model {
private $db_table = 'doctors';

function Frontend_doctors_model()
{
    parent::__construct();
}

function get_doctor($translite_name = '')
{
    
    $data = $this->db->get_where($this->db_table,array('translite'=>$translite_name));
    $doc = $data->result();
    $query = "SELECT name,outer_link, specialisation, ".$this->db_table.".text,images.path,medical_direction,".$this->db_table.".id,medical_directions.title as med_direct,medical_directions.path as direction_path,translite, published.published
                FROM ".$this->db_table."
                LEFT JOIN images ON
                images.parent_id = ".$this->db_table.".id AND images.parent_type = 'doctors'
                INNER JOIN doctor_direction ON
                doctor_direction.doctor_id=doctors.id
                INNER JOIN medical_directions ON
                medical_directions.id = doctor_direction.direction_id
                LEFT JOIN published ON
                published.type = 'doctors' AND published.record_id = ".$this->db_table.".id 
                WHERE ".$this->db_table.".translite = '$translite_name'";
    $data = $this->db->query($query);
    return $data->result();   
}

function get_doctor_directions($id = NULL)
{
    $query = "SELECT group_concat(doctor_direction.direction_id) as direction FROM doctor_direction
            WHERE
            doctor_direction.doctor_id = $id
            ";
    $data = $this->db->query($query);
    return $data->result();   
}

function get_other_doctors($direction = NULL,$doctor_id = NULL,$iskids = NULL)
{
    $kids = '';
    if($iskids){
        $kids = 'and '.$this->db_table.'.age in (0,2)';
    }
    $query = "SELECT name, specialisation, ".$this->db_table.".text,images.path,medical_directions.path as direct_path,translite,published.published
                FROM ".$this->db_table."
                LEFT JOIN images ON
                images.parent_id = ".$this->db_table.".id
                AND images.parent_type = 'doctors'
                INNER JOIN doctor_direction ON
                doctor_direction.doctor_id=doctors.id
                INNER JOIN medical_directions ON
                medical_directions.id = doctor_direction.direction_id
                LEFT JOIN published ON
                published.type = 'doctors' AND published.record_id = ".$this->db_table.".id 
                WHERE
                doctor_direction.direction_id IN ($direction)
                AND ".$this->db_table.".id<>$doctor_id $kids
                GROUP BY doctors.id
                ORDER BY name ASC";
    $data = $this->db->query($query);
    return $data->result();   
}

function get_doctors($age='')
{
    $age_string = '';
    if($age=='kids')
        $age_string = " where ( ".$this->db_table.".age=2 OR ".$this->db_table.".age=0 ) ";

    $query = "SELECT name, specialisation, ".$this->db_table.".text,images.path,medical_directions.path as direct_path,translite,doctor_direction.direction_id as medical_direction,published.published
            FROM ".$this->db_table."
            LEFT JOIN images ON
            images.parent_id = ".$this->db_table.".id
            AND images.parent_type = 'doctors'
            INNER JOIN doctor_direction ON
            doctor_direction.doctor_id=doctors.id
            INNER JOIN medical_directions ON
            medical_directions.id = doctor_direction.direction_id
            LEFT JOIN published ON
            published.type = 'doctors' AND published.record_id = ".$this->db_table.".id 
            " . $age_string . "
            ORDER BY name ASC";
    $data = $this->db->query($query);
    return $data->result();   
}

function search_doctors_by_text($search_text = '',$group_by_parameter = NULL)
{
    $group_by_parameter_text = '';
    if($group_by_parameter)
        $group_by_parameter_text = ' GROUP BY '.$group_by_parameter;
    if($search_text =='')
    {
        return $this->get_doctors();
    }
    else
    {
        $sql_search_string = str_replace(' ','%',$search_text);
        $query = "SELECT ".$this->db_table.".id, name, specialisation, ".$this->db_table.".text,images.path,medical_directions.path as direct_path,translite,doctor_direction.direction_id as medical_direction,doctor_direction.direction_id as medical_direction2,published.published
                FROM ".$this->db_table."
                LEFT JOIN images ON
                images.parent_id = ".$this->db_table.".id
                AND images.parent_type = 'doctors'
                INNER JOIN doctor_direction ON
                doctor_direction.doctor_id=doctors.id
                INNER JOIN medical_directions ON
                medical_directions.id = doctor_direction.direction_id
                LEFT JOIN published ON
                published.type = 'doctors' AND published.record_id = ".$this->db_table.".id 
                WHERE
                ".$this->db_table.".name LIKE '%$sql_search_string%'
                OR ".$this->db_table.".specialisation LIKE '%$sql_search_string%'".$group_by_parameter_text.' ORDER BY name ASC';
        $data = $this->db->query($query);
        return $data->result();   
    }
}

function search_doctors_by_text_n_speciality($search_text = '',$speciality_id = NULL)
{
    $dop = '';
    if($search_text !='')
    {
        $sql_search_string = str_replace(' ','%',$search_text);
        $dop = "WHERE $this->db_table.name LIKE '%$sql_search_string%'
                OR ".$this->db_table.".specialisation LIKE '%$sql_search_string%'
                ";
    }
        $query = "SELECT DISTINCT ".$this->db_table.".id, name, specialisation, ".$this->db_table.".text,images.path,medical_directions.path as direct_path,translite,doctor_direction.direction_id as medical_direction,published.published
                FROM ".$this->db_table."
                LEFT JOIN images ON
                images.parent_id = ".$this->db_table.".id
                AND images.parent_type = 'doctors'
                INNER JOIN doctor_direction ON
                doctor_direction.doctor_id=doctors.id
                INNER JOIN medical_directions ON
                medical_directions.id = doctor_direction.direction_id
                AND medical_directions.id=$speciality_id
                LEFT JOIN published ON
                published.type = 'doctors' AND published.record_id = ".$this->db_table.".id 
                $dop
                GROUP BY ".$this->db_table.".id
                ORDER BY name ASC";
        $data = $this->db->query($query);
        return $data->result();   
}


function search_doctor_complicated($age = 'all',$speciality = 'all',$clinic='all',$distinct = NULL)
{
    $speciality_string = '';
    $age_string = '';
    $clinic_string = '';
    $where = '';
    $group = '';
    if($distinct)
        $group = ' GROUP BY  doctors.id';
    if($speciality!='all')
        $speciality_string = " AND doctor_direction.direction_id=$speciality";
    if($age!='all')
        $age_string = "( age=$age OR age=0 )";
    if($clinic!='all')
        $clinic_string = "INNER JOIN clinic_doctor
        ON clinic_id=$clinic
        AND clinic_doctor.doctor_id=".$this->db_table.".id";
    if($age!='all')
        $where = ' WHERE ';
    $query = "SELECT DISTINCT ".$this->db_table.".id, name, specialisation, ".$this->db_table.".text,images.path,medical_directions.path as direct_path,translite,doctor_direction.direction_id as medical_direction,published.published
                FROM ".$this->db_table."
                LEFT JOIN images ON
                images.parent_id = ".$this->db_table.".id
                AND images.parent_type = 'doctors'
                INNER JOIN doctor_direction ON
                doctor_direction.doctor_id=doctors.id
                INNER JOIN medical_directions ON
                medical_directions.id = doctor_direction.direction_id $speciality_string
                LEFT JOIN published ON
                published.type = 'doctors' AND published.record_id = ".$this->db_table.".id 
                $clinic_string
                $where $age_string
                $group
                ORDER BY name ASC";
    $data = $this->db->query($query);
    return $data->result();   
}

//выборка клиник с наличием врачей указанной специализации и возраста пациентов
function get_selected_clinics($age= "all",$speciality = 'all')
{
    $this->db->distinct('clinics.id');
    $this->db->select(array('clinics.title', 'clinics.district','published.published','clinics.id'));
    $this->db->from('clinics');
    $this->db->join('clinic_doctor', "clinics.id = clinic_doctor.clinic_id",'INNER');
    $this->db->join('doctors', "doctors.id = clinic_doctor.doctor_id",'INNER');
    $this->db->join('doctor_direction', "doctor_direction.doctor_id = doctors.id",'INNER');
    $this->db->join('published', "published.type='clinics' AND published.record_id=clinics.id",'LEFT');
    $this->db->where('(published.published=1 OR published.published IS NULL)');
    if($speciality!='all')    
        $this->db->where("doctor_direction.direction_id = $speciality");
    if($age!='all')
        $this->db->where(" ( age=$age OR age=0 )");
    $this->db->order_by("clinics.title", "ASC"); 
    return $this->db->get()->result();
}

function get_doctors_by_specialisation_text($direction = NULL)
{
    $query = "SELECT name, specialisation, ".$this->db_table.".text,images.path,medical_directions.path as direct_path,translite,published.published
                FROM ".$this->db_table."
                LEFT JOIN images ON
                images.parent_id = ".$this->db_table.".id
                AND images.parent_type = 'doctors'
                INNER JOIN doctor_direction ON
                doctor_direction.doctor_id=doctors.id
                INNER JOIN medical_directions ON
                medical_directions.id = doctor_direction.direction_id
                LEFT JOIN published ON
                published.type = 'doctors' AND published.record_id = ".$this->db_table.".id 
                WHERE
                medical_directions.path = '$direction'
                ORDER BY name ASC";
    $data = $this->db->query($query);
    return $data->result();   
}

function get_specialisation($link = NULL)
{
    $this->db->select('medical_directions.*');
    $this->db->from('medical_directions');
    $this->db->join('published', "published.type = 'medical_directions' AND published.record_id = medical_directions.id ",'left');
    $this->db->where('(published.published=1 OR published.published IS NULL)');
    $this->db->where(array('path' => $link));
    $this->db->order_by("title", "asc");
    return $this->db->get()->result();
}

function get_rubrics()
{
    $this->db->select('medical_directions.*');
    $this->db->from('medical_directions');
    $this->db->join('published', "published.type = 'medical_directions' AND published.record_id = medical_directions.id ",'left');
    $this->db->where('(published.published=1 OR published.published IS NULL)');
    $this->db->where('medical_directions.id<>'.ANALYZE_ID);
    $this->db->order_by("title", "asc");
    return $this->db->get()->result();
}

function get_doctor_rubrics($doc_id)
{
    $query = "SELECT count( * ) as services_qty , service_type
                FROM services,service_doctor
                WHERE
                service_doctor.service_id=med_id
                AND service_doctor.doctor_id=$doc_id
                GROUP BY service_type
                ORDER BY `services`.`id` ASC";
    $data = $this->db->query($query);
    return $data->result();
}

function get_doctor_services($doc_id = NULL)
{
    $query = "SELECT services.id,med_id,title, services.price, rubric,service_type,service_doctor.price as unic_price,service_doctor.id as sd_id
                FROM services,service_doctor
                WHERE
                service_doctor.service_id=med_id
                AND service_doctor.doctor_id=$doc_id
                ORDER BY services.id ASC";
    $data = $this->db->query($query);
    return $data->result();
}

//Выборка клиник где оказывает врач услуги на странице врача
function get_doctor_clinics($doc_id = NULL)
{
    $this->db->select(array('clinics.id', 'title', 'district', 'path', 'published.published'));
    $this->db->from('clinics');
    $this->db->join('clinic_doctor','clinic_doctor.clinic_id=clinics.id AND clinic_doctor.doctor_id='.$doc_id,'INNER');
    $this->db->join('published', "published.type='clinics' AND published.record_id=clinics.id",'LEFT');
    $this->db->where('(published.published=1 OR published.published IS NULL)');
    $this->db->order_by("clinics.title", "ASC");
    return $this->db->get()->result();
}

//Вероятно стоит уничтожить потом
function get_clinics()
{
    $data = $this->db->get('clinics');
    return $data->result();
}

}