<?
//Модель публикации/снятия с публикации материалов
class Admin_published_model extends MY_Model
{
    function Admin_published_model()
    {
        parent::__construct();
        $this->db_table = 'published';
    }
    
    //изменение или создание отметки о статусе публикации записи
    function update_published($record_type,$id,$published = 0)
    {
        $search_params = array('type' => $record_type,'record_id' => $id);
        $this->db->select('*')
                ->from($this->db_table)
                ->where($search_params);
        $query = $this->db->get()->result();
        //Если запись уже существует - обновить ,если нет - добавить новую
        if(count($query)>0)
        {
            $this->db->set('published', $published, FALSE)
                    ->where($search_params)
                    ->update($this->db_table);
        }
        else
        {
            $this->db->insert($this->db_table,array('type' => $record_type,'record_id' => $id,'published' => $published));
        }
    }
    
    //получение значения отметки о статусе публикации записи
    function get_status($record_type,$id)
    {
        $search_params = array('type' => $record_type,'record_id' => $id);
        $this->db->select('*')
                ->from($this->db_table)
                ->where($search_params);
        return $this->db->get()->result();
    }
    
    //удаление записи о публикации
    function delete_published($record_type, $id)
    {
        $this->db->where(array('type' => $record_type,'record_id' => $id));
	$this->db->delete($this->db_table);
    }
}