<div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Добавить медицинскую программу</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form id="add_medical_program" data-parsley-validate class="form-horizontal form-label-left">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Название <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="title" class="form-control col-md-7 col-xs-12"   name="title" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">Изображение в баннер</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input name="image" type="file" id="image">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="banner_text">Текст для баннера <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="banner_text" class="form-control col-md-7 col-xs-12"   name="banner_text" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="path">Путь <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="path" class="form-control col-md-7 col-xs-12"   name="path" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="target">Цель программы</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea name="target" class="form-control" placeholder="" rows="3" style="width: 100%; height: 135px;resize: none" id="target"></textarea>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Адреса</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea name="address" class="form-control" placeholder="" rows="3" style="width: 100%; height: 135px;resize: none" id="address"></textarea>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="specialists">Специалисты</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea name="specialists" class="form-control" placeholder="" rows="3" style="width: 100%; height: 135px;resize: none" id="specialists"></textarea>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="diagnostics">Диагностика</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea name="diagnostics" class="form-control" placeholder="" rows="3" style="width: 100%; height: 135px;resize: none" id="diagnostics"></textarea>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="research">Лабораторные исследования</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea name="research" class="form-control" placeholder="" rows="3" style="width: 100%; height: 135px;resize: none" id="research"></textarea>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="bonus">Бонусы</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea name="bonus" class="form-control" placeholder="" rows="3" style="width: 100%; height: 135px;resize: none" id="bonus"></textarea>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cost">Цена <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="cost" class="form-control col-md-7 col-xs-12"   name="cost" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="isForKids">Для детей</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="checkbox" class="flat" name="isForKids">
                        </div>
                      </div>
                      <?=$this->load->view('admin/publish_view');?>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="add_user_final">
                          <button type="submit" class="btn btn-success">Добавить</button>
                        </div>
                      </div>
                    </form>  
                  </div>
                </div>
              </div>
            </div>
</div>
<script src="/admin_js/admin_info/admin_add_medical_program.js"></script>