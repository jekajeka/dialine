<script src="/vendors/select2/dist/js/select2.full.min.js"></script>
<link href="/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
<div class="">

    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?= $title ?></h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br/>
                    <input type="hidden" id="id" name="id" value="<?= $children_page_title[0]->id ?>">
                    <form id="edit_children" data-parsley-validate class="form-horizontal form-label-left">
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Заголовок <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="title" class="form-control col-md-7 col-xs-12"
                                       value="<?= $children_page_title[0]->title ?>" name="title" placeholder=""
                                       required="required" type="text">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="path">Сегмент адреса
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="path" class="form-control col-md-7 col-xs-12"
                                       value="<?= $children_page_title[0]->path ?>" name="path" placeholder=""
                                       type="text">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="text">Текст<span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="form-control" name="text" placeholder="" rows="3"
                                          style="width: 100%; height: 135px;resize: none"
                                          id="text"><?= $children_page_title[0]->text ?></textarea>
                            </div>
                        </div>
                        <? if (isset($seo)): ?>
                            <? $this->load->view('admin/seo', $seo); ?>
                        <? endif; ?>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">Изображения для галлереи
                            </label>
                        </div>

                        <?php foreach ($children_page_img as $item) { ?>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <? if (!empty($item->path)) { ?>
                                    <img width="100%" src="<?= $item->path ?>"/>
                                    <span class="children_img_delete"><a class="text-danger" href="/admin_children/children_page_delete_img/<?=$item->id?>">Удалить</a></span>
                                    <div class="children_other_img"><span>Выбрать другое изображение</span>
                                        <input name="<?= $item->parent_type ?>" type="file" id="<?= $item->parent_type ?>">
                                    </div>
                                <? } else { ?>
<!--                            </div>-->
<!--                            <div class="col-md-6 col-sm-6 col-xs-12">-->
                                <div class="children_other_img">
                                    <span>Выбрать изображение</span>
                                <input name="<?= $item->parent_type ?>" type="file" id="<?= $item->parent_type ?>">
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="add_user_final">
                                <button type="submit" class="btn btn-success">Сохранить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/admin_js/admin_info/admin_children_page_title.js"></script>
