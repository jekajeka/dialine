<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?= $title ?></h2>
                    <!--                    <ul class="nav navbar-right panel_toolbox">-->
                    <!--                      <li><a title="Удалить подготовку к исследованиям" href="/admin_doctors/delete_medical_direction/-->
                    <? //=$medical_direction[0]->id?><!--" class="add-link"><i class="fa fa-trash"></i></a>-->
                    <!--                      </li>-->
                    <!--                    </ul>-->
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br/>
                    <input type="hidden" id="id" value="<?= $medical_direction[0]->id ?>">
                    <form id="edit_doctor_kids_specialization" data-parsley-validate
                          class="form-horizontal form-label-left">
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Направление <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input readonly id="title" class="form-control col-md-7 col-xs-12"
                                       value="<?= $medical_direction[0]->title ?>" name="title" placeholder=""
                                       required="required" type="text">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="path">Сегмент адреса <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input readonly id="path" class="form-control col-md-7 col-xs-12"
                                       value="<?= $medical_direction[0]->path ?>" name="path" placeholder=""
                                       required="required" type="text">
                            </div>
                        </div>

                        <? if (isset($seo)): ?>
                            <? $this->load->view('admin/seo', $seo); ?>
                        <? endif; ?>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="add_user_final">
                                <button type="submit" class="btn btn-success">Сохранить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="x_panel">
                <div class="x_title">
                    <h2>Услуги по специализации
                        <small></small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                            <tr class="headings">
<!--                                <th>-->
<!--                                    <input type="checkbox" id="check-all" class="flat">-->
<!--                                </th>-->
                                <th class="column-title">Для детей</th>
                                <th class="column-title">ID</th>
                                <th class="column-title">ID услуги</th>
                                <th class="column-title">Услуга</th>
                                <th class="column-title">Тип услуги</th>
                                <th class="column-title">Цена</th>
                                <th class="bulk-actions" colspan="4">
                                    <a class="antoo" style="color:#fff; font-weight:500;">Всего выбрано ( <span
                                                class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                </th>
                            </tr>
                            </thead>

                            <tbody id="service_table">
                            <? foreach ($services as $service) { ?>
                                <tr class="even pointer">
                                    <td class="a-center ">
                                        <? if ($service->sd_id)
                                            $checked = 'checked';
                                        else $checked = ''; ?>
                                        <input type="checkbox" <?=$checked;?> med_id="<?= $service->med_id ?>" curr_id="<?= $service->id ?>" class="flat" name="table_records">
                                    </td>
                                    <td><?= $service->id ?></td>
                                    <td><?= $service->med_id ?></td>
                                    <td><?= $service->title ?></td>
                                    <td><?= $service->service_type ?></td>
                                    <td><input type="text" id="price_<?= $service->id ?>" value="<?= $service->price; ?>"/></td>
                                </tr>
                            <? } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12" id="save_kids_services">
                            <button type="submit" class="btn btn-success">Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
<script src="/admin_js/admin_info/edit_doctor_kids_specialization.js"></script>