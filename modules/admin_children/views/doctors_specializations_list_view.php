<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2><?= $this->data['title'] ?></small></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a href="/admin_children/add_medical_direction_page" class="add-link"><i class="fa fa-plus"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
                <thead>
                <tr>
                    <th><input type="checkbox" id="check-all" class="flat"></th>
                    <th>id</th>
                    <th>Название</th>
                    <th>Ссылка</th>
                </tr>
                </thead>


                <tbody>
                <? foreach ($doctors_specializations as $med_direction) { ?>
                    <tr>
                        <td><input type="checkbox" class="flat" name="table_records"></td>
                        <td><?= $med_direction->id ?></td>
                        <td>
                            <a href="/admin_children/edit_doctor_kids_specialization_page/<?= $med_direction->id ?>"><?= $med_direction->title ?></a>
                        </td>
                        <td><a href="/admin_children/edit_doctor_kids_specialization_page/<?= $med_direction->id ?>">kids/<?= $med_direction->path ?></a>
                        </td>
                    </tr>
                <? } ?>
                </tbody>
            </table>
        </div>
    </div>


</div>
              