<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_children extends Admin_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('children_model', '', TRUE);
		$this->load->model('admin_published/admin_published_model', '', TRUE);
		$this->data['title'] = 'Редактирование страницы для детей';
	}

//	function discount()
//	{
//		$this->data['discount'] = $this->info_model->get_discount();
//		$this->data['title'] = 'Редактировать страницу скидочной информации';
//		$this->getSeo('discount_page', 0);
//		$this->load->view('edit_discount_view',$this->data);
//	}
//
//	function edit_discount()
//	{
//		$this->view_type = 2;
//		$additional_data = array('title' => $this->input->post('title'),
//					'subtitle' => $this->input->post('subtitle'),
//					'card_text' => $this->input->post('card_text'),
//					'second_title' => $this->input->post('second_title'),
//					'text' => $this->input->post('text'));
//		$result = $this->info_model->edit_discount(1,$additional_data);
//		$this->saveSeo('discount_page', 0);
//		$this->data['result'] = $result;
//		//логирование
//		$this->admin_logs_model->add(1, 'discount_page',$this->input->post('title'),'/admin_info/discount','Редактирование');
//		echo $this->load->view('admin/json_result_view',$this->data);
//	}
//
	function children_page_title()
	{
		$this->data['children_page_title'] = $this->children_model->get_page(17);
		$this->data['children_page_img'] = $this->children_model->get_single_medical_program_imgs(17);
		$this->data['title'] = 'Редактировать описание страницы "'.$this->data['children_page_title'][0]->title.'"';
		//$this->getSeo('medical_program', 0);
		$this->getSeo('complex_page', 17);
		$this->load->view('edit_children_page_title_view',$this->data);
	}

	function simple_page($id = NULL)
	{
		$this->data['komplex_programs_title'] = $this->children_model->get_page($id);
		$this->data['title'] = 'Редактировать описание страницы "'.$this->data['komplex_programs_title'][0]->title.'"';
		$this->getSeo('complex_page', $id);
		$this->load->view('edit_komplex_programs_title_view',$this->data);
	}

	function children_page_delete_img($id){
        $this->children_model->children_img_delete($id);
        redirect('/admin_children/children_page_title');

    }
//	function add_simple_page_page()
//	{
//		$this->data['title'] = 'Добавить простую страницу';
//		$this->load->view('add_simple_page_view',$this->data);
//	}
//
//	/**
//	* Добавление простой страницы
//	* @param
//	* @return вывод результатов сохранения
//	*/
//	function add_simple_page()
//	{
//		$this->view_type = 2;
//		$additional_data = array('title' => $this->input->post('title'),
//					'text' => $this->input->post('text'),
//					'path' => $this->input->post('path'));
//		$result = $this->info_model->add_simple_page($additional_data);
//		$this->data['result'] = $result['success'];
//		//логирование
//		$this->admin_logs_model->add($result['insert_id'], 'Текстовые страницы',$this->input->post('title'),'/admin_info/simple_page/'.$result['insert_id'],'Добавление');
//		echo $this->load->view('admin/json_result_view',$this->data);
//	}
//
//	function simple_page_list()
//	{
//		$this->data['title'] = 'Текстовые страницы и заголовки спецстраниц';
//		$this->data['simple_pages'] = $this->info_model->get_simple_pages();
//		$this->data['users_list'] = $this->load->view('simple_pages_list_view',$this->data,TRUE);
//		$this->load->view('admin/base_node_dashboard_view');
//		//$this->load->view('edit_komplex_programs_title_view',$this->data);
//	}
//
	function edit_children_title($id = 17)
	{
		$this->view_type = 2;
		$additional_data = array('title' => $this->input->post('title'),
					'path' => $this->input->post('path'),
					'text' => $this->input->post('text'));
		$result = $this->children_model->edit_children_title($id,$additional_data);
		$this->saveSeo('complex_page', $id);
		$this->data['result'] = $result;

//		echo '<pre>';
//		var_dump($_FILES);
//		echo '</pre>';
//		die;

		if($this->data['result']){

            $config['upload_path'] = './uploads/children_page/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']	= '10000';
			$config['max_width']  = '2000';
			$config['max_height']  = '1500';
			$this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('children_pic1'))
			{
// 				$this->data['result'] =  $this->upload->display_errors();
			}
			else
			{
				$this->images_model->clear_all_parent_images($id,'children_pic1');
				$data = array('upload_data' => $this->upload->data());
				$this->data['result'] = $this->images_model->add_image($id,'children_pic1','/uploads/children_page/'.$data['upload_data']['file_name']);
			}

            if ( ! $this->upload->do_upload('children_pic2'))
			{
// 				$this->data['result'] =  $this->upload->display_errors();
			}
			else
			{
				$this->images_model->clear_all_parent_images($id,'children_pic2');
				$data = array('upload_data' => $this->upload->data());
				$this->data['result'] = $this->images_model->add_image($id,'children_pic2','/uploads/children_page/'.$data['upload_data']['file_name']);
			}

			if ( ! $this->upload->do_upload('children_pic3'))
			{
// 				$this->data['result'] =  $this->upload->display_errors();
			}
			else
			{
				$this->images_model->clear_all_parent_images($id,'children_pic3');
				$data = array('upload_data' => $this->upload->data());
				$this->data['result'] = $this->images_model->add_image($id,'children_pic3','/uploads/children_page/'.$data['upload_data']['file_name']);
			}

			if ( ! $this->upload->do_upload('children_pic4'))
			{
// 				$this->data['result'] =  $this->upload->display_errors();
			}
			else
			{
				$this->images_model->clear_all_parent_images($id,'children_pic4');
				$data = array('upload_data' => $this->upload->data());
				$this->data['result'] = $this->images_model->add_image($id,'children_pic4','/uploads/children_page/'.$data['upload_data']['file_name']);
			}
        }

		//логирование
		$this->admin_logs_model->add($id, 'complex_page',$this->input->post('title'),'/admin_children/children_page_title','Редактирование');
		echo $this->load->view('admin/json_result_view',$this->data);
	}
//
//	function komplex_programs_advantages()
//	{
//		$this->data['advantages_programs'] = $this->info_model->get_advantages_programs();
//		$this->data['title'] = 'Редактировать преимущества комплексных программ';
//		$this->load->view('komplex_programs_advantages_list_view',$this->data);
//	}
//	function add_programs_advantages_page()
//	{
//		$this->data['title'] = 'Добавить преимущество';
//		$this->load->view('add_advantage_view',$this->data);
//	}
//
//	/**
//	* Добавление преимуществ комплексных программ
//	* @param
//	* @return вывод результатов сохранения
//	*/
//	function add_programs_advantages()
//	{
//		$this->load->model('images_model', '', TRUE);
//		$this->view_type = 2;
//		$additional_data = array('title' => $this->input->post('title'),
//					'text' => $this->input->post('text'));
//		$result = $this->info_model->add_advantage($additional_data);
//		$this->data['result'] = $result['success'];
//		if($this->data['result'])
//		{
//			$config['upload_path'] = './uploads/';
//			$config['allowed_types'] = 'gif|jpg|png';
//			$config['max_size']	= '10000';
//			$config['max_width']  = '2000';
//			$config['max_height']  = '1500';
//			$this->load->library('upload', $config);
//			if ( ! $this->upload->do_upload('image'))
//			{
//				$this->data['result'] =  $this->upload->display_errors();
//			}
//			else
//			{
//				$data = array('upload_data' => $this->upload->data());
//				$this->data['result'] = $this->images_model->add_image($result['insert_id'],'advantages','/uploads/'.$data['upload_data']['file_name']);
//			}
//			//логирование
//			$this->admin_logs_model->add($result['insert_id'], 'Преимущества системы комплексных программ',$this->input->post('title'),'/admin_info/edit_advantage_page/'.$result['insert_id'],'Добавление');
//		}
//		echo $this->load->view('admin/json_result_view',$this->data);
//	}
//
//	function edit_advantage_page($advantage_id)
//	{
//		$this->data['advantage'] = $this->info_model->get_single_advantage($advantage_id);
//		$this->data['title'] = 'Редактировать преимущество '.$this->data['advantage'][0]->title;
//		$this->load->view('edit_advantage_view',$this->data);
//	}
//
//	/**
//	* Редактирование преимуществ комплексных программ
//	* @param $advantage_id id преимущества
//	* @return вывод результатов сохранения
//	*/
//	function edit_advantage($advantage_id)
//	{
//		$this->load->model('images_model', '', TRUE);
//		$this->view_type = 2;
//		$additional_data = array('title' => $this->input->post('title'),
//					'text' => $this->input->post('text')
//				);
//		$result = $this->info_model->edit_advantage($advantage_id,$additional_data);
//		$this->data['result'] = $result;
//		if($this->data['result'])
//		{
//			$config['upload_path'] = './uploads/';
//			$config['allowed_types'] = 'gif|jpg|png';
//			$config['max_size']	= '10000';
//			$config['max_width']  = '2000';
//			$config['max_height']  = '1500';
//			$this->load->library('upload', $config);
//			if ( ! $this->upload->do_upload('image'))
//			{
//				$this->data['result'] =  $this->upload->display_errors();
//			}
//			else
//			{
//				$this->images_model->clear_all_parent_images($advantage_id,'advantages');
//				$data = array('upload_data' => $this->upload->data());
//				$this->data['result'] = $this->images_model->add_image($advantage_id,'advantages','/uploads/'.$data['upload_data']['file_name']);
//			}
//			//логирование
//			$this->admin_logs_model->add($advantage_id, 'Преимущества системы комплексных программ',$this->input->post('title'),'/admin_info/edit_advantage_page/'.$advantage_id,'Редактирование');
//		}
//		echo $this->load->view('admin/json_result_view',$this->data);
//	}
//
//
//	function medical_programs()
//	{
//		$this->data['medical_programs'] = $this->info_model->get_medical_programs();
//		$this->data['title'] = 'Список комплексных программ';
//		$this->load->view('medical_programs_list_view',$this->data);
//	}
//
//	function add_medical_program_page()
//	{
//		$this->load->view('add_medical_program_page_view');
//	}
//
//	function edit_medical_program_page($program_id = NULL)
//	{
//		$this->data['medical_program'] = $this->info_model->get_single_medical_program($program_id);
//		$this->data['medical_program_img'] = $this->info_model->get_single_medical_program_imgs($program_id);
//		$this->data['title'] = 'Редактирование программы '.$this->data['medical_program'][0]->title;
//		$this->data['published'] = $this->admin_published_model->get_status('medical_program',$program_id);
//		$this->getSeo('medical_program', $program_id);
//		$this->load->view('edit_medical_program_page_view',$this->data);
//	}
//
//	/**
//	* Добавление медицинских комплексных программ
//	* @param
//	* @return вывод результатов сохранения
//	*/
//	function add_medical_program()
//	{
//		$this->view_type = 2;
//		if($this->input->post('isForKids') == 'on')
//			$isForKids = 1;
//		else
//			$isForKids = 0;
//		$additional_data = array('title' => $this->input->post('title'),
//					'path' => $this->input->post('path'),
//					'target' => $this->input->post('target'),
//					'banner_text' => $this->input->post('banner_text'),
//					'address' => $this->input->post('address'),
//					'specialists' => $this->input->post('specialists'),
//					'diagnostics' => $this->input->post('diagnostics'),
//					'research' => $this->input->post('research'),
//					'bonus' => $this->input->post('bonus'),
//					'cost' => $this->input->post('cost'),
//					'isForKids' => $isForKids);
//		$result = $this->info_model->add_medical_program($additional_data);
//		$this->data['result'] = $result['success'];
//		if($this->data['result'])
//		{
//			$config['upload_path'] = './uploads/programs/';
//			$config['allowed_types'] = 'gif|jpg|png';
//			$config['max_size']	= '10000';
//			$config['max_width']  = '2000';
//			$config['max_height']  = '1500';
//			$this->load->library('upload', $config);
//			if ( ! $this->upload->do_upload('image'))
//			{
//				$this->data['result'] =  $this->upload->display_errors();
//			}
//			else
//			{
//				$data = array('upload_data' => $this->upload->data());
//				$this->data['result'] = $this->images_model->add_image($result['insert_id'],'med_program_banner','/uploads/programs/'.$data['upload_data']['file_name']);
//			}
//				if ( ! $this->upload->do_upload('image_mini1'))
//			{
//				$this->data['result'] =  $this->upload->display_errors();
//			}
//			else
//			{
//				$data = array('upload_data' => $this->upload->data());
//				$this->data['result'] = $this->images_model->add_image($result['insert_id'],'med_prog_pic1','/uploads/programs/'.$data['upload_data']['file_name']);
//			}
//			//Извлечение состояния публикации медпрограммы
//			if($this->input->post('published') == 'on')
//			    $published = 1;
//			else
//			    $published = 0;
//			$this->admin_published_model->update_published('medical_program', $result['insert_id'],$published);
//			//логирование
//			$this->admin_logs_model->add($result['insert_id'], 'Комплексные программы',$this->input->post('title'),'/admin_info/edit_medical_program_page/'.$result['insert_id'],'Добавление');
//		}
//
//		echo $this->load->view('admin/json_result_view',$this->data);
//	}
//
//	/**
//	* Редактирование медицинских комплексных программ
//	* @param  $id идентификатор медпрограммы
//	* @return вывод результатов сохранения
//	*/
//	function edit_medical_program($id = NULL)
//	{
//		$this->view_type = 2;
//		if($this->input->post('isForKids') == 'on')
//			$isForKids = 1;
//		else
//			$isForKids = 0;
//		$additional_data = array(
//			'title' => $this->input->post('title'),
//			'path' => $this->input->post('path'),
//			'banner_text' => $this->input->post('banner_text'),
//			'target' => $this->input->post('target'),
//			'address' => $this->input->post('address'),
//			'specialists' => $this->input->post('specialists'),
//			'diagnostics' => $this->input->post('diagnostics'),
//			'research' => $this->input->post('research'),
//			'bonus' => $this->input->post('bonus'),
//			'cost' => $this->input->post('cost'),
//			'banner_text_adv1' => $this->input->post('banner_text_adv1'),
//			'banner_text_adv2' => $this->input->post('banner_text_adv2'),
//			'banner_text_adv3' => $this->input->post('banner_text_adv3'),
//			'banner_text_adv4' => $this->input->post('banner_text_adv4'),
//			'isForKids' => $isForKids);
//		$result = $this->info_model->edit_single_medical_program($id,$additional_data);
//		$this->data['result'] = $result;
//		if($this->data['result'])
//		{
//			$config['upload_path'] = './uploads/programs/';
//			$config['allowed_types'] = 'gif|jpg|png';
//			$config['max_size']	= '10000';
//			$config['max_width']  = '2000';
//			$config['max_height']  = '1500';
//			$this->load->library('upload', $config);
//			if ( ! $this->upload->do_upload('image'))
//			{
//// 				$this->data['result'] =  $this->upload->display_errors();
//			}
//			else
//			{
//				$this->images_model->clear_all_parent_images($id,'med_program_banner');
//				$data = array('upload_data' => $this->upload->data());
//				$this->data['result'] = $this->images_model->add_image($id,'med_program_banner','/uploads/programs/'.$data['upload_data']['file_name']);
//			}
//				if ( ! $this->upload->do_upload('image_mini1'))
//			{
//// 				$this->data['result'] =  $this->upload->display_errors();
//			}
//			else
//			{
//				$this->images_model->clear_all_parent_images($id,'med_prog_pic1');
//				$data = array('upload_data' => $this->upload->data());
//				$this->data['result'] = $this->images_model->add_image($id,'med_prog_pic1','/uploads/programs/'.$data['upload_data']['file_name']);
//			}
//				if ( ! $this->upload->do_upload('image_mini2'))
//			{
//// 				$this->data['result'] =  $this->upload->display_errors();
//			}
//			else
//			{
//				$this->images_model->clear_all_parent_images($id,'med_prog_pic2');
//				$data = array('upload_data' => $this->upload->data());
//				$this->data['result'] = $this->images_model->add_image($id,'med_prog_pic2','/uploads/programs/'.$data['upload_data']['file_name']);
//			}
//			if ( ! $this->upload->do_upload('image_mini3'))
//			{
//// 				$this->data['result'] =  $this->upload->display_errors();
//			}
//			else
//			{
//				$this->images_model->clear_all_parent_images($id,'med_prog_pic3');
//				$data = array('upload_data' => $this->upload->data());
//				$this->data['result'] = $this->images_model->add_image($id,'med_prog_pic3','/uploads/programs/'.$data['upload_data']['file_name']);
//			}
//			if ( ! $this->upload->do_upload('image_mini4'))
//			{
//// 				$this->data['result'] =  $this->upload->display_errors();
//			}
//			else
//			{
//				$this->images_model->clear_all_parent_images($id,'med_prog_pic4');
//				$data = array('upload_data' => $this->upload->data());
//				$this->data['result'] = $this->images_model->add_image($id,'med_prog_pic4','/uploads/programs/'.$data['upload_data']['file_name']);
//			}
//			//Извлечение состояния публикации медпрограммы
//			if($this->input->post('published') == 'on')
//			    $published = 1;
//			else
//			    $published = 0;
//			$this->admin_published_model->update_published('medical_program', $id,$published);
//			//логирование
//			$this->admin_logs_model->add($id, 'Комплексные программы',$this->input->post('title'),'/admin_info/edit_medical_program_page/'.$id,'Редактирование');
//		}
//		$this->saveSeo('medical_program', $id);
//		echo $this->load->view('admin/json_result_view',$this->data);
//	}
//
//	/**
//	* Удаление медицинских комплексных программ
//	* @param  $id идентификатор медпрограммы
//	* @return вывод результатов сохранения
//	*/
//	function delete_medical_program($id=NULL)
//	{
//		$this->view_type = 2;
//		$this->data['medical_program'] = $this->info_model->get_single_medical_program($id);
//		$this->info_model->delete_medical_program($id);
//		$this->admin_published_model->delete_published('medical_program', $id);
//		$this->images_model->clear_all_parent_images($id,'med_program_banner');
//		//логирование
//		$this->admin_logs_model->add($id, 'Комплексные программы',$this->data['medical_program'][0]->title,'/admin_info/edit_medical_program_page/'.$id,'Удаление');
//	}
//
//	function oms()
//	{
//		$this->data['oms'] = $this->info_model->get_oms_page();
//		$this->data['title'] = 'Редактирование ОМС ';
//		$this->getSeo('oms', 0);
//		$this->load->view('edit_oms_view',$this->data);
//	}
//
//	/**
//	* Редактирование страницы ОМС
//	* @param  $id идентификатор страницы ОМС
//	* @return вывод результатов сохранения
//	*/
//	function edit_oms($id = 1)
//	{
//		$this->view_type = 2;
//		$additional_data = array('title' => $this->input->post('title'),
//					'text' => $this->input->post('text'),
//					'second_text' => $this->input->post('second_text'));
//		$result = $this->info_model->edit_oms($id,$additional_data);
//		$this->data['result'] = $result;
//		$this->saveSeo('oms', 0);
//		//логирование
//		$this->admin_logs_model->add($id, 'ОМС',$this->input->post('title'),'/admin_info/oms','Редактирование');
//		echo $this->load->view('admin/json_result_view',$this->data);
//	}
//
//	public function oms_directions_page()
//	{
//		$this->data['oms_directions'] = $this->info_model->get_oms_directions();
//		$this->data['title'] = 'Список направлений услуг по ОМС';
//		$this->load->view('oms_directions_list_view',$this->data);
//	}
//
//	function add_oms_direction_page()
//	{
//		$this->data['title'] = 'Добавить направление по ОМС ';
//		$this->load->view('add_oms_direction_page_view',$this->data);
//	}
//
//	/**
//	* Добавить направление по ОМС
//	* @param  $id идентификатор страницы ОМС
//	* @return вывод результатов сохранения
//	*/
//	function add_oms_direction()
//	{
//		$this->view_type = 2;
//		$additional_data = array('title' => $this->input->post('title'),
//					'text' => $this->input->post('text'),
//					'color' => $this->input->post('color'));
//		$result = $this->info_model->add_oms_direction($additional_data);
//		$this->data['result'] = $result['success'];
//		//логирование
//		$this->admin_logs_model->add($result['insert_id'], 'Направление ОМС',$this->input->post('title'),'/admin_info/edit_oms_direction_page/'.$result['insert_id'],'Добавление');
//		echo $this->load->view('admin/json_result_view',$this->data);
//	}
//
//	function edit_oms_direction_page($id = NULL)
//	{
//		$this->data['oms_direction'] = $this->info_model->get_single_oms_direction($id);
//		$this->data['title'] = 'Редактирование направления по ОМС '.$this->data['oms_direction'][0]->title;
//		$this->load->view('edit_oms_direction_page_view',$this->data);
//	}
//
//	/**
//	* редактировать направление по ОМС
//	* @param  $id идентификатор направления ОМС
//	* @return вывод результатов сохранения
//	*/
//	function edit_oms_direction($id = NULL)
//	{
//		$this->view_type = 2;
//		$additional_data = array('title' => $this->input->post('title'),
//					'text' => $this->input->post('text'),
//					'color' => $this->input->post('color')
//				);
//		$result = $this->info_model->edit_oms_direction($id,$additional_data);
//		$this->data['result'] = $result;
//		//логирование
//		$this->admin_logs_model->add($id, 'Направление ОМС',$this->input->post('title'),'/admin_info/edit_oms_direction_page/'.$id,'Редактирование');
//		echo $this->load->view('admin/json_result_view',$this->data);
//	}
//
//	/**
//	* удалить направление по ОМС
//	* @param  $id идентификатор направления ОМС
//	* @return вывод результатов сохранения
//	*/
//	function delete_oms_direction($id = NULL)
//	{
//		$this->data['oms_direction'] = $this->info_model->get_single_oms_direction($id);
//		$this->view_type = 2;
//		//логирование
//		$this->admin_logs_model->add($id, 'Направление ОМС',$this->data['oms_direction'][0]->title,'/admin_info/edit_oms_direction_page/'.$id,'Удаление');
//		$this->info_model->delete_oms_direction($id);
//	}

    function all_doctors_specializations_list(){
        $this->data['doctors_specializations'] = $this->children_model->get_kids_medical_directions_list();
        $this->data['title'] = 'Все врачебные направления (специализации докторов)';
        $this->data['users_list'] = $this->load->view('doctors_specializations_list_view',$this->data,TRUE);
        $this->load->view('admin/base_node_dashboard_view');
    }

    function edit_doctor_kids_specialization_page($id = NULL){
        $this->data['medical_direction'] = $this->admin_doctors_model->get_medical_direction($id);

        $this->data['services'] = $this->children_model->get_service_kids_specialization_list($id);

        $this->data['title'] = 'Изменить мета-теги для докторской специализации детского направления '.$this->data['medical_direction'][0]->title;
        $this->getSeo('kids_doctors_specializations', $id);
        $this->load->view('edit_doctor_kids_specialization_view',$this->data);
    }

    function edit_doctor_kids_specialization($id = NULL){
        $this->view_type = 2;
        $this->saveSeo('kids_doctors_specializations', $id);
        $this->data['result'] = true;
        echo $this->load->view('admin/json_result_view',$this->data);
    }

    function edit_doctor_kids_services($serv_id = NULL)
    {
        $this->view_type = 2;
        $this->children_model->clear_kids_services_specialization($serv_id);

        if(count(json_decode($this->input->post('kids_services'))))
            $this->data['result'] = $this->children_model->add_kids_services(json_decode($this->input->post('kids_services')));
        else $this->data['result'] = 'Все услуги для данного детского направления очищены';
//        //логирование
//        $this->admin_logs_model->add($doc_id, 'doctors','','/admin_doctors/edit_doctor_page/'.$doc_id,'Изменение списка услуг, оказываемых врачом');
        echo $this->load->view('admin/json_result_view',$this->data);
    }


}
