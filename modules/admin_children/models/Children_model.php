<?
class Children_model extends CI_Model {
	private $enterprise_query_data_list = '';

	function Children_model()
	{
		parent::__construct();
		$this->load->model('images_model', '', TRUE);
	}

//	function get_discount()
//	{
//		$query = "SELECT
//					*
//					FROM discount_page
//					ORDER BY id ASC LIMIT 1";
//		$data = $this->db->query($query);
//		return $data->result();
//	}

	function get_page($id = NULL)
	{
		$query = "SELECT
					*
					FROM complex_page
					WHERE id = $id
					ORDER BY id ASC LIMIT 1";
		$data = $this->db->query($query);
		return $data->result();   
	}

    function get_kids_medical_directions_list()
    {

        $this->db->select('medical_directions.*');
        $this->db->from('doctors');
        $this->db->join('doctor_direction','doctor_direction.doctor_id=doctors.id');
        $this->db->join('medical_directions','medical_directions.id = doctor_direction.direction_id');
        $this->db->join('published','published.type = "doctors" AND published.record_id = doctors.id');
        $this->db->join('published pb2','pb2.type = "medical_directions" AND pb2.record_id = medical_directions.id');
        $where = '(doctors.age=0 or doctors.age=2 ) and (published.type = "doctors" AND published.record_id = doctors.id and published.published = 1)
                    and (pb2.type = "medical_directions" AND pb2.record_id = medical_directions.id AND (pb2.published=1 OR pb2.published IS NULL) )';//
        $this->db->where($where);
        $this->db->group_by('doctor_direction.direction_id');
        $this->db->order_by('title', 'ASC');
        return $this->db->get()->result();

//        $this->db->select('*');
//        $this->db->from('medical_directions');
//        $this->db->order_by('title', 'ASC');
//        return $this->db->get()->result();

    }

    function children_img_delete($id){
		$this->db->delete('images', array('id' => $id));

//        $data = array(
//            'path' => ''
//        );
//
//        $this->db->where('id', $id);
//        $this->db->update('images', $data);
    }

//	function get_simple_pages()
//	{
//		$query = "SELECT
//					*
//					FROM complex_page
//					ORDER BY id DESC";
//		$data = $this->db->query($query);
//		return $data->result();
//	}
//
//	function edit_discount($discount_id=1,$additional_data= array())
//	{
//		$this->db->where('id', $discount_id);
//		return $this->db->update('discount_page', $additional_data);
//	}

	function edit_children_title($komplex_title=1,$additional_data= array())
	{
		$this->db->where('id', $komplex_title);
		return $this->db->update('complex_page', $additional_data);
	}

    function get_service_kids_specialization_list($direction_id = NULL,$is_vs = 0,$doc_id = NULL)
    {
        $search_vs='';
//        if($is_vs)
//            $search_vs = " AND med_id LIKE '%ВС%' ";
//        else
//            $search_vs = " AND med_id NOT LIKE '%ВС%' ";
        $rubrics = '';
//        if($direction_id == '')
//            $search_vs = str_replace('AND',''," AND med_id NOT LIKE '%ВС%' ");
//        else
            $rubrics = "services.rubric IN ($direction_id)";
        $query = "SELECT services.id,med_id,title, services.price, rubric,service_type,service_kids.id as sd_id
                FROM services
                                
                LEFT JOIN service_kids
                ON service_kids.service_id=med_id
                AND service_kids.specialization_id=$direction_id
                
                WHERE ".$rubrics.$search_vs."
                ORDER BY services.id";
        $data = $this->db->query($query);
        return $data->result();
    }

    function clear_kids_services_specialization($serv_id = NULL)
    {
        $this->db->delete('service_kids', array('specialization_id' => $serv_id));
    }

    function add_kids_services($data = array())
    {
        return $this->db->insert_batch('service_kids', $data);
    }

//	function get_advantages_programs()
//	{
//		$query = "SELECT
//					*
//					FROM advantage
//					ORDER BY id ASC";
//		$data = $this->db->query($query);
//		return $data->result();
//	}
//
//	function add_advantage($additional_data)
//	{
//		$insert_result = array();
//		$insert_result['success'] = $this->db->insert('advantage', $additional_data);
//		$insert_result['insert_id'] = $this->db->insert_id();
//		return $insert_result;
//	}
//
//	function get_single_advantage($advantage_id)
//	{
//		$query = "SELECT
//					advantage.id,title,text,images.path
//					FROM advantage,images
//					WHERE
//					images.parent_id=advantage.id
//					AND
//					images.parent_type='advantages'
//					AND advantage.id=$advantage_id
//					ORDER BY advantage.id ASC";
//		$data = $this->db->query($query);
//		return $data->result();
//	}
//
//	function edit_advantage($advantage_id = NULL,$additional_data = array())
//	{
//		$this->db->where('id', $advantage_id);
//		return $this->db->update('advantage', $additional_data);
//	}
//
//	//������� ����������� ��� �������� ������ ����������� � �������
//	function get_medical_programs()
//	{
//		$this->db->select(array('medical_program.*','published.published'));
//		$this->db->from('medical_program');
//		$this->db->join('published', "published.type = 'medical_program' AND published.record_id = medical_program.id ",'left');
//		return $this->db->get()->result();
//	}
//
//	function add_medical_program($additional_data = array())
//	{
//		$insert_result = array();
//		$insert_result['success'] = $this->db->insert('medical_program', $additional_data);
//		$insert_result['insert_id'] = $this->db->insert_id();
//		return $insert_result;
//	}
//
//	function add_simple_page($additional_data = array())
//	{
//		$insert_result = array();
//		$insert_result['success'] = $this->db->insert('complex_page', $additional_data);
//		$insert_result['insert_id'] = $this->db->insert_id();
//		return $insert_result;
//	}
//
//	function get_single_children_page($program_id = NULL)
//	{
//		$query = "SELECT
//					medical_program.*,images.path as image_path
//					FROM medical_program
//					LEFT JOIN images
//					ON
//					images.parent_id=$program_id
//					AND
//					images.parent_type='children_page_gallery'
//					WHERE
//					medical_program.id=$program_id
//					ORDER BY medical_program.id ASC
//					LIMIT 1";
//		$data = $this->db->query($query);
//		return $data->result();
//	}
//
//	function edit_single_medical_program($program_id = NULL,$additional_data)
//	{
//		$this->db->where('id', $program_id);
//		return $this->db->update('medical_program', $additional_data);
//	}
//
//	function delete_medical_program($id = NULL)
//	{
//		$this->db->where('id', $id);
//		$this->db->delete('medical_program');
//		$this->admin_published_model->delete_published('medical_program', $id);
//	}
//
//	function get_oms_page()
//	{
//		$data = $this->db->get_where('oms', array('id' => 1), 1);
//		return $data->result();
//	}
//
//	function edit_oms($id = 1,$additional_data)
//	{
//		$this->db->where('id', $id);
//		return $this->db->update('oms', $additional_data);
//	}
//
//	function get_oms_directions()
//	{
//		$data = $this->db->get('oms_directions');
//		return $data->result();
//	}
//
//	function add_oms_direction($additional_data)
//	{
//		$insert_result = array();
//		$insert_result['success'] = $this->db->insert('oms_directions', $additional_data);
//		$insert_result['insert_id'] = $this->db->insert_id();
//		return $insert_result;
//	}
//
//	function get_single_oms_direction($id = NULL)
//	{
//		$data = $this->db->get_where('oms_directions', array('id' => $id), 1);
//		return $data->result();
//	}
//
//	function edit_oms_direction($id = NULL,$additional_data)
//	{
//		$this->db->where('id', $id);
//		return $this->db->update('oms_directions', $additional_data);
//	}
//
//	function delete_oms_direction($id = NULL)
//	{
//		$this->db->where('id', $id);
//		$this->db->delete('oms_directions');
//	}
//
	function get_single_medical_program_imgs($id = NULL)
	{
		$query = "SELECT *
				FROM
				images
				WHERE
				images.parent_type like 'children_pic%'

				AND images.parent_id = $id";
		$data = $this->db->query($query);

		$img_arr_tmp = '';

		for($i=1;$i<=4;$i++){
            $find = array_filter($data->result(), function($v, $k) use ($i) {
//                if($v->parent_type == 'children_pic' . $i)
                return $v->parent_type == 'children_pic' . $i;
            }, ARRAY_FILTER_USE_BOTH);

            $find_one = '';
            foreach ($find as $el)
                $find_one = $el;

            if(count($find)) {
                $img_arr_tmp[] = $find_one;
            } else {
                $img_arr_tmp[] = (object)['parent_type' => 'children_pic' . $i];
            }

        }

//        echo '<pre>';
//        var_dump($img_arr_tmp);
//        echo '</pre>';
//
//		exit;

		return $img_arr_tmp;
	}

}
