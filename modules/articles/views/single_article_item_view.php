<?foreach($articles_objects as $article){?>
<div class="single_news_item_page">                
    <div class="img_news_wrap">
        <a title="<?=$article->title?>" href="/articles/<?=$article->path?>"><img alt="<?=$article->title?>" width="130px" src="<?=$article->image_path?>" /></a>
    </div>
    <div class="single_news_item_page_text">
        <h4><a title="<?=$article->title?>" href="/articles/<?=$article->path?>"><?=$article->title?></a></h4>
        <p>
            <?=$article->preview_text?>
        </p>
    </div>
</div>
<?}?>