    <div class="row row_breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="">Главная</a></li> | <li><a href="">Полезные статьи</a></li>
            </ul>
        </div>
    </div>
    <div class="row row_title_h1">
        <div class="container">
            <h1>Полезные статьи</h1>
        </div>
    </div>
    <div class="row news_on_page_all useful_news_on_page_all">
        <div class="container" style="padding-bottom: 20px">
            <div class="text_search_rules">
                <input type="text" id="search_articles" placeholder="Начните вводить запрос...">
            </div>
        </div>
        <div class="container" id="all_articles_wrap">
            <?=$this->load->view('single_article_item_view',$this->data,TRUE);?>
        </div>
    </div>