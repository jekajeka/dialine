<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Articles extends Frontend_Controller  {
function __construct()
{
    parent::__construct();
    $this->load->model('articles_frontend_model', '', TRUE);
    $this->title = 'Статьи/Диалайн';
}

public function index()
{
    $this->data['title'] = 'Статьи/Диалайн';
    $this->data['articles_objects'] = $this->articles_frontend_model->get_all_articles();
	$this->getSeo('articles', 0);
    $this->load->view('all_articles_page_view',$this->data);
}

function view($news_code = NULL)
{
    if($news_code)
    {
        $this->data['breadcrumbs_segment'] = '<li><a href="/articles">Полезные статьи</a></li> |';
		$this->data['news'] = $this->articles_frontend_model->get_single_article($news_code);
        $this->data['title'] = $this->data['news'][0]->title;
        $this->getSeo('news', $this->data['news'][0]->id);
		$this->load->view('news/news_page_view',$this->data);
    }
}

function articles_search()
{
    $this->view_type = 2;
    $this->data['articles_objects'] = $this->articles_frontend_model->search_article_by_text($this->input->post('search_text'));
    $this->load->view('articles/single_article_item_view',$this->data);
}


}
?>
