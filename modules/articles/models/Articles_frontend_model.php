<?
class Articles_frontend_model extends CI_Model {
private $enterprise_query_data_list = '';

function Articles_frontend_model()
{
    parent::__construct();
}    

function get_single_article($path = NULL)
{
 $query = " SELECT news.id, news.title, news.subtitle, news.full_text, news.date_pub AS news_date_pub, images.path
            FROM news
            LEFT JOIN images ON news.id = images.parent_id
            AND images.parent_type = 'news'
            WHERE news.path = '$path' ";
    $data = $this->db->query($query);
    return $data->result();   
}

function get_all_articles()
{
 $query = " SELECT news.id, news.title, news.preview_text, news.date_pub AS news_date_pub, news.path, images.path AS image_path
            FROM news, images, headings_node
            WHERE news.id = images.parent_id
            AND images.parent_type = 'news'
            AND news.id = headings_node.node_id
            AND headings_node.headings_id =2
            ORDER BY news.date_pub DESC";
    $data = $this->db->query($query);
    return $data->result();   
}

function search_article_by_text($search_string= '' )
{
if($search_string=='')
    return $this->get_all_articles();
else
{
    $sql_search_string = str_replace(' ','%',$search_string);
    $query = " SELECT DISTINCT news.id, news.title, news.preview_text, news.date_pub AS news_date_pub, news.path, images.path AS image_path
            FROM news, images, headings_node
            WHERE news.id = images.parent_id
            AND images.parent_type = 'news'
            AND news.id = headings_node.node_id
            AND headings_node.headings_id =2
            AND (news.title LIKE '%$sql_search_string%'
            OR news.subtitle LIKE '%$sql_search_string%'
            OR news.full_text LIKE '%$sql_search_string%')
            GROUP BY news.id
            ORDER BY news.date_pub DESC";
    $data = $this->db->query($query);
    return $data->result();
}
}


}
