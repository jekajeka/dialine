<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_services_description extends Admin_Controller  {
function __construct()
{
    parent::__construct();
    $this->load->model('admin_services_description_model', '', TRUE);
    $this->title = '';
    $this->data['title'] = 'Редактирование описания услуг';
}

function index()
{
    $this->data['title'] = 'Все описания услуг';
    $this->data['services_objects'] = $this->admin_services_description_model->get_all_services();
    $this->data['users_list'] = $this->load->view('services_list_view',$this->data,TRUE);
    $this->load->view('admin/base_node_dashboard_view');
}


function add_service_description_page()
{
    $this->data['title'] = 'Добавить описание';
    $this->load->view('add_service_description_page_view',$this->data);
}

public function add_service_description()
{
    $this->view_type = 2;
    $additional_data = array('title' => $this->input->post('title'),
			    'text' => $this->input->post('text'),
			    'service_id' => $this->input->post('service_id'),
			    'path' => $this->input->post('path'));
    $result = $this->admin_services_description_model->add_service_description($additional_data);
    $this->data['result'] = $result['success'];
    echo $this->load->view('admin/json_result_view',$this->data);
}

function edit_services_description_page($id = NULL)
{
    $this->data['service'] = $this->admin_services_description_model->get_single_service($id);
    $this->data['title'] = 'Редактировать описание услуги '.$this->data['service'][0]->title;
    $this->load->view('edit_services_description_page_view',$this->data);
}

function edit_service_description($id = NULL)
{
    $this->view_type = 2;
    $additional_data = array('title' => $this->input->post('title'),
			    'text' => $this->input->post('text'),
			    'service_id' => $this->input->post('service_id'),
			    'path' => $this->input->post('path'));
    $result = $this->admin_services_description_model->edit_service_description($id,$additional_data);
    $this->data['result'] = $result;
    echo $this->load->view('admin/json_result_view',$this->data);
}

function delete_services_description($id = NULL)
{
    $this->view_type = 2;
    $this->admin_services_description_model->delete_service_description($id);
}



}
?>