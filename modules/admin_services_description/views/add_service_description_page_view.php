<script src="/vendors/select2/dist/js/select2.full.min.js"></script>
<link href="/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
<div class="">
            
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?=$title?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form id="add_service_description" data-parsley-validate class="form-horizontal form-label-left">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Название услуги <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="title" class="form-control col-md-7 col-xs-12"   name="title" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="service_id">Идентификатор услуги <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="service_id" class="form-control col-md-7 col-xs-12"   name="service_id" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="path">Адрес услуги (латиницей)<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="path" class="form-control col-md-7 col-xs-12"  data-validate-words="1" name="path" placeholder="Латиница и знаки подчеркивания без пробелов" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="text">Текст</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea name="text" class="form-control" placeholder="" rows="3" style="width: 100%; height: 135px;resize: none" id="text"></textarea>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="add_user_final">
                          <button type="submit" class="btn btn-success">Добавить</button>
                        </div>
                      </div>

                    </form>
                    
                  </div>
                </div>
              </div>
            </div>
</div>
<script src="/admin_js/admin_info/add_service_description_page.js"></script>