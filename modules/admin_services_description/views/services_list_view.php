<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?=$this->data['title']?></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a href="/admin_news/add_news_page" class="add-link"><i class="fa fa-plus"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
                      <thead>
                        <tr>
                          <th><input type="checkbox" id="check-all" class="flat"></th>
                          <th>id</th>
                          <th>Заголовок</th>
                          <th>Идентификатор услуги</th>
                          <th>Адрес</th>
                        </tr>
                      </thead>


                      <tbody>
                <?foreach($this->data['services_objects'] as $service ){?>
                        <tr>
                          <td><input type="checkbox" class="flat" name="table_records"></td>
                          <td><?=$service->id?></td>
                          <td><a href="/admin_services_description/edit_services_description_page/<?=$service->id?>"><?=$service->title?></a></td>
                          <td><?=$service->service_id?></td>
                          <td><?=$service->path?></td>
                        </tr>
                        <?}?>
                      </tbody>
                    </table>
                  </div>
                </div>
</div>
              