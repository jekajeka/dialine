<?
class Admin_services_description_model extends CI_Model {
private $enterprise_query_data_list = '';
private $db_table = 'services_description';
function Admin_services_description_model()
{
    parent::__construct();
    
}

function add_service_description($descript = array())
{
    $insert_result = array();
    $insert_result['success'] = $this->db->insert($this->db_table, $descript);
    $insert_result['insert_id'] = $this->db->insert_id();
    return $insert_result;
}

function get_all_services()
{
    $data = $this->db->get($this->db_table);
    return $data->result();
}

function get_single_service($id = NULL)
{
    $data = $this->db->get_where($this->db_table, array('id' => $id), 1);
    return $data->result(); 
}

function edit_service_description($id = NULL,$additional_data = array())
{
     $this->db->where('id', $id);
    return $this->db->update($this->db_table, $additional_data);
}

function delete_service_description($id = NULL)
{
    $this->db->where('id', $id);
    $this->db->delete($this->db_table);
}

}?>