<?
class Admin_services_model extends CI_Model {
private $db_table = 'services';

function Admin_services_model()
{
    parent::__construct();
}

/**
 *Удаление всех услуг указанной рубрики
 *
 *Удаляет сами услуги и связки с ними в указанной рубрике, только для клиник, связки с врачами не удаляет
 *@rubric_id идентификатор очищаемой рубрики
 */
function clear_services($rubric_id = NULL)
{
    $sql = 'DELETE service_clinic_row
            FROM service_clinic service_clinic_row
            INNER JOIN services services_row
            ON services_row.rubric = '.$rubric_id.' AND service_clinic_row.service_id = services_row.med_id';
    $this->db->query($sql);
    $this->db->delete($this->db_table, array('rubric' => $rubric_id));
}

function load_data($data = array(),$rubric_id = NULL)
{
    $this->clear_services($rubric_id);
    return $this->db->insert_batch($this->db_table, $data);
}

function get_doctors_list()
{
    $data = $this->db->get($this->db_table);
    return $data->result();
}


function load_data_service_clinic($service_clinic = array())
{
    return $this->db->insert_batch('service_clinic', $service_clinic);
}

function get_service_doctors_list($direction_id = NULL,$is_vs = 0,$doc_id = NULL)
{
    if($is_vs)
        $search_vs = " AND med_id LIKE '%ВС%' ";
    else
        $search_vs = " AND med_id NOT LIKE '%ВС%' ";
    $rubrics = '';
    if($direction_id == '')
        $search_vs = str_replace('AND',''," AND med_id NOT LIKE '%ВС%' ");
    else
        $rubrics = $this->db_table.".rubric IN ($direction_id)";
    $query = "SELECT ".$this->db_table.".id,med_id,title, ".$this->db_table.".price, rubric,service_type,service_doctor.price as unic_price,service_doctor.id as sd_id
                FROM ".$this->db_table."
                LEFT JOIN service_doctor
                ON service_doctor.service_id=med_id
                AND service_doctor.doctor_id=$doc_id
                WHERE ".$rubrics.$search_vs."
                ORDER BY ".$this->db_table.".id";
    $data = $this->db->query($query);
    return $data->result();
}

function add_doctors_services($data = array())
{
    return $this->db->insert_batch('service_doctor', $data);
}

function clear_doctors_services($doc_id = NULL)
{
    $this->db->delete('service_doctor', array('doctor_id' => $doc_id));
}

}