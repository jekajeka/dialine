<?
class Admin_doctors_model extends CI_Model
{
    private $enterprise_query_data_list = '';
    private $db_table = 'doctors';

    function Admin_doctors_model()
    {
	parent::__construct();
	//$this->load->library('seo');
    }

    function add_doctor($additional_data = array())
    {
	$insert_result = array();
	$insert_result['success'] = $this->db->insert($this->db_table, $additional_data);
	$insert_result['insert_id'] = $this->db->insert_id();
	return $insert_result;
    }

    //������� ������ ������(����� ������������ ������ � ������� �� ������� ��� �������� /admin_doctors/all_doctors_list, �� ��� �������)
    function get_doctors_list()
    {
        $this->db->select(array($this->db_table.'.name',$this->db_table.'.id',$this->db_table.'.specialisation','published.published',$this->db_table.'.outer_link'));
        $this->db->from($this->db_table);
        $this->db->join('published', "published.type = 'doctors' AND published.record_id = ".$this->db_table.".id ",'left');
        return $this->db->get()->result();
    }

    function get_single_doctor($id = NULL)
    {
	$query = "SELECT ".$this->db_table.".id,".$this->db_table.".outer_link,name, specialisation, group_concat(doctor_direction.direction_id) as medical_direction, text,images.path,translite,is_vs,age
		FROM ".$this->db_table."
		LEFT JOIN images ON
		images.parent_id = ".$this->db_table.".id
		AND images.parent_type = 'doctors'
		LEFT JOIN doctor_direction ON
		doctor_direction.doctor_id=$id
                WHERE ".$this->db_table.".id = $id";
	$data = $this->db->query($query);
	return $data->result();   
    }

	function edit_doctor($id = NULL,$additional_data = array())
	{
		$this->db->where('id', $id);
		return $this->db->update($this->db_table, $additional_data);
	}

	function edit_medical_direction($id = NULL,$additional_data = array())
	{
		//$this->seo->save('medical_directions', $id, $additional_data);
		$this->db->where('id', $id);
		return $this->db->update('medical_directions', $additional_data);
	}

	function add_medical_direction($additional_data = array())
	{
		$insert_result = array();
		$insert_result['success'] = $this->db->insert('medical_directions', $additional_data);
		$insert_result['insert_id'] = $this->db->insert_id();
		return $insert_result;
	}

	function get_all_medical_directions_list()
	{
	    $this->db->select('*');
	    $this->db->from('medical_directions');
	    $this->db->order_by('title', 'ASC');
	    return $this->db->get()->result();
	}

	function get_medical_direction($id = NULL)
	{
	    $data = $this->db->get_where('medical_directions', array('id' => $id), 1);
	    $data = $data->result();
	    //$this->appendSeo('medical_directions', $id, $data[0]);
	    return $data;
	}
		
		/**
		* @deprecated
		* Decided to do all stuff in controller because of fragility and incosistency of models-views-controllers.
		* Since controller is aware of everything logic has to be moved there.
		*/
		private function appendSeo($table, $id, &$obj){
			$seo = $this->seo->read($table, $id);
			foreach($seo as $key => $value){
				$seoProperty = 'seo_'.$key;
				$obj->$seoProperty = $value;
			}
			//return $obj;
		}

	function delete_medical_direction($id = NULL)
	{
		$this->db->where('id', $id);
		$this->db->delete('medical_directions');
	}

	function get_doctors_clinics($doc_id = NULL)
	{
		$query = "SELECT clinics.title,clinics.address,clinics.id,clinic_doctor.clinic_id as is_clinic,district
					FROM clinics
					LEFT JOIN clinic_doctor
					ON
					clinics.id=clinic_doctor.clinic_id
					AND
					clinic_doctor.doctor_id=$doc_id
					";
		$data = $this->db->query($query);
		return $data->result();
	}

	function clear_doctors_clinics($doc_id = NULL)
	{
		$this->db->delete('clinic_doctor', array('doctor_id' => $doc_id));
	}

	function add_doctors_clinics($data = array())
	{
		return $this->db->insert_batch('clinic_doctor', $data);
	}

	function clear_doctor_direction($doc_id = NULL)
	{
		$this->db->where('doctor_id', $doc_id);
		$this->db->delete('doctor_direction');
	}

	function add_doctor_direction($doc_id = NULL,$doc_dir = array())
	{
		$data = array();
		foreach($doc_dir as $dc_dr)
		{
			$data[] = array('doctor_id'=>$doc_id,'direction_id'=>$dc_dr);
		}
		return $this->db->insert_batch('doctor_direction', $data);
	}

	function delete_doctor($id = NULL)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->db_table);
	}

	

}
