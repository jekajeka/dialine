<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_doctors extends Admin_Controller  {
function __construct()
{
    parent::__construct();
    $this->load->library('seo');
    $this->load->model('admin_doctors_model', '', TRUE);
    $this->load->model('admin_services_model', '', TRUE);
    $this->load->model('admin_published/admin_published_model', '', TRUE);
    $this->load->model('admin_a_b_test/admin_a_b_test_model', '', TRUE);
    $this->data['title'] = 'Редактирование врачей';
}

function add_doctor_page()
{
    $this->data['title'] = 'Добавить врача';
    $this->data['medical_directions'] = $this->admin_doctors_model->get_all_medical_directions_list();
    $this->load->view('add_doctor_view',$this->data);
}

function add_doctor()
{
    $this->load->model('images_model', '', TRUE);
    $this->view_type = 2;
    if($this->input->post('is_vs') == 'on')
        $is_vs = 1;
    else
        $is_vs = 0;
    $additional_data = array('name' => $this->input->post('name'),
                            'specialisation' => $this->input->post('specialisation'),
                            'translite' => $this->input->post('translite'),
                            'is_vs' => $is_vs,
			    'outer_link' => $this->input->post('outer_link'),
                            'age' => $this->input->post('age'),
			    'text' => $this->input->post('text'));
    $result = $this->admin_doctors_model->add_doctor($additional_data);
    $this->data['result'] = $result['success'];
    if($this->data['result'])
    {
	$config['upload_path'] = './uploads/';
	$config['allowed_types'] = 'gif|jpg|png';
	$config['max_size']	= '10000';
	$config['max_width']  = '2000';
	$config['max_height']  = '1500';
	$this->load->library('upload', $config);
	if ( ! $this->upload->do_upload('image'))
	{
	    $this->data['result'] =  $this->upload->display_errors();
	}	
	else
	{
	    $data = array('upload_data' => $this->upload->data());
	    $this->data['result'] = $this->images_model->add_image($result['insert_id'],'doctors','/uploads/'.$data['upload_data']['file_name']);
	}
        $doc_dir = explode(',',$this->input->post('medical_direction_real'));
        $this->admin_doctors_model->add_doctor_direction($result['insert_id'],$doc_dir);
	//добавление статуса опубликованности записи
	if($this->input->post('published') == 'on')
	    $published = 1;
	else
	    $published = 0;
	$this->admin_published_model->update_published('doctors', $result['insert_id'],$published);
	//логирование добавления записи
	$this->admin_logs_model->add($result['insert_id'], 'doctors',$this->input->post('name'),'/admin_doctors/edit_doctor_page/'.$result['insert_id'],'Добавление');
    }
    echo $this->load->view('admin/json_result_view',$this->data);
}

function all_doctors_list()
{
    $this->data['doctors'] = $this->admin_doctors_model->get_doctors_list();
    $this->data['title'] = 'Все врачи';
    $this->data['users_list'] = $this->load->view('doctors_list_view',$this->data,TRUE);
    $this->load->view('admin/base_node_dashboard_view');
    
}

function all_medical_direction_list()
{
    $this->data['medical_directions'] = $this->admin_doctors_model->get_all_medical_directions_list();
    $this->data['title'] = 'Все врачебные направления';
    $this->data['users_list'] = $this->load->view('medical_directions_list_view',$this->data,TRUE);
    $this->load->view('admin/base_node_dashboard_view');
}

function all_doctors_specializations_list(){
    $this->data['doctors_specializations'] = $this->admin_doctors_model->get_all_medical_directions_list();
    $this->data['title'] = 'Все врачебные направления (специализации докторов)';
    $this->data['users_list'] = $this->load->view('doctors_specializations_list_view',$this->data,TRUE);
    $this->load->view('admin/base_node_dashboard_view');

}

function edit_doctor_page($id = NULL)
{
    $this->data['doctor'] = $this->admin_doctors_model->get_single_doctor($id);
    $this->data['medical_directions'] = $this->admin_doctors_model->get_all_medical_directions_list($id);
    $this->data['title'] = 'Редактировать врача '.$this->data['doctor'][0]->name;
    $this->data['clinics'] = $this->admin_doctors_model->get_doctors_clinics($id);
    $this->data['published'] = $this->admin_published_model->get_status('doctors',$id);
    $this->data['ages'] = '<option value="0">Все</option>
                        <option value="1">Взрослые</option>
                        <option value="2">Дети</option>';
    $this->data['ages'] = str_replace('value="'.$this->data['doctor'][0]->age.'"',' selected="1" value="'.$this->data['doctor'][0]->age.'"',$this->data['ages']);
    $this->data['services'] = $this->admin_services_model->get_service_doctors_list($this->data['doctor'][0]->medical_direction,$this->data['doctor'][0]->is_vs,$id);
    $this->getSeo('doctors', $id);
    $this->load->view('edit_doctor_view',$this->data);
    //$this->output->enable_profiler(TRUE);
}

function edit_doctor_specialization_page($id = NULL){
    $this->data['medical_direction'] = $this->admin_doctors_model->get_medical_direction($id);
    $this->data['title'] = 'Изменить мета-теги для докторской специализации '.$this->data['medical_direction'][0]->title;
    $this->getSeo('doctors_specializations', $id);
    $this->load->view('edit_doctor_specialization_view',$this->data);
}

function edit_doctor_specialization($id = NULL){
    $this->view_type = 2;
    $this->saveSeo('doctors_specializations', $id);
    $this->data['result'] = true;
    echo $this->load->view('admin/json_result_view',$this->data);

}

function edit_doctor($id = NULL)
{
    $this->load->model('images_model', '', TRUE);
    $this->view_type = 2;
    if($this->input->post('is_vs') == 'on')
        $is_vs = 1;
    else
        $is_vs = 0;
    $additional_data = array('name' => $this->input->post('name'),
                            'specialisation' => $this->input->post('specialisation'),
                            'translite' => $this->input->post('translite'),
			    'outer_link' => $this->input->post('outer_link'),
                            'is_vs' => $is_vs,
                            'age' => $this->input->post('age'),
			    'text' => $this->input->post('text')
			);
    $result = $this->admin_doctors_model->edit_doctor($id,$additional_data);
    $this->data['result'] = $result;
    $this->admin_doctors_model->clear_doctor_direction($id);
    $doc_dir = explode(',',$this->input->post('medical_direction_real'));
    $this->admin_doctors_model->add_doctor_direction($id,$doc_dir);
    if($this->data['result'])
    {
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '10000';
		$config['max_width']  = '2000';
		$config['max_height']  = '1500';
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload('image'))
		{
			//$this->data['result'] =  $this->upload->display_errors();
		}	
		else
		{
			$this->images_model->clear_all_parent_images($id,'doctors');
			$data = array('upload_data' => $this->upload->data());
			$this->data['result'] = $this->images_model->add_image($id,'doctors','/uploads/'.$data['upload_data']['file_name']);
		}
    }
    $this->saveSeo('doctors', $id);
    //Извлечение состояния публикации врача
    if($this->input->post('published') == 'on')
        $published = 1;
    else
        $published = 0;
    $this->admin_published_model->update_published('doctors', $id,$published);
    //логирование изменения записи
    $this->admin_logs_model->add($id, 'doctors',$this->input->post('name'),'/admin_doctors/edit_doctor_page/'.$id,'Редактирование');
    echo $this->load->view('admin/json_result_view',$this->data);
}

function edit_medical_direction_page($id = NULL)
{
    $this->data['medical_direction'] = $this->admin_doctors_model->get_medical_direction($id);
    $this->data['published'] = $this->admin_published_model->get_status('medical_directions',$id);
    $this->data['a_b_test'] = $this->admin_a_b_test_model->get_status('medical_directions',$id);
    if(!($this->data['a_b_test'] = $this->admin_a_b_test_model->get_status('medical_directions',$id)))
	$this->data['a_b_test'] = array(0=>(object)array('status'=>0));
    $this->data['title'] = 'Изменить медицинское направление '.$this->data['medical_direction'][0]->title;
    $this->data['child_title'] = 'Изменить детское направление '.$this->data['medical_direction'][0]->title;
    //$this->data = array_merge($this->data, $this->getSeo('medical_directions', $id));
    $this->getSeo('medical_directions', $id);
    $this->load->view('edit_medical_direction_view',$this->data);
}
	
function edit_medical_direction($id = NULL)
{
    $this->view_type = 2;
    $additional_data = array(
        'title' => $this->input->post('title'),
        'path' => $this->input->post('path'),
        'service_path' => $this->input->post('service_path'),
        'text' => $this->input->post('text'),
        'kids_text' => $this->input->post('kids_text'),
        'service_title' => $this->input->post('service_title'),
        'service_rubric_title' => $this->input->post('service_rubric_title'),
        'meta_title' => $this->input->post('seo_title'),
        'meta_description' => $this->input->post('seo_description')
    );
    $result = $this->admin_doctors_model->edit_medical_direction($id,$additional_data);
    $this->saveSeo('medical_directions', $id);
    $this->data['result'] = $result;
    //логирование редактирования меднаправления
    $this->admin_logs_model->add($id, 'medical_directions',$this->input->post('title'),'/admin_doctors/edit_medical_direction_page/'.$id,'Редактирование');
    //Извлечение состояния публикации врача
    if($this->input->post('published') == 'on')
        $published = 1;
    else
        $published = 0;
    $this->admin_published_model->update_published('medical_directions', $id,$published);
    $this->admin_a_b_test_model->update('medical_directions',$id,$this->input->post('a_b_test'));
    echo $this->load->view('admin/json_result_view',$this->data);
}

function add_medical_direction_page()
{
    $this->data['title'] = 'Добавить медицинское направление';
    $this->load->view('add_medical_direction_view',$this->data);
}


function add_medical_direction()
{
    $this->view_type = 2;
    $additional_data = array(
		'title' => $this->input->post('title'),
		'path' => $this->input->post('path'),
		'service_path' => $this->input->post('service_path'),
		'text' => $this->input->post('text'),
		'service_title' => $this->input->post('service_title'),
	        'service_rubric_title' => $this->input->post('service_rubric_title')
    );
    $result = $this->admin_doctors_model->add_medical_direction($additional_data);
    //Извлечение состояния публикации врача
    if($this->input->post('published') == 'on')
        $published = 1;
    else
        $published = 0;
    $this->admin_published_model->update_published('medical_directions', $result['insert_id'],$published);
    //логирование редактирования меднаправления
    $this->admin_logs_model->add($result['insert_id'], 'medical_directions',$this->input->post('title'),'/admin_doctors/edit_medical_direction_page/'.$result['insert_id'],'Добавление');
    $this->data['result'] = $result['success'];
    echo $this->load->view('admin/json_result_view',$this->data);
}

function delete_medical_direction($id = NULL)
{
    $this->view_type = 2;
    $this->admin_doctors_model->delete_medical_direction($id);
    $this->admin_a_b_test_model->delete('medical_directions',$id);
    $this->admin_published_model->delete_published('medical_directions', $id);
    //логирование удаления меднаправления
    $this->admin_logs_model->add($id, 'medical_directions',$this->input->post('title'),'/admin_doctors/edit_medical_direction_page/'.$id,'Удаление');
}

function service_clinic_array($cell_id = NULL,$service_single_line = array())
{
    $clinic_id = NULL;
    switch($cell_id)
    {
        case 5:
            $clinic_id = 3;
	break;
        case 6:
            $clinic_id = 4;
        break;
        case 7:
            $clinic_id = 2;
        break;
        case 8:
            $clinic_id = 5;
        break;
        case 9:
            $clinic_id = 6;
        break;
        case 10:
            $clinic_id = 7;
        break;
        case 11:
            $clinic_id = 9;
        break;
        case 12:
            $clinic_id = 8;
        break;
        case 13:
            $clinic_id = 11;
        break;
	case 14:
            $clinic_id = 16;
        break;
    }
    if(!empty($service_single_line[$cell_id]))
    {
        return array('service_id'=>$service_single_line[1],'clinic_id'=>$clinic_id);
    }
    
}

function load_file($id)
{
    $this->view_type = 2;
    
    $config['upload_path'] = './uploads/';
    $config['allowed_types'] = 'txt';
    $config['max_size']	= '10000';
    $this->load->library('upload', $config);
    if ( ! $this->upload->do_upload('load_data'))
    {
	$this->data['result'] =  $this->upload->display_errors();
	$this->admin_logs_model->add($id, 'medical_directions','Имя файла: '.$data['upload_data']['file_name'],'/admin_doctors/edit_medical_direction_page/'.$id,'Неудачный импорт данных '.$this->upload->display_errors());
    }	
    else
    {
	$data = array('upload_data' => $this->upload->data());
	$services_lines = array();
	$handle = fopen('./uploads/'.$data['upload_data']['file_name'], "r");
	while (!feof($handle))
        {
	    $buffer = fgets($handle);
	    $services_lines[] = $buffer;
	}
	fclose($handle);
	$services_list = array();
	foreach($services_lines as $key=>$service_line)
	{
	    $services_list[$key] = explode ("	",$service_line);
	}
	$rubric = $id;
	$service_type = 'Общие услуги';
	$rubrics_array = array();
	$service_for_db = array();
        $service_clinic = array();
	foreach($services_list as $service_single)
	{
	    if(!empty($service_single[2]))
	    {
                
		if(empty($service_single[1]))
		{
		    $service_type = $service_single[2];
		}
		else
		{
		    $service_for_db[] = array('title'=>$service_single[2],'price'=>str_replace(' ','',trim($service_single[3])),'rubric'=>$rubric,'service_type'=>$service_type,'med_id'=>$service_single[1],'link'=>$service_single[4]);
                    for($i=4;$i<=14;$i++)
                    {
                        if(is_numeric(trim($service_single[$i])))
                            $service_clinic[] = $this->service_clinic_array($i,$service_single);
                    }
		}
	    }
	}
	$this->data['result'] = $this->admin_services_model->load_data($service_for_db,$rubric);
	$this->admin_services_model->load_data_service_clinic($service_clinic);
	$this->admin_logs_model->add($id, 'medical_directions','Имя файла: '.$data['upload_data']['file_name'],'/admin_doctors/edit_medical_direction_page/'.$id,'Успешный импорт данных');
    }
    echo $this->load->view('admin/json_result_view',$this->data);
}

function edit_doctor_services($doc_id = NULL)
{
    $this->view_type = 2;
    $this->admin_services_model->clear_doctors_services($doc_id);
    if(count(json_decode($this->input->post('doctor_services'))))
        $this->data['result'] = $this->admin_services_model->add_doctors_services(json_decode($this->input->post('doctor_services')));
    else $this->data['result'] = 'Все услуги данного врача очищены';
    //логирование
    $this->admin_logs_model->add($doc_id, 'doctors','','/admin_doctors/edit_doctor_page/'.$doc_id,'Изменение списка услуг, оказываемых врачом');
    echo $this->load->view('admin/json_result_view',$this->data);
}

function edit_doctor_clinics($doc_id = NULL)
{
    $this->view_type = 2;
    $this->admin_doctors_model->clear_doctors_clinics($doc_id);
    if(count(json_decode($this->input->post('doctor_clinics'))))
        $this->data['result'] = $this->admin_doctors_model->add_doctors_clinics(json_decode($this->input->post('doctor_clinics')));
    else $this->data['result'] = 'Все клиники приема данного врача очищены';
    //логирование
    $this->admin_logs_model->add($doc_id, 'doctors','','/admin_doctors/edit_doctor_page/'.$doc_id,'Изменение списка клиник приема врача');
    echo $this->load->view('admin/json_result_view',$this->data);
}

function delete_doctor($id = NULL)
{
    $this->load->model('images_model', '', TRUE);
    $this->view_type = 2;
    $this->admin_doctors_model->clear_doctor_direction($id);
    $this->admin_doctors_model->clear_doctors_clinics($id);
    $this->admin_services_model->clear_doctors_services($id);
    $this->images_model->clear_all_parent_images($id,'doctors');
    $this->admin_published_model->delete_published('doctors', $id);
    $this->data['result'] = $this->admin_doctors_model->delete_doctor($id);
    //логирование удаления записи
    $this->admin_logs_model->add($id, 'doctors',$this->input->post('name'),'/admin_doctors/edit_doctor_page/'.$id,'Удаление');
    echo $this->load->view('admin/json_result_view',$this->data);
}

}
