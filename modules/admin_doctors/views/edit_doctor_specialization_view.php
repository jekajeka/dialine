<div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?=$title?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a title="Удалить подготовку к исследованиям" href="/admin_doctors/delete_medical_direction/<?=$medical_direction[0]->id?>" class="add-link"><i class="fa fa-trash"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <input type="hidden" id="id" value="<?=$medical_direction[0]->id?>">
                    <form id="edit_doctor_specialization" data-parsley-validate class="form-horizontal form-label-left">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Направление <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input readonly id="title" class="form-control col-md-7 col-xs-12" value="<?=$medical_direction[0]->title?>"  name="title" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="path">Сегмент адреса <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input readonly id="path" class="form-control col-md-7 col-xs-12" value="<?=$medical_direction[0]->path?>"  name="path" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      
                      <?if(isset($seo)):?>
						<?$this->load->view('admin/seo', $seo);?>
					  <?endif;?>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="add_user_final">
                          <button type="submit" class="btn btn-success">Сохранить</button>
                        </div>
                      </div>
                    </form>  
                  </div>
                </div>
              </div>
            </div>
</div>
<script src="/admin_js/admin_info/edit_doctor_specialization.js"></script>