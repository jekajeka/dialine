<link href="/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
<script src="/vendors/iCheck/icheck.min.js"></script>
<div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?=$title?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a title="Удалить врача" href="/admin_doctors/delete_doctor/<?=$doctor[0]->id?>" class="add-link"><i class="fa fa-trash"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <input type="hidden" id="id" value="<?=$doctor[0]->id?>">
                    <form id="edit_doctor" data-parsley-validate class="form-horizontal form-label-left">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Заголовок <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="name" class="form-control col-md-7 col-xs-12" value="<?=$doctor[0]->name?>"  name="name" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="translite">Транслитерация имени <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="translite" class="form-control col-md-7 col-xs-12" value="<?=$doctor[0]->translite?>"  name="translite" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="translite">Карточка врача на Инфоклинике или т.п. 
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="outer_link" class="form-control col-md-7 col-xs-12" value="<?=$doctor[0]->outer_link?>"  name="outer_link" placeholder="" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="medical_direction">Врачебное направление <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select multiple name="medical_direction" class="form-control" placeholder="Выберите тип материала" id="medical_direction">
                                                <option value="0">Выберите медицинское направление</option>
                                                <?foreach($medical_directions as $med_direct){?>
                                                <?
                                                $active_heading = '';
                                                $doc_direction_array = explode(',',$doctor[0]->medical_direction);
                                                if(in_array($med_direct->id,$doc_direction_array)) $active_heading = 'selected';?>
                                                <option <?=$active_heading?> value="<?=$med_direct->id?>"><?=$med_direct->title?></option>
                                                <?}?>
                                              </select>
                                    <input type="hidden" name="medical_direction_real" value="<?=$doctor[0]->medical_direction?>" />
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="age">Возраст пациентов <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select name="age" class="form-control" placeholder="Выберите тип материала" id="age">
                                                <?=$ages?>
                                    </select>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="is_vs">Ведущий специалист
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <?
                        $check = '';
                        if($doctor[0]->is_vs) $check = 'checked = "1"';?>
                          <input type="checkbox" class="flat" <?=$check?>  name="is_vs">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="specialisation">Специализация <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="specialisation" class="form-control col-md-7 col-xs-12" value="<?=$doctor[0]->specialisation?>"  name="specialisation" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">Изображение
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <img src="<?=$doctor[0]->path?>">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">&nbsp;</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input name="image" type="file" id="image">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="text">Текст</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea name="text" class="form-control" placeholder="" rows="3" style="width: 100%; height: 135px;resize: none" id="text"><?=$doctor[0]->text?></textarea>
                        </div>
                      </div>
                      <?if(isset($seo)):?>
						<?$this->load->view('admin/seo', $seo);?>
					  <?endif;?>
                        <?=$this->load->view('admin/publish_view');?>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="add_user_final">
                          <button type="submit" class="btn btn-success">Редактировать</button>
                        </div>
                      </div>
                    </form>  
                  </div>
                </div>
              </div>
            </div>
</div>


                <div class="x_panel">
                  <div class="x_title">
                    <h2>Услуги по специальности <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th>
                              <input type="checkbox" id="check-all" class="flat">
                            </th>
                            <th class="column-title">ID </th>
                            <th class="column-title">ID услуги </th>
                            <th class="column-title">Услуга </th>
                            <th class="column-title">тип услуги </th>
                            <th class="column-title">Цена </th>
                            <th class="bulk-actions" colspan="4">
                              <a class="antoo" style="color:#fff; font-weight:500;">Всего выбрано ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                          </tr>
                        </thead>

                        <tbody id="service_table">
                        <?foreach($services as $service){?>
                          <tr class="even pointer">
                            <td class="a-center ">
                        <?if($service->sd_id)
                                    $checked = 'checked';
                        else $checked = '';?>
                              <input type="checkbox" <?=$checked?> med_id="<?=$service->med_id?>" curr_id="<?=$service->id?>"  class="flat" name="table_records">
                            </td>
                            <td ><?=$service->id?></td>
                            <td ><?=$service->med_id?></td>
                            <td ><?=$service->title?></td>
                            <td ><?=$service->service_type?></td>
                            <?if($service->unic_price)
                                    $value = $service->unic_price;
                                    else $value = $service->price;?>
                            <td><input type="text" id="price_<?=$service->id?>" value="<?=$value?>" /></td>
                          </tr>
                          <?}?>
                        </tbody>
                      </table>
                    </div>
                        <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12" id="save_doctors_services">
                                      <button type="submit" class="btn btn-success">Сохранить</button>
                                    </div>
                        </div>
                  </div>
                </div>
                
                
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Клиники приема <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th>
                              <input type="checkbox" id="check-all" class="flat">
                            </th>
                            <th class="column-title">ID </th>
                            <th class="column-title">Клиника </th>
                            <th class="column-title">Район </th>
                            <th class="bulk-actions" colspan="4">
                              <a class="antoo" style="color:#fff; font-weight:500;">Всего выбрано ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                          </tr>
                        </thead>

                        <tbody id="table_clinic">
                        <?foreach($clinics as $clinic){?>
                          <tr class="even pointer">
                            <td class="a-center ">
                        <?if($clinic->is_clinic)
                                    $checked = 'checked';
                        else $checked = '';?>
                              <input type="checkbox" <?=$checked?> clinic_id="<?=$clinic->id?>" class="flat" name="table_records">
                            </td>
                            <td ><?=$clinic->id?></td>
                            <td ><?=$clinic->title?></td>
                            <td ><?=$clinic->district?></td>
                          </tr>
                          <?}?>
                        </tbody>
                      </table>
                    </div>
                        <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12" id="save_doctors_clinic">
                                      <button type="submit" class="btn btn-success">Сохранить</button>
                                    </div>
                        </div>
                  </div>
                </div>
              
<script src="/admin_js/admin_info/admin_edit_doctor.js"></script>
