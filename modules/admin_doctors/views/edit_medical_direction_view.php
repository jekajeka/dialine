<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?= $title ?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a title="Удалить подготовку к исследованиям"
                               href="/admin_doctors/delete_medical_direction/<?= $medical_direction[0]->id ?>"
                               class="add-link"><i class="fa fa-trash"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br/>
                    <input type="hidden" id="id" value="<?= $medical_direction[0]->id ?>">
                    <form id="edit_medical_direction" data-parsley-validate class="form-horizontal form-label-left">
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Направление <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="title" class="form-control col-md-7 col-xs-12"
                                       value="<?= $medical_direction[0]->title ?>" name="title" placeholder=""
                                       required="required" type="text">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="path">Сегмент адреса <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="path" class="form-control col-md-7 col-xs-12"
                                       value="<?= $medical_direction[0]->path ?>" name="path" placeholder=""
                                       required="required" type="text">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="service_title">Название типа
                                услуг (например, Гинеколог) <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="service_title" class="form-control col-md-7 col-xs-12"
                                       value="<?= $medical_direction[0]->service_title ?>" name="service_title"
                                       placeholder="" required="required" type="text">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="service_rubric_title">Название
                                раздела услуг (например, Акушерство и гинекология) <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="service_rubric_title" class="form-control col-md-7 col-xs-12"
                                       value="<?= $medical_direction[0]->service_rubric_title ?>"
                                       name="service_rubric_title" placeholder="" required="required" type="text">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="service_path">Адрес типа услуг
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="service_path" class="form-control col-md-7 col-xs-12"
                                       value="<?= $medical_direction[0]->service_path ?>" name="service_path"
                                       placeholder="" required="required" type="text">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="text">Текст <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea name="text" class="form-control" placeholder="" rows="3"
                                          style="width: 100%; height: 135px;resize: none"
                                          id="text"><?= $medical_direction[0]->text ?></textarea>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kids_text">Текст для детского направления<span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea name="kids_text" class="form-control" placeholder="" rows="3"
                                          style="width: 100%; height: 135px;resize: none"
                                          id="kids_text"><?= $medical_direction[0]->kids_text ?></textarea>
                            </div>
                        </div>
                        <? if (isset($seo)): ?>
                            <? $this->load->view('admin/seo', $seo); ?>
                        <? endif; ?>
                        <?= $this->load->view('admin/publish_view'); ?>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="a_b_test">Отображение
                                АБ-тестирования (0 - первым выводится текст, 1 - врачи и услуги ) </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="a_b_test" class="form-control col-md-7 col-xs-12"
                                       value="<?= $a_b_test[0]->status ?>" name="a_b_test" placeholder="" type="text">
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="add_user_final">
                                <button type="submit" class="btn btn-success">Сохранить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/admin_js/admin_info/edit_medical_direction.js"></script>

<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?= $title ?></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br/>
                    <form id="load_services" data-parsley-validate class="form-horizontal form-label-left">
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">Файл импорта данных
                                услуг <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input name="load_data" type="file" id="load_data">
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="add_user_final2">
                                <button type="submit" class="btn btn-success">Добавить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/admin_js/admin_info/load_services.js"></script>
