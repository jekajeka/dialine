<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                                <div class="x_title">
                                                <h2><?=$this->data['title']?></small></h2>
                                                <ul class="nav navbar-right panel_toolbox">
                                                                <li><a href="/admin_doctors/add_doctor_page" class="add-link"><i class="fa fa-plus"></i></a></li>
                                                </ul>
                                                <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                                <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
                                                                <thead>
                                                                                <tr>
                                                                                                <th><input type="checkbox" id="check-all" class="flat"></th>
                                                                                                <th>id</th>
                                                                                                <th>Имя</th>
                                                                                                <th>Специализация</th>
                                                                                                <th>Внешняя ссылка</th>
                                                                                                <th>Опубликовано</th>
                                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?
                                                                foreach($doctors as $doctor ){?>
                                                                                <tr>
                                                                                                <td><input type="checkbox" class="flat" name="table_records"></td>
                                                                                                <td><?=$doctor->id?></td>
                                                                                                <td><a href="/admin_doctors/edit_doctor_page/<?=$doctor->id?>"><?=$doctor->name?></a></td>
                                                                                                <td><a href="/admin_doctors/edit_doctor_page/<?=$doctor->id?>"><?=$doctor->specialisation?></a></td>
                                                                                                <th><?=$doctor->outer_link?></th>
                                                                                                <td>
                                                                                                <?
                                                                                                if($doctor->published)
                                                                                                                $published = 'Опубликовано';
                                                                                                if($doctor->published == 0)
                                                                                                                $published = 'Не опубликовано';
                                                                                                if($doctor->published == NULL)
                                                                                                                $published = 'Опубликовано (галочка не стоит)';
                                                                                                echo $published;
                                                                                                ?>
                                                                                                </td>
                                                                                </tr>
                                                                <?}?>
                                                                </tbody>
                                                </table>
                                </div>
                </div>
</div>
              