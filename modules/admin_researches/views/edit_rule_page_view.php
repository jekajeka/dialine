<div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Редактировать подготовку к исследованиям "<?=$rule[0]->title?>"</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a title="Удалить подготовку к исследованиям" href="/admin_researches/delete_rule/<?=$rule[0]->id?>" class="add-link"><i class="fa fa-trash"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <input type="hidden" id="id" value="<?=$rule[0]->id?>">
                    <form id="edit_rule" data-parsley-validate class="form-horizontal form-label-left">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Заголовок <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="title" class="form-control col-md-7 col-xs-12"   name="title" placeholder="" required="required" type="text" value="<?=$rule[0]->title?>">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="headings">Тип материала <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="headings" class="form-control" placeholder="Выберите тип материала" id="headings">
                            <option value="0">Выберите тип исследования</option>
                            <?foreach($news_type as $n_type){?>
                            <?
                            $active_heading = '';
                            if($n_type->id == $rule[0]->headings_id) $active_heading = 'selected'?>
                            <option <?=$active_heading?> value="<?=$n_type->id?>"><?=$n_type->title?></option>
                            <?}?>
                          </select>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Текст</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea name="text" class="form-control" placeholder="" rows="3" style="width: 100%; height: 135px;resize: none" id="text"><?=$rule[0]->text?></textarea>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="add_user_final">
                          <button type="submit" class="btn btn-success">Изменить</button>
                        </div>
                      </div>
                    </form>  
                  </div>
                </div>
              </div>
            </div>
</div>
<script src="/admin_js/admin_info/edit_rule.js"></script>
