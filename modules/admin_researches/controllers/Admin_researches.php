<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_researches extends Admin_Controller  {
function __construct()
{
    parent::__construct();
    $this->load->model('admin_researches_model', '', TRUE);
    $this->load->model('headings_model', '', TRUE);
    //$this->title = 'Редактирование инфостраницы';
    $this->data['title'] = 'Редактирование подготовок к исследованиям';
    $this->logs_data_type = 'Подготовка к исследованиям';
}

function add_research_page()
{
    $this->data['title'] = 'Добавить подготовку к исследованиям';
    $this->data['news_type'] = $this->headings_model->select_headings_by_type(2);
    $this->load->view('add_research_view',$this->data);
}

/**
* Добавление подготовки к исследованиям
* @param  
* @return вывод результатов сохранения
*/
function add_research()
{
    $this->view_type = 2;
    $additional_data = array('title' => $this->input->post('title'),
			    'text' => $this->input->post('text'));
    $result = $this->admin_researches_model->add_research($additional_data);
    $this->data['result'] = $result['success'];
    $this->headings_model->create_node_headings($result['insert_id'],$this->input->post('headings'));
    //логирование
    $this->admin_logs_model->add($result['insert_id'], $this->logs_data_type,$this->input->post('title'),'/admin_researches/edit_rule_page/'.$result['insert_id'],'Добавление');
    echo $this->load->view('admin/json_result_view',$this->data);
}

function prepare_list()
{
    $this->data['rules'] = $this->admin_researches_model->get_rules_list();
    $this->data['title'] = 'Список правил перед проведением процедур';
    $this->data['users_list'] = $this->load->view('rules_list_view',$this->data,TRUE);
    $this->load->view('admin/base_node_dashboard_view');
    
}

function edit_rule_page($id = NULL )
{
    $this->data['rule'] = $this->admin_researches_model->get_single_rule($id);
    $this->data['title'] = 'Редактирование правила обследования '.$this->data['rule'][0]->title;
    $this->data['news_type'] = $this->headings_model->select_headings_by_type(2);
    $this->load->view('edit_rule_page_view',$this->data);
}

/**
* Редактирование подготовки к исследованиям
* @param  $id айди подготовки к исследованиям
* @return вывод результатов сохранения
*/
function edit_rule($id = NULL )
{
    $this->view_type = 2;
    $additional_data = array('title' => $this->input->post('title'),
			    'text' => $this->input->post('text'),);
    $result = $this->admin_researches_model->edit_rule($id,$additional_data);
    $this->data['result'] = $result;
    $headings_type = 2;
    $this->headings_model->clear_all_node_headings($id,$headings_type);
    $this->headings_model->create_node_headings($id,$this->input->post('headings'));
    //логирование
    $this->admin_logs_model->add($id, $this->logs_data_type,$this->input->post('title'),'/admin_researches/edit_rule_page/'.$id,'Редактирование');
    echo $this->load->view('admin/json_result_view',$this->data);
}

/**
* Удаление подготовки к исследованиям
* @param  $id айди подготовки к исследованиям
* @return вывод результатов сохранения
*/
function delete_rule($id = NULL )
{
    $this->view_type = 2;
    $this->data['rule'] = $this->admin_researches_model->get_single_rule($id);
    //логирование
    $this->admin_logs_model->add($id, $this->logs_data_type,$this->data['rule'][0]->title,'/admin_researches/edit_rule_page/'.$id,'Удаление');
    $this->admin_researches_model->delete_rule($id);
    $headings_type = 2;
    $this->headings_model->clear_all_node_headings($id,$headings_type);
}

}
