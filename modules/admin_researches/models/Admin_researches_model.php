<?
class Admin_researches_model extends CI_Model {
private $db_class = 'rules';

function Admin_researches_model()
{
    parent::__construct();
}

function add_research($additional_data = array())
{
    $insert_result = array();
    $insert_result['success'] = $this->db->insert($this->db_class, $additional_data);
    $insert_result['insert_id'] = $this->db->insert_id();
    return $insert_result;
}

function get_rules_list()
{
    $query = 'SELECT rules.id, rules.title, t1.title AS rubric
            FROM rules
            LEFT JOIN
            (SELECT title,node_id
            FROM headings_node 
            INNER JOIN headings ON headings_node.headings_id = headings.id
            AND headings.headings_type = 2) t1
            ON t1.node_id = rules.id';
    $data = $this->db->query($query);
    return $data->result();   
}

function get_single_rule($id = NULL)
{
    $query = "SELECT rules.id, rules.title,rules.text, t1.headings_id,t1.title as rubric
            FROM rules
            LEFT JOIN
            (SELECT title,node_id,headings_id
            FROM headings_node 
            INNER JOIN headings ON headings_node.headings_id = headings.id
            AND headings.headings_type = 2) t1
            ON t1.node_id = rules.id
            WHERE
            rules.id = $id";
    $data = $this->db->query($query);
    return $data->result(); 
}

function edit_rule($id = NULL,$additional_data = array())
{
    $this->db->where('id', $id);
    return $this->db->update($this->db_class, $additional_data);
}

function delete_rule($id = NULL)
{
    $this->db->where('id', $id);
    $this->db->delete($this->db_class);
}

}