<div class="switch_text active" id="specialist_switch">
    <h2>Перечень услуг</h2>
    <div class="structured_data_wrap">
		<?$openFirst = true?>
		<?$this->load->view('frontend/services-price-list', ['services' => $services, 'openFirst' => $openFirst, 'orderLinkClass' => 'subservice_from_service'])?>
    </div>
    <?if(count($doctors)){?>
    <div class="wrap_specialists_carousel">
        <h2>Специалисты направления "<?=$service[0]->service_rubric_title?>"</h2>
        <div class="specialists_carousel">
            <div class="swiper-wrapper">
            <?foreach($doctors as $doc){
                if(($doc->published==1)||($doc->published == NULL)){?>
                <div class="swiper-slide">
                    <a style="width:270px" class="single_scrubs_wrap" href="/doctors/<?=$doc->direct_path?>/<?=$doc->translite?>">
                        <div class="single_scrubs_img"><img src="<?=$doc->path?>" /></div>
                        <h4><?=$doc->name?></h4>
                        <div class="single_scrubs_text">
                            <?=$doc->specialisation?>
                        </div>
                        <div class="single_scrubs_border"></div>
                    </a>
                </div>
                <?}}?>
            </div>
        </div>
        <a class="specialists_carousel_link specialists_carousel_link_left"></a>
        <a class="specialists_carousel_link specialists_carousel_link_right"></a>
    </div>
    <?}?>
</div>
<div class="overlay_form_wrapper" id="send_service_subservice">
    <div class="overlay_form">
        <a class="close_overlay_form"></a>
        <div class="overlay_form_inner">
            <div class="overlay_form_title">Записаться на прием</div>
            <input type="hidden" id="page_title" value="<?=$service[0]->service_rubric_title?>" />
            <input type="hidden" id="service_subservice_purpose" value="" />
            <input type="text" id="subservice_service_name" placeholder="Имя" />
            <input type="text" id="subservice_service_phone" placeholder="Телефон" />
            <div class="wrap_agree_checkbox">
                <input type="checkbox"   id="subservice_service_agree"><label>Оставляя свои персональные данные, Вы даете добровольное согласие на их обработку. <a target="_blank" href="/uploads/files/Politika_obrabotki_i_zashiti_personalnih_dannih1.pdf">Подробнее</a></label>
            </div>
            <a class="send_overlay_form" id="send_service_subservice_btn">Отправить</a>
            <p>*С Вами свяжутся в ближайшее время и запишут на удобное время к выбранному специалисту</p>
        </div>
    </div>
</div>