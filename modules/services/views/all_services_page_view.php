<div class="row row_title_image">
        <div class="container">
            <h1>Услуги</h1>
        </div>
    </div>
    <div class="row row_breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="">Главная</a></li> | <li><a href="">Услуги</a></li>
            </ul>
        </div>
    </div>
    <div class="row services_wrap">
        <div class="container">
            <div class="left_menu">
                <select id="direction" style="width: 90%">
                    <option value="all">Все услуги</option>
                    <option value="directions">Направления</option>
                    <option value="complex">Комплексные программы</option>
                    <option value="corporate">Корпоративным клиентам</option>
                </select>
            </div>
            <div class="block_with_left_menu">
                <input class="search_service" type="text" id="search_all_service" placeholder="Начните вводить запрос..." />
                <div class="all_services_all_data_block">
                        <?=$this->load->view('all_services_data_block_view',$this->data,true)?>
                </div>
            </div>
        </div>
    </div>