<?foreach($service_types as $key=>$service_type){?>
        <div class="structured_data_content_wrap">
            <a class="structured_data_title active">
                <h4><?=$service_type->service_type?></h4><span class="structured_data_title_qty"><?=$service_type->services_qty?></span>
            </a>
            <div class="structured_data_table_wrap" style="display:block">
                <div class="structured_data_table_wrap_order">
                <?foreach($services as $single_service){
                    if(($single_service->service_type == $service_type->service_type)&&($single_service->rubric == $service_type->rubric)){?>
                    <div class="structured_data_table_line">
                        <div class="structured_data_table_line_border">
                            <h4>
                                <?if(!empty($single_service->link)){?>
                                    <a title="<?=$single_service->title?>" href="<?=$single_service->link?>"><?=$single_service->title?></a>
                                <?}
                                else {?>
                                    <?=$single_service->title?>
                                <?}?>
                            </h4>
                            <div class="structured_data_table_line_price"><?=$single_service->price?> р.</div>
                        </div>
                        <? if(strpos($service_type->service_type,'не требующие записи') == NULL) {?>
                        <a data-service="<?=$single_service->title?> за <?=$single_service->price?> р." class="subservice_from_service structured_data_table_link">Записаться</a>
                        <?}?>
                    </div>
                <?}}?>
                </div>
            </div>
        </div>
<?}?>