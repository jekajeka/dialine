<div class="switch_text switch_text_hidden active rolled_up">
    <?=$service[0]->text?>
</div>
<div class="open_switch_text_wrap">
    <a class="open_switch_text"></a>
</div>
<div style="text-align: center">
    <a style="color: #fff;margin-top: 40px" id="order_specify_btn" class="green_btn box_shadow">Уточнить информацию</a>
</div>
<div class="switch_text active" id="specialist_switch">
    <h2>Перечень услуг</h2>
    <div class="searh_service_container">
        <input style="width:240px" id="search_service_analyze" placeholder="Поиск..." type="text" />
        <a id="search_service_go" class="green_btn">Найти</a>
    </div>
    <div class="structured_data_wrap">
		<?$this->load->view('frontend/services-price-list', ['services' => $services])?>
    </div>
</div>
<div class="overlay_form_wrapper" id="send_service_subservice">
    <div class="overlay_form">
        <a class="close_overlay_form"></a>
        <div class="overlay_form_inner">
            <div class="overlay_form_title">Записаться на анализы</div>
            <input type="hidden" id="page_title" value="Анализы" />
            <input type="hidden" id="service_subservice_purpose" value="" />
            <input type="text" id="subservice_service_name" placeholder="Имя" />
            <input type="text" id="subservice_service_phone" placeholder="Телефон" />
            <div class="wrap_agree_checkbox">
                <input type="checkbox"   id="subservice_service_agree"><label>Оставляя свои персональные данные, Вы даете добровольное согласие на их обработку. <a target="_blank" href="/uploads/files/Politika_obrabotki_i_zashiti_personalnih_dannih1.pdf">Подробнее</a></label>
            </div>
            <a class="send_overlay_form" id="send_service_subservice_btn">Отправить</a>
            <p>*С Вами свяжутся в ближайшее время и запишут на удобное время к выбранному специалисту</p>
        </div>
    </div>
</div>

<div class="overlay_form_wrapper" id="specify">
    <div class="overlay_form">
        <a class="close_overlay_form"></a>
        <div class="overlay_form_inner">
            <div class="overlay_form_title">Уточнить информацию</div>
            <input type="text" id="specify_name" placeholder="Имя" />
            <input type="text" id="specify_phone" placeholder="Телефон" />
            <input type="text" id="specify_question" placeholder="Ваш вопрос" />
            <div class="wrap_agree_checkbox">
                <input type="checkbox"   id="specify_agree"><label>Оставляя свои персональные данные, Вы даете добровольное согласие на их обработку. <a target="_blank" href="/uploads/files/Politika_obrabotki_i_zashiti_personalnih_dannih1.pdf">Подробнее</a></label>
            </div>
            <a class="send_overlay_form" id="specify_btn">Отправить</a>
            <p>*С Вами свяжутся в ближайшее время и ответят на возникшие вопросы</p>
        </div>
    </div>
</div>