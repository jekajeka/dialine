    <div class="row row_breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="">Главная</a></li> | <li><a href="/services">Услуги</a></li> | <li><a href="/<?=$service[0]->service_path?>"><?=$service[0]->service_rubric_title?></a></li>
            </ul>
        </div>
    </div>
    <div class="row row_single_service">
        <div class="container">
            <div class="single_service_left_list_wrap">
                <div class="single_service_left_list box_shadow">
                    <h3>Направления</h3>
                    <ul>
                        <?foreach($all_services as $single_service){
                            $active = '';
                            if($single_service->id == $service[0]->id)
                                $active = 'active';?>
                        <li class="<?=$active?>">
                            <a  href="/<?=$single_service->service_path?>"><?=$single_service->service_rubric_title?></a>
                        </li>
                        <?}?>
                    </ul>
                </div>
            </div>
            <div class="single_service_wrap">
                <div class="h1_substitute"><?=$service[0]->service_rubric_title?></div>
                <?if(!$this->data['a_b_test'][0]->status){?>
                <div class="swither_wrap">
                    <a class="switch_data active box_shadow">Описание</a>
                    <a class="switch_data specialist_switch">Услуги и специалисты</a>
                </div>
                <?}
                else {?>
                <div class="swither_wrap">
                    <a class="switch_data active box_shadow specialist_switch">Услуги и специалисты</a>
                    <a class="switch_data ">Описание</a>
                </div>
                <?}?>
                <div class="switch_text_wrap">
                    <?if(!$this->data['a_b_test'][0]->status)
                    {
                        $this->load->view('service_page_text_view',$this->data);
                        $this->load->view('service_page_specialists_view',$this->data);
                    }
                    else
                    {
                        $this->load->view('service_page_specialists_view',$this->data);
                        $this->load->view('service_page_text_view',$this->data);
                    }
                    ?>
                    
                </div>
                
            </div>
        </div>
    </div>