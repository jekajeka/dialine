<div class="row row_title_image">
    <div class="container">
        <div class="h1_substitute"><?=$service[0]->service_rubric_title?></div>
    </div>
</div>
    <div class="row row_breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="">Главная</a></li> | <li><a href="/services">Услуги</a></li> | <li><a href="/<?=$service[0]->service_path?>"><?=$service[0]->service_rubric_title?></a></li>
            </ul>
        </div>
    </div>
    <div class="row row_single_service">
        <div class="container">
            <div class="single_service_left_list_wrap">
                <div class="single_service_left_list box_shadow">
                    <h3>Направления</h3>
                    <ul>
                        <?foreach($all_services as $single_service){
                            $active = '';
                            if($single_service->id == $service[0]->id)
                                $active = 'active';?>
                        <li class="<?=$active?>">
                            <a  href="/<?=$single_service->service_path?>"><?=$single_service->service_rubric_title?></a>
                            <?if(($single_service->id == $service[0]->id) &&(count($subservicesLeft)>0)) {?>
                            <ul class="subservices">
                                <?foreach($subservicesLeft as $subserviceLeft){?>
                                <li><a  href="/<?=$single_service->service_path?>/<?=$subserviceLeft->path?>"><?=$subserviceLeft->title?></a></li>
                                <?}?>
                            </ul>
                            <?}?>
                        </li>
                        <?}?>
                    </ul>
                </div>
            </div>
            <div class="single_service_wrap">
                <div class="switch_text_wrap">
                    <input type="hidden" id="serviceId" value="<?=$service[0]->id?>">
                    <?
                    if($link == 'analizy')
                    {
			$this->load->view('analizy/analyzy_service_page_text_view',$this->data);
                    }
		    else
                    {
                        $this->load->view('new_service_page_text_view',$this->data);
                        $this->load->view('new_service_page_specialists_view',$this->data);
                    ?>
                    <div style="text-align: center">
                        <a style="color: #fff;" id="order_service_btn2" class="green_btn box_shadow">Записаться на прием</a>
                    </div>
                    <?}?>
                </div>
            </div>
        </div>
    </div>