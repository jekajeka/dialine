<div class="toggle_wrapper" id="directions">
                    <a class="toggle_title active"><h2>Направления</h2></a>
                    <div class="toggle_data" style="display: block">
                        <?foreach($services_top as $service) {?>
                        <a href="/<?=$service->service_path?>"><?=$service->service_rubric_title?></a><br/>
                        <?}?>
                    </div>
                </div>
                <div class="toggle_wrapper" id="complex">
                    <a class="toggle_title active"><h2>Комплексные программы</h2></a>
                    <div class="toggle_data" style="display: block">
                        <h3 style="margin: 0;color: #0abab5">Медицинские программы для взрослых</h3>
                        <?foreach($medical_programs as $med_program) {?>
                        <a href="/komplex_programm/<?=$med_program->path?>"><?=$med_program->title?></a><br/>
                        <?}
                        if(count($medical_programs_kids)){?>
                        <h3 style="margin: 0;color: #0abab5">Медицинские программы для детей</h3>
                        <?foreach($medical_programs_kids as $med_program) {?>
                        <a href="/komplex_programm/<?=$med_program->path?>"><?=$med_program->title?></a><br/>
                        <?}}?>
                    </div>
                </div>
                <div class="toggle_wrapper" id="corporate">
                    <a class="toggle_title active"><h2>Корпоративным клиентам</h2></a>
                    <div class="toggle_data" style="display: block">
                        <?foreach($corporates as $corporate) {?>
                        <a href="/services/additional/<?=$corporate->path?>"><?=$corporate->title?></a><br/>
                        <?}?>
                    </div>
                </div>