    <div class="row row_breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="">Главная</a></li> | <li><a href="/services">Услуги</a></li> | <li><a href="/services/additional/<?=$service[0]->path?>"><?=$service[0]->title?></a></li>
            </ul>
        </div>
    </div>
    <div class="row row_single_service">
        <div class="container">
            <div class="single_service_left_list_wrap">
                <div class="single_service_left_list box_shadow">
                    <? switch($service[0]->type)
                            {
                                case 0:
                                    $service_type = 'Диагностика';
                                break;
                                case 1:
                                    $service_type = 'Лечение';
                                break;
                                case 2:
                                    $service_type = 'Профилактика';
                                break;
                                case 3:
                                    $service_type = 'Корпоративным клиентам';
                                break;
                                default:
                                    $service_type = 'Не определено';
                            }?>
                    <h3><?=$service_type?></h3>
                    <ul>
                        <?foreach($all_services as $single_service){
                            $active = '';
                            if($single_service->id == $service[0]->id)
                                $active = 'active';?>
                        <li class="<?=$active?>"><a  href="/services/additional/<?=$single_service->path?>"><?=$single_service->title?></a></li>
                        <?}?>
                    </ul>
                </div>
            </div>
            <div class="single_service_wrap simple_service">
                <h1><?=$service[0]->title?></h1>
                
                <div class="switch_text_wrap">
                    <div class="switch_text active">
                        <?=$service[0]->text?>
                        <a style="color: #fff" id="order_service_btn" class="green_btn box_shadow">Записаться на прием</a>
                    </div>
                </div>
            </div>
        </div>
    </div>