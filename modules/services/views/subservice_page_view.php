<div class="row row_title_image">
    <div class="container">
        <div class="h1_substitute"><?=$subservice[0]->title?></div>
    </div>
</div>
    <div class="row row_breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="">Главная</a></li> | <li><a href="/services">Услуги</a></li> | <li><a href="/<?=$service[0]->service_path?>"><?=$service[0]->service_rubric_title?></a></li> | <li><a href="/<?=$service[0]->service_path?>/<?=$subservice[0]->path?>"><?=$subservice[0]->title?></a></li>
            </ul>
        </div>
    </div>
    <div class="row row_single_service">
        <div class="container">
            <div class="single_service_left_list_wrap">
                <div class="single_service_left_list box_shadow">
                    <h3>Направления</h3>
                    <ul>
                        <?foreach($all_services as $single_service){
                            $active = '';
                            if($single_service->id == $service[0]->id)
                                $active = 'active';?>
                        <li class="<?=$active?>">
                            <a  href="/<?=$single_service->service_path?>"><?=$single_service->service_rubric_title?></a>
                            <?if($single_service->id == $service[0]->id) {?>
                            <ul class="subservices">
                                <?foreach($subservicesLeft as $subserviceLeft){
                                    $activeLeft = '';
                                if($subserviceLeft->id == $subservice[0]->id)
                                    $activeLeft = 'active';
                                    ?>
                                <li class="<?=$activeLeft?>"><a  href="/<?=$single_service->service_path?>/<?=$subserviceLeft->path?>"><?=$subserviceLeft->title?></a></li>
                                <?}?>
                            </ul>
                            <?}?>
                        </li>
                        <?}?>
                    </ul>
                </div>
            </div>
            <div class="single_service_wrap">
                <div class="switch_text_wrap" >
                    <div class="switch_text switch_text_hidden active rolled_up" >
                        <h1><?=$subservice[0]->title_h1?></h1>
                        <?=$subservice[0]->text?>
                    </div>
                    <div class="open_switch_text_wrap">
                        <a class="open_switch_text"></a>
                    </div>
                    <div class="structured_data_wrap" id="specialist_switch">
                        <div class="structured_data_content_wrap">
                            <a class="structured_data_title active">
                                <h4>Услуги</h4><span class="structured_data_title_qty"><?=count($otherSubservices)?></span>
                            </a>
                            <div class="structured_data_table_wrap" style="display: block">
                                <div class="structured_data_table_wrap_order">
                                <?foreach($otherSubservices as $otherSubservice){?>
                                    <div class="structured_data_table_line">
                                        <div class="structured_data_table_line_border">
                                            <h4>
												<?if ($otherSubservice->link):?>
													<a title="<?=$otherSubservice->title?>" href="<?=$otherSubservice->link?>"><?=$otherSubservice->title?><?=$otherSubservice->mz_service_id ? "&nbsp;&mdash; ".$otherSubservice->mz_service_id : ""?></a>
												<?else:?>
													<?=$otherSubservice->title?><?=$otherSubservice->mz_service_id ? "&nbsp;&mdash; ".$otherSubservice->mz_service_id : ""?>
												<?endif;?>
											</h4>
                                            <div class="structured_data_table_line_price"><?=$otherSubservice->price?> р.</div>
                                        </div>
                                        <a data-service="<?=$otherSubservice->title?> за <?=$otherSubservice->price?> р." class="subservice_from_service structured_data_table_link">Записаться</a>
                                    </div>
                                    <?}?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="overlay_form_wrapper" id="send_service_subservice">
    <div class="overlay_form">
        <a class="close_overlay_form"></a>
        <div class="overlay_form_inner">
            <div class="overlay_form_title">Записаться на прием</div>
            <input type="hidden" id="page_title" value="<?=$subservice[0]->title?>" />
            <input type="hidden" id="service_subservice_purpose" value="" />
            <input type="text" id="subservice_service_name" placeholder="Имя" />
            <input type="text" id="subservice_service_phone" placeholder="Телефон" />
            <div class="wrap_agree_checkbox">
                <input type="checkbox"   id="subservice_service_agree"><label>Оставляя свои персональные данные, Вы даете добровольное согласие на их обработку. <a target="_blank" href="/uploads/files/Politika_obrabotki_i_zashiti_personalnih_dannih1.pdf">Подробнее</a></label>
            </div>
            <a class="send_overlay_form" id="send_service_subservice_btn">Отправить</a>
            <p>*С Вами свяжутся в ближайшее время и запишут на удобное время к выбранному специалисту</p>
        </div>
    </div>
</div>