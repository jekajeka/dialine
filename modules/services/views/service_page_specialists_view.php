<?
$active = '';
if($this->data['a_b_test'][0]->status)
    $active = 'active';
?>
<div class="switch_text <?=$active?>" id="specialist_switch">
    <?if(count($doctors)){?>
    <h2>Специалисты направления "<?=$service[0]->service_rubric_title?>"</h2>
    <div class="specialists_carousel">
        <div class="swiper-wrapper">
        <?foreach($doctors as $doc){
            if(($doc->published==1)||($doc->published == NULL)){?>
            <div class="swiper-slide">
                <a style="width:270px" class="single_scrubs_wrap" href="/doctors/<?=$doc->direct_path?>/<?=$doc->translite?>">
                    <div class="single_scrubs_img"><img src="<?=$doc->path?>" /></div>
                    <h4><?=$doc->name?></h4>
                    <div class="single_scrubs_text">
                        <?=$doc->specialisation?>
                    </div>
                    <div class="single_scrubs_border"></div>
                </a>
            </div>
            <?}}?>
        </div>
    </div>
    <a class="specialists_carousel_link specialists_carousel_link_left"></a>
    <a class="specialists_carousel_link specialists_carousel_link_right"></a>
    <?}?>
    <div class="structured_data_wrap">
    <?foreach($service_types as $service_type){?>
        <div class="structured_data_content_wrap">
            <a class="structured_data_title">
                <h4><?=$service_type->service_type?></h4><span class="structured_data_title_qty"><?=$service_type->services_qty?></span>
            </a>
            <div class="structured_data_table_wrap">
                <div class="structured_data_table_wrap_order">
                <?foreach($services as $service){
                    if(($service->service_type == $service_type->service_type)&&($service->rubric == $service_type->rubric)){?>
                    <div class="structured_data_table_line">
                        <div class="structured_data_table_line_border">
                            <h4><?=$service->title?></h4>
                            <div class="structured_data_table_line_price"><?=$service->price?> р.</div>
                        </div>
                        <?if(!empty($service->link)){?>
                        <a href="<?=$service->link?>" class="structured_data_table_link">Подробнее</a>
                        <?}?>
                    </div>
                <?}}?>
                </div>
            </div>
        </div>
    <?}?>
    </div>
</div>