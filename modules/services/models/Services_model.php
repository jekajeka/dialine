<?
class Services_model extends CI_Model {
private $db_table = 'medical_directions';

function Services_model()
{
    parent::__construct();
    $this->load->library('ServicesLibrary');
}

function get_service($link = "")
{
    $this->db->select($this->db_table.'.*');
    $this->db->from($this->db_table);
    $this->db->join('published', "published.type = 'medical_directions' AND published.record_id = medical_directions.id ",'left');
    $this->db->where('(published.published=1 OR published.published IS NULL)');
    $this->db->where(array('service_path' => $link));
    return $this->db->get()->result();   
}

function get_all_services()
{
    $this->db->select($this->db_table.'.*');
    $this->db->from($this->db_table);
    $this->db->join('published', "published.type = 'medical_directions' AND published.record_id = medical_directions.id ",'left');
    $this->db->where('(published.published=1 OR published.published IS NULL)');
    $this->db->order_by("service_rubric_title", "ASC");
    return $this->db->get()->result();
}

/*function get_doctors_by_specialisation_text($direction = NULL)
{
    $query = "SELECT name, specialisation, images.path,medical_directions.path as direct_path,translite
                FROM doctors
                LEFT JOIN images ON
                images.parent_id = doctors.id
                AND images.parent_type = 'doctors'
                INNER JOIN medical_directions ON
                medical_directions.id = doctors.medical_direction
                WHERE
                medical_directions.service_path = '$direction'";
    $data = $this->db->query($query);
    return $data->result();   
}*/

function get_doctors_by_specialisation_text($direction = NULL)
{
    $query = "SELECT name, specialisation, doctors.text,images.path,medical_directions.path as direct_path,translite
                FROM doctors
                LEFT JOIN images ON
                images.parent_id = doctors.id
                AND images.parent_type = 'doctors'
                INNER JOIN doctor_direction ON
                doctor_direction.doctor_id=doctors.id
                INNER JOIN medical_directions ON
                medical_directions.id = doctor_direction.direction_id
                WHERE
                medical_directions.path = '$direction'
                ORDER BY name ASC";
    $data = $this->db->query($query);
    return $data->result();   
}

function get_services($medical_direction_id = NULL)
{
    return $this->serviceslibrary->getServicesForSpecialization($medical_direction_id);
}

/**
    * Поиск услуг по заголовку и разделу AJAX
    * @return сформированная страница услуги
*/
function searchServicesByTitleAndRubric($searchText,$serviceId)
{
    $s_text = str_replace(' ','%',$searchText);
    $this->db->select('services.id,services.title,services.price,services.rubric,services.service_type,services.link');
    $this->db->from('services');
    $this->db->join('service_link', " service_link.med_id=services.med_id ",'left');
    $this->db->where(array('services.rubric'=>$serviceId));
    $this->db->where(array('services.rubric'=>$serviceId));
    $this->db->where("title LIKE '%$s_text%' ");
    $this->db->order_by("services.id", "ASC");
    return $this->db->get()->result();
}


function get_med_service_type($medical_direction_id = NULL)
{
    $query = "SELECT count( * ) as services_qty , medical_directions.service_rubric_title,rubric,service_type
                FROM services,medical_directions
                WHERE services.rubric=medical_directions.id
                AND medical_directions.id = $medical_direction_id
                GROUP BY service_type
                ORDER BY `services`.`id`,`services`.`rubric` ASC";
    $data = $this->db->query($query);
    return $data->result();
}

function getServiceTypeWithSearchString($serviceTypeId,$search_text)
{
    $s_text = str_replace(' ','%',$search_text);
    $this->db->select(array('count( * ) as services_qty' , 'rubric','service_type'));
    $this->db->from(array('services'));    
    $this->db->where('services.rubric', $serviceTypeId);
    $this->db->where("services.title LIKE '%$s_text%' ");
    $this->db->group_by("service_type");
    $this->db->order_by("services.id,services.rubric", "ASC");
    return $this->db->get()->result();
}

function get_overlay_services()
{
    $this->db->select('service_title,service_rubric_title, service_path, medical_directions.id');
    $this->db->from('medical_directions');
    $this->db->join('published', "published.type = 'medical_directions' AND published.record_id = medical_directions.id ",'left');
    $this->db->where('(published.published=1 OR published.published IS NULL)');
    $this->db->order_by("service_rubric_title", "ASC");
    return $this->db->get()->result();
}

//Выборка одной простой услуги по ее линку
function get_simple_service_by_link($link = NULL)
{
    $this->db->select(array('simple_services.*','published.published'));
    $this->db->from('simple_services');
    $this->db->join('published', "published.type = 'simple_services' AND published.record_id = simple_services.id ",'left');
    $this->db->where('(published.published=1 OR published.published IS NULL)');
    $this->db->where(array('path' => $link));
    return $this->db->get()->result();
}

function get_simple_services_by_type($type = NULL)
{
    $this->db->select(array('simple_services.*','published.published'));
    $this->db->from('simple_services');
    $this->db->join('published', "published.type = 'simple_services' AND published.record_id = simple_services.id ",'left');
    $this->db->where('(published.published=1 OR published.published IS NULL)');
    if($type!==NULL)
        $this->db->where("simple_services.type", $type);
    $this->db->order_by("title", "ASC");
    $data = $this->db->get();
    return $data->result();
}

function get_simple_services_by_text($text = '',$type = NULL)
{
    $this->db->select(array('simple_services.*','published.published'));
    $this->db->from('simple_services');
    $this->db->join('published', "published.type = 'simple_services' AND published.record_id = simple_services.id ",'left');
    $this->db->where('(published.published=1 OR published.published IS NULL)');
    $this->db->where("(simple_services.title LIKE '%$text%' OR simple_services.text LIKE '%$text%')");
    if($type!==NULL)
        $this->db->where("simple_services.type", $type);
    $this->db->order_by("title", "ASC");
    return $this->db->get()->result();
}

function get_services_by_text_old($text = '')
{
    $query = "SELECT service_path,medical_directions.id,service_rubric_title, text
            FROM
            medical_directions
            INNER JOIN
            services
            ON
            services.rubric=medical_directions.id
            WHERE
            service_rubric_title LIKE '%$text%'
            OR
            text LIKE '%$text%'
            OR
            services.title  LIKE '%$text%'
            GROUP BY medical_directions.id";
    $data = $this->db->query($query);
    return $data->result();
}

/*
 *поиск основных услуг по тексту
 *$text строка поиска
 *return список услуг
 */
function get_services_by_text($text = '')
{
    $this->db->select(array('service_path','medical_directions.id','service_rubric_title', 'text'));
    $this->db->from('medical_directions');
    $this->db->join('services', "services.rubric=medical_directions.id",'INNER');
    $this->db->join('published', "published.type = 'medical_directions' AND published.record_id = medical_directions.id ",'left');
    $this->db->where('(published.published=1 OR published.published IS NULL)');
    $this->db->where("(service_rubric_title LIKE '%$text%' OR text LIKE '%$text%' OR services.title  LIKE '%$text%')");
    $this->db->group_by('medical_directions.id');
    return $this->db->get()->result();
}

function get_programs_by_text($text = '')
{
    $query = "SELECT path,id,title,target
            FROM
            medical_program
            WHERE
            title LIKE '%$text%'
            OR
            banner_text LIKE '%$text%'
            OR
            target LIKE '%$text%'
            OR
            specialists LIKE '%$text%'
            OR
            diagnostics LIKE '%$text%'
            OR
            research LIKE '%$text%'
            OR
            bonus LIKE '%$text%'
            GROUP BY id";
    $data = $this->db->query($query);
    return $data->result();
}

function get_medical_programs_titles()
{
    $query = "SELECT
            id, title,cost,path
            FROM medical_program
            ORDER BY id ASC";
    $data = $this->db->query($query);
    return $data->result();
}

}