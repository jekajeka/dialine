<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends Frontend_Controller  {
	function __construct()
	{
		parent::__construct();
		$this->load->model('services_model', '', TRUE);
		$this->load->model('doctors/frontend_doctors_model', '', TRUE);
		$this->load->model('info/frontend_info_model', '', TRUE);
		$this->load->model('clinic/frontend_clinics_model', '', TRUE);
		$this->data['clinics'] = $this->frontend_clinics_model->get_clinics_list();
		$this->title = 'Услуги/Диалайн';
	}
	
	/**
	* основная страница списка услуг
	* @return html страница со свписком всех услуг
	*/
	function index()
	{
		$this->data['title'] = 'Услуги/Диалайн, Волгоград';
		$this->data['services_top'] = $this->services_model->get_overlay_services();
		$this->data['diagnostics'] = $this->services_model->get_simple_services_by_type(0);
		$this->data['cures'] = $this->services_model->get_simple_services_by_type(1);
		$this->data['medical_programs'] =  $this->frontend_info_model->get_all_medical_programs();
		$this->data['medical_programs_kids'] =  $this->frontend_info_model->get_all_medical_programs('kids');
		$this->data['profilactics'] = $this->services_model->get_simple_services_by_type(2);
		$this->data['corporates'] = $this->services_model->get_simple_services_by_type(3);
		$this->getSeo('services_additional', 0);
		$this->load->view('all_services_page_view',$this->data);
	}

	function old_page($link = '',$subLink = NULL)
	{
		$this->load->model('admin_a_b_test/admin_a_b_test_model', '', TRUE);
		$this->data['service'] = $this->services_model->get_service($link);
		if(!empty($this->data['service']))
		{
			$this->load->model('admin_subservices/admin_subservices_model', '', TRUE);
			$this->data['all_services'] = $this->services_model->get_all_services();
			$this->data['subservicesLeft'] = $this->admin_subservices_model->getNodeOfParent($this->data['service'][0]->id);
			if(!$subLink)
			{
				if(!($this->data['a_b_test'] = $this->admin_a_b_test_model->get_status('medical_directions',$this->data['service'][0]->id)))
					$this->data['a_b_test'] = array(0=>(object)array('status'=>0));
				$this->data['title'] = $this->data['service'][0]->service_title;
				$this->data['doctors'] = $this->frontend_doctors_model->get_doctors_by_specialisation_text($link);
				$this->data['service_types'] = $this->services_model->get_med_service_type($this->data['service'][0]->id);
				$this->data['services'] = $this->services_model->get_services($this->data['service'][0]->id);
				$this->data['breadcrumbs_segment'] = '';
				$this->getSeo('medical_directions', $this->data['service'][0]->id);
				$this->load->view('service_page_view',$this->data);
			}
			else
			{
				$this->data['subservice'] = $this->admin_subservices_model->getNodeByPath($subLink);
				if(!empty($this->data['subservice']))
				{
					$this->data['title'] = $this->data['subservice'][0]->title;
					$this->data['otherSubservices'] = $this->admin_subservices_model->get_other_service_list_visible($this->data['subservice'][0]->id,$this->data['service'][0]->id);
					$this->getSeo('subservices', $this->data['subservice'][0]->id);
					$this->load->view('subservice_page_view',$this->data);
				}
				else
					show_404('page');
			}
		}
		else
		{
			show_404('page');
		}
	}
	
	
	/**
	* Новая страница составных услуг
	* @param string $link мнемонический сегмент адреса составной услуги
	* @param string $subLink мнемонический сегмент адреса составной подуслуги
	* @return сформированная страница услуги
	*/
	function page($link = '',$subLink = NULL)
	{
		$this->load->model('admin_a_b_test/admin_a_b_test_model', '', TRUE);
		$this->data['service'] = $this->services_model->get_service($link);
		$this->data['link'] = $link;
		if(!empty($this->data['service']))
		{
			$this->load->model('admin_subservices/admin_subservices_model', '', TRUE);
			$this->data['all_services'] = $this->services_model->get_all_services();
			$this->data['subservicesLeft'] = $this->admin_subservices_model->getNodeOfParent($this->data['service'][0]->id);
			if(!$subLink)
			{
				$this->data['all_services'] = $this->services_model->get_all_services();
				//if(!($this->data['a_b_test'] = $this->admin_a_b_test_model->get_status('medical_directions',$this->data['service'][0]->id)))
					//$this->data['a_b_test'] = array(0=>(object)array('status'=>0));
				$this->data['a_b_test'] = array(0=>(object)array('status'=>1));
				$this->data['title'] = $this->data['service'][0]->service_title;
				$this->data['doctors'] = $this->frontend_doctors_model->get_doctors_by_specialisation_text($link);
				$this->data['service_types'] = $this->services_model->get_med_service_type($this->data['service'][0]->id);
				$this->data['services'] = $this->services_model->get_services($this->data['service'][0]->id);
				$this->data['breadcrumbs_segment'] = '';
				$this->getSeo('medical_directions', $this->data['service'][0]->id);
				$this->load->view('new_service_page_view',$this->data);
			}
			else
			{
				$this->data['subservice'] = $this->admin_subservices_model->getNodeByPath($subLink);
				if(!empty($this->data['subservice']))
				{
					$this->data['title'] = $this->data['subservice'][0]->title;
					$this->data['otherSubservices'] = $this->admin_subservices_model->get_other_service_list_visible($this->data['subservice'][0]->id,$this->data['service'][0]->id);
					$this->getSeo('subservices', $this->data['subservice'][0]->id);
					$this->load->view('subservice_page_view',$this->data);
				}
				else
					show_404('page');
			}
		}
		else
		{
			show_404('page');
		}
	}


	function get_overlay_services()
	{
		$this->view_type = 2;
		$this->data['services_top'] = $this->services_model->get_overlay_services();
		$this->load->view('services_list_overflow_view',$this->data);
	}

	function additional($link = NULL)
	{
		if($link != NULL)
		{
			$this->data['service'] = $this->services_model->get_simple_service_by_link($link);
			if(!empty($this->data['service']))
			{
				$this->data['all_services'] = $this->services_model->get_simple_services_by_type($this->data['service'][0]->type);
				$this->data['title'] = $this->data['service'][0]->title.'/Диалайн, Волгоград';
				$this->getSeo('simple_services', $this->data['service'][0]->id);
				$this->load->view('simple_service_page_view',$this->data);
			}
			else
				show_404('page');
		}
	}

	function get_simple_services_list_by_type($type = NULL)
	{
		$this->view_type = 2;
		$this->data['all_services'] = $this->services_model->get_simple_services_by_type($type);
		$this->load->view('simple_services_list_overflow_view',$this->data);
	}

	function text_search_services()
	{
		$this->view_type = 2;
		$sql_search_string = str_replace(' ','%',$this->input->post('search_text'));
		$this->data['simple_services'] = $this->services_model->get_simple_services_by_text($sql_search_string);
		$this->data['services'] = $this->services_model->get_services_by_text($sql_search_string);
		$this->data['programs'] = $this->services_model->get_programs_by_text($sql_search_string);
		$this->load->view('overlay_text_search_services',$this->data);
	}

	function text_search_services_all_page()
	{
		$this->view_type = 2;
		$sql_search_string = str_replace(' ','%',$this->input->post('search_text'));
		$this->data['services_top'] = $this->services_model->get_services_by_text($sql_search_string);
		$this->data['diagnostics'] = $this->services_model->get_simple_services_by_text($sql_search_string,0);
		$this->data['cures'] = $this->services_model->get_simple_services_by_text($sql_search_string,1);
		$this->data['profilactics'] = $this->services_model->get_simple_services_by_text($sql_search_string,2);
		$this->data['medical_programs'] = $this->services_model->get_programs_by_text($sql_search_string);
		$this->data['corporates'] = $this->services_model->get_simple_services_by_text($sql_search_string,3);
		$this->load->view('all_services_data_block_view',$this->data);
	}
	
	/**
	* Поиск услуг по заголовку и разделу AJAX
	* @return сформированная страница услуги
	*/
	function searchServiceByTitleAndRubric()
	{
		$this->view_type = 2;
		$this->data['service_types'] = $this->services_model->getServiceTypeWithSearchString($this->input->post('serviceId'),$this->input->post('search_text'));
		$this->data['services'] = $this->services_model->searchServicesByTitleAndRubric($this->input->post('search_text'),$this->input->post('serviceId'));
		$this->load->view('analizy/analyzy_list_view',$this->data);
	}


}
