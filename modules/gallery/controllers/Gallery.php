<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends Frontend_Controller  {
function __construct()
{
    parent::__construct();
    $this->load->model('admin_gallery/admin_gallery_model', '', TRUE);
    $this->title = 'Информация/Диалайн';
}


function index()
{
    $this->data['title'] = 'Фотогалерея/Диалайн, Волгоград';
    $this->data['galleries'] = $this->admin_gallery_model->get_gallery_list();
    //$this->data['images'] = $this->admin_gallery_model->get_images_4_gallery_by_id($this->data['gallery'][0]->id);
    $this->getSeo('gallery', 0);
    $this->load->view('all_gallery_page_view',$this->data);
}

function awards()
{
    $this->data['title'] = 'Награды/Диалайн, Волгоград';
    $this->data['gallery'] = $this->admin_gallery_model->get_gallery_by_code('awards');
    $this->data['images'] = $this->admin_gallery_model->get_images_4_gallery_by_id($this->data['gallery'][0]->id);
    $this->getSeo('gallery', $this->data['gallery'][0]->id);
    $this->load->view('awards_page_view',$this->data);
}

function page($gallery_id = NULL)
{
    $config['image_library'] = 'GD2'; // выбираем библиотеку
    $config['create_thumb'] = TRUE; // ставим флаг создания эскиза
    $config['maintain_ratio'] = TRUE; // сохранять пропорции
    $config['width'] = 1500; // и задаем размеры
    $config['height'] = 1000;
    $this->data['gallery'] = $this->admin_gallery_model->get_gallery_by_code($gallery_id);
    if(!empty($this->data['gallery']))
    {
	$this->data['title'] = 'Фотогалерея '.$this->data['gallery'][0]->title.'/Диалайн, Волгоград';
	$this->data['images'] = $this->admin_gallery_model->get_images_4_gallery_by_id($this->data['gallery'][0]->id);
	foreach($this->data['images'] as $key=>$image)
	{
	    $config['source_image'] = substr($image->path,1);
	    $config['new_image'] = 'uploads/thumb/gallery/';
	    $this->load->library('image_lib'); // загружаем библиотеку
	    $this->image_lib->initialize($config);
	    if ( ! $this->image_lib->resize())
	    {
		$this->data['images'][$key]->path = $this->data['images'][$key]->path;
	    }
	    else
	    $this->data['images'][$key]->path = '/uploads/thumb/gallery/'.basename($image->path,'.jpg').'_thumb.jpg';
	}
	$this->getSeo('gallery', $this->data['gallery'][0]->id);
	$this->load->view('awards_page_view',$this->data);
    }
    else
	show_404('page');
}

}?>
