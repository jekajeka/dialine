<div class="row row_breadcrumbs">
    <div class="container">
        <ul>
            <li><a href="/">Главная</a></li> | <li><a href="/info">О компании</a></li> |  <li><a href="/gallery">Фотогалерея</a></li> |  <li><a href=""><?=$gallery[0]->title?></a></li>
        </ul>
    </div>
</div>
<div class="row row_title_h1">
    <div class="container">
        <h1><?=$gallery[0]->title?></h1>
    </div>
</div>
<div class="row single_news_page row_gallery">
    <div class="container">
        <?foreach($images as $image){?>
        <a data-lightbox="zech2" href="<?=$image->path?>"><img height="200" style="margin: 0 20px 20px 0" src="<?=$image->path?>" /></a>
        <?}?>
    </div>
</div>
<script src="/frontend/js/lightbox-2.6.min.js"></script>
<link href="/frontend/css/lightbox.css" rel="stylesheet" />

