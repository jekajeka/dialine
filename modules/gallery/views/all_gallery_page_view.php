<div class="row row_breadcrumbs">
    <div class="container">
        <ul>
            <li><a href="/">Главная</a></li> | <li><a href="/info">О компании</a></li> |  <li><a href="/gallery">Фотогалерея</a></li>
        </ul>
    </div>
</div>
<div class="row row_title_h1">
    <div class="container">
        <h1>Фотогалерея</h1>
    </div>
</div>
<div class="row single_news_page row_gallery row_all_gallery">
    <div class="container">
        <?foreach($galleries as $gallery){?>
        <a  href="/gallery/page/<?=$gallery->gallery_id?>" style=""><span><?=$gallery->title?></span></a>
        <?}?>
    </div>
</div>