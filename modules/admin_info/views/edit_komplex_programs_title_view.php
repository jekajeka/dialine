<script src="/vendors/select2/dist/js/select2.full.min.js"></script>
<link href="/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
<div class="">
            
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?=$title?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <input type="hidden" id="id" name="id" value="<?=$komplex_programs_title[0]->id?>">
                    <form id="edit_komplex" data-parsley-validate class="form-horizontal form-label-left">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Заголовок <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="title" class="form-control col-md-7 col-xs-12" value="<?=$komplex_programs_title[0]->title?>"   name="title" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="path">Сегмент адреса 
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="path" class="form-control col-md-7 col-xs-12" value="<?=$komplex_programs_title[0]->path?>"   name="path" placeholder="" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="text">Текст<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea class="form-control" name="text" placeholder="" rows="3" style="width: 100%; height: 135px;resize: none" id="text"><?=$komplex_programs_title[0]->text ?></textarea>
                        </div>
                      </div>
                      <?if(isset($seo)):?>
						<?$this->load->view('admin/seo', $seo);?>
					  <?endif;?>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="add_user_final">
                          <button type="submit" class="btn btn-success">Сохранить</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>      
</div>
<script src="/admin_js/admin_info/admin_komplex_program_title.js"></script>
