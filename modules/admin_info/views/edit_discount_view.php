<script src="/vendors/select2/dist/js/select2.full.min.js"></script>
<link href="/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
<div class="">
            
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?=$title?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <input type="hidden" id="id" name="id" value="<?=$discount[0]->id?>">
                    <form id="edit_discount" data-parsley-validate class="form-horizontal form-label-left">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Заголовок <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="title" class="form-control col-md-7 col-xs-12" value="<?=$discount[0]->title?>"   name="title" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="subtitle">Подзаголовок <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="subtitle" class="form-control col-md-7 col-xs-12" value="<?=$discount[0]->subtitle?>"   name="subtitle" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="card_text">Текст для карточки<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea class="form-control" name="card_text" placeholder="" rows="3" style="width: 100%; height: 135px;resize: none" id="card_text"><?=$discount[0]->card_text ?></textarea>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="second_title">Второй заголовок</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="second_title" class="form-control col-md-7 col-xs-12" value="<?=$discount[0]->second_title?>"   name="second_title" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="text">Полный текст</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea name="text" class="form-control" placeholder="" rows="3" style="width: 100%; height: 135px;resize: none" id="text"><?=$discount[0]->text?></textarea>
                        </div>
                      </div>
                      <?if(isset($seo)):?>
						<?$this->load->view('admin/seo', $seo);?>
					  <?endif;?>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="add_user_final">
                          <button type="submit" class="btn btn-success">Сохранить</button>
                        </div>
                      </div>

                    </form>
                    
                  </div>
                </div>
              </div>
            </div>      
</div>
<script src="/admin_js/admin_info/admin_discount.js"></script>
