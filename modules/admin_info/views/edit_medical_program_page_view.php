<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Изменить медицинскую программу "<?= $medical_program[0]->title ?>"</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a title="Удалить медпрограмму"
                               href="/admin_info/delete_medical_program/<?= $medical_program[0]->id ?>"
                               class="add-link"><i class="fa fa-trash"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br/>
                    <input id="id" type="hidden" value="<?= $medical_program[0]->id ?>">
                    <form id="edit_medical_program" data-parsley-validate class="form-horizontal form-label-left">
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Название <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="title" class="form-control col-md-7 col-xs-12"
                                       value="<?= $medical_program[0]->title ?>" name="title" placeholder=""
                                       required="required" type="text">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">Изображение в баннер
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <img width="100%" src="<?= $medical_program[0]->image_path ?>"/>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">&nbsp;</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input name="image" type="file" id="image">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="preview">Изображение превью
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <img width="100%" src="<?= $medical_program[0]->preview_path ?>"/>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="preview">&nbsp;</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input name="preview" type="file" id="preview">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="banner_text">Текст для баннера
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="banner_text" class="form-control col-md-7 col-xs-12"
                                       value="<?= $medical_program[0]->banner_text ?>" name="banner_text" placeholder=""
                                       required="required" type="text">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="path">Путь <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="path" class="form-control col-md-7 col-xs-12"
                                       value="<?= $medical_program[0]->path ?>" name="path" placeholder=""
                                       required="required" type="text">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="target">Цель программы</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea name="target" class="form-control" placeholder="" rows="3"
                                          style="width: 100%; height: 135px;resize: none"
                                          id="target"><?= $medical_program[0]->target ?></textarea>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Адреса</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea name="address" class="form-control" placeholder="" rows="3"
                                          style="width: 100%; height: 135px;resize: none"
                                          id="address"><?= $medical_program[0]->address ?></textarea>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                   for="specialists">Специалисты</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea name="specialists" class="form-control" placeholder="" rows="3"
                                          style="width: 100%; height: 135px;resize: none"
                                          id="specialists"><?= $medical_program[0]->specialists ?></textarea>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                   for="diagnostics">Диагностика</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea name="diagnostics" class="form-control" placeholder="" rows="3"
                                          style="width: 100%; height: 135px;resize: none"
                                          id="diagnostics"><?= $medical_program[0]->diagnostics ?></textarea>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="research">Лабораторные
                                исследования</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea name="research" class="form-control" placeholder="" rows="3"
                                          style="width: 100%; height: 135px;resize: none"
                                          id="research"><?= $medical_program[0]->research ?></textarea>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="bonus">Бонусы</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea name="bonus" class="form-control" placeholder="" rows="3"
                                          style="width: 100%; height: 135px;resize: none"
                                          id="bonus"><?= $medical_program[0]->bonus ?></textarea>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="banner_text_adv1">Текст 1го
                                преимущества <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="banner_text_adv1" class="form-control col-md-7 col-xs-12"
                                       name="banner_text_adv1" placeholder="" required="required" type="text"
                                       value="<?= $medical_program[0]->banner_text_adv1 ?>">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="banner_text_adv2">Текст 2го
                                преимущества <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="banner_text_adv2" class="form-control col-md-7 col-xs-12"
                                       name="banner_text_adv2" placeholder="" required="required" type="text"
                                       value="<?= $medical_program[0]->banner_text_adv2 ?>">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="banner_text_adv3">Текст 3го
                                преимущества <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="banner_text_adv3" class="form-control col-md-7 col-xs-12"
                                       name="banner_text_adv3" placeholder="" required="required" type="text"
                                       value="<?= $medical_program[0]->banner_text_adv3 ?>">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="banner_text_adv4">Текст 4го
                                преимущества <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="banner_text_adv4" class="form-control col-md-7 col-xs-12"
                                       name="banner_text_adv4" placeholder="" required="required" type="text"
                                       value="<?= $medical_program[0]->banner_text_adv4 ?>">
                            </div>
                        </div>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">Иконка 1 <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <? if (!empty($medical_program_img[0]->path)) { ?>
                                    <img width="100%" src="<?= $medical_program_img[0]->path ?>"/>
                                <? } ?>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">&nbsp;</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input name="image_mini1" type="file" id="image_mini1">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">Иконка 2 <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <? if (!empty($medical_program_img[1]->path)) { ?>
                                    <img width="100%" src="<?= $medical_program_img[1]->path ?>"/>
                                <? } ?>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">&nbsp;</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input name="image_mini2" type="file" id="image_mini2">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">Иконка 3 <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <? if (!empty($medical_program_img[2]->path)) { ?>
                                    <img width="100%" src="<?= $medical_program_img[2]->path ?>"/>
                                <? } ?>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">&nbsp;</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input name="image_mini3" type="file" id="image_mini3">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">Иконка 4 <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <? if (!empty($medical_program_img[3]->path)) { ?>
                                    <img width="100%" src="<?= $medical_program_img[3]->path ?>"/>
                                <? } ?>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">&nbsp;</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input name="image_mini4" type="file" id="image_mini4">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cost"> Цена<span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="cost" class="form-control col-md-7 col-xs-12" name="cost" placeholder=""
                                       required="required" type="text" value="<?= $medical_program[0]->cost ?>">
                            </div>
                        </div>
                        <? if (isset($seo)): ?>
                            <? $this->load->view('admin/seo', $seo); ?>
                        <? endif; ?>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="isForKids">Для детей</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?
                                $isForKids = "";
                                if ($medical_program[0]->isForKids == 1)
                                    $isForKids = "checked"; ?>
                                <input type="checkbox" <?= $isForKids ?> class="flat" name="isForKids">
                            </div>
                        </div>
                        <?= $this->load->view('admin/publish_view'); ?>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="add_user_final">
                                <button type="submit" class="btn btn-success">Сохранить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/admin_js/admin_info/admin_edit_medical_program.js"></script>
