<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?=$this->data['title']?></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a href="/admin_info/add_medical_program_page" class="add-link"><i class="fa fa-plus"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
                      <thead>
                        <tr>
                          <th>id</th>
                          <th>Название программы</th>
                          <th>Стоимость</th>
                          <th>Опубликовано</th>
                          <th>Страница программы на сайте</th>
                        </tr>
                      </thead>
                      <tbody>
                                <?foreach($medical_programs as $med_prog ){?>
                        <tr>
                          <td><?=$med_prog->id?></td>
                          <td><a href="/admin_info/edit_medical_program_page/<?=$med_prog->id?>"><?=$med_prog->title?></a></td>
                          <td><?=$med_prog->cost?></td>
                          <td>
                                <?
                                if($med_prog->published)
                                                $published = 'Опубликовано';
                                if($med_prog->published == 0)
                                                $published = 'Не опубликовано';
                                if($med_prog->published == NULL)
                                                $published = 'Опубликовано (галочка не стоит)';
                                echo $published;
                                ?>
                          </td>
                          <td><a target="_blank" href="/komplex_programm/<?=$med_prog->path?>">Страница медпрограммы</a></td>
                        </tr>
                        <?}?>
                      </tbody>
                    </table>
                  </div>
                </div>
</div>
              