<?
class Info_model extends CI_Model {
	private $enterprise_query_data_list = '';

	function Info_model()
	{
		parent::__construct();
		$this->load->model('images_model', '', TRUE);
	}

	function get_discount()
	{
		$query = "SELECT
					*
					FROM discount_page
					ORDER BY id ASC LIMIT 1";
		$data = $this->db->query($query);
		return $data->result();   
	}

	function get_page($id = NULL)
	{
		$query = "SELECT
					*
					FROM complex_page
					WHERE id = $id
					ORDER BY id ASC LIMIT 1";
		$data = $this->db->query($query);
		return $data->result();   
	}

	function get_simple_pages()
	{
		$query = "SELECT
					*
					FROM complex_page
					ORDER BY id DESC";
		$data = $this->db->query($query);
		return $data->result();   
	}

	function edit_discount($discount_id=1,$additional_data= array())
	{
		$this->db->where('id', $discount_id);
		return $this->db->update('discount_page', $additional_data);
	}

	function edit_komplex_title($komplex_title=1,$additional_data= array())
	{
		$this->db->where('id', $komplex_title);
		return $this->db->update('complex_page', $additional_data);
	}

	function get_advantages_programs()
	{
		$query = "SELECT
					*
					FROM advantage
					ORDER BY id ASC";
		$data = $this->db->query($query);
		return $data->result();   
	}

	function add_advantage($additional_data)
	{
		$insert_result = array();
		$insert_result['success'] = $this->db->insert('advantage', $additional_data);
		$insert_result['insert_id'] = $this->db->insert_id();
		return $insert_result;
	}

	function get_single_advantage($advantage_id)
	{
		$query = "SELECT
					advantage.id,title,text,images.path
					FROM advantage,images
					WHERE
					images.parent_id=advantage.id
					AND
					images.parent_type='advantages'
					AND advantage.id=$advantage_id
					ORDER BY advantage.id ASC";
		$data = $this->db->query($query);
		return $data->result();   
	}

	function edit_advantage($advantage_id = NULL,$additional_data = array())
	{
		$this->db->where('id', $advantage_id);
		return $this->db->update('advantage', $additional_data);
	}

	//������� ����������� ��� �������� ������ ����������� � �������
	function get_medical_programs()
	{
		$this->db->select(array('medical_program.*','published.published'));
		$this->db->from('medical_program');
		$this->db->join('published', "published.type = 'medical_program' AND published.record_id = medical_program.id ",'left');
		return $this->db->get()->result();
	}

	function add_medical_program($additional_data = array())
	{
		$insert_result = array();
		$insert_result['success'] = $this->db->insert('medical_program', $additional_data);
		$insert_result['insert_id'] = $this->db->insert_id();
		return $insert_result;
	}

	function add_simple_page($additional_data = array())
	{
		$insert_result = array();
		$insert_result['success'] = $this->db->insert('complex_page', $additional_data);
		$insert_result['insert_id'] = $this->db->insert_id();
		return $insert_result;
	}

	function get_single_medical_program($program_id = NULL)
	{
		$query = "SELECT
					medical_program.*,bnr.path as image_path,pvw.path as preview_path
					FROM medical_program
					LEFT JOIN images bnr
					ON
					bnr.parent_id=$program_id
					AND
					bnr.parent_type='med_program_banner'

					LEFT JOIN images pvw
					ON
					pvw.parent_id=$program_id
					AND
					pvw.parent_type='med_program_preview'
					
					WHERE
					medical_program.id=$program_id
					ORDER BY medical_program.id ASC
					LIMIT 1";

		$data = $this->db->query($query);
		return $data->result(); 
	}

	function edit_single_medical_program($program_id = NULL,$additional_data)
	{
		$this->db->where('id', $program_id);
		return $this->db->update('medical_program', $additional_data);
	}

	function delete_medical_program($id = NULL)
	{
		$this->db->where('id', $id);
		$this->db->delete('medical_program');
		$this->admin_published_model->delete_published('medical_program', $id);
	}

	function get_oms_page()
	{    
		$data = $this->db->get_where('oms', array('id' => 1), 1);
		return $data->result(); 
	}

	function edit_oms($id = 1,$additional_data)
	{
		$this->db->where('id', $id);
		return $this->db->update('oms', $additional_data);
	}

	function get_oms_directions()
	{
		$data = $this->db->get('oms_directions');
		return $data->result();
	}

	function add_oms_direction($additional_data)
	{
		$insert_result = array();
		$insert_result['success'] = $this->db->insert('oms_directions', $additional_data);
		$insert_result['insert_id'] = $this->db->insert_id();
		return $insert_result;
	}

	function get_single_oms_direction($id = NULL)
	{
		$data = $this->db->get_where('oms_directions', array('id' => $id), 1);
		return $data->result(); 
	}

	function edit_oms_direction($id = NULL,$additional_data)
	{
		$this->db->where('id', $id);
		return $this->db->update('oms_directions', $additional_data);
	}

	function delete_oms_direction($id = NULL)
	{
		$this->db->where('id', $id);
		$this->db->delete('oms_directions');
	}

	function get_single_medical_program_imgs($id = NULL)
	{
		$query = "SELECT path
				FROM
				images
				WHERE
				images.parent_type ='med_prog_pic1'
				AND images.parent_id = $id
				
				UNION
				SELECT path
				FROM
				images
				WHERE
				images.parent_type ='med_prog_pic2'
				AND images.parent_id = $id
				
				UNION
				SELECT path
				FROM
				images
				WHERE
				images.parent_type ='med_prog_pic3'
				AND images.parent_id = $id
				
				UNION
				SELECT path
				FROM
				images
				WHERE
				images.parent_type ='med_prog_pic4'
				AND images.parent_id = $id";
		$data = $this->db->query($query);
		return $data->result(); 
	}

}
