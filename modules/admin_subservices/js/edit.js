CKEDITOR.replace( 'text' );
//Редактирование новости
$('form#add_subservice').submit(function(e) {
        e.preventDefault();
	for (instance in CKEDITOR.instances) {
		CKEDITOR.instances[instance].updateElement();
	    }
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }
        if (submit)
        {
	var $that = $(this),
	formData = new FormData($that.get(0)); // создаем новый экземпляр объекта и передаем ему нашу форму (*)
            $.ajax({
		    type: "POST",
                    url: "/admin_subservices/edit/"+$('#id').val(),
                    cache: false,
		    scriptCharset: "utf-8",
		    dataType: "html",
		    processData: false,
		    contentType: false,
		    data: formData,
		    success: function(data){
                            $('#add_user_final').prepend(data);
			}
		});
        }
        return false;
      });

 $('input').iCheck({
	handle: 'checkbox',
	checkboxClass: 'icheckbox_flat-green',
	radioClass: 'iradio_flat-red'
  });

$('#save_subservices button').click(function(e){
	e.preventDefault()
	var arr = [];
	$("tbody#service_table input:checkbox:checked").each(function(indx, element){
		arr[indx] = Object();
		arr[indx].serviceId = $(element).attr('data-attr-service');
		arr[indx].subserviceId = $('#id').val();
	      });
	//alert(JSON.stringify(arr));
	$.ajax({
		    type: "POST",
                    url: "/admin_subservices/save_subservices/"+$('#id').val(),
                    cache: false,
		    scriptCharset: "utf-8",
		    dataType: "html",
		    data: {subservice_services:JSON.stringify(arr)},
		    success: function(data){
                            $('#save_doctors_services').prepend(data)
			}
		});
	})

$('.fa-trash').click(function(e){
    e.preventDefault()
    $.ajax({
		    type: "POST",
                    url: "/admin_subservices/delete/"+$('#id').val(),
                    cache: false,
		    scriptCharset: "utf-8",
		    dataType: "html",
		    processData: false,
		    contentType: false,
		    data: {},
		    success: function(data){
                            alert("Запись успешно удалена");
			}
		});
    });