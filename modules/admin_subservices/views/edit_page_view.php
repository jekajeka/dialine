<link href="/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
<script src="/vendors/iCheck/icheck.min.js"></script>
<div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?=$title?></h2>
		    <ul class="nav navbar-right panel_toolbox">
                      <li><a title="Удалить подкатегорию" class="add-link"><i class="fa fa-trash"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <input type="hidden" value="<?=$node[0]->id?>" id="id">
                    <form id="add_subservice" data-parsley-validate class="form-horizontal form-label-left">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Название подраздела <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="title" class="form-control col-md-7 col-xs-12" value="<?=$node[0]->title?>"  name="title" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title_h1">Название подраздела в H1<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="title_h1" class="form-control col-md-7 col-xs-12"  value="<?=$node[0]->title_h1?>" name="title_h1" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="path">Сегмент адреса (1 слово латиницей с подчеркиваниями)<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="path" class="form-control col-md-7 col-xs-12" value="<?=$node[0]->path?>"  name="path" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Текст</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea name="text" class="form-control" placeholder="" rows="3" style="width: 100%; height: 135px;resize: none" id="text">
                                <?=$node[0]->text?>
                            </textarea>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="showOnLeftSide">Показывается в меню слева</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?
                            $check = '';
                            if($node[0]->showOnLeftSide) $check = 'checked = "1"';?>
                            <input type="checkbox" class="flat" <?=$check?>  name="showOnLeftSide">
                        </div>
                      </div>
                      <?if(isset($seo)):?>
			<?$this->load->view('admin/seo', $seo);?>
		      <?endif;?>
                      <?=$this->load->view('admin/publish_view');?>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="add_user_final">
                          <button type="submit" class="btn btn-success">Сохранить</button>
                        </div>
                      </div>
                    </form>  
                  </div>
                </div>
              </div>
            </div>
</div>
<div class="x_panel">
                  <div class="x_title">
                    <h2>Похожие услуги для показа на странице <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th>
                              <input type="checkbox" id="check-all" class="flat">
                            </th>
                            <th class="column-title">ID </th>
                            <th class="column-title">Услуга </th>
                            <th class="column-title">Цена </th>
                            <th class="bulk-actions" colspan="4">
                              <a class="antoo" style="color:#fff; font-weight:500;">Всего выбрано ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                          </tr>
                        </thead>

                        <tbody id="service_table">
                        <?foreach($services as $service){?>
                          <tr class="even pointer">
                            <td class="a-center ">
                        <?if($service->subserviceServiceId)
                                    $checked = 'checked';
                        else $checked = '';?>
                              <input type="checkbox" data-attr-service="<?=$service->med_id?>" <?=$checked?> class="flat" name="table_records">
			      
                            </td>
                            <td ><?=$service->med_id?></td>
                            <td ><?=$service->title?></td>
                            <td> <?=$service->price?></td>
                          </tr>
                          <?}?>
                        </tbody>
                      </table>
                    </div>
                        <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12" id="save_subservices">
                                      <button type="submit" class="btn btn-success">Сохранить</button>
                                    </div>
                        </div>
                  </div>
                </div>
<script src="/modules/admin_subservices/js/edit.js"></script>