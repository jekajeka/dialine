<?
class Admin_subservices_model extends MY_Model {

function Admin_subservices_model()
{
    parent::__construct();
    $this->db_table = 'subservices';
}

/**
* Получение ноды по линку
* @$nodePath урл ноды
* @return массив данных ноды
*/
function getNodeByPath($nodePath = NULL)
{
    $this->db->select($this->db_table.'.*');
    $this->db->from($this->db_table);
    $this->db->join('published', "published.type = 'admin_subservices' AND published.record_id = ".$this->db_table.".id ",'left');
    $this->db->where("path", $nodePath);
    $this->db->where('(published.published=1 OR published.published IS NULL)');
    return $this->db->get()->result();
}

/**
* Получение нод подразделов для текущего родителя
* @$parentId - идентификатор родителя
* @return массив подразделов текущего родителя
*/
function getNodeOfParent($parentId = NULL)
{
    $this->db->select($this->db_table.'.*');
    $this->db->from($this->db_table);
    $this->db->join('published', "published.type = 'admin_subservices' AND published.record_id = ".$this->db_table.".id ",'left');
    $this->db->where("serviceId", $parentId);
    $this->db->where("showOnLeftSide", 1);
    $this->db->where('(published.published=1 OR published.published IS NULL)');
    return $this->db->get()->result();
}

/**
* Получение нод подразделов 
* @$parentId - идентификатор родителя
* @$nodeId - идентификатор ноды, которую не надо искать
* @return массив подразделов текущего родителя с отметками, показывать ли их в данном разделе
*/
function get_other_service_list($nodeId = NULL,$serviceId = NULL)
{
    $this->db->select(array('services.*','subservice_service_new.id as subserviceServiceId'));
    $this->db->from("services");
    $this->db->join('subservice_service_new', 'subservice_service_new.subserviceId='.$nodeId.' AND subservice_service_new.serviceId = services.med_id','left');
    $this->db->where("services.rubric", $serviceId);
    return $this->db->get()->result();
}

/**
* Получение только отображаемых нод подразделов 
* @$parentId - идентификатор родителя
* @$nodeId - идентификатор ноды
* @return массив подразделов текущего родителя с отметками, показывать ли их в данном разделе
*/
function get_other_service_list_visible($nodeId = NULL,$serviceId = NULL)
{
    $this->db->select(array('services.*','subservice_service_new.id'));
    $this->db->from("services");
    $this->db->join('subservice_service_new', 'subservice_service_new.subserviceId='.$nodeId.' AND subservice_service_new.serviceId = services.med_id','left');
    $this->db->where("NOT (subservice_service_new.id IS NULL)");
    $this->db->where("services.rubric", $serviceId);
    return $this->db->get()->result();
}

function clear_subservice_services($nodeId = NULL)
{
    $this->db->delete('subservice_service_new', array('subserviceId' => $nodeId));
}

function add_subservice_services($data = array())
{
    return $this->db->insert_batch('subservice_service_new', $data);
}


}
?>