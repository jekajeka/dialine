<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_subservices extends Admin_Controller  {
function __construct()
{
    parent::__construct();
    $this->load->library('seo');
    $this->load->model('admin_published/admin_published_model', '', TRUE);
    $this->load->model('admin_subservices_model', '', TRUE);
    $this->data['title'] = 'Редактирование подкатегорий';
    $this->logs_data_type = 'Подкатегории';
}

/**
* страница добавления подкатегории услуги
* @param integer $parentCategory идентификатор категории-родителя
* @return html страница редактирования
*/
function add_page($parentCategory = NULL)
{
    $this->data['serviceId'] = $parentCategory;
    $this->load->view('add_page_view',$this->data);
}

/**
* Добавление подкатегории услуги
* 
* @return html результат редактирования
*/
function add()
{
    $this->view_type = 2;
    if($this->input->post('showOnLeftSide') == 'on')
        $showOnLeftSide = 1;
    else
        $showOnLeftSide = 0;
    $additional_data = array('title' => $this->input->post('title'),
                             'title_h1' => $this->input->post('title_h1'),
                             'serviceId' => $this->input->post('serviceId'),
                             'path' => $this->input->post('path'),
                             'showOnLeftSide' => $showOnLeftSide,
                             'text' => $this->input->post('text'));
    $result = $this->admin_subservices_model->add_node($additional_data);
    $this->data['result'] = $result;
    //Извлечение состояния публикации подкатегории
    if($this->input->post('published') == 'on')
        $published = 1;
    else
        $published = 0;
    $this->admin_published_model->update_published('admin_subservices', $result['insert_id'],$published);
    //логирование
    $this->admin_logs_model->add($result['insert_id'], $this->logs_data_type,$this->input->post('title'),'/admin_subservices/edit_page/'.$result['insert_id'],'Добавление');
    $this->data['result'] = $result['success'];
    echo $this->load->view('admin/json_result_view',$this->data);
}

/**
* страница редактирования подкатегории услуги
* @param integer $nodeId идентификатор подкатегории услуги
* @return html страница редактирования
*/
function edit_page($nodeId = NULL)
{
    $this->data['node'] = $this->admin_subservices_model->get_node($nodeId);
    $this->getSeo('subservices', $nodeId);
    $this->data['published'] = $this->admin_published_model->get_status('admin_subservices',$nodeId);
    //$this->data['services'] = $this->admin_subservices_model->get_node($nodeId);
    $this->data['services'] = $this->admin_subservices_model->get_other_service_list($nodeId,$this->data['node'][0]->serviceId);
    $this->load->view('edit_page_view',$this->data);
}

function edit($nodeId = NULL)
{
    $this->view_type = 2;
    if($this->input->post('showOnLeftSide') == 'on')
        $showOnLeftSide = 1;
    else
        $showOnLeftSide = 0;
    $additional_data = array('title' => $this->input->post('title'),
                             'title_h1' => $this->input->post('title_h1'),
                             'path' => $this->input->post('path'),
                             'text' => $this->input->post('text'),
                             'showOnLeftSide' => $showOnLeftSide);
    $result = $this->admin_subservices_model->edit_node($nodeId,$additional_data);
    $this->saveSeo('subservices', $nodeId);
    $this->data['result'] = $result;
    //логирование редактирования меднаправления
    $this->admin_logs_model->add($nodeId, $this->logs_data_type,$this->input->post('title'),'/admin_subservices/edit_page/'.$nodeId,'Редактирование');
    //Извлечение состояния публикации врача
    if($this->input->post('published') == 'on')
        $published = 1;
    else
        $published = 0;
    $this->admin_published_model->update_published('admin_subservices', $nodeId,$published);
    echo $this->load->view('admin/json_result_view',$this->data);
}

/**
* Перезапись списка показываемых услуг
* @param integer $nodeId идентификатор подкатегории услуги
* @return html страница редактирования
*/
function save_subservices($nodeId = NULL)
{
    $this->view_type = 2;
    $this->admin_subservices_model->clear_subservice_services($nodeId);
    if(count(json_decode($this->input->post('subservice_services'))))
        $this->data['result'] = $this->admin_subservices_model->add_subservice_services(json_decode($this->input->post('subservice_services')));
    else $this->data['result'] = 'Все услуги данного врача очищены';
    //логирование
    //$this->admin_logs_model->add($doc_id, 'doctors','','/admin_doctors/edit_doctor_page/'.$doc_id,'Изменение списка услуг, оказываемых врачом');
    echo $this->load->view('admin/json_result_view',$this->data);
}


/**
* Удаление списка показываемых услуг
* @param integer $nodeId идентификатор подкатегории услуги
* @return html страница редактирования
*/
function delete($nodeId = NULL)
{
    $this->view_type = 2;
    $this->data['node'] = $this->admin_subservices_model->get_node($nodeId);
    $result = $this->admin_subservices_model->delete_node($nodeId);
    $this->data['result'] = $result;
    $this->admin_subservices_model->clear_subservice_services($nodeId);
    $this->admin_published_model->delete_published('admin_subservices', $nodeId,$published);
    //логирование редактирования меднаправления
    $this->admin_logs_model->add($nodeId, $this->logs_data_type,$this->data['node'][0]->title,'/admin_subservices/edit_page/'.$nodeId,'Удаление');
    echo $this->load->view('admin/json_result_view',$this->data);
}

}























