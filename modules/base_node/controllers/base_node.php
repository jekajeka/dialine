<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Base_node extends Admin_Controller  {
function __construct()
{
    parent::__construct();
    $this->load->model('base_node_model', '', TRUE);
    $this->title = 'Настройки сайта';
}

public function index()
{
    $this->data['title'] = 'Все записи';
    //$this->data['users_objects'] = $this->base_node_model->get_all_users();
    $this->data['users_list'] = $this->load->view('base_node_list_view',$this->data,TRUE);
    $this->load->view('base_node_dashboard_view');
}
}