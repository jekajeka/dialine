<?
class Base_node_model extends CI_Model {
private $enterprise_query_data_list = '';

function Base_node_model()
{
    parent::__construct();
}    

function clear()
{
    $this->db->truncate('node');
}
function get_all_nodes()
{
    $query = "SELECT
                *
                FROM node
                ORDER BY id ASC";
    $data = $this->db->query($query);
    return $data->result();
}
function get_single_user($user_id = NULL)
{
    $query = "SELECT
                id,username,email,created_on,last_login,first_name,last_name,company,phone,active
                FROM users
                WHERE
                id=$user_id
                ORDER BY id ASC";
    $data = $this->db->query($query);
    return $data->result();
}

function update($params = array())
{
    $this->db->where('id', $params['id']);
    array_shift($params);
    return $this->db->update('users', $params);
}
}