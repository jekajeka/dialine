<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?= $title ?></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br/>
                    <input id="id" type="hidden" value="<?= $banner[0]->id ?>">
                    <form id="edit_banner" data-parsley-validate class="form-horizontal form-label-left">
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Заголовок <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="title" class="form-control col-md-7 col-xs-12"
                                       value='<?= $banner[0]->title ?>' name="title" placeholder="" required="required"
                                       type="text">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="link">Ссылка <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="link" class="form-control col-md-7 col-xs-12" value="<?= $banner[0]->link ?>"
                                       name="link" placeholder="" type="text">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number_in_row">Номер в
                                карусели (0 - не показываемый баннер) <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="number_in_row" class="form-control col-md-7 col-xs-12"
                                       value="<?= $banner[0]->number_in_row ?>" name="number_in_row" placeholder=""
                                       required="required" type="text">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">Изображение <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <img style="max-width: 100%" src="<?= $banner[0]->path ?>">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">Изображение <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input name="image" type="file" id="image">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="text">Текст</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea name="text" class="form-control" placeholder="" rows="3"
                                          style="width: 100%; height: 135px;resize: none"
                                          id="text"><?= $banner[0]->text ?></textarea>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kids">В раздел "Для детей"</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="checkbox" name="kids" id="kids" value="1" <?php $banner[0]->kids ? print 'checked="checked"' : ''; ?> >
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="add_user_final">
                                <button type="submit" class="btn btn-success">Сохранить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/admin_js/admin_info/edit_banner.js"></script>