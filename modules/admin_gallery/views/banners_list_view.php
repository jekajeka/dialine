<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2><?=$this->data['title']?></small></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a href="/admin_gallery/add_gallery_page" class="add-link"><i class="fa fa-plus"></i></a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
                <thead>
                    <tr>
                        <th><input type="checkbox" id="check-all" class="flat"></th>
                        <th>id</th>
                        <th>Заголовок</th>
                        <th>Ссылка</th>
                        <th>Порядковый номер</th>
                    </tr>
                </thead>
                <tbody>
                    <?foreach($banners as $banner){?>
                    <tr>
                        <td><input type="checkbox" class="flat" name="table_records"></td>
                        <td><?=$banner->id?></td>
                        <td><a href="/admin_gallery/edit_banner_page/<?=$banner->id?>"><?=$banner->title?></a></td>
                        <td><a href="/admin_gallery/edit_banner_page/<?=$banner->id?>"><?=$banner->link?></a></td>
                        <td><?=$banner->number_in_row?></td>
                    </tr>
                    <?}?>
                </tbody>
            </table>
        </div>
    </div>
</div>