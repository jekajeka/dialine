<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_gallery extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin_gallery_model', '', TRUE);
        $this->load->model('images_model', '', TRUE);
        $this->data['title'] = 'Фотогалереи';
    }

    function index()
    {
        $this->data['galleries'] = $this->admin_gallery_model->get_gallery_list();
        $this->data['title'] = 'Все фотогалереи';
        $this->data['users_list'] = $this->load->view('gallery_list_view', $this->data, TRUE);
        $this->load->view('admin/base_node_dashboard_view');
    }

    function add_gallery_page()
    {
        $this->data['title'] = 'Добавить фотогалерею';
        $this->load->view('add_gallery_page_view', $this->data);
    }

    function add_banner_page()
    {
        $this->data['title'] = 'Добавить баннер';
        $this->load->view('add_banner_page_view', $this->data);
    }

    function banners()
    {
        $this->data['banners'] = $this->admin_gallery_model->get_all_banners(true,true);
        $this->data['title'] = 'Все баннеры';
        $this->data['users_list'] = $this->load->view('banners_list_view', $this->data, TRUE);
        $this->load->view('admin/base_node_dashboard_view');
    }

    function add_gallery()
    {
        $this->view_type = 2;
        $additional_data = array('title' => $this->input->post('title'),
            'gallery_id' => $this->input->post('gallery_id'),
        );
        $result = $this->admin_gallery_model->add_gallery($additional_data);
        $this->data['result'] = $result['success'];
        //логирование добавления записи
        $this->admin_logs_model->add($result['insert_id'], 'gallery', $this->input->post('title'), '/admin_gallery/edit_gallery_page/' . $result['insert_id'], 'Добавление');
        echo $this->load->view('admin/json_result_view', $this->data);
    }

    function add_banner()
    {
        $this->view_type = 2;
        $additional_data = array('title' => $this->input->post('title'),
            'link' => $this->input->post('link'),
            'number_in_row' => $this->input->post('number_in_row'),
            'kids' => $this->input->post('kids'),
            'text' => $this->input->post('text')
        );
        $result = $this->admin_gallery_model->add_banner($additional_data);
        $this->data['result'] = $result['success'];
        if ($this->data['result']) {
            $config['upload_path'] = './uploads/banners/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '10000';
            $config['max_width'] = '2000';
            $config['max_height'] = '1500';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('image')) {
                $this->data['result'] = $this->upload->display_errors();
            } else {
                $data = array('upload_data' => $this->upload->data());
                $this->data['result'] = $this->images_model->add_image($result['insert_id'], 'banners', '/uploads/banners/' . $data['upload_data']['file_name']);
            }
            //логирование добавления записи
            $this->admin_logs_model->add($result['insert_id'], 'banner', $this->input->post('title'), '/admin_gallery/edit_banner_page/' . $result['insert_id'], 'Добавление');
        }
        echo $this->load->view('admin/json_result_view', $this->data);
    }


    function edit_gallery($id = NULL)
    {
        $this->view_type = 2;
        $additional_data = array('title' => $this->input->post('title'),
            'gallery_id' => $this->input->post('gallery_id'),
        );
        $result = $this->admin_gallery_model->edit_gallery($id, $additional_data);
        $this->saveSeo('gallery', $id);
        $this->data['result'] = $result;
        //логирование добавления записи
        $this->admin_logs_model->add($id, 'gallery', $this->input->post('title'), '/admin_gallery/edit_gallery_page/' . $id, 'Редактирование');
        echo $this->load->view('admin/json_result_view', $this->data);
    }

    function edit_banner($id = NULL)
    {
        $this->view_type = 2;
        $additional_data = array('title' => $this->input->post('title'),
            'link' => $this->input->post('link'),
            'number_in_row' => $this->input->post('number_in_row'),
            'kids' => $this->input->post('kids'),
            'text' => $this->input->post('text')
        );
        $this->data['result'] = $this->admin_gallery_model->edit_banner($id, $additional_data);
        if ($this->data['result']) {
            $config['upload_path'] = './uploads/banners/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '10000';
            $config['max_width'] = '2000';
            $config['max_height'] = '1500';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('image')) {
                $this->data['result'] = $this->upload->display_errors();
            } else {
                $this->images_model->clear_all_parent_images($id, 'banners');
                $data = array('upload_data' => $this->upload->data());
                $this->data['result'] = $this->images_model->add_image($id, 'banners', '/uploads/banners/' . $data['upload_data']['file_name']);
            }
            //логирование
            $this->admin_logs_model->add($id, 'banner', $this->input->post('title'), '/admin_gallery/edit_banner_page/' . $id, 'Редактирование');
        }
        echo $this->load->view('admin/json_result_view', $this->data);
    }

    function upload($id = NULL)
    {
        $this->view_type = 2;
        $this->data['result'] = 1;
        if ($this->data['result']) {
            $config['upload_path'] = './uploads/galleries';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '10000';
            $config['max_width'] = '3000';
            $config['max_height'] = '2000';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('file')) {
                $this->data['result'] = $this->upload->display_errors();
            } else {
                $data = array('upload_data' => $this->upload->data());
                $this->data['result'] = $this->images_model->add_image($id, 'galleries', '/uploads/galleries/' . $data['upload_data']['file_name']);
                //логирование
                $this->admin_logs_model->add($id, 'gallery', 'Адрес изображения: <a target="_blank" href="' . '/uploads/galleries/' . $data['upload_data']['file_name'] . '">/uploads/galleries/' . $data['upload_data']['file_name'] . '</a>', '/admin_gallery/edit_gallery_page/' . $id, 'Закачка изображения в галерею');
            }
        }
        //$this->load->view('add_gallery_page_view',$this->data);
        echo $this->load->view('admin/json_result_view', $this->data);
    }


    function edit_gallery_page($id = NULL)
    {
        $this->data['gallery'] = $this->admin_gallery_model->get_gallery_by_id($id);
        $this->data['images'] = $this->admin_gallery_model->get_images_4_gallery_by_id($id);
        $this->data['title'] = $this->data['gallery'][0]->title;
        $this->getSeo('gallery', $id);
        $this->load->view('edit_gallery_page_view', $this->data);
    }

    function edit_banner_page($id = NULL)
    {
        $this->data['banner'] = $this->admin_gallery_model->get_banner_by_id($id);
        $this->data['title'] = $this->data['banner'][0]->title;
        $this->load->view('edit_banner_page_view', $this->data);
    }

    function delete_gallery($id = NULL)
    {
        $this->view_type = 2;
        $this->data['gallery'] = $this->admin_gallery_model->get_gallery_by_id($id);
        $this->admin_gallery_model->delete_gallery($id);
        $this->images_model->clear_all_parent_images($id, 'galleries');
        //логирование
        $this->admin_logs_model->add($id, 'gallery', $this->data['gallery'][0]->title, '/admin_gallery/edit_gallery_page/' . $id, 'Удаление');
    }


    function delete_image($gallery_id, $image_id)
    {
        $this->view_type = 2;
        $this->data['gallery'] = $this->admin_gallery_model->get_gallery_by_id($gallery_id);
        $result = $this->images_model->delete($image_id);
        //логирование
        $this->admin_logs_model->add($gallery_id, 'gallery', $this->data['gallery'][0]->title, '/admin_gallery/edit_gallery_page/' . $gallery_id, 'Удаление изображения из галереи');
        echo $result;
    }

}
