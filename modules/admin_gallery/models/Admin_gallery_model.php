<?
class Admin_gallery_model extends CI_Model {

private $db_table = 'gallery';

function Admin_gallery_model()
{
    parent::__construct();
}

function add_gallery($additional_data = array())
{
    $insert_result = array();
    $insert_result['success'] = $this->db->insert($this->db_table, $additional_data);
    $insert_result['insert_id'] = $this->db->insert_id();
    return $insert_result;
}

function add_banner($additional_data = array())
{
    $insert_result = array();
    $insert_result['success'] = $this->db->insert('banners', $additional_data);
    $insert_result['insert_id'] = $this->db->insert_id();
    return $insert_result;
}


function get_gallery_list()
{
    $this->db->select('*')->from($this->db_table)->order_by("title", "ASC");
    $data = $this->db->get();
    return $data->result();
}

function get_gallery_by_id($id = NULL)
{
    $this->db->select('*')->from($this->db_table)->where(array('id'=>$id))->order_by("title", "ASC");
    $data = $this->db->get();
    return $data->result();
}

function get_gallery_by_code($gallery_id = NULL)
{
    $this->db->select('*')->from($this->db_table)->where(array('gallery_id'=>$gallery_id))->order_by("title", "ASC");
    $data = $this->db->get();
    return $data->result();
}

function edit_gallery($id = NULL,$additional_data = array())
{
    $this->db->where('id', $id);
    return $this->db->update($this->db_table, $additional_data);
}

function edit_banner($id = NULL,$additional_data = array())
{
    $this->db->where('id', $id);
    return $this->db->update('banners', $additional_data);
}

function get_images_4_gallery_by_id($id = NULL)
{
    $this->db->select('*')->from('images')->where(array('parent_id'=>$id,'parent_type'=>'galleries'))->order_by("id", "DESC");
    $data = $this->db->get();
    return $data->result();
}

function delete_gallery($id = NULL)
{
    $this->db->where('id', $id);
    $this->db->delete($this->db_table);
}

function get_all_banners($show_disabled = false, $kids_page = false)
{
    $show_disabled_query = '';
    if(!$show_disabled)
        $show_disabled_query = ' AND number_in_row > 0 ';
    $kids_page_query = '';
    if(!$kids_page)
        $kids_page_query  = ' banners.kids is null and ';

    $query = "SELECT banners.id,title,text,path,link,number_in_row
            FROM banners,images
            WHERE 
            banners.id = images.parent_id
            AND $kids_page_query
            images.parent_type = 'banners'
            $show_disabled_query
            ORDER BY number_in_row ASC";
    $data = $this->db->query($query);
    return $data->result();
}

    function get_all_child_banners($show_disabled = false)
    {
        $show_disabled_query = '';
        if(!$show_disabled)
            $show_disabled_query = ' AND number_in_row > 0 ';
        $query = "SELECT banners.id,title,text,path,link,number_in_row
            FROM banners,images
            WHERE banners.kids = 1 and
            banners.id = images.parent_id
            AND
            images.parent_type = 'banners'
            $show_disabled_query
            ORDER BY number_in_row ASC";
        $data = $this->db->query($query);
        return $data->result();
    }

function get_banner_by_id($id = NULL)
{
    $query = "SELECT banners.id,title,text,path,link,number_in_row,kids
            FROM banners,images
            WHERE
            banners.id = images.parent_id
            AND
            images.parent_type = 'banners'
            AND banners.id = $id
            ORDER BY number_in_row ASC";
    $data = $this->db->query($query);
    return $data->result();
}

}?>