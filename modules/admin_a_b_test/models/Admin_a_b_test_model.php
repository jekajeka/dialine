<?
//Модель состояния страницы для АБ-тестирования
class Admin_a_b_test_model extends MY_Model
{
    function Admin_a_b_test_model()
    {
        parent::__construct();
        $this->db_table = 'a_b_test';
    }
    
    //изменение или создание отметки о статусе публикации записи
    function update($record_type,$id,$published = 0)
    {
        $search_params = array('record_type' => $record_type,'record_id' => $id);
        $this->db->select('*')
                ->from($this->db_table)
                ->where($search_params);
        $query = $this->db->get()->result();
        //Если запись уже существует - обновить ,если нет - добавить новую
        if(count($query)>0)
        {
            $this->db->set('status', $published, FALSE)
                    ->where($search_params)
                    ->update($this->db_table);
        }
        else
            $this->db->insert($this->db_table,array('record_type' => $record_type,'record_id' => $id,'status' => $published));
    }
    
    //получение значения отметки о статусе публикации записи
    function get_status($record_type,$id)
    {
        $search_params = array('record_type' => $record_type,'record_id' => $id);
        $this->db->select('*')
                ->from($this->db_table)
                ->where($search_params);
        return $this->db->get()->result();
    }
    
    //удаление записи о публикации
    function delete($record_type, $id)
    {
        $this->db->where(array('record_type' => $record_type,'record_id' => $id));
	$this->db->delete($this->db_table);
    }
}