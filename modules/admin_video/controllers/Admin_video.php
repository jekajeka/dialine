<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_video extends Admin_Controller  {
function __construct()
{
    parent::__construct();
    $this->load->model('video_model', '', TRUE);
    $this->data['title'] = 'Редактирование видео на главной';
    $this->logs_data_type = 'Видео';
}

/**
* Страница редактирование видеозаписи
* 
* @return html страница редакитрования
*/
function index()
{
    $this->data['node'] = $this->video_model->get_node(1);
    $this->load->view('edit_video_view',$this->data);
}

/**
* Редактирование видеозаписи
* @param integer $id айди записи
* @return html результат сохранения
*/
function edit_video($id = NULL)
{
    $this->view_type = 2;
    $additional_data = array('title' => $this->input->post('title'),
                            'content' => $this->input->post('content'),
                            'video_link' => $this->input->post('video_link'));
    $result = $this->video_model->edit_node($id,$additional_data);
    $this->data['result'] = $result;
    //логирование
    $this->admin_logs_model->add($id, $this->logs_data_type,$this->input->post('title'),'/admin_video/index','Редактирование');
    echo $this->load->view('admin/json_result_view',$this->data);
}

}?>