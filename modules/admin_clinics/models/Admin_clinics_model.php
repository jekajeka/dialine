<?
class Admin_clinics_model extends CI_Model {
private $enterprise_query_data_list = '';
private $db_table = 'clinics';

function Admin_clinics_model()
{
    parent::__construct();
}

function add_clinic($additional_data = array())
{
    $insert_result = array();
    $insert_result['success'] = $this->db->insert($this->db_table, $additional_data);
    $insert_result['insert_id'] = $this->db->insert_id();
    return $insert_result;
}

//���������� ������ ���� ������ ������������  �� �������� ������ ���� ������ � �������, ����� �� ������� �������������� � Frontend_clinics_model.php
function get_clinics_list()
{
    $this->db->select(array($this->db_table.'.*','published.published'));
    $this->db->from($this->db_table);
    $this->db->join('published', "published.type = 'clinics' AND published.record_id = ".$this->db_table.".id ",'left');
    return $this->db->get()->result();
}

function get_clinic($id = NULL)
{
    $query = "SELECT ".$this->db_table.".id,".$this->db_table.".path,title,subtitle,address,district,phones,work_time,coordinates, text,images.path as image_path,year
                FROM ".$this->db_table."
                LEFT JOIN images ON
                images.parent_id = ".$this->db_table.".id
                AND images.parent_type = 'clinics'
                WHERE ".$this->db_table.".id = $id";
    $data = $this->db->query($query);
    return $data->result();   
}

function edit_clinic($id = NULL,$additional_data = array())
{
    $this->db->where('id', $id);
    return $this->db->update($this->db_table, $additional_data);
}


function delete_clinic($id = NULL)
{
    $this->db->where('id', $id);
    $this->db->delete($this->db_table);
}


}

?>