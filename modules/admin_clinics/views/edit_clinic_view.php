<div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?=$title?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a title="Удалить клинику" href="/admin_clinics/delete_clinic/<?=$clinic[0]->id?>" class="add-link"><i class="fa fa-trash"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <input type="hidden" id="id" value="<?=$clinic[0]->id?>">
                    <form id="edit_clinic" data-parsley-validate class="form-horizontal form-label-left">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Название клиники <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="title" class="form-control col-md-7 col-xs-12"   name="title" placeholder="" required="required" type="text" value="<?=$clinic[0]->title?>">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">Изображение <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <img src="<?=$clinic[0]->image_path?>">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">&nbsp; <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input name="image" type="file" id="image">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="path">Адрес страницы <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="path" class="form-control col-md-7 col-xs-12"   name="path" placeholder="" required="required" type="text" value="<?=$clinic[0]->path?>">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="year">Год основания <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="year" class="form-control col-md-7 col-xs-12"   name="year" placeholder="" required="required" type="text" value="<?=$clinic[0]->year?>">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="subtitle">Подзаголовок <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="subtitle" class="form-control col-md-7 col-xs-12"   name="subtitle" placeholder="" required="required" type="text" value="<?=$clinic[0]->subtitle?>">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Физический адрес <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="address" class="form-control col-md-7 col-xs-12"   name="address" placeholder="" required="required" type="text" value="<?=$clinic[0]->address?>">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="district">Район <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="district" class="form-control col-md-7 col-xs-12"   name="district" placeholder="" required="required" type="text" value="<?=$clinic[0]->district?>">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="coordinates">Координаты <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="coordinates" class="form-control col-md-7 col-xs-12"   name="coordinates" placeholder="" required="required" type="text" value="<?=$clinic[0]->coordinates?>">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phones">Телефоны (по телефону на строку)</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea name="phones" class="form-control" placeholder="" rows="3" style="width: 100%; height: 135px;resize: none" id="phones"><?=$clinic[0]->phones?></textarea>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="work_time">Рабочее время (по периоду на строку)</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea name="work_time" class="form-control" placeholder="" rows="3" style="width: 100%; height: 135px;resize: none" id="work_time"><?=$clinic[0]->work_time?></textarea>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="text">Описание</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea name="text" class="form-control" placeholder="" rows="3" style="width: 100%; height: 135px;resize: none" id="text"><?=$clinic[0]->text?></textarea>
                        </div>
                      </div>
			<?if(isset($seo)):?>
						<?$this->load->view('admin/seo', $seo);?>
					  <?endif;?>
			<?=$this->load->view('admin/publish_view');?>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="add_user_final">
                          <button type="submit" class="btn btn-success">Сохранить</button>
                        </div>
                      </div>
                    </form>  
                  </div>
                </div>
              </div>
            </div>
</div>
<script src="/admin_js/admin_info/edit_clinic.js"></script>
