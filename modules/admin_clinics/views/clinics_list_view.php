<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?=$this->data['title']?></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a href="/admin_clinics/add_clinic_page" class="add-link"><i class="fa fa-plus"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
                      <thead>
                        <tr>
                          <th><input type="checkbox" id="check-all" class="flat"></th>
                          <th>id</th>
                          <th>Название</th>
                          <th>Район</th>
                          <th>Опубликовано</th>
                          <th>Страница клиники</th>
                        </tr>
                      </thead>


                      <tbody>
                        <?foreach($clinics as $clinic ){?>
                        <tr>
                          <td><input type="checkbox" class="flat" name="table_records"></td>
                          <td><?=$clinic->id?></td>
                          <td><a href="/admin_clinics/edit_clinic_page/<?=$clinic->id?>"><?=$clinic->title?></a></td>
                          <td><a href="/admin_clinics/edit_clinic_page/<?=$clinic->id?>"><?=$clinic->district?></a></td>
                          <td>
                                <?
                                if($clinic->published)
                                                $published = 'Опубликовано';
                                if($clinic->published == 0)
                                                $published = 'Не опубликовано';
                                if($clinic->published == NULL)
                                                $published = 'Опубликовано (галочка не стоит)';
                                echo $published;
                                ?>
                          </td>
                          <td>
                                <a target="_blank" href="/clinic/<?=$clinic->path?>">Страница клиники на сайте</a>
                          </td>
                        </tr>
                        <?}?>
                      </tbody>
                    </table>
                  </div>
                </div>
</div>
              