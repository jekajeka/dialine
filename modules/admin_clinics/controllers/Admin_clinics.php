<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_clinics extends Admin_Controller  {
function __construct()
{
    parent::__construct();
    $this->load->model('admin_clinics_model', '', TRUE);
    $this->load->model('admin_published/admin_published_model', '', TRUE);
    $this->data['title'] = 'Редактирование клиник';
}

function add_clinic_page()
{
    $this->data['title'] = 'Добавить клинику';
    //$this->data['medical_directions'] = $this->admin_clinics_model->get_all_medical_directions_list();
    $this->load->view('add_clinic_view',$this->data);
}

function add_clinic()
{
    $this->load->model('images_model', '', TRUE);
    $this->view_type = 2;
    $additional_data = array('title' => $this->input->post('title'),
                            'path' => $this->input->post('path'),
                            'subtitle' => $this->input->post('subtitle'),
			    'address' => $this->input->post('address'),
			    'phones' => $this->input->post('phones'),
                            'work_time' => $this->input->post('work_time'),
                            'text' => $this->input->post('text'),
			    'year' => $this->input->post('year'),
			    'district' => $this->input->post('district'),
                            'coordinates' => $this->input->post('coordinates'),
			);
    $result = $this->admin_clinics_model->add_clinic($additional_data);
    $this->data['result'] = $result['success'];
    if($this->data['result'])
    {
	$config['upload_path'] = './uploads/';
	$config['allowed_types'] = 'gif|jpg|png';
	$config['max_size']	= '10000';
	$config['max_width']  = '2000';
	$config['max_height']  = '1500';
	$this->load->library('upload', $config);
	if ( ! $this->upload->do_upload('image'))
	{
	    $this->data['result'] =  $this->upload->display_errors();
	}	
	else
	{
	    $data = array('upload_data' => $this->upload->data());
	    $this->data['result'] = $this->images_model->add_image($result['insert_id'],'clinics','/uploads/'.$data['upload_data']['file_name']);
	}
	//Извлечение состояния публикации клиники
	if($this->input->post('published') == 'on')
	    $published = 1;
	else
	    $published = 0;
	$this->admin_published_model->update_published('clinics', $result['insert_id'],$published);
	//логирование добавления записи
	$this->admin_logs_model->add($result['insert_id'], 'clinics',$this->input->post('title'),'/admin_clinics/edit_clinic_page/'.$result['insert_id'],'Добавление');
    }
    echo $this->load->view('admin/json_result_view',$this->data);
}



function edit_clinic_page($id)
{
    $this->data['clinic'] = $this->admin_clinics_model->get_clinic($id);
    $this->data['title'] = 'Изменить клинику "'.$this->data['clinic'][0]->title.'"';
    $this->data['published'] = $this->admin_published_model->get_status('clinics',$id);
    $this->getSeo('clinics', $id);
    $this->load->view('edit_clinic_view',$this->data);
}

function all_clinics_page()
{
    $this->data['clinics'] = $this->admin_clinics_model->get_clinics_list();
    $this->data['title'] = 'Все клиники';
    $this->data['users_list'] = $this->load->view('clinics_list_view',$this->data,TRUE);
    $this->load->view('admin/base_node_dashboard_view');
}

function edit_clinic($id = NULL)
{
    $this->load->model('images_model', '', TRUE);
    $this->view_type = 2;
    $additional_data = array('title' => $this->input->post('title'),
                            'path' => $this->input->post('path'),
                            'subtitle' => $this->input->post('subtitle'),
			    'address' => $this->input->post('address'),
			    'phones' => $this->input->post('phones'),
                            'work_time' => $this->input->post('work_time'),
                            'text' => $this->input->post('text'),
			    'year' => $this->input->post('year'),
			    'district' => $this->input->post('district'),
                            'coordinates' => $this->input->post('coordinates'),
			);
    $result = $this->admin_clinics_model->edit_clinic($id,$additional_data);
    $this->saveSeo('clinics', $id);
    //Извлечение состояния публикации клиники
    if($this->input->post('published') == 'on')
        $published = 1;
    else
        $published = 0;
    $this->admin_published_model->update_published('clinics', $id,$published);
    //логирование
    $this->admin_logs_model->add($id, 'clinics',$this->input->post('title'),'/admin_clinics/edit_clinic_page/'.$id,'Редактирование');
    $this->data['result'] = $result;
    if($this->data['result'])
    {
	$config['upload_path'] = './uploads/';
	$config['allowed_types'] = 'gif|jpg|png';
	$config['max_size']	= '10000';
	$config['max_width']  = '2000';
	$config['max_height']  = '1500';
	$this->load->library('upload', $config);
	if ( ! $this->upload->do_upload('image'))
	{
	    //$this->data['result'] =  $this->upload->display_errors();
	}	
	else
	{
	    $this->images_model->clear_all_parent_images($id,'clinics');
	    $data = array('upload_data' => $this->upload->data());
	    $this->data['result'] = $this->images_model->add_image($id,'clinics','/uploads/'.$data['upload_data']['file_name']);
	}
    }
    echo $this->load->view('admin/json_result_view',$this->data);
}

function delete_clinic($id)
{
    $this->load->model('images_model', '', TRUE);
    $this->view_type = 2;
    $this->data['clinic'] = $this->admin_clinics_model->get_clinic($id);
    $this->admin_clinics_model->delete_clinic($id);
    $this->admin_published_model->delete_published('clinics', $id);
    $this->images_model->clear_all_parent_images($id,'clinics');
    //логирование
    $this->admin_logs_model->add($id, 'clinics',$this->data['clinic'][0]->title,'/admin_clinics/edit_clinic_page/'.$id,'Удаление');
}


}?>
