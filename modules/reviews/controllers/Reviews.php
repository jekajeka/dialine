<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reviews extends Frontend_Controller  {
function __construct()
{
    parent::__construct();
    $this->load->model('admin_reviews/admin_reviews_model', '', TRUE);
    $this->data['title'] = 'Отзывы о клиниках и врачах/Диалайн, Волгоград';
}


function index()
{
    $this->data['reviews'] = $this->admin_reviews_model->get_moderated_reviews();
    $this->load->view('reviews_view.php',$this->data);
}

function add_review()
{
    $this->view_type = 2;
    $additional_data = array('name' => $this->input->post('name',TRUE),
                            'phone' => $this->input->post('phone',TRUE),
                            'clinic_id' => $this->input->post('clinic',TRUE),
                            'moderated' => 0,
			    'text' => $this->input->post('text',TRUE),
			    'respondent_name' => '',
			    'response' => '',
                            'date_review'=>date("Y-m-d"));
    $result = $this->admin_reviews_model->add_review($additional_data);
    $this->data['result'] = $result['success'];
    $this->load->library('email');
    $this->email->from('notify@dialine.org', 'Клиника Диалайн');
    $this->email->to('dzhumanova.d@dialine.org,shtirkhunova.e@dialine.org');
    $message = 'Имя: '.$this->input->post('name',TRUE).'
Телефон:  '.$this->input->post('phone',TRUE).'
Клиника:  '.$this->input->post('clinic_name',TRUE).'
text:  '.$this->input->post('text',TRUE);
    if($this->input->post('order'))
        $message .= '
'.$this->input->post('order');
    $this->email->subject('Новый отзыв (диалайн.рф)');
    $this->email->message($message);
    $this->email->send();
}

function article($id = NULL)
{
    $this->data['review'] = $this->admin_reviews_model->get_reviews_frontend($id);
    $this->load->view('review_article_view.php',$this->data);
}

function articles($id = NULL)
{
    show_404('page');
}


}?>