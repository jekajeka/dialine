<div class="row row_breadcrumbs">
    <div class="container">
        <ul>
            <li><a href="">Главная</a></li> | <li><a href="/reviews">Отзывы о клиниках и врачах</a></li>
        </ul>
    </div>
</div>
<div class="row row_title_h1">
    <div class="container">
        <h1>Отзывы о клиниках и врачах</h1>
    </div>
</div>
<div class="row help_row">
    <div class="container">
        <?foreach($reviews as $review){?>
        <a class="reviews_page_link" href="/reviews/article/<?=$review->id?>">
            <div class="reviews_man"><?=$review->name?></div>
            <div class="reviews_page_text">
                <p>
                    <?=mb_substr(strip_tags($review->text),0,100).'...'?>
                </p>
            </div>
            <div class="reviews_page_clinic"><?=$review->title?> <?=$review->district?></div>
            <div class="reviews_page_date"><?=date("d.m.Y", strtotime($review->date_review))?></div>
        </a>
        <?}?>
    </div>
</div>