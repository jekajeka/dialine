<div class="row row_breadcrumbs">
    <div class="container">
        <ul>
            <li><a href="">Главная</a></li> | <li><a href="/reviews">Отзывы о клиниках и врачах</a></li> | <li><a href="/reviews/article/<?=$review[0]->id?>">Отзыв, <?=$review[0]->title?> <?=$review[0]->district?>, <?=$review[0]->name?></a></li>
        </ul>
    </div>
</div>
<div class="row row_title_h1">
    <div class="container">
        <h1>Отзыв</h1>
    </div>
</div>
<div class="row help_row">
    <div class="container">
        <h2 class="name_review"><?=$review[0]->name?></h2>
        <h3 class="clinic_review"><?=$review[0]->title?> <?=$review[0]->district?>&nbsp;&nbsp;&nbsp;<?=date("d.m.Y", strtotime($review[0]->date_review))?></h2>
        <div class="review_body">
            <p>
                <?=$review[0]->text?>
            </p>
        </div>
        <?if (($review[0]->respondent_name)) {?>
        <h2 class="name_review"><?=$review[0]->respondent_name?></h2>
        <div class="review_body">
            <p>
                <?=$review[0]->response?>
            </p>
        </div>
        <?}?>
        <a class="back_to_reviews" title="Отзывы о клинике Диалайн, Волгоград" href="/reviews">Назад, ко всем отзывам</a>
    </div>
</div>