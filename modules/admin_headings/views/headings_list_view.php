<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2><?=$this->data['title']?></small></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a href="/admin_clinics/add_clinic_page" class="add-link"><i class="fa fa-plus"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>Тип записи</th>
                        <th>Рубрика</th>
                        <th>Редактировать</th>
                    </tr>
                </thead>

                <tbody>
                <?foreach($headings as $heading ){?>
                    <tr>
                        <td><?=$heading->id?></td>
                        <td><?
                            if ($heading->headings_type == 1)
                            echo 'Новостные разделы';
                            else
                            echo 'Разделы подготовки исследований';
                            ?>
                        </td>
                        <td><?=$heading->title?></td>
                        <td><a target="_blank" href="/admin_headings/edit_page/<?=$heading->id?>">Редактировать</a></td>
                    </tr>
                <?}?>
                </tbody>
            </table>
        </div>
    </div>
</div>