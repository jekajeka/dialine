<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?=$title?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a title="Удалить" href="/admin_headings/delete/<?=$heading[0]->id?>" class="add-link"><i class="fa fa-trash"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <input type="hidden" id="id" value="<?=$heading[0]->id?>">
                    <form id="edit_headings" data-parsley-validate class="form-horizontal form-label-left">
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="service_title">Название рубрики <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="title" class="form-control col-md-7 col-xs-12" value="<?=$heading[0]->title?>"  name="title" placeholder="" required="required" type="text">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="headings_type">Тип записи <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="headings_type"  name="headings_type">
                                    <?
                                    if ($heading[0]->headings_type == 1)
                                    {?>
                                    <option selected="selected" value="1">Новостные разделы</option>
                                    <option value="2">Разделы подготовки исследований</option>
                                    <?}
                                    else{?>
                                        <option value="1">Новостные разделы</option>
                                    <option selected="selected" value="2">Разделы подготовки исследований</option>
                                    <?}?>
                                </select>
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="add_user_final">
                                <button type="submit" class="btn btn-success">Сохранить</button>
                            </div>
                        </div>
                    </form>  
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/admin_js/admin_headings/edit_headings.js"></script>
