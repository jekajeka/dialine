<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_headings extends Admin_Controller  {
function __construct()
{
    parent::__construct();
    $this->load->model('admin_headings_model', '', TRUE);
    $this->data['title'] = 'Рубрики сайта';
}

function index()
{
    $this->data['headings'] = $this->admin_headings_model->get_all();
    $this->data['title'] = 'Все рубрики сайта';
    $this->data['users_list'] = $this->load->view('headings_list_view',$this->data,TRUE);
    $this->load->view('admin/base_node_dashboard_view');
}

function  edit_page($id = NULL)
{
    $this->data['heading'] = $this->admin_headings_model->get_node($id);
    $this->data['title'] = 'Редактировать рубрику '.$this->data['heading'][0]->title;
    $this->load->view('edit_heading_view',$this->data);
}

function  add_page()
{
    $this->data['title'] = 'Добавить рубрику';
    $this->load->view('add_heading_view',$this->data);
}

function  edit($id = NULL)
{
    $this->view_type = 2;
    $additional_data = array('title' => $this->input->post('title'),
                            'headings_type' => $this->input->post('headings_type'));
    $result = $this->admin_headings_model->edit_node($id,$additional_data);
    $this->data['result'] = $result;
    //логирование изменения записи
    $this->admin_logs_model->add($id, 'Рубрики сайта',$this->input->post('title'),'/admin_headings/edit_page/'.$id,'Редактирование');
    echo $this->load->view('admin/json_result_view',$this->data);
}

function  add()
{
    $this->view_type = 2;
    $additional_data = array('title' => $this->input->post('title'),
                            'headings_type' => $this->input->post('headings_type'));
    $result = $this->admin_headings_model->add_node($additional_data);
    $this->data['result'] = $result['success'];
    //логирование изменения записи
    $this->admin_logs_model->add($result['insert_id'], 'Рубрики сайта',$this->input->post('title'),'/admin_headings/edit_page/'.$result['insert_id'],'Редактирование');
    echo $this->load->view('admin/json_result_view',$this->data);
}

function delete($id = NULL)
{
    $this->view_type = 2;
    $this->data['heading'] = $this->admin_headings_model->get_node($id);
    $this->admin_headings_model->delete_node($id);
    //логирование удаления меднаправления
    $this->admin_logs_model->add($id, 'Рубрики сайта',$this->data['heading'][0]->title,'/admin_headings/edit_page/'.$id,'Удаление');
}


}?>