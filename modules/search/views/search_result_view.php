<div class="row row_breadcrumbs">
    <div class="container">
        <ul>
            <li><a href="/">Главная</a></li> | <li><a href="/search">Поиск</a></li>
        </ul>
    </div>
</div>
<div class="row row_speciality_rule">
    <div class="container">
        <div class="text_search_speciality_wrap">
            <form action="/search" method="POST">
                <input id="full_text_search" name="search_string" value="<?=$search_string?>" placeholder="Начните поиск, например, хирург" type="text">
            </form>
        </div>
    </div>
</div>
<div class="row specialists_row">
    <div class="container search_result">
        <?foreach($services as $service){?>
        <h2><a href="/<?=$service->service_path?>"><?=$service->service_rubric_title?></a></h2>
        <p><? echo mb_substr(strip_tags($service->text),0,300,'UTF-8').'...';?></p>
        <?}?>
        <?foreach($simple_services as $simple_service){?>
        <h2><a href="/services/additional/<?=$simple_service->path?>"><?=$simple_service->title?></a></h2>
        <p><? echo mb_substr(strip_tags($simple_service->text),0,300,'UTF-8').'...';?></p>
        <?}?>
        <?foreach($programs as $program){?>
        <h2><a href="/komplex_programm/<?=$program->path?>"><?=$program->title?></a></h2>
        <p><?=$program->target?></p>
        <?}?>
        <?foreach($doctors as $doctor){?>
        <h2><a href="/doctors/<?=$doctor->direct_path?>/<?=$doctor->translite?>"><?=$doctor->name?></a></h2>
        <p><?=$doctor->specialisation?></p>
        <?}?>
        <?foreach($clinics as $clinic){?>
        <h2><a href="/clinic/<?=$clinic->path?>"><?=$clinic->title?></a></h2>
        <p><? echo mb_substr(strip_tags($clinic->text),0,300,'UTF-8').'...';?></p>
        <?}?>
        <?foreach($news as $single_news){?>
        <h2><a href="/articles/<?=$single_news->path?>"><?=$single_news->title?></a></h2>
        <p><?=$single_news->preview_text?></p>
        <?}?>
        <?foreach($rules as $rule){?>
        <h2><a href="/info/podgotovka-k-issledovaniyam"><?=$rule->title?></a></h2>
        <p><? echo mb_substr(strip_tags($rule->text),0,300,'UTF-8').'...';?></p>
        <?}?>
        <?foreach($laws as $law){?>
        <h2><a href="/info/pravovaya_informaciya"><?=$law->title?></a></h2>
        <?}?>
        <?foreach($control_organisation as $control){?>
        <h2><a href="/info/kontroliruyushchie-organizatsii"><?=$control->title?></a></h2>
        <p><? echo mb_substr(strip_tags($control->text),0,300,'UTF-8').'...';?></p>
        <?}?>
        <?=$no_results?>
    </div>
</div>