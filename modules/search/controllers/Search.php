<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends Frontend_Controller  {
function __construct()
{
    parent::__construct();
    $this->load->model('news/news_frontend_model', '', TRUE);
    $this->load->model('doctors/frontend_doctors_model', '', TRUE);
    $this->load->model('services/services_model', '', TRUE);
    $this->load->model('clinic/frontend_clinics_model', '', TRUE);
    $this->load->model('info/frontend_info_model', '', TRUE);
    $this->title = 'Поиск/Диалайн, Волгоград';
    $this->data['news'] = array();
    $this->data['doctors'] = array();
    $this->data['simple_services'] = array();
    $this->data['programs'] = array();
    $this->data['services'] = array();
    $this->data['clinics'] = array();
    
}


function index()
{
    $search = str_replace(' ','%',$this->input->post('search_string',TRUE));
    $this->data['title'] = 'Поиск';
    $this->data['search_string'] = $this->input->post('search_string',TRUE);
    $this->data['no_results'] = '';
    if(strlen($this->input->post('search_string',TRUE))>0)
    {
    $this->data['news'] = $this->news_frontend_model->get_news_by_search_text($search);
    $this->data['doctors'] = $this->frontend_doctors_model->search_doctors_by_text($search,' doctors.id ');
    $this->data['simple_services'] = $this->services_model->get_simple_services_by_text($search);
    $this->data['programs'] = $this->services_model->get_programs_by_text($search);
    $this->data['services'] = $this->services_model->get_services_by_text($search);
    $this->data['clinics'] = $this->frontend_clinics_model->get_clinics_by_text($search);
    $this->data['rules'] = $this->frontend_info_model->search_rules_by_text($search);
    $this->data['laws'] = $this->frontend_info_model->search_laws($search);
    $this->data['control_organisation'] = $this->frontend_info_model->search_control_organisation($search);
    if(!count(array_merge($this->data['news'],$this->data['doctors'],$this->data['simple_services'],$this->data['programs'],$this->data['services'],$this->data['clinics'],$this->data['rules'],$this->data['laws'],$this->data['control_organisation'])))
        $this->data['no_results'] = '<h2>По вашему запросу ничего не найдено</h2>';
    //$this->data['rubrics'] = $this->frontend_doctors_model->get_rubrics();
    //$this->data['clinics'] = $this->frontend_doctors_model->get_clinics();
    }
    $this->load->view('search_result_view',$this->data);
}


}