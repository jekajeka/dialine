<?php
class Frontend_info_model extends CI_Model {
	private $enterprise_query_data_list = '';

	function Frontend_info_model()
	{
		parent::__construct();
	}

	function get_discount()
	{
		$query = "SELECT
					*
					FROM discount_page
					ORDER BY id ASC LIMIT 1";
		$data = $this->db->query($query);
		return $data->result();   
	}

	function edit_discount($discount_id=1,$additional_data= array())
	{
		$this->db->where('id', $discount_id);
		return $this->db->update('discount_page', $additional_data);
	}

	function get_komplex_program_text_header($id = 1)
	{
		$query = "SELECT
			*
			FROM complex_page
			WHERE id = $id
			ORDER BY id ASC LIMIT 1";
		$data = $this->db->query($query);
		return $data->result();  
	}

	function get_komplex_program_advantages()
	{
		$query = "SELECT
			title,text,images.path
			FROM advantage,images
			WHERE
			images.parent_id=advantage.id
			AND
			images.parent_type='advantages'
			ORDER BY advantage.id ASC";
		$data = $this->db->query($query);
		return $data->result();   
	}
	
	/**
	* Выборка медицинских программ
	* @param age - возраст - или adults или kids
	* @return вывод результатов сохранения
	*/
	function get_all_medical_programs($age = "adults")
	{
		$this->db->select(array('medical_program.*','images.path as preview_path','published.published'));
		$this->db->from('medical_program');
        $this->db->join('images', "images.parent_id=medical_program.id AND images.parent_type='med_program_preview'",'left');
		$this->db->join('published', "published.type = 'medical_program' AND published.record_id = medical_program.id ",'left');
		$this->db->where('(published.published=1 OR published.published IS NULL)');
		if($age == 'kids')
			$this->db->where('isForKids',1);
		else
			$this->db->where('(isForKids = 0 OR isForKids IS NULL)');
		$this->db->order_by('medical_program.id','ASC');
		return $this->db->get()->result();
	}

	function get_compared_programs($compared_list = '',$age = "adults")
	{
		
		$this->db->select(array('medical_program.*','published.published'));
		$this->db->from('medical_program');
		$this->db->join('published', "published.type = 'medical_program' AND published.record_id = medical_program.id ",'left');
		$this->db->where('(published.published=1 OR published.published IS NULL)');
		if($compared_list!='')
			$this->db->where(" medical_program.id IN ($compared_list) ");
		if($age == 'kids')
			$this->db->where('isForKids',1);
		else
			$this->db->where('(isForKids = 0 OR isForKids IS NULL)');
		$this->db->order_by('medical_program.id','ASC');
		return $this->db->get()->result();
	}

	//получение основной записи медицинской программы
	function get_single_medical_program($path = NULL)
	{
		$this->db->select(array('medical_program.*','images.path as image_path','published.published'));
		$this->db->from('medical_program');
		$this->db->join('images', "images.parent_id=medical_program.id AND images.parent_type='med_program_banner'",'left');
		$this->db->join('published', "published.type = 'medical_program' AND published.record_id = medical_program.id ",'left');
		$this->db->where('(published.published=1 OR published.published IS NULL)');
		$this->db->where('medical_program.path',$path);
		$this->db->order_by('medical_program.id','ASC');
		return $this->db->get()->result();
	}

	function get_oms($id = 1)
	{
		$data = $this->db->get_where('oms', array('id' => $id), 1);
		return $data->result(); 
	}

	function get_oms_directions()
	{
		$data = $this->db->get('oms_directions');
		return $data->result(); 
	}

	function get_all_rules()
	{
		$query = 'SELECT rules.id, rules.title, rules.text, headings_node.headings_id
				FROM rules, headings_node, headings
				WHERE headings_node.node_id = rules.id
				AND headings_node.headings_id = headings.id
				AND headings.headings_type =2
				ORDER BY rules.id ASC';
		$data = $this->db->query($query);
		return $data->result();
	}


	function get_page($id = NULL)
	{
		$data = $this->db->get_where('complex_page', array('id' => $id), 1);
		return $data->result();
	}

	function search_rules_by_text($search_text = '')
	{
		$query = "SELECT DISTINCT rules.id, rules.title, rules.text, headings_node.headings_id
				FROM rules, headings_node, headings
				WHERE headings_node.node_id = rules.id
				AND headings_node.headings_id = headings.id
				AND headings.headings_type =2
				AND (rules.title LIKE '%$search_text%'
				OR rules.text LIKE '%$search_text%')
				GROUP BY rules.id
				ORDER BY rules.id ASC";
		$data = $this->db->query($query);
		return $data->result();
	}

	function get_all_clinics()
	{
		$query = "SELECT clinics.title, clinics.subtitle,clinics.address,clinics.coordinates, clinics.text,images.path as image_path,clinics.id,clinics.path,clinics.phones,clinics.work_time,clinics.district
					FROM clinics
					LEFT JOIN images ON
					images.parent_id = clinics.id
					AND images.parent_type = 'clinics'";
		$data = $this->db->query($query);
		return $data->result();
	}

	function get_med_rubrics()
	{
		/*
		 *потом удалить полностью .если не будет нареканий
		 *$query = "SELECT count( * ) as services_qty , medical_directions.service_rubric_title,rubric,medical_directions.id
			FROM services,medical_directions
			WHERE services.rubric=medical_directions.id
			GROUP BY rubric
			ORDER BY medical_directions.service_rubric_title ASC";*/
		//$data = $this->db->query($query);
		//return $data->result();
		$this->db->select(array('count( * ) as services_qty','medical_directions.service_rubric_title','rubric','medical_directions.id'));
		$this->db->from('medical_directions');
		$this->db->join('services', "services.rubric=medical_directions.id",'INNER');
		$this->db->join('published', "published.type = 'medical_directions' AND published.record_id = medical_directions.id ",'left');
		$this->db->where('(published.published=1 OR published.published IS NULL)');
		$this->db->group_by("rubric");
		$this->db->order_by("medical_directions.service_rubric_title", "asc");
		return $this->db->get()->result();
	}


	function get_med_service_type()
	{
		$query = "SELECT service_type, rubric
			FROM `services`
			WHERE 1
			GROUP BY service_type,rubric
			ORDER BY id, `services`.`rubric` ASC";
		$data = $this->db->query($query);
		return $data->result();
	}

	function get_services()
	{
		$query = "SELECT
			services.id,services.title,services.price,services.rubric,services.service_type,services.link
			FROM
			services
			LEFT JOIN
			service_link
			ON service_link.med_id=services.med_id
			ORDER BY services.id ASC";
		$data = $this->db->query($query);
		return $data->result();
	}

	function get_services_by_text_title($search_text)
	{
		$s_text = str_replace(' ','%',$search_text);
		$query = "SELECT
				services.id,services.title,services.price,services.rubric,services.service_type,services.link
				FROM
				services
				LEFT JOIN
				service_link
				ON service_link.med_id=services.med_id
				WHERE services.title LIKE '%$s_text%'
				ORDER BY services.id ASC";
		$data = $this->db->query($query);
		return $data->result();
	}

	function get_med_rubrics_by_text_title($search_text)
	{
		$s_text = str_replace(' ','%',$search_text);
		/*$query = "SELECT count( * ) as services_qty , medical_directions.service_rubric_title,rubric
			FROM services,medical_directions
			WHERE services.rubric=medical_directions.id
			AND services.title LIKE '%$s_text%'
			GROUP BY rubric
			ORDER BY medical_directions.service_rubric_title ASC";*/
		//$data = $this->db->query($query);
		//return $data->result();
		$this->db->select(array('count( * ) as services_qty', 'medical_directions.service_rubric_title','rubric'));
		$this->db->from('medical_directions');
		$this->db->join('services', "services.rubric=medical_directions.id",'INNER');
		$this->db->join('published', "published.type = 'medical_directions' AND published.record_id = medical_directions.id ",'left');
		$this->db->where('(published.published=1 OR published.published IS NULL)');
		$this->db->where("services.title LIKE '%$s_text%'");
		$this->db->group_by("rubric");
		$this->db->order_by("medical_directions.service_rubric_title", "asc");
		return $this->db->get()->result();
	}

	function get_med_service_type_by_text_title($search_text)
	{
		$s_text = str_replace(' ','%',$search_text);
		$query = "SELECT service_type, rubric
					FROM `services`
					WHERE 
					services.title LIKE '%$s_text%'
					GROUP BY service_type,rubric
					ORDER BY id, `services`.`rubric` ASC";
		$data = $this->db->query($query);
		return $data->result();
	}

	function get_licenses()
	{
		$query = "SELECT title,text,path
				FROM license,files
				WHERE
				files.parent_id=license.id
				AND
				files.parent_type='license'
				ORDER BY title";
		$data = $this->db->query($query);
		return $data->result();
	}

	function get_info_data($id = NULL)
	{
		$data = $this->db->get_where('complex_page', array('id' => $id));
		return $data->result();
	}

	function get_info_data_url($url = NULL)
	{
		$data = $this->db->get_where('complex_page', array('path' => $url));
		return $data->result();
	}	

	function get_laws()
	{
		$query = "SELECT title,path
			FROM law_info,files
			WHERE
			files.parent_id=law_info.id
			AND
			files.parent_type='law'
			ORDER BY title";
		$data = $this->db->query($query);
		return $data->result();
	}
	
	/*!
	@brief поиск правовой информации по ключевой фразе 
	@details Антон Вопилов 16.10.2017
	@param[in] $search поисковая строка
	@return массив данных с правовой информацией
	*/
	function search_laws($search = '')
	{
		$this->db->select(array('*'));
		$this->db->from('law_info');
		$this->db->where("title LIKE '%$search%'");
		return $this->db->get()->result();
	}
	
	/*!
	@brief поиск информации со страницы Контакты контролирующих организаций
	@details Антон Вопилов 17.10.2017
	@param[in] $search поисковая строка
	@return массив данных с страницы Контакты контролирующих организаций, если на ней что-то нашлось
	*/
	function search_control_organisation($search = '')
	{
		$this->db->select(array('*'));
		$this->db->from('complex_page');
		$this->db->where("id = 4");
		$this->db->where("title LIKE '%$search%' OR text LIKE '%$search%'");
		return $this->db->get()->result();
	}

	function get_services_by_params($age = 'all', $speciality_id = "all")
	{
		$spec_query = '';
		$age_query = '';
		$and_query = '';
		$additional_query = '';
		if($speciality_id != 'all')
			$spec_query = 'services.rubric = '.$speciality_id.' ';
		if($age == 'adult')
			$age_query = " services.med_id NOT LIKE '%Д%' ";
		if($age == 'child')
			$age_query = " services.med_id LIKE '%Д%' ";
		if($spec_query && $age_query)
			$additional_query = " WHERE $spec_query AND $age_query";
		else if($spec_query || $age_query)
			$additional_query = " WHERE $spec_query $age_query";
		
		$query = "SELECT
				services.id,services.title,services.price,services.rubric,services.service_type,services.link
				FROM
				services
				LEFT JOIN
				service_link
				ON service_link.med_id=services.med_id
				$additional_query
				ORDER BY services.id ASC";
		$data = $this->db->query($query);
		return $data->result();
	}

	function get_med_rubrics_by_params($age = 'all', $speciality_id = "all")
	{
		$spec_query = '';
		$age_query = '';
		$and_query = '';
		$additional_query = '';
		if($speciality_id != 'all')
			$spec_query = 'AND services.rubric = '.$speciality_id.' ';
		if($age == 'adult')
			$age_query = " AND services.med_id NOT LIKE '%Д%' ";
		if($age == 'child')
			$age_query = " AND services.med_id LIKE '%Д%' ";
		if($spec_query || $age_query)
			$additional_query = "  $spec_query $age_query";
		$query = "SELECT count( * ) as services_qty , medical_directions.service_rubric_title,rubric
					FROM services,medical_directions
					WHERE services.rubric=medical_directions.id
					$additional_query
					GROUP BY rubric
					ORDER BY medical_directions.service_rubric_title ASC";
		$data = $this->db->query($query);
		return $data->result();
	}

	function get_med_service_type_by_params($age = 'all', $speciality_id = "all")
	{
		$spec_query = '';
		$age_query = '';
		$and_query = '';
		$additional_query = '';
		if($speciality_id != 'all')
			$spec_query = 'services.rubric = '.$speciality_id.' ';
		if($age == 'adult')
			$age_query = " services.med_id NOT LIKE '%Д%' ";
		if($age == 'child')
			$age_query = " services.med_id LIKE '%Д%' ";
		if($spec_query && $age_query)
			$additional_query = " WHERE $spec_query AND $age_query";
		else if($spec_query || $age_query)
			$additional_query = " WHERE $spec_query $age_query";
		$query = "SELECT service_type, rubric
					FROM `services`
					$additional_query
					GROUP BY service_type,rubric
					ORDER BY id, `services`.`rubric` ASC";
		$data = $this->db->query($query);
		return $data->result();
	}


}
