<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Info extends Frontend_Controller  {
	
	private $no_complex_programs = '<h2>Не выбрано комплексных программ для сравнения, <a style="color:#0abab5" href="/komplex_programm">перейти к списку программ</a></h2>';
	function __construct()
	{
		parent::__construct();
		$this->load->model('frontend_info_model', '', TRUE);
		$this->load->model('clinic/frontend_clinics_model','', TRUE);
		$this->data['clinics'] = $this->frontend_clinics_model->get_clinics_list();
		$this->title = 'Информация/Диалайн';
	}

	public function diskontnye_karty()
	{
		$this->data['title'] = 'Дисконтная программа/Диалайн, Волгоград';
		$this->data['discount'] = $this->frontend_info_model->get_discount();
		$this->data['breadcrumbs_segment'] = '<li><a href="/info">О компании</a></li> | <li><a href="/info/patsientu">Справочная</a></li> | <li><a href="/info/diskontnye_karty">Дисконтная программа</a></li>';
		$this->getSeo('discount_page', 0); // no way other than hardcode for now! (28.06.17)
		$this->load->view('discount_view',$this->data);
	}

	/*!
	@brief вывод верстки списка медпрограмм через аяксовый запрос (обычно при обновлении списка программ по добавлению удалению из из списка сравнения)
	@details Антон Вопилов 21.01.2018
	@param[in] $forKids страница программ для детей
	@return Верстка списка медпрограмм на странице медпрограмм
	*/
	public function komplex_programm($ageCategory = 'adults')
	{
		$this->data['activity'] = array('adults'=>'','kids'=>'');
		$this->data['activity'][$ageCategory] = 'active';
		if($ageCategory != "kids")
		{
			
			$this->data['title'] = 'Комплексные программы/Диалайн';
			$this->data['kidsSegment'] = '';
			$this->data['komplex_program_text_header'] = $this->frontend_info_model->get_komplex_program_text_header($this->config->item('complexProgramsForAdultsId'));
			$this->data['komplex_program_advantages'] = $this->frontend_info_model->get_komplex_program_advantages();
			$this->data['medical_programs'] = $this->frontend_info_model->get_all_medical_programs('adults');
			$this->data['programs_in_compare'] = json_decode(get_cookie('med_prog_list', TRUE));
			$this->data['count_programs_in_compare'] = 0;
			if(is_array($this->data['programs_in_compare']))
				foreach($this->data['programs_in_compare'] as $programInCompare)
				{
					foreach($this->data['medical_programs'] as $medProgram)
						if($programInCompare == $medProgram->id)
							$this->data['count_programs_in_compare']++;
				}
			$this->data['breadcrumbs_segment'] = ' <li><a href="/services">Услуги</a></li> ';
			$this->getSeo('complex_page', 1); // no way other than hardcode for now! (28.06.17)
			
		}
		else
		{	
			$this->data['title'] = 'Комплексные программы для детей/Диалайн';
			$this->data['kidsSegment'] = '/kids';
			$this->data['komplex_program_text_header'] = $this->frontend_info_model->get_komplex_program_text_header($this->config->item('complexProgramsForKidsId'));
			$this->data['komplex_program_text_header'][0]->title = "Комплексные медицинские программы для детей";
			$this->data['komplex_program_advantages'] = $this->frontend_info_model->get_komplex_program_advantages();
			$this->data['medical_programs'] = $this->frontend_info_model->get_all_medical_programs('kids');
			$this->data['programs_in_compare'] = json_decode(get_cookie('med_prog_list', TRUE));
			$this->data['count_programs_in_compare'] = 0;
			if(is_array($this->data['programs_in_compare']))
				foreach($this->data['programs_in_compare'] as $programInCompare)
				{
					foreach($this->data['medical_programs'] as $medProgram)
						if($programInCompare == $medProgram->id)
							$this->data['count_programs_in_compare']++;
				}
			$this->data['breadcrumbs_segment'] = ' <li><a href="/services">Услуги</a></li> ';
			$this->getSeo('complex_page', $this->config->item('complexProgramsForKidsId')); // no way other than hardcode for now! (28.06.17)
		}
		$this->load->view('komplex_program_page_view',$this->data);
	}

	/*!
	@brief вывод верстки списка медпрограмм через аяксовый запрос (обычно при обновлении списка программ по добавлению удалению из из списка сравнения)
	@details Антон Вопилов 18.10.2017
	@param[in] $ageCategory выборка по взрослым или детям 'adults' - 'kids'
	@return Верстка списка медпрограмм на странице медпрограмм
	*/
	public function refresh_medical_program_list($ageCategory = 'adults')
	{
		$this->view_type = 2;
		$this->data['medical_programs'] = $this->frontend_info_model->get_all_medical_programs($ageCategory);
		$this->data['programs_in_compare'] = json_decode(get_cookie('med_prog_list', TRUE));
		$this->load->view('medical_program_items_view',$this->data);
	}
	
	/*!
	@brief вывод верстки списка медпрограмм через аяксовый запрос (обычно при обновлении списка программ по добавлению удалению из из списка сравнения)
	@details Антон Вопилов 18.10.2017
	@param[in] 
	@return Верстка списка медпрограмм на странице медпрограмм
	*/
	public function refresh_medical_program_count($ageCategory = 'adults')
	{
		$this->view_type = 2;
		$this->data['medical_programs'] = $this->frontend_info_model->get_all_medical_programs($ageCategory);
		$this->data['programs_in_compare'] = json_decode(get_cookie('med_prog_list', TRUE));
		$this->data['count_programs_in_compare'] = 0;
		foreach($this->data['programs_in_compare'] as $programInCompare)
		{
			foreach($this->data['medical_programs'] as $medProgram)
				if($programInCompare == $medProgram->id)
					$this->data['count_programs_in_compare']++;
		}
		$this->data['breadcrumbs_segment'] = ' <li><a href="/services">Услуги</a></li> ';
		echo $this->data['count_programs_in_compare'];
	}
	
	public function single_medical_programm($path = NULL)
	{
		$this->load->model('admin_info/info_model','', TRUE);
		$this->data['medical_program'] = $this->frontend_info_model->get_single_medical_program($path);
		if(!empty($this->data['medical_program']))
		{
			$this->data['breadcrumbs_segment'] = ' <li><a href="/services">Услуги</a></li>  | <li><a href="/komplex_programm">Комплексные программы</a></li>';
			$this->data['icons'] = $this->info_model->get_single_medical_program_imgs($this->data['medical_program'][0]->id);
			$this->data['title'] = $this->data['medical_program'][0]->title.'/Диалайн, Волгоград';
			$this->getSeo('medical_program', $this->data['medical_program'][0]->id);
			$this->load->view('single_medical_program_view',$this->data);
		}
		else show_404('page');
	}

	
	/*!
	@brief вывод страницы сравнения комплексных программ
	@details Антон Вопилов 18.10.2017
	@param[in] $ageCategory = параметр возраста для отсеивания ненужных программ
	@return Верстка страницы сравнения комплексных программ
	*/
	public function komplex_programm_compare($ageCategory = 'adults')
	{
		$this->data['kidsSegment'] = '';
		if($ageCategory == 'kids')
			$this->data['kidsSegment'] = '/kids';
		$this->data['breadcrumbs_segment'] = '  <li><a href="/services">Услуги</a></li> | <li><a href="/komplex_programm'.$this->data['kidsSegment'].'">Комплексные программы</a></li>';
		$programs = '';
		if(get_cookie('med_prog_list', TRUE))
			$programs = implode(",", json_decode(get_cookie('med_prog_list', TRUE)));
		if(strlen($programs)>0)
			$this->data['medical_programs'] = $this->frontend_info_model->get_compared_programs($programs, $ageCategory);
		else $this->data['medical_programs'] = $this->no_complex_programs;
		$this->data['title'] = 'Сравнение медицинских программ/Диалайн, Волгоград';
		$this->load->view('comparing_medical_program_view',$this->data);
	}
	
	/*!
	@brief вывод верстки сравнения медпрограмм через аяксовый запрос (обычно при обновлении списка сравнения программ по добавлению удалению из из списка сравнения)
	@details Антон Вопилов 18.10.2017
	@param[in] $ageCategory = параметр возраста для отсеивания ненужных программ
	@return Верстка списка сравнения медпрограмм на странице сравнения медпрограмм
	*/
	public function refresh_medical_program_compare_list($ageCategory = 'adults')
	{
		$this->view_type = 2;
		$programs = '';
		if(get_cookie('med_prog_list', TRUE))
			$programs = implode(",", json_decode(get_cookie('med_prog_list', TRUE)));
		if(strlen($programs)>0)
			$this->data['medical_programs'] = $this->frontend_info_model->get_compared_programs($programs, $ageCategory);
		else $this->data['medical_programs'] = $this->no_complex_programs;
		$this->load->view('medical_programs_in_contest_view',$this->data);
	}

	public function oms()
	{
		$this->data['breadcrumbs_segment'] = '| <li><a href="/info/">О компании</a></li> |  <li><a href="/info/patsientu">Справочная</a></li> | ';
		$this->data['oms'] = $this->frontend_info_model->get_oms();
		$this->data['oms_directions'] = $this->frontend_info_model->get_oms_directions();
		$this->data['title'] = 'Операция по полису ОМС. Перечень операций по ОМС/Диалайн, Волгоград';
		$this->getSeo('oms', 0);
		$this->load->view('oms_view',$this->data);
	}

	public function podgotovka_k_issledovaniyam()
	{
		$this->data['breadcrumbs_segment'] = '| <li><a href="/info/">О компании</a></li> |  <li><a href="/info/patsientu">Справочная</a></li> | ';
		$this->load->model('headings_model', '', TRUE);
		$this->data['research_groups'] = $this->headings_model->select_headings_by_type(2);
		$this->data['title_data'] = $this->frontend_info_model->get_page(2);
		$this->data['rules'] = $this->frontend_info_model->get_all_rules();
		$this->data['title'] = 'Правила подготовки к диагностическим исследованиям';
		$this->getSeo('complex_page', 2); // no way other than hardcode for now! (28.06.17)
		$this->load->view('rules_view',$this->data);
	}

	function rules_search()
	{
		$this->view_type = 2;
		$this->load->model('headings_model', '', TRUE);
		$this->data['research_groups'] = $this->headings_model->select_headings_by_type(2);
		$this->data['rules'] = $this->frontend_info_model->search_rules_by_text($this->input->post('search_text'));
		$this->load->view('rules_items_view',$this->data);
	}

	public function patsientu()
	{
		$this->data['breadcrumbs_segment'] = '| <li><a href="/info/">О компании</a></li> | ';
		$this->data['title'] = 'Для пациента/Диалайн, Волгоград';
		$this->getSeo('patsientu', 0);
		$this->load->view('patsientu_view',$this->data);
	}


	

	/*!
	@brief вывод страницы стоимость 
	@details Антон Вопилов 19.10.2016
	@param[in] $segment лишний параметр для обработки как 404 страниц за слэшем
	@return Верстка страницы прайса
	*/
	public function price($segment = NULL)
	{
		//превращение всех страниц за слэшем в 404
		if(!$segment)
		{
			$this->data['title'] = 'Стоимость  медицинского обслуживания ';
			$this->data['rubrics'] = $this->frontend_info_model->get_med_rubrics();
			$this->data['service_types'] = $this->frontend_info_model->get_med_service_type();
			$this->data['services'] = $this->frontend_info_model->get_services();
			$this->getSeo('price', 0);
			$this->load->view('price_view',$this->data);
		}
		else
			show_404('page');
	}

	public function index()
	{
		$this->data['breadcrumbs_segment'] = '| <li><a href="/info/">О компании</a></li> | ';
		$this->data['about'] = $this->frontend_info_model->get_info_data(3);
		$this->data['licenses'] = $this->frontend_info_model->get_licenses();
		$this->data['clinics'] = $this->frontend_clinics_model->get_all_clinics_list('year');
		$timeline = array();
		$years_count = 0;

		foreach($this->data['clinics'] as $key=>$value)
		{
			if($key==0)
			{
				$timeline[0]['year'] = $value->year;
				$timeline[0]['address'] = '<a trg="'.$key.'">'.$value->address.'</a>';
				$timeline[0]['delta'] = 0;
			}
			else
			{
				if($value->year == $timeline[$years_count]['year'])
				{
					$timeline[$years_count]['address'] .= ' и '.'<a trg="'.$key.'">'.$value->address.'</a>';
				}
				else
				{
					$years_count++;
					$timeline[$years_count]['address'] = '<a trg="'.$key.'">'.$value->address.'</a>';
					$timeline[$years_count]['year'] = $value->year;
					$timeline[$years_count]['delta'] = $timeline[$years_count]['year'] - $timeline[$years_count-1]['year'];
				}
			}
		}

		$this->data['timeline'] = $timeline;
		foreach($this->data['licenses'] as $key=>$value)
		{
			$this->data['licenses'][$key]->filesize = $this->get_filesize(substr($value->path, 1));
		}



		$this->data['title'] = 'Для пациента/Диалайн, Волгоград';
		$this->getSeo('complex_page', 3); // no way other than hardcode for now! (29.06.17) fuck fuck fuck fuck fuck fuck!!!
		$this->load->view('info_page_view',$this->data);
	}

	function get_filesize($file)
	{
		// идем файл
		if(!file_exists($file)) return "Файл  не найден";
	// теперь определяем размер файла в несколько шагов
	$filesize = filesize($file);
	// Если размер больше 1 Кб
	if($filesize > 1024)
	{
		$filesize = ($filesize/1024);
		// Если размер файла больше Килобайта
		// то лучше отобразить его в Мегабайтах. Пересчитываем в Мб
		if($filesize > 1024)
		{
				$filesize = ($filesize/1024);
			// А уж если файл больше 1 Мегабайта, то проверяем
			// Не больше ли он 1 Гигабайта
			if($filesize > 1024)
			{
				$filesize = ($filesize/1024);
				$filesize = round($filesize, 1);
				return $filesize." ГБ";       
			}
			else
			{
				$filesize = round($filesize, 1);
				return $filesize." MБ";   
			}       
		}
		else
		{
			$filesize = round($filesize, 1);
			return $filesize." Кб";   
		}  
	}
	else
	{
		$filesize = round($filesize, 1);
		return $filesize." байт";   
	}
	}

	function pravovaya_informaciya()
	{
		$this->data['title'] = 'Правовая информация/Диалайн, Волгоград';
		$this->data['laws'] = $this->frontend_info_model->get_laws();
		$this->getSeo('legal_info', 0);
		$this->load->view('pravovaya_informaciya_page_view',$this->data);
	}

	function kontroliruyushchie_organizatsii()
	{
		$this->data['breadcrumbs_segment'] = '| <li><a href="/info/">О компании</a></li> | ';
		$this->data['info_data'] = $this->frontend_info_model->get_info_data(4);
		$this->data['title'] = $this->data['info_data'][0]->title.'/Диалайн, Волгоград';
		$this->getSeo('complex_page', 4); // no way other than hardcode for now! (28.06.17)
		$this->load->view('kontroliruyushchie_organizatsii_view',$this->data);
	}

	function page($id = null)
	{
		
		$this->data['info_data'] = $this->frontend_info_model->get_info_data_url($id);
		if(!empty($this->data['info_data']))
		{
			$this->data['breadcrumbs_segment'] = '| <li><a href="/info/">О компании</a></li> | ';
			$this->data['title'] = $this->data['info_data'][0]->title.'/Диалайн, Волгоград';
			$this->getSeo('complex_page', $this->data['info_data'][0]->id);
			$this->load->view('kontroliruyushchie_organizatsii_view',$this->data);
		}
		else
			show_404('page');
	}


	function program_overlay()
	{
		$this->view_type = 2;
		$this->data['services'] = $this->frontend_info_model->get_all_medical_programs('adults');
		$this->data['services_kids'] = $this->frontend_info_model->get_all_medical_programs('kids');
		$this->load->view('program_overlay_view',$this->data);
	}

	function search_service_by_title()
	{
		$this->view_type = 2;
		$this->data['rubrics'] = $this->frontend_info_model->get_med_rubrics_by_text_title($this->input->post('search_text'));
		$this->data['service_types'] = $this->frontend_info_model->get_med_service_type_by_text_title($this->input->post('search_text'));
		$this->data['services'] = $this->frontend_info_model->get_services_by_text_title($this->input->post('search_text'));
		$this->load->view('price_data_view',$this->data);
	}

	function search_service_by_params()
	{
		$this->view_type = 2;
		$this->data['rubrics'] = $this->frontend_info_model->get_med_rubrics_by_params($this->input->post('age'),$this->input->post('speciality'));
		$this->data['service_types'] = $this->frontend_info_model->get_med_service_type_by_params($this->input->post('age'),$this->input->post('speciality'));
		$this->data['services'] = $this->frontend_info_model->get_services_by_params($this->input->post('age'),$this->input->post('speciality'));
		$this->load->view('price_data_view',$this->data);
	}

}
