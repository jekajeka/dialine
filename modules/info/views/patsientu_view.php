
    <div class="row row_breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="">Главная</a></li> <?=$breadcrumbs_segment?> <li><a href="">Справочная</a></li>
            </ul>
        </div>
    </div>
    <div class="row row_title_h1">
        <div class="container">
            <h1>Справочная</h1>
        </div>
    </div>
    <div class="row help_row">
        <div class="container">
            <a class="help_item_wrap help_item_wrap_rules" href="/info/podgotovka-k-issledovaniyam/">
                <h2>Правила подготовки</h2>
            </a>
            <a class="help_item_wrap help_item_wrap_discont" href="/info/diskontnye_karty/">
                <h2>Дисконтная политика</h2>
            </a>
            <a class="help_item_wrap help_item_wrap_time" href="/info/priem_grazhdan">
                <h2>График приема граждан</h2>
            </a>
            <a class="help_item_wrap help_item_wrap_useful" href="/articles">
                <h2>Полезные статьи</h2>
            </a>
            <?/*
            <a class="help_item_wrap help_item_wrap_insurance" href="/info/oms">
                <h2>Обязательное медстрахование</h2>
            </a>*/
            ?>
            <a class="help_item_wrap help_item_wrap_news" href="/news">
                <h2>Новости</h2>
            </a>
        </div>
    </div>
    