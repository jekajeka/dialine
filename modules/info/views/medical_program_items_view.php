<?foreach($medical_programs as $m_prog){?>
<a class="single_program" href="/komplex_programm/<?=$m_prog->path?>" title="<?=$m_prog->title?>">
    <h3><?=$m_prog->title?></h3>
    <div class="single_program_footer">
        <div class="price_prog_wrap">
            <span>Стоимость программы:</span> <?=$m_prog->cost?> <span class="price"> р.<span>
        </div>
        <?
        $contest_string = "В сравнение";
        $delete_from_contest = '';
        if(count($programs_in_compare))
        if(in_array($m_prog->id,$programs_in_compare))
        {
           $contest_string = "Убрать из сравнения";
           $delete_from_contest = 'delete_from_contest';
        }
        ?>
        <div program_id="<?=$m_prog->id?>" class="add_to_contest <?=$delete_from_contest?>"><?=$contest_string?></div>
    </div>
</a>
<?}?>