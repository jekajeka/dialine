    <div class="row single_medical_program_banner" style="background: url('<?=$medical_program[0]->image_path?>') no-repeat center;background-size: cover">
        <div class="overlay">
            <div class="container">
                <h1><?=$medical_program[0]->banner_text?></h1>
                <div class="medical_program_banner_icon_wrap">
                    
                    <div class="medical_program_banner_icon_item">
                        <? if(isset($icons[0]->path)) {?>
                        <img src="<?=$icons[0]->path?>" />
                        <?}?>
                        <div class="medical_program_banner_icon_text"><?=$medical_program[0]->banner_text_adv1?></div>
                    </div>
                    <div class="medical_program_banner_icon_item">
                        <? if(isset($icons[1]->path)) {?>
                        <img src="<?=$icons[1]->path?>" />
                        <?}?>
                        <div class="medical_program_banner_icon_text"><?=$medical_program[0]->banner_text_adv2?></div>
                    </div>
                    <div class="medical_program_banner_icon_item">
                        <? if(isset($icons[2]->path)) {?>
                        <img src="<?=$icons[2]->path?>" />
                        <?}?>
                        <div class="medical_program_banner_icon_text"><?=$medical_program[0]->banner_text_adv3?></div>
                    </div>
                    <div class="medical_program_banner_icon_item">
                        <? if(isset($icons[3]->path)) {?>
                        <img src="<?=$icons[3]->path?>" />
                        <?}?>
                        <div class="medical_program_banner_icon_text"><?=$medical_program[0]->banner_text_adv4?></div>
                    </div>
                </div>
            </div>
            <div  class="row row_have_a_question">
                <div class="container">
                    <div class="have_a_question_title">Заказать программу или<br/>получить консультацию</div>
                    <div class="have_a_question_form">
                        <input type="text" id="name_med_prog_header" placeholder="Ваше имя" />
                        <input type="text" id="phone_med_prog_header" placeholder="Ваш телефон" />
                        <a id="send_med_prog_header_form" class="send_question_form">Отправить</a>
                        <div class="footer_agree">
                            <input type="checkbox" id="header_med_prog_agree"><label>Добровольно согласен(а) на обработку своих персональных данных. <a target="_blank" href="/uploads/files/Politika_obrabotki_i_zashiti_personalnih_dannih1.pdf">Подробнее</a></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row row_breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="/">Главная</a></li> | <?=$breadcrumbs_segment?> | <li><a href=""><?=$medical_program[0]->title?></a></li>
            </ul>
        </div>
    </div>
    <div class="row row_medical_programm_price">
        <div class="container">
            <div class="medical_programm_price_text">Стоимость программы &laquo;<?=$medical_program[0]->title?>&raquo;</div>
            <div class="medical_programm_price_data"><?=$medical_program[0]->cost?> р.</div>
        </div>
    </div>
    <?if(!empty($medical_program[0]->target)){?>
    <div class="row row_medical_programm_price medical_programm_target">
        <div class="container">
            <?=$medical_program[0]->target?>
        </div>
    </div>
    <?}?>
    <div class="row row_complex">
        <div class="container">
            <div class="programm_legend_wrap_page">
                <?if(!empty($medical_program[0]->research)){?>
                <div class="programm_legend_item_wrap">
                    <div class="programm_legend_item ">Исследования</div>
                    <div class="programm_legend_data "><?=$medical_program[0]->research?></div>
                </div>
                <?}?>
                <!--<?if(!empty($medical_program[0]->address)){?>
                <div class="programm_legend_item_wrap">
                    <div class="programm_legend_item ">Программа действует в клиниках по адресу</div>
                    <div class="programm_legend_data "><?=$medical_program[0]->address?></div>
                </div>
                <?}?>-->
                <?if(!empty($medical_program[0]->diagnostics)){?>
                <div class="programm_legend_item_wrap">
                    <div class="programm_legend_item ">Диагностика</div>
                    <div class="programm_legend_data "><?=$medical_program[0]->diagnostics?></div>
                </div>
                <?}?>
                <?if(!empty($medical_program[0]->specialists)){?>
                <div class="programm_legend_item_wrap">
                    <div class="programm_legend_item ">Приемы специалистов</div>
                    <div class="programm_legend_data "><?=$medical_program[0]->specialists?></div>
                </div>
                <?}?>
                <!--<?if(!empty($medical_program[0]->bonus)){?>
                <div class="programm_legend_item_wrap">
                    <div class="programm_legend_item ">Бонусы</div>
                    <div class="programm_legend_data "><?=$medical_program[0]->bonus?></div>
                </div>
                <?}?>-->
            </div>
    
        </div>
    </div>
    <div class="row row_medical_programm_price medical_programm_target" style="margin-bottom: 50px">
        <div class="container">
            <a title="Запись по медпрограммме '<?=$medical_program[0]->title?>'" id="send_medical_program" style="font-size: 1.5rem;" class="green_btn box_shadow">Оставить заявку</a>
        </div>
    </div>