
    <div class="row row_breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="/">Главная</a></li> | <li><a href="/info">О компании</a></li> | <li><a href="/info/patsientu">Справочная</a></li> | <li><a href="">График приема граждан</a></li>
            </ul>
        </div>
    </div>
    <div class="row row_title_h1">
        <div class="container">
            <h1>График приема граждан</h1>
        </div>
    </div>
    <div class="row audition_row">
        <div class="container">
            <?foreach($clinics as $clinic){?>
            <div class="audition_wrap ">
                <h2 class=" box_shadow"><?=$clinic->title?>  <span><?=$clinic->district?></span></h2>
                <div class="audition_data_wrap ">
                    <div class="audition_data_wrap_img">
                        <a href="/clinic/<?=$clinic->path?>"><img src="<?=$clinic->image_path?>" /></a>
                    </div>
                    <div class="audition_data_wrap_phone">
                        <div class="audition_data_wrap_phone_title">Телефон:</div>
                        <a href=""><?=$clinic->phones?></a>
                    </div>
                    <div class="audition_data_wrap_time">
                        <div class="audition_data_wrap_phone_title">Режим работы:</div>
                        <div class="audition_data_wrap_time_data">
                            <?=$clinic->work_time?>
                        </div>
                    </div>
                </div>
            </div>
            <?}?>
        </div>
    </div>
    