<div class="row row_breadcrumbs">
    <div class="container">
        <ul>
            <li><a href="/">Главная</a></li> | <?=$breadcrumbs_segment?> | <li><a href=""><?=$komplex_program_text_header[0]->title?></a></li>
        </ul>
    </div>
</div>
<div class="row row_complex">
    <div class="container">
        <h1><?=$komplex_program_text_header[0]->title?></h1>
        <div class="complex_pretext">
            <?=$komplex_program_text_header[0]->text?>
        </div>
        <div class="complex_teasers_wrap">
            <?foreach($komplex_program_advantages as $advantage){?>
            <div class="complex_teaser" style="background: url(<?=$advantage->path?>) no-repeat top left">
                <div class="complex_teaser_title"><?=$advantage->title?></div>
                <p><?=$advantage->text?></p>
            </div>
            <?}?>
        </div>
        <div class="select_programs_wrap">
            <div class="adults_child_wrap">
                <a title="Комплексные медицинские программы" href="/komplex_programm" class="adults <?=$activity['adults']?>">Для взрослых</a>
                <a title="Детские комплексные программы" href="/komplex_programm/kids" class="childs <?=$activity['kids']?>">Для детей</a>
            </div>
            <div class="count_contest_wrapper">
                <div class="count_contest_text_1">В сравнении </div>
                <div class="count_contest_text_data"> <?=$count_programs_in_compare?> </div>
                <div class="count_contest_text_2"> программ </div>
            </div>
            <a href="/komplex_programm_compare<?=$kidsSegment?>" class="contest_button">Сравнить</a>
        </div>
        <div class="list_programs_wrap">
            <div class="list_programs_type_wrap">
                <? echo $this->load->view('medical_program_items_view',$this->data,TRUE) ?>
            </div>
        </div>
    </div>
</div>