<?
if(is_array($medical_programs))
foreach($medical_programs as $m_prog){?>
<div class="swiper-slide">
    <div class="program_column">
        <div class="program_column_title"><?=$m_prog->title?></div>
        <div class="programm_column_cell programm_legend_item_target"><?=$m_prog->target?></div>
        <div class="programm_column_cell programm_legend_item_address"><?=$m_prog->address?></div>
        <div class="programm_column_cell programm_legend_item_specialist"><?=$m_prog->specialists?></div>
        <div class="programm_column_cell programm_legend_item_diagnost"><?=$m_prog->diagnostics?></div>
        <div class="programm_column_cell programm_legend_item_laborator"><?=$m_prog->research?></div>
        <!--<div class="programm_column_cell programm_legend_item_bonus"><?=$m_prog->bonus?></div>-->
        <div class="programm_column_cell programm_legend_item_price"><?=$m_prog->cost?> р.</div>
    </div>
    <div class="column_line"></div>
    <a href="/komplex_programm/<?=$m_prog->path?>" title="Комплексная медицинская программа <?=$m_prog->title?>" class="order_program">Подробнее</a><br/>
    <a href="#<?=$m_prog->path?>" program_id="<?=$m_prog->id?>" title="Комплексная медицинская программа <?=$m_prog->title?>" class="drop_order_program order_program">&nbsp;&nbsp;&nbsp;Удалить&nbsp;&nbsp;&nbsp;</a>
</div>
<?}
else{?>
<div class="swiper-slide"><?=$medical_programs;?></div>
<?}?>