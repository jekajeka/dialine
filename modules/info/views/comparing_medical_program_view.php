<div class="row row_breadcrumbs">
    <div class="container">
        <ul>
            <li><a href="/">Главная</a></li> | <?=$breadcrumbs_segment?> | <li><a href="">Сравнение комплексных программ</a></li>
        </ul>
    </div>
</div>
<div class="row row_complex_contest">
    <div class="container">
        <input type="hidden" id="path_segment" value="<?=$kidsSegment?>">
        <?
        if(is_array($this->data['medical_programs'])) {?>
        <div class="programm_legend_wrap">
            <div class="programm_legend_item programm_legend_item_target">Цель программы</div>
            <div class="programm_legend_item programm_legend_item_address">Программа действует в клиниках по адресу</div>
            <div class="programm_legend_item programm_legend_item_specialist">Приемы специалистов</div>
            <div class="programm_legend_item programm_legend_item_diagnost">Диагностика</div>
            <div class="programm_legend_item programm_legend_item_laborator">Лабораторные исследования</div>
            <!--<div class="programm_legend_item programm_legend_item_bonus">Бонусы</div>-->
            <div class="programm_legend_item programm_legend_item_price">Стоимость программы</div>
        </div>
        <?}?>
        <div class="programs_carousel_block">
            <h1>Сравнение КОМПЛЕКСНЫх ПРОГРАММ</h1>
            <div class="programs_carousel_wrap">
                <div class="swiper-wrapper">
                    <? echo $this->load->view('medical_programs_in_contest_view',$this->data,TRUE)?>
                </div>
            </div>
            <a class="program_carousel_link program_carousel_link_left"></a>
            <a class="program_carousel_link program_carousel_link_right"></a>
        </div>
    </div>
</div>