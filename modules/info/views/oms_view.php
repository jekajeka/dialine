
    <div class="row row_breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="/">Главная</a></li> <?=$breadcrumbs_segment?> <li><a href=""><?=$oms[0]->title?></a></li>
            </ul>
        </div>
    </div>
    <div class="row row_title_h1">
        <div class="container">
            <h1><?=$oms[0]->title?></h1>
        </div>
    </div>
    <div class="row oms_row">
        <div class="container">
            <div class="oms_top_data">
                <?=$oms[0]->text?>
            </div>
            <div class="oms_card_data_wrap">
                <?foreach($oms_directions as $oms_direction){?>
                <div class="oms_card_item" style="background-color: <?=$oms_direction->color?>">
                    <h2><?=$oms_direction->title?></h2>
                    <?=$oms_direction->text?>
                </div>
                <?}?>
            </div>
            <div class="oms_bottom_data">
                <?=$oms[0]->second_text?>
            </div>
        </div>
    </div>
    