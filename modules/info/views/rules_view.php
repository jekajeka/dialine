
    <div class="row row_breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="/">Главная</a></li> <?=$breadcrumbs_segment?> <li><a href=""><?=$title?></a></li>
            </ul>
        </div>
    </div>
    <div class="row row_title_h1">
        <div class="container">
            <h1><?=$title_data[0]->title?></h1>
        </div>
    </div>
    <div class="row rules_row">
        <div class="container">
            <div class="oms_top_data">
                <?=$title_data[0]->text?>
                <div class="text_search_rules">
                    <input id="search_rules" type="text" placeholder="Начните вводить запрос...">
                </div>
            </div>
            <div id="rules_data">
                <?=$this->load->view('rules_items_view',$this->data, TRUE);?>
            </div>
        </div>
    </div>
    