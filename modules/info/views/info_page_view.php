<div class="row row_breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="/">Главная</a></li> | <li><a href="/info">О компании</a></li>
            </ul>
        </div>
    </div>
    <div class="row row_title_h1">
        <div class="container">
            <h1><?=$about[0]->title?></h1>
        </div>
    </div>
    <div class="row about_row">
        <div class="container">
            <?=$about[0]->text?>
        </div>
    </div>
    <div class="row licenses_row">
        <div class="container">
            <?foreach($licenses as $license){?>
            <div class="single_license">
                <h2><?=$license->title?></h2>
                <?=$license->text?>
                <a target="_blank" href="<?=$license->path?>">Скачать  <span>(<?=$license->filesize?>)</span></a>
            </div>
            <?}?>
        </div>
    </div>
    <div class="row image_structire">
        <div class="container">
			<div class="image_map_wrap">
				<img id="image_map" src="/frontend/images/image_structire2.png" usemap="#direct_map"/>
				
				<map name="direct_map">
					<area shape="circle" name="tests" coords="278, 85, 84"  href="/services/additional/analizy"/>
					<area shape="circle" name="doctors" coords="592, 203, 117" href="/doctors"/>
					<area shape="circle" name="rentgen" coords="1033, 127, 75" href="/rentgenologi"/>
					<area shape="circle" name="hirurgiya" coords="1118, 487, 75" href="/hirurg"/>
					<area shape="circle" name="fiziolechenie" coords="845, 367, 94" href="/fizioterapevt"/>
					<area shape="circle" name="endoskopia" coords="426, 455, 80" href="/endoskopiya"/>
					<area shape="circle" name="funkc_diagnostika" coords="218, 456, 73" href="/funktsionalnyj-dignost"/>
					<area shape="circle" name="uzi" coords="85, 312, 83" title="Сделать УЗИ в Волгограде, Диалайн" href="/uzi"/>
				</map>
				
			</div>
        </div>
    </div>
    <div class="row story_row">
        <div class="container">
            <h2>Карта клиник</h2>
            <p>
                Интерактивная карта с расположением мед. центров Диалайн.
            </p>
            <div class="story_slider">
                <div class="swiper-wrapper">
                    <div class="swiper-slide" >
                        <div class="data_line" style="width: 1600px">
                            <?foreach($timeline as $t_line){?>
                            <div class="data_dot year_<?=$t_line['delta']?>">
                                <div class="data_dot_text">
                                    <div class="data_dot_text_title"><?=$t_line['year']?> г. </div>
                                    <div class="data_dot_text_addr"><?=$t_line['address']?> </div>
                                </div>
                            </div>
                            <?}?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row about_map_row">
        <div class="container">
            <div id="map"></div>
        </div>
    </div>
<script>
    function init () {
        myMap = new ymaps.Map ("map", {
                                    center: [48.758625,44.818625],
                                    zoom: 16
                                });
        var myPlacemark = new Array;
        <?foreach($clinics as $key=>$clinic) {?>
        myPlacemark[<?=$key?>] = new ymaps.Placemark([<?=$clinic->coordinates?>], {
            balloonContent: '<?=$clinic->title?>',
            iconCaption: '<?=$clinic->title?>'
        }, {
            preset: 'islands#hospitalIcon',
	    iconColor: '#00abb2'
        });
        <?}?>
        <?foreach($clinics as $key=>$clinic) {?>
        myMap.geoObjects.add(myPlacemark[<?=$key?>]);
        <?}?>
$('.data_dot_text_addr a').click(function(e){
                                        e.preventDefault();
                                        var opts = Object;
                                        opts.duration = 2000;
                                        myMap.panTo(myPlacemark[$(this).attr('trg')].geometry.getBounds(),opts);
                                    });
    
myMap.behaviors.disable('scrollZoom');
    
}

$(document).ready(function(){
    ymaps.ready(init);
    
    
});
</script>
<script src="/frontend/js/jquery.imagemapster.js"></script>
<script src="/frontend/js/mapinit.js"></script>