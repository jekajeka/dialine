<?foreach($rubrics as $rubric){?>
<div class="structured_data_content_wrap">
    <a class="structured_data_title">
        <h4><?=$rubric->service_rubric_title?></h4><span class="structured_data_title_qty"><?=$rubric->services_qty?></span>
    </a>
    <div class="structured_data_table_wrap">
        <?foreach($service_types as $service_type){
        if($service_type->rubric == $rubric->rubric){?>
        <h3><?=$service_type->service_type?></h3>
        <div class="structured_data_table_wrap_order">
        <?foreach($services as $service){
        if(($service->service_type == $service_type->service_type)&&($service->rubric == $service_type->rubric)){?>
            <div class="structured_data_table_line">
                <div class="structured_data_table_line_border">
                    <h4>
                        <?if(!empty($service->link)){?>
                        <a href="<?=$service->link?>"><?=$service->title?></a>
                        <?}
                        else{?>
                        <?=$service->title?>
                        <?}?>
                    </h4>
                    <div class="structured_data_table_line_price"><?=$service->price?> р.</div>
                </div>
                <? if(strpos($service_type->service_type,'не требующие записи') == NULL) {?>
                    <a id="table_link_<?=$service->id?>" class="structured_data_table_link">Добавить</a>
                <?}?>
            </div>
            <?}}?>
        </div>
        <?}}?>
    </div>
</div>
<?}?>