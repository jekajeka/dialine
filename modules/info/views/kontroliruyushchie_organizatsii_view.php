<div class="row row_breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="/">Главная</a></li> <?=$breadcrumbs_segment?>  <li><a href=""><?=$info_data[0]->title?></a></li>
            </ul>
        </div>
    </div>
    <div class="row row_title_h1">
        <div class="container">
            <h1><?=$info_data[0]->title?></h1>
        </div>
    </div>
    <div class="row single_news_page">
        <div class="container">
            <div class="single_news_text">
                <?=$info_data[0]->text?>
            </div>
        </div>
    </div>