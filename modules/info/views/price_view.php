<div class="row row_title_image">
    <div class="container">
        <h1><?=$title?></h1>
    </div>
</div>
<div class="row row_breadcrumbs">
    <div class="container">
        <ul>
            <li><a href="/">Главная</a></li> | <li><a href="/info/price"><?=$title?></a></li>
        </ul>
    </div>
</div>
<div class="row row_speciality_rule">
    <div class="container">
        <div class="text_search_speciality_wrap">
            <input style="width:240px" type="text" id="search_service_by_title" placeholder="Поиск...">
        </div>
        <div class="text_search_speciality_long_wrap">
            <select id="age_page">
                <option value="all">Все</option>
                <option value="child">Детям</option>
                <option value="adult">Взрослым</option>
            </select>
            <select id="speciality_page_price" style="width: 250px">
                <option value="all">Все</option>
                <?foreach($rubrics as $rubric){?>
                <option value="<?=$rubric->id?>"><?=$rubric->service_rubric_title?></option>
                <?}?>
            </select>
            <a id="page_price_go" class="green_btn">Подобрать</a>
        </div>
    </div>
</div>
<div class="row row_price">
    <div class="container">
        <div class="price_list_wrap">
            <div class="structured_data_wrap">
                <?=$this->load->view('price_data_view',$this->data,true)?>
            </div>
            <div style="font-size: 17px;float: left">
                <p>
                    Стоимость услуг указана справочно. Уточнить действующую стоимость на момент получения медицинской услуги можно в регистратуре или по телефонам колл-центра ДИАЛАЙН.
                </p>
                <p>
                    Дата обновления прайса 09.11.17
                </p>
            </div>
        </div>
        <div class="service_result_wrap_wrap">
            <div class="service_result_wrap_inner">
                <div class="service_result_wrap">
                    <div class="service_result_title">Это поможет вам сделать предварительный расчет стоимости посещения специалистов и диагностирования.</div>
                    <div class="service_result_calc_wrap box_shadow">
                        <div class="service_result_total">Всего добавлено:<span>0 услуг</span></div>
                        <div class="service_result_data_wrap"></div>
                        <div class="total_data">На сумму:<span> 0 р.</span></div>
                        <a title="Записаться" id="send_order_services" class="green_btn">Записаться</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>