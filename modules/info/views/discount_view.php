
    <div class="row row_breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="/">Главная</a></li> | <?=$breadcrumbs_segment?>
            </ul>
        </div>
    </div>
    <div class="row row_title_h1">
        <div class="container">
            <h1><?=$discount[0]->title?></h1>
        </div>
    </div>
    <div class="row discount_row">
        <div class="container">
            <div class="card_wrap">
                <div class="card_wrap_text" style="width: 100%">
                    <h2><?=$discount[0]->subtitle?></h2>
                    <?=$discount[0]->card_text?>
                </div>
                <!--<div class="plactic_card box_shadow">
                    <h3>Единая дисконтная карта</h3>
                    <div class="wrap_discount_input">
                        <input id="discount_name" type="text" placeholder="ФИО" />
                        <input id="discount_phone" type="text" placeholder="Телефон" />
                    </div>
                    <a class="plactic_card_btn">Получить карту</a>
                </div>-->
            </div>
            <h2 class="card_middle"><?=$discount[0]->second_title?></h2>
            <?=$discount[0]->text?>
        </div>
    </div>