<div class="row row_breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="/">Главная</a></li> | <li><a href="/info">О компании</a></li> | <li><a href="/info/pravovaya_informaciya">Правовая информация</a></li>
            </ul>
        </div>
    </div>
    <div class="row row_title_h1">
        <div class="container">
            <h1>Правовая информация</h1>
        </div>
    </div>
    <div class="row rules_row">
        <div class="container">
            <a class="law_wrap" href="/info/kontroliruyushchie-organizatsii">
                <div class="law_wrap_data">
                    Контакты контролирующих организаций
                </div>
            </a>
            <?foreach($laws as $law){?>
            <a class="law_wrap law_wrap_download" href="<?=$law->path?>" target="_blank">
                <div class="law_wrap_data">
                    <?=$law->title?>
                </div>
                <div class="law_wrap_data_label">Скачать</div>
            </a>
            <?}?>
        </div>
    </div>