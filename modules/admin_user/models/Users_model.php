<?
class Users_model extends CI_Model {
private $enterprise_query_data_list = '';

function Users_model()
{
    parent::__construct();
}    

function clear()
{
    $this->db->truncate('enterprise');
}
function get_all_users()
{
    $query = "SELECT
            id,username,email,created_on,last_login,first_name,last_name,company,phone
            FROM users
            ORDER BY id ASC";
    $data = $this->db->query($query);
    return $data->result();
}
function get_single_user($user_id = NULL)
{
    $query = "SELECT
            id,username,email,created_on,last_login,first_name,last_name,company,phone,active
            FROM users
            WHERE
            id=$user_id
            ORDER BY id ASC";
    $data = $this->db->query($query);
    return $data->result();
}

function update($params = array())
{
    $this->db->where('id', $params['id']);
    array_shift($params);
    return $this->db->update('users', $params);
}
}