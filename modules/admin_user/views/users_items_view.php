<?foreach($this->data['users_objects'] as $user_data){?>
<div class="col-md-4 col-sm-4 col-xs-12 profile_details">
    <div class="well profile_view">
        <div class="col-sm-12">
            <h4 class="brief"><i><?=$user_data->username?></i></h4>
            <div class="left col-xs-12">
                <h2><?=$user_data->first_name?> <?=$user_data->last_name?></h2>
                <p><strong>Состоит в группах: </strong>
                <?
                foreach($user_data->groups as $user_group)
                    echo $user_group->name.'('.$user_group->description.') / ';
                ?>
                </p>
                <ul class="list-unstyled">
                    <li><i class="fa fa-building"></i> Компания: <?=$user_data->company?></li>
                    <li><i class="fa fa-phone"></i> Телефон: <?=$user_data->phone?></li>
                    <li><i class="fa fa-envelope-square"></i> E-mail: <?=$user_data->email?></li>
                </ul>
            </div>
            <div class="right col-xs-5 text-center">
                <img src="images/img.jpg" alt="" class="img-circle img-responsive">
            </div>
        </div>
        <div class="col-xs-12 bottom text-center">
            <div class="col-xs-12 col-sm-12 emphasis">
                <a href="/admin_user/user/<?=$user_data->id?>" class="btn btn-primary btn-xs"><i class="fa fa-user"> </i> Редактировать</a>
            </div>
        </div>
    </div>
</div>
<?}?>