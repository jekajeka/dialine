<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_user extends Admin_Controller  {
    function __construct()
    {
	parent::__construct();
	$this->load->model('users_model', '', TRUE);
	$this->title = 'Настройки сайта';
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    public function index()
    {
	$this->data['title'] = 'Пользователи';
	$this->data['users_objects'] = $this->users_model->get_all_users();
	foreach($this->data['users_objects'] as $key=>$user)
	    $this->data['users_objects'][$key]->groups = $this->ion_auth_model->get_users_groups($user->id);
	$this->data['users_list'] = $this->load->view('users_items_view',$this->data,TRUE);
	$this->load->view('all_users_view');
    }
    
    public function user($user_id = NULL)
    {
	$this->data['user_object'] = $this->users_model->get_single_user($user_id);
	$this->data['title'] = $this->data['user_object'][0]->username.' / Пользователи';
	if($this->ion_auth->is_admin())
	    $this->load->view('single_user_view');
	else
	    $this->load->view('illegal_user_deny_view');
    }
    
    public function user_update()
    {
	$this->view_type = 2;
	$active = 0;
	if($this->input->post('active')=='on')
	    $active = 1;
	$params = array('id'=>$this->input->post('id'),
			'username'=>$this->input->post('username'),
			'email'=>$this->input->post('email'),
			'first_name'=>$this->input->post('first_name'),
			'last_name'=>$this->input->post('last_name'),
			'company'=>$this->input->post('company'),
			'phone'=>$this->input->post('phone'),
			'active'=>$active
			);
	$this->data['result'] = $this->users_model->update($params);
	echo $this->load->view('admin/json_result_view',$this->data);
    }
    
    public function change_password()
    {
	$this->load->model('ion_auth_model');
	$this->view_type = 2;
	$this->data['result'] = $this->ion_auth_model->change_password($this->input->post('id'), $this->input->post('old_pwd'), $this->input->post('new_pwd'));
	echo $this->load->view('admin/json_result_view',$this->data);
    }
    
    public function add_user_page()
    {
	$this->data['title'] = 'Добавить пользователя';
	$this->load->view('add_user_view');
    }
    
    public function delete($id = NULL)
    {
	$this->view_type = 2;
	$active = 0;
	$this->data['result'] = $this->ion_auth->delete_user($id);
	echo $this->load->view('admin/json_result_view',$this->data);
    }
    
    public function add_user()
    {
	$this->view_type = 2;
	$active = 0;
	if($this->input->post('active')=='on')
	    $active = 1;
	$additional_data = array('first_name' => $this->input->post('first_name'),
				    'last_name' => $this->input->post('last_name'),
				    'company' => $this->input->post('company'),
				    'phone'=>$this->input->post('phone'));
	if ($this->ion_auth->register($this->input->post('username'), $this->input->post('new_pwd'), $this->input->post('email'), $additional_data))
	{
	    $this->data['result'] = 'Пользователь успешно добавлен';
	    echo $this->load->view('admin/json_result_view',$this->data);
	}
	else
	{
	    //display the create user form
	    //set the flash data error message if there is one
	    $this->data['result'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
	    echo $this->load->view('admin/json_result_view',$this->data);
	}
    }
    
    /**
    * переписан вывод всего, что обрабатывается в этом контроллере для того. чтобы неадмин не мог редактировать никого
    * 
    * @return html
    * @author Антон Вопилов
    **/
    function _output($data)
    {
	if($this->view_type==1)
	{
	    if(!$this->ion_auth->is_admin())
	    {
		$this->data['content'] = $this->load->view('illegal_user_deny_view','',TRUE);
	    }
	    else
		$this->data['content'] = $this->output->get_output();
	    $this->data['user'] = $this->ion_auth->get_current_user();
	    echo $this->load->view('template/main_page_view',$this->data,TRUE);
	}
	else
	{
	    echo $this->output->get_output();
        }
    }

}
