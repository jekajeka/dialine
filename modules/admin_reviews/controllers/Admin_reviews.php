<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_reviews extends Admin_Controller  {
function __construct()
{
    parent::__construct();
    $this->load->model('admin_reviews_model', '', TRUE);
    $this->load->model('admin_clinics/admin_clinics_model', '', TRUE);
    $this->data['title'] = 'Редактирование отзывов';
    $this->logs_data_type = 'Отзывы';
}

function index()
{
    $this->data['reviews'] = $this->admin_reviews_model->get_reviews_list();
    $this->data['title'] = 'Все отзывы';
    $this->data['users_list'] = $this->load->view('reviews_list_view',$this->data,TRUE);
    $this->load->view('admin/base_node_dashboard_view');
}

function edit_review_page($id = NULL)
{
    $this->data['review'] = $this->admin_reviews_model->get_review($id);
    $this->data['clinics'] = $this->admin_clinics_model->get_clinics_list();
    $this->data['clinics_html'] = '';
    foreach($this->data['clinics'] as $clinic)
    {
        if($clinic->id == $this->data['review'][0]->clinic_id)
            $this->data['clinics_html'] .= '<option selected value="'.$clinic->id.'">'.$clinic->title.' '.$clinic->district.'</option>';
        else
            $this->data['clinics_html'] .= '<option value="'.$clinic->id.'">'.$clinic->title.' '.$clinic->district.'</option>';
    }
    $this->data['title'] = 'Изменить отзыв ';
    $this->load->view('edit_review_view',$this->data);
}

function edit_review($id = NULL)
{
    $this->view_type = 2;
    $date_time = $this->input->post('date_review');
    if(empty($date_time))
	$date_pub = date("Y-m-d");
    else
	$date_pub = date("Y-m-d", strtotime($this->input->post('date_review')));
    if($this->input->post('moderated') == 'on')
        $is_vs = 1;
    else
        $is_vs = 0;
    $additional_data = array('name' => $this->input->post('name'),
			    'text' => $this->input->post('text'),
                            'moderated' => $is_vs,
			    'phone' => $this->input->post('phone'),
			    'clinic_id' => $this->input->post('clinic_id'),
			    'respondent_name' => $this->input->post('respondent_name'),
			    'response' => $this->input->post('response'),
			    'date_review'=>$date_pub);
    $result = $this->admin_reviews_model->edit_review($id,$additional_data);
    $this->data['result'] = $result;
    //логирование
    $this->admin_logs_model->add($id, $this->logs_data_type,$this->input->post('name'),'/admin_reviews/edit_review_page/'.$id,'Редактирование');
    echo $this->load->view('admin/json_result_view',$this->data);
}

function delete_review($id = NULL)
{
    $this->view_type = 2;
    $this->admin_reviews_model->delete_review($id);
    //логирование
    $this->admin_logs_model->add($id, $this->logs_data_type,'','/admin_reviews/edit_review_page/'.$id,'Удаление');
}

function add_review_page()
{
    $this->data['clinics'] = $this->admin_clinics_model->get_clinics_list();
    $this->data['clinics_html'] = '';
    foreach($this->data['clinics'] as $clinic)
    {
        $this->data['clinics_html'] .= '<option value="'.$clinic->id.'">'.$clinic->title.' '.$clinic->district.'</option>';
    }
    $this->data['title'] = 'Добавить отзыв ';
    $this->load->view('add_review_view',$this->data);
}

function add_review()
{
    $this->view_type = 2;
    $date_time = $this->input->post('date_review');
    if(empty($date_time))
	$date_pub = date("Y-m-d");
    else
	$date_pub = date("Y-m-d", strtotime($this->input->post('date_review')));
    if($this->input->post('moderated') == 'on')
        $is_vs = 1;
    else
        $is_vs = 0;
    $additional_data = array('name' => $this->input->post('name'),
			    'text' => $this->input->post('text'),
                            'moderated' => $is_vs,
			    'phone' => $this->input->post('phone'),
			    'clinic_id' => $this->input->post('clinic_id'),
			    'respondent_name' => $this->input->post('respondent_name'),
			    'response' => $this->input->post('response'),
			    'date_review'=>$date_pub);
    $result = $this->admin_reviews_model->add_review($additional_data);
    $this->data['result'] = $result['success'];
    //логирование
    $this->admin_logs_model->add($result['insert_id'], $this->logs_data_type,$this->input->post('name'),'/admin_reviews/edit_review_page/'.$result['insert_id'],'Добавление');
    echo $this->load->view('admin/json_result_view',$this->data);
}


}