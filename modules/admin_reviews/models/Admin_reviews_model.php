<?
class Admin_reviews_model extends CI_Model {
private $enterprise_query_data_list = '';
private $db_table = 'reviews';

function Admin_simple_services_model()
{
    parent::__construct();
}

function add_review($additional_data = array())
{
    $insert_result = array();
    $insert_result['success'] = $this->db->insert($this->db_table, $additional_data);
    $insert_result['insert_id'] = $this->db->insert_id();
    return $insert_result;
}

function get_reviews_list()
{
    $query = "SELECT reviews.*,clinics.title,clinics.district
    FROM reviews
    LEFT JOIN
    clinics
    ON
    clinics.id=reviews.clinic_id
    ORDER BY date_review DESC";
    $data = $this->db->query($query);
    return $data->result();
}

function get_review($id = NULL)
{
    $data = $this->db->get_where($this->db_table, array('id' => $id), 1);
    return $data->result();   
}

function edit_review($id = NULL,$additional_data = array())
{
    $this->db->where('id', $id);
    return $this->db->update($this->db_table, $additional_data);
}

function get_moderated_reviews()
{
    $query = "SELECT reviews.*,clinics.title,clinics.district
            FROM reviews
            LEFT JOIN
            clinics
            ON
            clinics.id=reviews.clinic_id
            WHERE
            reviews.moderated=1
            ORDER BY date_review DESC";
    $data = $this->db->query($query);
    return $data->result();
}

function get_reviews_frontend($id = NULL)
{
    $query = "SELECT reviews.*,clinics.title,clinics.district
            FROM reviews
            LEFT JOIN
            clinics
            ON
            clinics.id=reviews.clinic_id
            WHERE
            reviews.moderated=1
            AND reviews.id=$id";
    $data = $this->db->query($query);
    return $data->result();
}

function delete_review($id = NULL)
{
    $this->db->where('id', $id);
    $this->db->delete($this->db_table);
}

function get_reviews_count_from($count = 0,$pos = 0)
{
    $offset = '';
    $qty = '';
    if($count!=0)
        $qty = " LIMIT $count ";
    if($pos!=0)
        $offset = " OFFSET $pos ";
    $query = "SELECT reviews.*,clinics.title,clinics.district
            FROM reviews
            LEFT JOIN
            clinics
            ON
            clinics.id=reviews.clinic_id
            WHERE
            reviews.moderated=1
            ORDER BY date_review DESC
            $qty $offset";
    $data = $this->db->query($query);
    return $data->result();
}

}?>