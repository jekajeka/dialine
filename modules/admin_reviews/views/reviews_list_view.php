<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?=$this->data['title']?></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a href="/admin_reviews/add_services_page" class="add-link"><i class="fa fa-plus"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
                      <thead>
                        <tr>
                          <th><input type="checkbox" id="check-all" class="flat"></th>
                          <th>id</th>
                          <th>Отправитель</th>
                          <th>Телефон отправителя</th>
                          <th>Клиника отзыва</th>
                          <th>Дата отзыва</th>
                          <th>Состояние модерации</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?foreach($reviews as $review ){?>
                        <tr>
                          <td><input type="checkbox" class="flat" name="table_records"></td>
                          <td><?=$review->id?></td>
                          <td><a href="/admin_reviews/edit_review_page/<?=$review->id?>"><?=$review->name?></a></td>
                          <td><a href="/admin_reviews/edit_review_page/<?=$review->id?>"><?=$review->phone?></a></td>
                          <td><a href="/admin_reviews/edit_review_page/<?=$review->id?>"><?=$review->title.' '.$review->district?></a></td>
                          <td><a href="/admin_reviews/edit_review_page/<?=$review->id?>"><?=$review->date_review?></a></td>
                          <td><a href="/admin_reviews/edit_review_page/<?=$review->id?>"><?=$review->moderated?></a></td>
                        </tr>
                        <?}?>
                      </tbody>
                    </table>
                  </div>
                </div>
</div>
              