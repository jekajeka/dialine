<link href="/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
<script src="/vendors/iCheck/icheck.min.js"></script>
<div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?=$title?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a title="Удалить отзыв" href="/admin_reviews/delete_review/<?=$review[0]->id?>" class="add-link"><i class="fa fa-trash"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <input type="hidden" value="<?=$review[0]->id?>" id="id">
                    <form id="edit_review" data-parsley-validate class="form-horizontal form-label-left">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Клиент <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="name" class="form-control col-md-7 col-xs-12"  value="<?=$review[0]->name?>" name="name" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="moderated">Опубликовано?
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label class="">
                                <?if($review[0]->moderated) {?>
                                <input checked name="moderated" id="moderated" type="checkbox" class="flat" />
                                <?}
                                else {?>
                                <input name="moderated" id="moderated" type="checkbox" class="flat" />
                                <?}?>
                            </label>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">Телефон клиента<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="phone" class="form-control col-md-7 col-xs-12"  value="<?=$review[0]->phone?>"  name="phone" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="date_review">Дата отзыва<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="date_review" class="form-control has-feedback-left"  value="<?=date("m/d/Y", strtotime($review[0]->date_review))?>"  name="date_review" placeholder="" required="required" type="text">
                          <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                          <span id="inputSuccess2Status" class="sr-only">(success)</span>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type"> <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select name="clinic_id" class="form-control" placeholder="Выберите тип материала" id="clinic_id">
                                       <?=$clinics_html?>
                                    </select>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="text">Текст</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea name="text" class="form-control" placeholder="" rows="3" style="width: 100%; height: 135px;resize: none" id="text"><?=$review[0]->text?></textarea>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="respondent_name">Имя отвечающего
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="respondent_name" class="form-control col-md-7 col-xs-12"  value="<?=$review[0]->respondent_name?>"  name="respondent_name" placeholder=""  type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="response">Текст</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea name="response" class="form-control" placeholder="" rows="3" style="width: 100%; height: 135px;resize: none" id="response"><?=$review[0]->response?></textarea>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="add_user_final">
                          <button type="submit" class="btn btn-success">Сохранить</button>
                        </div>
                      </div>
                    </form>  
                  </div>
                </div>
              </div>
            </div>
</div>
<script src="/admin_js/admin_info/edit_review.js"></script>