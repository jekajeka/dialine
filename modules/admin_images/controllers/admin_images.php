<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_images extends Admin_Controller  {
function __construct()
{
    parent::__construct();
    $this->load->model('images_model', '', TRUE);
    //$this->title = 'Новости';
}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

    //Функция добавления изображений через цк эдитор
    public function add_image()
    {
	$this->view_type = 2;
	$callback = $_GET['CKEditorFuncNum'];
	$file_name = $_FILES['upload']['name'];
	$file_name_tmp = $_FILES['upload']['tmp_name'];
	$file_new_name = FCPATH.'/uploads/ckeditor/'; // серверный адрес - папка для сохранения файлов. (нужны права на запись)
	$full_path = $file_new_name.$file_name;
	$http_path = '/uploads/ckeditor/'.$file_name; // адрес изображения для обращения через http
	$error = '';
	if( move_uploaded_file($file_name_tmp, $full_path) )
	{
	// можно добавить код при успешном выполнение загрузки файла
	} else
	{
	$error = 'Ошибка, повторите попытку позже'; // эта ошибка появится в браузере если скрипт не смог загрузить файл
	$http_path = '';
	}
	echo "<script type=\"text/javascript\">// <![CDATA[
	window.parent.CKEDITOR.tools.callFunction(".$callback.",  \"".$http_path."\", \"".$error."\" );
	// ]]></script>";
    }

	
    function delete_image($id)
    {
	$this->view_type = 2;
	$result = $this->images_model->delete($id);
	echo $result;
    }

}

?>