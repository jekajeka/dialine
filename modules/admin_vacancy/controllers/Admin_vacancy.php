<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_vacancy extends Admin_Controller  {
function __construct()
{
    parent::__construct();
    $this->load->model('admin_vacancy_model', '', TRUE);
    $this->data['title'] = 'Редактирование вакансий';
    $this->logs_data_type = 'Вакансии';
}


function index()
{
    $this->data['vacancy'] = $this->admin_vacancy_model->get_vacancy_list();
    $this->data['title'] = 'Все вакансии';
    $this->data['users_list'] = $this->load->view('vacancy_list_view',$this->data,TRUE);
    $this->load->view('admin/base_node_dashboard_view');
}

function add_vacancy_page()
{
    $this->data['title'] = 'Добавить вакансию';
    $this->load->view('add_vacancy_page_view',$this->data);
}

function add_vacancy()
{
    $this->view_type = 2;
    if($this->input->post('enabled') == 'on')
        $enabled = 1;
    else
        $enabled = 0;
    $additional_data = array('title' => $this->input->post('title'),
                            'enabled' => $enabled,
                            'price' => $this->input->post('price'),
                            'vacancy_type' => $this->input->post('vacancy_type'),
                            'text' => $this->input->post('text'));
    $result = $this->admin_vacancy_model->add($additional_data);
    $this->data['result'] = $result['success'];
    //логирование
    $this->admin_logs_model->add($result['insert_id'], $this->logs_data_type,$this->input->post('title'),'/admin_vacancy/edit_vacancy_page/'.$result['insert_id'],'Добавление');
    echo $this->load->view('admin/json_result_view',$this->data);
}

function edit_vacancy($id = NULL)
{
    $this->view_type = 2;
    if($this->input->post('enabled') == 'on')
        $enabled = 1;
    else
        $enabled = 0;
    $additional_data = array('title' => $this->input->post('title'),
                            'enabled' => $enabled,
                            'price' => $this->input->post('price'),
                            'vacancy_type' => $this->input->post('vacancy_type'),
                            'text' => $this->input->post('text'));
    $result = $this->admin_vacancy_model->edit_node($id,$additional_data);
    $this->data['result'] = $result;
    //логирование
    $this->admin_logs_model->add($id, $this->logs_data_type,$this->input->post('title'),'/admin_vacancy/edit_vacancy_page/'.$id,'Редактирование');
    echo $this->load->view('admin/json_result_view',$this->data);
}

function edit_vacancy_page($id = NULL)
{
    $this->data['vacancy'] = $this->admin_vacancy_model->get_vacancy($id);
    $this->data['select'] = '<option value="0">Выберите тип вакансии</option>
                            <option value="Врачи">Врачи</option>
                            <option value="Средний медицинский персонал">Средний медицинский персонал</option>
                            <option value="Прочие вакансии">Прочие вакансии</option>';
    $this->data['select'] = str_replace('value="'.$this->data['vacancy'][0]->vacancy_type.'"', ' selected value="'.$this->data['vacancy'][0]->vacancy_type.'"',$this->data['select']);
    $this->data['title'] = 'Изменить вакансию '.$this->data['vacancy'][0]->title;
    $this->load->view('edit_vacancy_view',$this->data);
}

function delete_vacancy($id = NULL)
{
    $this->view_type = 2;
    $this->data['vacancy'] = $this->admin_vacancy_model->get_vacancy($id);
    //логирование
    $this->admin_logs_model->add($id, $this->logs_data_type,$this->data['vacancy'][0]->title,'/admin_vacancy/edit_vacancy_page/'.$id,'Удаление');
    $this->admin_vacancy_model->delete_node($id);
}

}
