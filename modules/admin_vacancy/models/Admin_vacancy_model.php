<?
class Admin_vacancy_model extends MY_Model {

function Admin_vacancy_model()
{
    parent::__construct();
    $this->db_table = 'vacancy';
}

function add($additional_data = array())
{
    $insert_result = array();
    $insert_result['success'] = $this->db->insert($this->db_table, $additional_data);
    $insert_result['insert_id'] = $this->db->insert_id();
    return $insert_result;
}

function get_moderated_vacancy()
{
    $this->db->select('*')->from($this->db_table)->where(array('enabled'=>1))->order_by("title", "ASC");
    $data = $this->db->get();
    return $data->result();
}

function get_vacancy_list()
{
    $this->db->select('*')->from($this->db_table)->order_by("title", "ASC");
    $data = $this->db->get();
    return $data->result();
}

function get_vacancy($id = NULL)
{
    $this->db->select('*')->from($this->db_table)->where(array('id'=>$id))->order_by("title", "ASC");
    $data = $this->db->get();
    return $data->result();
}

}
?>