<link href="/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
<script src="/vendors/iCheck/icheck.min.js"></script>
<div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?=$title?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form id="add_vacancy" data-parsley-validate class="form-horizontal form-label-left">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Описание вакансии <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="title" class="form-control col-md-7 col-xs-12"   name="title" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Оклад
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="price" class="form-control col-md-7 col-xs-12"   name="price" placeholder="" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="vacancy_type">Тип вакансии <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="vacancy_type" class="form-control" placeholder="Выберите тип вакансии" id="headings">
                            <option value="0">Выберите тип вакансии</option>
                            <option value="Врачи">Врачи</option>
                            <option value="Средний медицинский персонал">Средний медицинский персонал</option>
                            <option value="Прочие вакансии">Прочие вакансии</option>
                          </select>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="vacancy_type">Опубликовано
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="checkbox" class="flat" name="enabled">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="text">Текст</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea name="text" class="form-control" placeholder="" rows="3" style="width: 100%; height: 135px;resize: none" id="text"></textarea>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="add_user_final">
                          <button type="submit" class="btn btn-success">Добавить</button>
                        </div>
                      </div>
                    </form>  
                  </div>
                </div>
              </div>
            </div>
</div>
<script src="/modules/admin_vacancy/js/add_vacancy.js"></script>