<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?=$this->data['title']?></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a href="/admin_vacancy/add_vacancy_page" class="add-link"><i class="fa fa-plus"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
                      <thead>
                        <tr>
                          <th><input type="checkbox" id="check-all" class="flat"></th>
                          <th>id</th>
                          <th>Вакансия</th>
                          <th>Категория вакансии</th>
                          <th>Опубликовано?</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?foreach($vacancy as $vacancy_item ){?>
                        <tr>
                          <td><input type="checkbox" class="flat" name="table_records"></td>
                          <td><?=$vacancy_item->id?></td>
                          <td><a href="/admin_vacancy/edit_vacancy_page/<?=$vacancy_item->id?>"><?=$vacancy_item->title?></a></td>
                          <td><a href="/admin_reviews/edit_vacancy_page/<?=$vacancy_item->id?>"><?=$vacancy_item->vacancy_type?></a></td>
                          <td><a href="/admin_reviews/edit_vacancy_page/<?=$vacancy_item->id?>"><?=$vacancy_item->enabled?></a></td>
                        </tr>
                        <?}?>
                      </tbody>
                    </table>
                  </div>
                </div>
</div>
              