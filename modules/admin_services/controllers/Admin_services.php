<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_services extends Admin_Controller  {
function __construct()
{
    parent::__construct();
    $this->load->model('admin_services_model', '', TRUE);
    $this->data['title'] = 'Редактирование услуг';
}


function load_services_page()
{
    $this->data['title'] = 'Загрузка услуг';
    $this->load->view('load_services_view',$this->data);
}

function load_file($id = NULL)
{
    $this->view_type = 2;
    
    $config['upload_path'] = './uploads/';
    $config['allowed_types'] = 'txt';
    $config['max_size']	= '10000';
    $this->load->library('upload', $config);
    if ( ! $this->upload->do_upload('load_data'))
    {
	$this->data['result'] =  $this->upload->display_errors();
    }	
    else
    {
	$data = array('upload_data' => $this->upload->data());
	$services_lines = array();
	$handle = fopen('./uploads/'.$data['upload_data']['file_name'], "r");
	while (!feof($handle))
        {
	    $buffer = fgets($handle);
	    $services_lines[] = $buffer;
	}
	fclose($handle);
	$services_list = array();
	foreach($services_lines as $key=>$service_line)
	{
	    $services_list[$key] = explode ("	",$service_line);
	}
	
	$rubric = 'Без рубрики';
	$service_type = 'Общие услуги';
	$rubrics_array = array();
	$service_for_db = array();
	foreach($services_list as $service_single)
	{
	    if(!empty($service_single[1]))
	    {
		if($service_single[0] == 'url')
		{
		    $rubric = $service_single[1];
		    $service_type = 'Общие услуги';
		}
		else if(!is_numeric(str_replace(' ','',trim($service_single[2]))))
		{
		    $service_type = $service_single[1];
		}
		else
		{
		    $service_for_db[] = array('title'=>$service_single[1],'price'=>str_replace(' ','',trim($service_single[2])),'rubric'=>$rubric,'service_type'=>$service_type);
		}
	    }
	}
        
	$this->data['result'] = $this->admin_services_model->load_data($service_for_db);
    }
    echo $this->load->view('admin/json_result_view',$this->data);
}


function services_page()
{
    $this->data['services'] = $this->admin_services_model->get_doctors_list();
    $this->data['title'] = 'Все услуги';
    $this->data['users_list'] = $this->load->view('services_list_view',$this->data,TRUE);
    $this->load->view('admin/base_node_dashboard_view');
}

}