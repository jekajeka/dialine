<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends Frontend_Controller  {
function __construct()
{
    parent::__construct();
    //$this->load->model('frontend_info_model', '', TRUE);
    //$this->title = 'Информация/Диалайн';
}

function send_message()
{
    $this->view_type = 2;
    $this->load->library('email');
    $this->email->from('notify@dialine.org', 'Клиника Диалайн');
    if($this->input->post('purpose') == 'Получить дисконтную карту')
        $this->email->to('dzhumanova.d@dialine.org');
    else
        $this->email->to('ask@dialine.org,ccbox@dialine.org');
    $message = 'Имя: '.$this->input->post('name').'
Телефон:  '.$this->input->post('phone');
    if($this->input->post('order'))
        $message .= '
'.$this->input->post('order');
    $this->email->subject($this->input->post('purpose').' (диалайн.рф)');
    $this->email->message($message);
    $this->email->send();
}

/*!
 *@brief Отправка почты со страницы врача по заказу конкретных услуг
 *@details Антон Вопилов 17.10.2017
 *@return 
 */

    function send_doctor_service()
    {
        $this->view_type = 2;
        $this->load->library('email');
        $this->email->from('notify@dialine.org', 'Клиника Диалайн');
        if($this->input->post('purpose') == 'Получить дисконтную карту')
            $this->email->to('dzhumanova.d@dialine.org');
        else
            $this->email->to('ask@dialine.org,ccbox@dialine.org');

        $message = 'Врач: '.$this->input->post('doctor_name').'
Услуга: '.$this->input->post('purpose').'
Имя: '.$this->input->post('name').'
Телефон:  '.$this->input->post('phone');

        if($this->input->post('order'))
            $message .= '
'.$this->input->post('order');
        $this->email->subject('Заявка из врача на услугу: (диалайн.рф)');
        $this->email->message($message);
        $this->email->send();
        echo $message;
    }

/*!
 *@brief Отправка почты со страницы клиники по заказу кокретных услуг
 *@details Антон Вопилов 17.10.2017
 *@return 
 */
function send_clinic_service()
{
    $this->view_type = 2;
    $this->load->library('email');
    $this->email->from('notify@dialine.org', 'Клиника Диалайн');
    $this->email->to('ask@dialine.org,ccbox@dialine.org');
    $message = 'Клиника: '.$this->input->post('clinic_name').'
Услуга: '.$this->input->post('purpose').'
Имя: '.$this->input->post('user_name').'
Телефон:  '.$this->input->post('phone');
    if($this->input->post('order'))
        $message .= '
'.$this->input->post('order');
    $this->email->subject('Заявка из клиники на услугу: (диалайн.рф)');
    $this->email->message($message);
    $this->email->send();
    echo $message;
}

function send_subservice_service()
{
    $this->view_type = 2;
    $this->load->library('email');
    $this->email->from('notify@dialine.org', 'Клиника Диалайн');
    $this->email->to('ask@dialine.org,ccbox@dialine.org');
    $message = 'Отправлено со страницы: '.$this->input->post('page_title').'
Услуга: '.$this->input->post('purpose').'
Имя: '.$this->input->post('name').'
Телефон:  '.$this->input->post('phone');
    if($this->input->post('order'))
        $message .= '
'.$this->input->post('order');
    $this->email->subject('Заявка из сервисной страницы на услугу: (диалайн.рф)');
    $this->email->message($message);
    $this->email->send();
    echo $message;
}

/*!
 *@brief Отправка почты только по хирургии
 *@details Антон Вопилов 1.11.2017
 *@return 
 */
function send_hirurg()
{
    $this->view_type = 2;
    $this->load->library('email');
    $this->email->from('notify@dialine.org', 'Клиника Диалайн');
    $this->email->to('us.o@dialine.org,kuzmenko.o@dialine.org,orlova.m@dialine.org,reception4@dialine.org,ccbox@dialine.org,ask@dialine.org');
    $message = 'Врач: '.$this->input->post('doctor_name').'
Услуга: '.$this->input->post('purpose').'
Имя: '.$this->input->post('name').'
Телефон:  '.$this->input->post('phone');
    if($this->input->post('order'))
        $message .= '
'.$this->input->post('order');
    $this->email->subject('Заявка из врача на услугу: (диалайн.рф)');
    $this->email->message($message);
    $this->email->send();
    echo $message;
}

/*!
 *@brief Отправка вопроса по анализам
 *@details Антон Вопилов 2.4.2018
 *@return 
 */
function sendAnalyzeQuestion()
{
    $this->view_type = 2;
    $this->load->library('email');
    $this->email->from('notify@dialine.org', 'Клиника Диалайн');
    $this->email->to('ask@dialine.org,ccbox@dialine.org');
    $message = 'Имя: '.$this->input->post('name').'
Телефон: '.$this->input->post('phone').'
Вопрос: '.$this->input->post('question');
    $this->email->subject($this->input->post('purpose'));
    $this->email->message($message);
    $this->email->send();
    echo $message;
}


}

?>