<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?=$this->data['title']?></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a href="/admin_simple_services/add_simple_services_page" class="add-link"><i class="fa fa-plus"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
                      <thead>
                        <tr>
                          <th><input type="checkbox" id="check-all" class="flat"></th>
                          <th>id</th>
                          <th>Услуга</th>
                          <th>Адрес</th>
                          <th>Рубрика</th>
                          <th>Опубликовано</th>
                          <th>Посмотреть на сайте</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?foreach($simple_services as $service ){?>
                        <tr>
                          <td><input type="checkbox" class="flat" name="table_records"></td>
                          <td><?=$service->id?></td>
                          <td><a href="/admin_simple_services/edit_simple_service_page/<?=$service->id?>"><?=$service->title?></a></td>
                          <td><a href="/admin_simple_services/edit_simple_service_page/<?=$service->id?>"><?=$service->path?></a></td>
                          <td>
                            <?
                            switch($service->type)
                            {
                                case 0:
                                    $service_type = 'Диагностика';
                                break;
                                case 1:
                                    $service_type = 'Лечение';
                                break;
                                case 2:
                                    $service_type = 'Профилактика';
                                break;
                                case 3:
                                    $service_type = 'Корпоративным клиентам';
                                break;
                                default:
                                    $service_type = 'Не определено';
                            }
                            ?>
                            <a href="/admin_simple_services/edit_simple_service_page/<?=$service->id?>"><?=$service_type?></a>
                          </td>
                          <td>
                                <?
                                if($service->published)
                                                $published = 'Опубликовано';
                                if($service->published == 0)
                                                $published = 'Не опубликовано';
                                if($service->published == NULL)
                                                $published = 'Опубликовано (галочка не стоит)';
                                echo $published;
                                ?>
                          </td>
                          <td><a target="_blank" href="/services/additional/<?=$service->path?>">Посмотреть</a>  </td>
                        </tr>
                        <?}?>
                      </tbody>
                    </table>
                  </div>
                </div>
</div>
              