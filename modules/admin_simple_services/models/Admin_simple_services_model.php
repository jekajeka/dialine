<?
class Admin_simple_services_model extends CI_Model {
private $enterprise_query_data_list = '';
private $db_table = 'simple_services';

function Admin_simple_services_model()
{
    parent::__construct();
}

function add_simple_service($additional_data = array())
{
    $insert_result = array();
    $insert_result['success'] = $this->db->insert($this->db_table, $additional_data);
    $insert_result['insert_id'] = $this->db->insert_id();
    return $insert_result;
}

function get_simple_services_list()
{
    $this->db->select(array($this->db_table.'.*','published.published'));
    $this->db->from($this->db_table);
    $this->db->join('published', "published.type = 'simple_services' AND published.record_id = ".$this->db_table.".id ",'left');
    return $this->db->get()->result();
}

function get_single_service($id = NULL)
{
    $data = $this->db->get_where($this->db_table, array('id' => $id), 1);
    return $data->result();   
}

function edit_simple_service($id = NULL,$additional_data = array())
{
    $this->db->where('id', $id);
    return $this->db->update($this->db_table, $additional_data);
}

function delete_simple_service($id = NULL)
{
    $this->db->where('id', $id);
    $this->db->delete($this->db_table);
}

}?>