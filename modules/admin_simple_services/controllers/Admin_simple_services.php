<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_simple_services extends Admin_Controller  {
function __construct()
{
    parent::__construct();
    $this->load->model('admin_simple_services_model', '', TRUE);
    $this->load->model('admin_published/admin_published_model', '', TRUE);
    $this->data['title'] = 'Редактирование дополнительных услуг';
}

function index()
{
    $this->data['simple_services'] = $this->admin_simple_services_model->get_simple_services_list();
    $this->data['title'] = 'Все особые услуги';
    $this->data['users_list'] = $this->load->view('simple_services_list_view',$this->data,TRUE);
    $this->load->view('admin/base_node_dashboard_view');
}

function add_simple_services_page()
{
    $this->data['title'] = 'Добавить дополнительную услугу';
    $this->load->view('add_simple_service_page_view',$this->data);
}

function add_simple_service()
{
    $this->view_type = 2;
    $additional_data = array('title' => $this->input->post('title'),
                            'path' => $this->input->post('path'),
                            'type' => $this->input->post('type'),
			    'text' => $this->input->post('text')
			);
    $result = $this->admin_simple_services_model->add_simple_service($additional_data);
    $this->data['result'] = $result['success'];
    //Извлечение состояния публикации медпрограммы
    if($this->input->post('published') == 'on')
	$published = 1;
    else
    $published = 0;
    $this->admin_published_model->update_published('simple_services', $result['insert_id'],$published);
    echo $this->load->view('admin/json_result_view',$this->data);
}

function edit_simple_service_page($id = NULL)
{
    $this->data['simple_service'] = $this->admin_simple_services_model->get_single_service($id);
    $this->data['type'] = '<option value="all">Выберите тип услуги</option>
                            <option value="0">Диагностика</option>
                            <option value="1">Лечение</option>
                            <option value="2">Профилактика</option>
                            <option value="3">Корпоративным клиентам</option>';
    $this->data['type'] = str_replace('value="'.$this->data['simple_service'][0]->type.'"',' selected="1" value="'.$this->data['simple_service'][0]->type.'"',$this->data['type']);
    $this->getSeo('simple_services', $id);
    $this->data['published'] = $this->admin_published_model->get_status('simple_services',$id);
    $this->load->view('edit_simple_service_page_view',$this->data);
}

function edit_simple_service($id = NULL)
{
    $this->view_type = 2;
    $additional_data = array('title' => $this->input->post('title'),
                            'path' => $this->input->post('path'),
                            'type' => $this->input->post('type'),
			    'text' => $this->input->post('text')
			);
    $result = $this->admin_simple_services_model->edit_simple_service($id,$additional_data);
    //Извлечение состояния публикации медпрограммы
    if($this->input->post('published') == 'on')
	$published = 1;
    else
	$published = 0;
    $this->admin_published_model->update_published('simple_services', $id,$published);
    $this->saveSeo('simple_services', $id);
    $this->data['result'] = $result;
    echo $this->load->view('admin/json_result_view',$this->data);
}

function delete_simple_service($id = NULL)
{
    $this->view_type = 2;
    $this->admin_published_model->delete_published('simple_services', $id);
    $this->admin_simple_services_model->delete_simple_service($id);
}

}
