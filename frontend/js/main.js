
//расчет высоты 2х полей поска в всплывающем окне
function calc_search_fields_heights()
{
    $('.find_special_wrap_content .search_results_data').height($('.row_search').height() - 264);
    $('.find_scrubs_wrap .search_results_data').height($('.row_search').height() - 320);
}

$(document).ready(function(){
    calc_search_fields_heights()
    
    if ($(window).scrollTop()> 149)
	{
	    $('.row.main_menu').addClass('sticky').stop()
	    $('.row.row_search').addClass('higher').stop()
	    calc_search_fields_heights()
	    
	}
	
    
    
    $(window).on("orientationchange",function(event){
        if (event.orientation == 'landscape')
            $('meta[name="viewport"]').attr('content','width=1200')
        else
            $('meta[name="viewport"]').attr('content','width=640')
            
    });
    
    $(window).scroll(function(){
	if ($(window).scrollTop()> 149)
	{
	    $('.row.main_menu').addClass('sticky').stop()
	    $('.row.row_search').addClass('higher').stop()
	    setTimeout(function () {
		calc_search_fields_heights()
		}, 1000);
	}
	else
	{
	    $('.row.main_menu').removeClass('sticky').stop()
	    $('.row.row_search').removeClass('higher').stop()
	    setTimeout(function () {
		calc_search_fields_heights()
		}, 1000);
	}
	});
    
    
    
    
    //главный слайдер
    var main_Swiper = new Swiper('.row.super_slider',{
        loop:true,
        grabCursor: true,
        onlyExternal : false,
	autoplay: 5000,
        pagination: '.switches',
        paginationClickable: true
      })
    
    //прокрутка контента в адресах
    $(".addresses_wrap").mCustomScrollbar({
	    theme:"minimal"
	});
    
     
    
    //слайдер отзывов
    var review_Swiper = new Swiper('.reviews_container',{
        loop:true,
        grabCursor: false,
        slidesPerView : 2,
        onlyExternal : false
      })
    $('.reviews_left').on('click', function(e){
    e.preventDefault()
    review_Swiper.swipePrev()
  })

  $('.reviews_right').on('click', function(e){
    e.preventDefault()
    review_Swiper.swipeNext()
  })
  
  $('.search_specialist_row select#age').select2({
        minimumResultsForSearch: Infinity,
        width:'165px'
        });
  $('.search_specialist_row select#speciality').select2({
        minimumResultsForSearch: Infinity,
        width:'240px'
        });
  $('.search_specialist_row select#clinic').select2({
        minimumResultsForSearch: Infinity,
        width:'235px'
        });
  
  $('.search_specialist_row select#services_data_swith').select2({
        minimumResultsForSearch: Infinity,
        width:'155px'
        });
  
  
  function main_search_doc()
  { 
	$. ajax ({
      url: '/doctors/search_doctor_complicated_on_top/',
      type: 'POST',
      data: {
        age:$('#age').val(),
	speciality:$('#speciality').val(),
	clinic:$('#clinic').val()
        },
      dataType: 'HTML',
      success: function(data){
      	if(data){
            $('#vrachi_data .find_special_wrap_content .search_results .search_results_data .mCSB_container').html(data);
        }
        else{
            $('#vrachi_data .find_special_wrap_content .search_results .search_results_data .mCSB_container').html('<h2>Простите, по запросу не найдено врачей</h2>');
        }
      }
    });
  }
  
  $('.search_specialist_row select#age').on('select2:close', function (evt) {
    evt.preventDefault()
    main_search_doc();
  });
  
  $('.search_specialist_row select#speciality').on('select2:close', function (evt) {
    evt.preventDefault()
    main_search_doc();
  });
  
  $('.search_specialist_row select#clinic').on('select2:close', function (evt) {
    evt.preventDefault()
    main_search_doc();
  });
  
  
  $('.search_specialist_row select#services_data_swith').on('select2:close', function (evt) {
    $. ajax ({
      url: $('#services_data_swith').val(),
      type: 'POST',
      data: {},
      dataType: 'HTML',
      success: function(data){
      	if(data){
            $('#services_data .find_special_wrap_content .search_results .search_results_data .mCSB_container').html(data);
        }
      }
    });
  });
  
  $('#program_overlay').click(function(e){
	e.preventDefault()
	$. ajax ({
      url: '/info/program_overlay',
      type: 'POST',
      data: {},
      dataType: 'HTML',
      success: function(data){
      	if(data){
            $('#services_data .find_special_wrap_content .search_results .search_results_data .mCSB_container').html(data);
        }
      }
    });
  });
  
  $('#corporate_overlay').click(function(e){
	e.preventDefault()
	$. ajax ({
      url: '/services/get_simple_services_list_by_type/3',
      type: 'POST',
      data: {},
      dataType: 'HTML',
      success: function(data){
      	if(data){
            $('#services_data .find_special_wrap_content .search_results .search_results_data .mCSB_container').html(data);
        }
      }
    });
  });
  
  
  $('#overlay_form_select').select2({
        minimumResultsForSearch: Infinity
        });
  
  
  $('#vrachi_menu').click(function(e){
    e.preventDefault()
    $('.row.row_search').removeClass('active');
    $('.row.row_search#vrachi_data').toggleClass('active');
    })
  $('#services_menu').click(function(e){
    e.preventDefault()
    $('.row.row_search').removeClass('active');
    $('.row.row_search#services_data').toggleClass('active');
    })
  
  $('.row_search_close').click(function(e){
    e.preventDefault()
    $('.row.row_search').removeClass('active');
    })
  
  $('#direction').select2({
        minimumResultsForSearch: Infinity,
        width:'216px'
        });
  
  $('.left_menu #direction').select2({
        minimumResultsForSearch: Infinity
        //width:'216px'
        });
  $('.news_select').select2({
        minimumResultsForSearch: Infinity
        });
  
  $('body').on('click','.toggle_wrapper a.toggle_title',function(e){
    e.preventDefault()
    
    
    if($(this).hasClass('active'))
	$(this).next().slideUp();
    else
	$(this).next().slideDown();
	
    $(this).toggleClass('active');
    })
  
  $('select#age_page').select2({
        minimumResultsForSearch: Infinity,
        width:'140px'
        });
  $('select#speciality_page').select2({
        minimumResultsForSearch: Infinity,
        width:'174px'
        });
  $('select#clinic_page').select2({
        minimumResultsForSearch: Infinity,
        width:'240px'
        });
  
  $('.toggle_data.paginate_scrubs').pajinate({
	items_per_page : 8,
	item_container_id : '.paginate_scrubs_ul',
	nav_panel_id : '.paginate_scrubs_navigation'
    });
  
  $('select#speciality_page_price').select2({
        minimumResultsForSearch: Infinity
        });
  
  
  $('body').on('click','a.structured_data_title',function(e){
    e.preventDefault()
    if($(this).hasClass('active'))
	$(this).next().slideUp();
    else
	$(this).next().slideDown();
	
    $(this).toggleClass('active');
    })
  
  //прокрутка контента в дополнительных врачах
    $(".doctors_additional_panel").mCustomScrollbar({
	    theme:"minimal"
	});
    
    $(".search_results_data").mCustomScrollbar({
	    theme:"minimal"
	});
    
    //Заказ услуг на странице
    $('.structured_data_table_wrap_order .structured_data_table_link').click(function(e){
        e.preventDefault()
        $('.service_result_data_wrap').append('<div class="service_result_data_line" price="'+parseInt($(this).prev().find('.structured_data_table_line_price').text())+'"><div class="service_result_data_line_row"><div class="service_result_data_title">'+$(this).prev().find('h4').text()+'</div><a class="service_result_data_drop"></a></div></div>')
        var total = 0;
        $('.service_result_data_line').each(function(indx,element){
            total += parseInt($(element).attr('price'))
            $('.service_result_total span').text(indx+1 + ' услуг')
        });
        $('.total_data span').text(total+' р.')
        
        })
    
    $('body').on('click', '.service_result_data_drop', function(e){
        e.preventDefault()
        $(this).closest('.service_result_data_line').remove()
        var total = 0;
        $('.service_result_data_line').each(function(indx,element){
            total += parseInt($(element).attr('price'))
            $('.service_result_total span').text(indx+1 + ' услуг')
        });
        $('.total_data span').text(total+' р.')
    });
    
    //слайдер врачей конкретной специализации
    var specialists_carousel_Swiper = new Swiper('.specialists_carousel',{
        loop:false,
        grabCursor: true,
        onlyExternal : false,
        paginationClickable: false,
	slidesPerView : 'auto'
      })
    
    $('.specialists_carousel_link_left').on('click', function(e){
    e.preventDefault()
    specialists_carousel_Swiper.swipePrev()
  })
  $('.specialists_carousel_link_right').on('click', function(e){
    e.preventDefault()
    specialists_carousel_Swiper.swipeNext()
  })


  
  //Переключение вкладок на странице специализации
  $('.swither_wrap a').click(function(e){
	e.preventDefault()
	$('.swither_wrap a').removeClass('active').removeClass('box_shadow')
	$(this).addClass('active').addClass('box_shadow')
	$('.switch_text_wrap .switch_text').removeClass('active')
	$('.switch_text_wrap .switch_text:eq('+$(this).index()+')').addClass('active')
    })
  
  $('.specialist_switch').click(function(e){
    specialists_carousel_Swiper.reInit()
    })
  //Слайдер медицинских программ
  
  var programs_carousel_wrap_Swiper = new Swiper('.programs_carousel_wrap',{
    loop:false,
        grabCursor: false,
        onlyExternal : false,
        paginationClickable: false,
	slidesPerView : 'auto'
  });
  
  $('.program_carousel_link_left').on('click', function(e){
    e.preventDefault()
    programs_carousel_wrap_Swiper.swipePrev()
  })
  $('.program_carousel_link_right').on('click', function(e){
    e.preventDefault()
    programs_carousel_wrap_Swiper.swipeNext()
  })
    
    //Разворачивание пунктов графика приема
    $('body').on('click','.audition_wrap h2',function(e){
    e.preventDefault()
    if($(this).hasClass('active'))
	$(this).next().slideUp();
    else
	$(this).next().slideDown();
    $(this).toggleClass('active')
    });
    
    //Слайдер хронологии
    var hrono_carousel_wrap_Swiper = new Swiper('.story_slider',{
    loop:false,
        grabCursor: false,
        onlyExternal : false,
        paginationClickable: false,
	slidesPerView : 'auto'
  });
    
  
//открытие закрытие всплывающей формы
    
    $('.oms_card_item .green_btn').click(function(e){
	e.preventDefault()
	$('.overlay_form_wrapper').show()
    })
    
    $('.close_overlay_form').click(function(e){
	e.preventDefault()
	$('.overlay_form_wrapper').hide()
    })
    
    //Выравнивание блоков по высоте
    function equalHeight(group) {
    var tallest = 0;
    group.each(function() {
      thisHeight = $(this).height();
      if(thisHeight > tallest) {
        tallest = thisHeight;
      }
    });
    group.height(tallest);
  }
  equalHeight($(".programm_legend_item_target"));
  equalHeight($(".programm_legend_item_address"));
  equalHeight($(".programm_legend_item_specialist"));
  equalHeight($(".programm_legend_item_diagnost"));
  equalHeight($(".programm_legend_item_laborator"));
  equalHeight($(".programm_legend_item_bonus"));
  
  //Поиск врачей по текстам
    $('#search_doctor').keyup(function(event){
    if(event.keyCode == 13){
	
        event.preventDefault();
        $. ajax ({
      url: '/doctors/doctor_text_search',
      type: 'POST',
      data: {
        search_text:$('#search_doctor').val()
        },
      dataType: 'HTML',
      success: function(data){
      	if(data){
            $('.specialists_row .container').html(data);
        }
        else{
            $('.specialists_row .container').html('<h2>Простите, по запросу не найдено врачей</h2>');
        }
      }
    });
    }
    });
    
    //Поиск врачей по текстам c выпадающей формы
    $('.search_scrubs#search_scrubs_overflow').click(function(event){
    
	
        event.preventDefault();
        $. ajax ({
      url: '/doctors/doctor_text_search_4_main',
      type: 'POST',
      data: {
        search_text:$('#find_scribs_overflow').val()
        },
      dataType: 'HTML',
      success: function(data){
      	if(data){
            $('.find_scrubs_wrap .search_results .search_results_data#search_results_data_scrubs .mCSB_container').html(data);
        }
        else{
            $('.find_scrubs_wrap .search_results .search_results_data#search_results_data_scrubs .mCSB_container').html('<p>Простите, по запросу не найдено врачей</p>');
        }
      }
    });
    });
    
    
    //Поиск услуг по текстам c выпадающей формы
    $('.search_scrubs#search_services_overflow').click(function(event){
    
	
        event.preventDefault();
        $. ajax ({
      url: '/services/text_search_services',
      type: 'POST',
      data: {
        search_text:$('#find_services_overflow').val()
        },
      dataType: 'HTML',
      success: function(data){
      	if(data){
            $('.find_scrubs_wrap .search_results .search_results_data#search_results_data_services .mCSB_container').html(data);
        }
        else{
            $('.find_scrubs_wrap .search_results .search_results_data#search_results_data_services .mCSB_container').html('<p>Простите, по запросу не найдено услуг</p>');
        }
      }
    });
    });
    
    //Поиск врачей в специальности
    $('#search_doctor_speciality').keyup(function(event){
    if(event.keyCode == 13){
	
        event.preventDefault();
        $. ajax ({
      url: '/doctors/doctor_text_search_speciality/'+$('#search_doctor_speciality').attr('speciality'),
      type: 'POST',
      data: {
        search_text:$('#search_doctor_speciality').val()
        },
      dataType: 'HTML',
      success: function(data){
      	if(data){
            $('.specialists_row .container').html(data);
        }
        else{
            $('.specialists_row .container').html('<h2>Простите, по запросу не найдено врачей</h2>');
        }
      }
    });
    }
    });
    
    //Поиск услуг по странице услуг
    $('#search_all_service').keyup(function(event){
    if(event.keyCode == 13){
	
        event.preventDefault();
        $. ajax ({
      url: '/services/text_search_services_all_page/',
      type: 'POST',
      data: {
        search_text:$('#search_all_service').val()
        },
      dataType: 'HTML',
      success: function(data){
      	if(data){
            $('.all_services_all_data_block').html(data);
        }
        else{
            $('.all_services_all_data_block').html('<h2>Простите, по запросу не найдено услуг</h2>');
        }
      }
    });
    }
    });
    
    //Поиск услуг по странице прайса
    $('#search_service_by_title').keyup(function(event){
    if(event.keyCode == 13){
        event.preventDefault();
	$. ajax ({
	    url: '/info/search_service_by_title/',
	    type: 'POST',
	    data: {
	      search_text:$('#search_service_by_title').val()
	      },
	    dataType: 'HTML',
	    success: function(data){
	      if(data){
		  $('.structured_data_wrap').html(data);
	      }
	      else{
		  $('.structured_data_wrap').html('<h2>Простите, по запросу не найдено услуг</h2>');
	      }
	    }
	  });
    }
    });
    
    $('#page_price_go').click(function(e){
	e.preventDefault()
	$. ajax ({
	    url: '/info/search_service_by_params/',
	    type: 'POST',
	    data: {
	      age:$('#age_page').val(),
	      speciality:$('#speciality_page_price').val()
	      },
	    dataType: 'HTML',
	    success: function(data){
	      if(data){
		  $('.structured_data_wrap').html(data);
	      }
	      else{
		  $('.structured_data_wrap').html('<h2>Простите, по запросу не найдено услуг</h2>');
	      }
	    }
	  });
	})

    
    
    $('#search_doctor_complicated').click(function(e){
	e.preventDefault()
	$. ajax ({
      url: '/doctors/search_doctor_complicated/',
      type: 'POST',
      data: {
        age:$('#age_page').val(),
	speciality:$('#speciality_page').val(),
	clinic:$('#clinic_page').val()
        },
      dataType: 'HTML',
      success: function(data){
      	if(data){
            $('.specialists_row .container').html(data);
        }
        else{
            $('.specialists_row .container').html('<h2>Простите, по запросу не найдено врачей</h2>');
        }
      }
    });
	
	})
    
    $('.left_menu select#direction').on('select2:close', function (evt) {
	
	//alert($(this).val())
	$('.all_services_all_data_block .toggle_wrapper').hide()
	$('.all_services_all_data_block .toggle_wrapper#'+$(this).val()).show();
	if($(this).val()=='all')
	    $('.all_services_all_data_block .toggle_wrapper').show();
	
    });
    
    $('.open_overlay_form_wrapper').click(function(e){
	e.preventDefault()
	$('.overlay_form_wrapper').show();
	})
    
    
    $('.more_advantages_btn').click(function(e){
	e.preventDefault()
	$('.other_adv_wrap').show(500)
	$('.more_advantages_btn').hide();
	})
    
    
    //клик по геотаргетингу
    $('.geotargeting').click(function(e){
	e.preventDefault()
	$('body').scrollTo('.map_operate_wrap',500,{offset:-220});
	$('.more_advantages_btn').hide();
	})
    
    //переключение города
    $('.selector_sity a').click(function(e){
	e.preventDefault()
	Cookies.set('city_phone', $(this).attr('phone'));
	Cookies.set('city', $(this).text());
	$('.your_city_wrap>a').text($(this).text())
	$('.phone_header_data>a').text($(this).attr('phone'))
	$('.phone_header_data>a').attr('href','tel: '+$(this).attr('phone'))
	})
    
    //отправка простой формы заявки
    $('.send_overlay_form').click(function(e){
	e.preventDefault()
	if($(this).hasClass('disabled')) return;
	if(($('#overlay_form_phone').val() != '') || ($('#overlay_form_name').val() != ''))
	{
	     $.ajax({
			type: 'POST',
			url: '/post/send_message',
			data:{
			purpose:$('#overlay_form_select').val(),
			name:$('#overlay_form_name').val(),
			phone:$('#overlay_form_phone').val()
			},
			scriptCharset: "utf-8",
			success: function(data){
			    $('.send_overlay_form').text("Спасибо за обращение!").css('background','#00abb2')
			    $('.send_overlay_form').addClass('disabled')
			}
			});
	}
	else
	{
	    if($('#overlay_form_phone').val() == '')	    
		$('#overlay_form_phone').css('border','1px solid #e94067');
	    if($('#overlay_form_name').val() == '')
		$('#overlay_form_name').css('border','1px solid #e94067');
	}
	})
    
    
//    jQuery("#user-city").val(ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country);
//alert(ymaps.geolocation.city);
function init2 () {
    ymaps.geolocation.get({
    // Выставляем опцию для определения положения по ip
    provider: 'yandex',
    // Автоматически геокодируем полученный результат.
    autoReverseGeocode: true
}).then(function (result) {
    // Выведем в консоль данные, полученные в результате геокодирования объекта.
    var datares = new Object(result.geoObjects.get(0).properties.get('metaDataProperty'))
    console.log(datares.GeocoderMetaData.AddressDetails.Country.AddressLine);
    if(!Cookies.get('city'))
    {
	$('.selector_sity a[city="'+datares.GeocoderMetaData.AddressDetails.Country.AddressLine+'"]').click()
    }
});
}
ymaps.ready(init2);

$("#overlay_form_phone").mask("+7(999) 999 99 99");
    
    
});