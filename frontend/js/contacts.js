function init () {
    var myMap = new ymaps.Map("map", {
            center: [48.758625,44.818625],
            zoom: 18
        }, {
            searchControlProvider: 'yandex#search'
        }),

    // Создаем геообъект с типом геометрии "Точка".
        myGeoObject = new ymaps.GeoObject({
            // Описание геометрии.
            geometry: {
                type: "Point",
                coordinates: [55.8, 37.8]
            },
            // Свойства.
            properties: {
                // Контент метки.
                iconContent: 'Я тащусь',
                hintContent: 'Ну давай уже тащи'
            }
        }, {
            // Опции.
            // Иконка метки будет растягиваться под размер ее содержимого.
            preset: 'islands#blackStretchyIcon',
            // Метку можно перемещать.
            draggable: true
        });
myMap.behaviors.disable('scrollZoom');
    myMap.geoObjects
        .add(myGeoObject)
        
        .add(new ymaps.Placemark([48.758625,44.818625], {
            balloonContent: 'Центр Хирургии Диалайн',
            iconCaption: 'Центр Хирургии Диалайн'
        }, {
            preset: 'islands#hospitalIcon',
	    iconColor: '#00abb2'
        }));
}

