$(document).ready(function(){
    //Поиск статей полнотекстовый
    $('#search_rules').keyup(function(event){
    if(event.keyCode == 13){
        event.preventDefault();
        $. ajax ({
      url: '/info/rules_search',
      type: 'POST',
      data: {
        search_text:$('#search_rules').val()
        },
      dataType: 'HTML',
      success: function(data){
      	if(data){
            $('#rules_data').html(data);
        }
        else{
            $('#rules_data').html('<h2>Простите, по запросу не найдено статей</h2>');
        }
      }
    });
    }
    });
});