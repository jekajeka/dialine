$(document).ready(function () {
	   var image = $('#image_map');

	   image.mapster(
       {
       		fillOpacity: 0.4,
       		fillColor: "d42e16",
       		/*
			strokeColor: "3320FF",
       		strokeOpacity: 0.8,
       		strokeWidth: 4,
       		stroke: true,
			*/
            isSelectable: true,
           singleSelect: true,
            mapKey: 'name',
            listKey: 'name',
            
			
			onClick: function (e) {
                                 //window.location.href($(this).attr('href'))
                                 document.location.href=$(this).attr('href')
				//alert($(this).attr('name'));

            },

            areas: [
                {	
					key: "tests",
					isSelectable: false,
					fillColor: "FFFFFF"
                },
				{	
					key: "doctors",
					isSelectable: false,
					fillColor: "FFFFFF"
                },
				{	
					key: "rentgen",
					isSelectable: false,
					fillColor: "FFFFFF"
                },
				
				{	
					key: "hirurgiya",
					isSelectable: false,
					fillColor: "FFFFFF"
                },
				{	
					key: "fiziolechenie",
					isSelectable: false,
					fillColor: "FFFFFF"
                },
				{	
					key: "endoskopia",
					isSelectable: false,
					fillColor: "FFFFFF"
                },
				{	
					key: "funkc_diagnostika",
					isSelectable: false,
					fillColor: "FFFFFF"
                },
				{	
					key: "uzi",
					isSelectable: false,
					fillColor: "FFFFFF"
                },
                ]
        });
      });