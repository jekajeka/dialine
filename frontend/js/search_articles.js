$(document).ready(function(){
    //Поиск статей полнотекстовый
    
    $('#search_articles').keyup(function(event){
	event.preventDefault();
	
    if(event.keyCode == 13){
        event.preventDefault();
        $. ajax ({
      url: '/articles/articles_search',
      type: 'POST',
      data: {
        search_text:$('#search_articles').val()
        },
      dataType: 'HTML',
      success: function(data){
      	if(data){
            $('#all_articles_wrap').html(data);
        }
        else{
            $('#all_articles_wrap').html('<h2>Простите, по запросу не найдено статей</h2>');
        }
      }
    });
    }
    });
});