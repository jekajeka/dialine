//расчет высоты 2х полей поска в всплывающем окне
function calc_search_fields_heights()
{
    if($('.main_menu').hasClass('sticky'))
    {
	$('.active .find_special_wrap_content .search_results_data').height($('.row_search.active').height() - 270);
	$('.active .find_scrubs_wrap .search_results_data').height($('.row_search.active').height() - 320);
    }
    else
    {
	$('.find_special_wrap_content .search_results_data').height($('.row_search.active').height() - 286);
	$('.find_scrubs_wrap .search_results_data').height($('.row_search.active').height() - 320);
    }
}

$(document).ready(function(){
    //отдельный урл для страницы обратной связи
    if(window.location.hash == '#callback')
    {
	$('.overlay_form_wrapper#send_simple_wrap').show();
    }
    
    //клик по геотаргетингу
    $('.geotargeting').click(function(e){
	if(window.location.pathname == '/')
	    e.preventDefault()
	$('body').scrollTo('.map_operate_wrap',500,{offset:-220});
	//$('.more_advantages_btn').hide();
	})
    
    /*var $preloader = $('#page-preloader'),
        $spinner   = $preloader.find('.spinner');
    $spinner.delay(1000).fadeOut();
    $preloader.delay(1000).fadeOut('slow');*/
    
    //calc_search_fields_heights()
    
    if ($(window).scrollTop()> 149)
	{
	    $('.row.main_menu').addClass('sticky').stop()
	    $('.row.row_search').addClass('higher').stop()
	    calc_search_fields_heights()
	}
	
    
    
    $(window).on("orientationchange",function(event){
        if (event.orientation == 'landscape')
            $('meta[name="viewport"]').attr('content','width=1200')
        else
            $('meta[name="viewport"]').attr('content','width=640')
    });
    
    
    
    $(window).scroll(function(){
	if ($(window).scrollTop()> 149)
	{
	    $('.row.main_menu').addClass('sticky').stop()
	    $('.row.row_search').addClass('higher').stop()
	    $('.row.row_search.active').css('top','auto').stop()
	    setTimeout(function () {
		calc_search_fields_heights()
		}, 1000);
	}
	else
	{
	    $('.row.main_menu').removeClass('sticky').stop()
	    $('.row.row_search').removeClass('higher').stop()
	    setTimeout(function () {
		//calc_search_fields_heights()
		}, 1000);
	    var position = $(window).scrollTop();
	    $('.row.row_search.active').css('top',180-position).stop()
	}
	if ($(window).scrollTop()> 420)
	{
	    $('.service_result_wrap_wrap').addClass('sticky').stop()
	}
	if ($(window).scrollTop()<= 420)
	{
	    $('.service_result_wrap_wrap').removeClass('sticky').stop()
	}
	
	if ($(window).scrollTop()>= $(document).height() - 850)
	{
	    $('.service_result_wrap_wrap').removeClass('sticky').stop()
	}
	});
    
    $('#vrachi_menu').click(function(e){
	e.preventDefault()
	calc_search_fields_heights()
	var position = $(window).scrollTop();
	$('.row.row_search').removeClass('active');
	$('.row.row_search#vrachi_data').toggleClass('active');
	$('.row.row_search').removeProp('style');
	var position = $(window).scrollTop();
	$('.row.row_search.active').css('top',180-position).stop()
	if ($(window).scrollTop()> 149)
	{
	    $('.row.main_menu').addClass('sticky').stop()
	    
	    $('.row.row_search.active').addClass('higher').stop()
	}
	setTimeout(function () {
		$('.row.row_search#vrachi_data').removeProp('style');
		}, 500);
	calc_search_fields_heights()
    })
    $('#services_menu').click(function(e){
	var position = $(window).scrollTop();
	e.preventDefault()
	calc_search_fields_heights()
	$('.row.row_search').removeClass('active');
	$('.row.row_search#services_data').toggleClass('active');
	var position = $(window).scrollTop();
	$('.row.row_search').removeProp('style');
	$('.row.row_search.active').css('top',180-position).stop()
	if ($(window).scrollTop()> 149)
	{
	    $('.row.main_menu').addClass('sticky').stop()
	    $('.row.row_search#services_data').removeProp('style');
	    $('.row.row_search.active').addClass('higher').stop()
	}
	setTimeout(function () {
		$('.row.row_search#services_data').removeProp('style');
		}, 500);
	calc_search_fields_heights()
    })
    
    $('.row_search_close').click(function(e){
	e.preventDefault()
	$('.row.row_search').removeClass('active');
	$('.row.row_search').removeProp('style');
	$('.row.row_search').css(top,'-1000%');
    })
        
    
    //главный слайдер
    var main_Swiper = new Swiper('.row.super_slider',{
        loop:true,
        grabCursor: true,
        onlyExternal : false,
	autoplay: 5000,
        pagination: '.switches',
        paginationClickable: true
      })
    
    //прокрутка контента в адресах
    $(".addresses_wrap").mCustomScrollbar({
	    theme:"minimal"
	});
    
    
    
    
    
    //слайдер отзывов
    var review_Swiper = new Swiper('.reviews_container',{
        loop:false,
        grabCursor: false,
        slidesPerView : 2,
        onlyExternal : false
      })
    $('.reviews_left').on('click', function(e){
    e.preventDefault()
    review_Swiper.swipePrev()
  })
  $('.reviews_right').on('click', function(e){
    e.preventDefault()
    review_Swiper.swipeNext()
  })
  
  $('.search_specialist_row select#age').select2({
        minimumResultsForSearch: Infinity,
        width:'165px'
        });
  $('.search_specialist_row select#speciality').select2({
        minimumResultsForSearch: Infinity,
        width:'240px'
        });
  $('.search_specialist_row select#clinic').select2({
        minimumResultsForSearch: Infinity,
        width:'235px'
        });
  
  $('.search_specialist_row select#services_data_swith').select2({
        minimumResultsForSearch: Infinity,
        width:'155px'
        });
  
  
  function main_search_doc()
  { 
	$. ajax ({
      url: '/doctors/search_doctor_complicated_on_top',
      type: 'POST',
      data: {
        age:$('#age').val(),
	speciality:$('#speciality').val(),
	clinic:$('#clinic').val()
        },
      dataType: 'HTML',
      success: function(data){
      	if(data){
            $('#vrachi_data .find_special_wrap_content .search_results .search_results_data .mCSB_container').html(data);
        }
        else{
            $('#vrachi_data .find_special_wrap_content .search_results .search_results_data .mCSB_container').html('<h2>Простите, по запросу не найдено врачей</h2>');
        }
      }
    });
  }
  
  $('.search_specialist_row select#age').on('select2:close', function (evt) {
    evt.preventDefault()
    main_search_doc();
  });
  
  $('.search_specialist_row select#speciality').on('select2:close', function (evt) {
    evt.preventDefault()
    main_search_doc();
  });
  
  $('.search_specialist_row select#clinic').on('select2:close', function (evt) {
    evt.preventDefault()
    main_search_doc();
  });
  
  
  $('.search_specialist_row select#services_data_swith').on('select2:close', function (evt) {
    $. ajax ({
      url: $('#services_data_swith').val(),
      type: 'POST',
      data: {},
      dataType: 'HTML',
      success: function(data){
      	if(data){
            $('#services_data .find_special_wrap_content .search_results .search_results_data .mCSB_container').html(data);
        }
      }
    });
  });
  
  //Обработка нажатия на кнопку направления
  $('#directions_overlay').click(function(e){
    e.preventDefault();
    $. ajax ({
      url: '/services/get_overlay_services',
      type: 'POST',
      data: {},
      dataType: 'HTML',
      success: function(data){
      	if(data){
            $('#services_data .find_special_wrap_content .search_results .search_results_data .mCSB_container').html(data);
        }
      }
    });
  });
  
  
  $('#program_overlay').click(function(e){
	e.preventDefault()
	$. ajax ({
      url: '/info/program_overlay',
      type: 'POST',
      data: {},
      dataType: 'HTML',
      success: function(data){
      	if(data){
            $('#services_data .find_special_wrap_content .search_results .search_results_data .mCSB_container').html(data);
        }
      }
    });
  });
  
  $('#corporate_overlay').click(function(e){
	e.preventDefault()
	$. ajax ({
      url: '/services/get_simple_services_list_by_type/3',
      type: 'POST',
      data: {},
      dataType: 'HTML',
      success: function(data){
      	if(data){
            $('#services_data .find_special_wrap_content .search_results .search_results_data .mCSB_container').html(data);
        }
      }
    });
  });
  
  
  
  
  
  
  
  
  
  $('#direction').select2({
        minimumResultsForSearch: Infinity,
        width:'216px'
        });
  
  $('.left_menu #direction').select2({
        minimumResultsForSearch: Infinity
        //width:'216px'
        });
  $('#select_month').select2({
        minimumResultsForSearch: Infinity
        });
  
  $('#select_year').select2({
        minimumResultsForSearch: Infinity
        });
  //поиск новостей по году и месяцу
  $('#search_news').click(function(e){
    e.preventDefault()
	    window.location.href = '/news/filter/'+$('#news_type').val()+'/'+$('#select_year option:selected').val()+'/'+$('#select_month').val();
    })
  
  $('body').on('click','.toggle_wrapper a.toggle_title',function(e){
    e.preventDefault()
    
    
    if($(this).hasClass('active'))
	$(this).next().slideUp();
    else
	$(this).next().slideDown();
	
    $(this).toggleClass('active');
    })
  
  function select_clinics_list()
  {
    $. ajax ({
		url: '/doctors/filter_clinics',
		type: 'POST',
		data: {
		    age:$('#age_page').val(),
		    speciality:$('#speciality_page').val()
		  },
		dataType: 'HTML',
		success: function(data){
		  if(data){
		      $('#clinic_page').html(data);
		  }
		}
	      });
  }
  function select_clinics_list_top()
  {
    $. ajax ({
		url: '/doctors/filter_clinics',
		type: 'POST',
		data: {
		    age:$('#age').val(),
		    speciality:$('#speciality').val()
		  },
		dataType: 'HTML',
		success: function(data){
		  if(data){
		      $('#clinic').html(data);
		  }
		}
	      });
  }
  $('.text_search_speciality_long_wrap').css('opacity',1)
  $('select#age_page').select2({
        minimumResultsForSearch: Infinity,
        width:'140px'
        });
  
  $('select#age_page').on('select2:close', function (evt) {
	select_clinics_list();
	});
  $('select#speciality_page').on('select2:close', function (evt) {
	select_clinics_list();
	});
  
    $('select#speciality').on('select2:close', function (evt) {
	select_clinics_list_top();
	});
    $('select#age').on('select2:close', function (evt) {
	select_clinics_list_top();
	});
  
  $('select#speciality_page').select2({
        minimumResultsForSearch: Infinity,
        width:'174px'
        });
  $('select#clinic_page').select2({
        minimumResultsForSearch: Infinity,
        width:'240px'
        });
  
  
  
  $('select#speciality_page_price').select2({
        minimumResultsForSearch: Infinity
        });
  $('select.selectBox.clinic_dolg').select2({
        minimumResultsForSearch: Infinity
        });
  $('#review_clinic').select2({
        minimumResultsForSearch: Infinity
        });
  
  
  
  $('body').on('click','a.structured_data_title',function(e){
    e.preventDefault()
    if($(this).hasClass('active'))
	$(this).next().slideUp();
    else
	$(this).next().slideDown();
	
    $(this).toggleClass('active');
    })
  
  //прокрутка контента в дополнительных врачах
    $(".doctors_additional_panel").mCustomScrollbar({
	    theme:"minimal",
	    mouseWheel:{
		scrollAmount:200,
	    }
	    
	});
    
    $(".search_results_data").mCustomScrollbar({
	    theme:"minimal"
	});
    
    //Заказ услуг на странице
    $('body').on('click', '.structured_data_table_wrap_order .structured_data_table_link',function(e){
	if($(this).hasClass('clinic_service_link'))
	    return;//не выполнять эту функцию, если мы на странице клиники
	if($(this).hasClass('subservice_from_service'))
	    return;//не выполнять эту функцию, если мы на странице клиники
	if($(this).hasClass('added'))
	{
	    $('#'+'line_'+$(this).attr('id')+' .service_result_data_drop').click();
	    return;
	}
        if(!$(this).attr('href'))
	{
	    e.preventDefault()
	    $(this).text("Удалить").addClass('added');
	}
        $('.service_result_data_wrap').append('<div id="line_'+$(this).attr('id')+'" class="service_result_data_line" price="'+parseInt($(this).prev().find('.structured_data_table_line_price').text())+'"><div class="service_result_data_line_row"><div class="service_result_data_title">'+$(this).prev().find('h4').text()+'</div><a data-drop="'+$(this).attr('id')+'" class="service_result_data_drop"></a></div></div>')
        var total = 0;
        $('.service_result_data_line').each(function(indx,element){
            total += parseInt($(element).attr('price'))
            $('.service_result_total span').text(indx+1 + ' услуг')
        });
        $('.total_data span').text(total+' р.')
        $('body').append('<div class="add_service_downfall"></div>');
	setTimeout(function(){
	    $(".add_service_downfall").addClass('visible')
	    },100)	
	setTimeout(function(){
	    $(".add_service_downfall").addClass('dropdown')
	    },1000)	
        })
    //удаление услуг на странице
    $('body').on('click', '.service_result_data_drop', function(e){
        e.preventDefault()
	$('#'+$(this).attr('data-drop')).text("Добавить").removeClass('added');
        $(this).closest('.service_result_data_line').remove()
        var total = 0;
	var count_services = 0;
        $('.service_result_data_wrap .service_result_data_line').each(function(indx,element){
            total += parseInt($(element).attr('price'))
            count_services += 1;
        });
	$('.service_result_total span').text(count_services + ' услуг')
        $('.total_data span').text(total+' р.')
    });
    
    //слайдер врачей конкретной специализации
    var specialists_carousel_Swiper = new Swiper('.specialists_carousel',{
        loop:false,
        grabCursor: true,
        onlyExternal : false,
        paginationClickable: false,
	slidesPerView : 'auto'
      })
    
    $('.specialists_carousel_link_left').on('click', function(e){
    e.preventDefault()
    specialists_carousel_Swiper.swipePrev()
  })
  $('.specialists_carousel_link_right').on('click', function(e){
    e.preventDefault()
    specialists_carousel_Swiper.swipeNext()
  })
  
  
  
  //Переключение вкладок на странице специализации
  $('.swither_wrap a').click(function(e){
	e.preventDefault()
	$('.swither_wrap a').removeClass('active').removeClass('box_shadow')
	$(this).addClass('active').addClass('box_shadow')
	$('.switch_text_wrap .switch_text').removeClass('active')
	$('.switch_text_wrap .switch_text:eq('+$(this).index()+')').addClass('active')
    })
  
  $('.specialist_switch').click(function(e){
    specialists_carousel_Swiper.reInit()
    })
  //Слайдер медицинских программ
  
  var programs_carousel_wrap_Swiper = new Swiper('.programs_carousel_wrap',{
    loop:false,
        grabCursor: false,
        onlyExternal : false,
	calculateHeight: true,
        paginationClickable: false,
	slidesPerView : 'auto'
  });
  
  $('.program_carousel_link_left').on('click', function(e){
    e.preventDefault()
    programs_carousel_wrap_Swiper.swipePrev()
  })
  $('.program_carousel_link_right').on('click', function(e){
    e.preventDefault()
    programs_carousel_wrap_Swiper.swipeNext()
  })
    
    //Разворачивание пунктов графика приема
    $('body').on('click','.audition_wrap h2',function(e){
    e.preventDefault()
    if($(this).hasClass('active'))
	$(this).next().slideUp();
    else
	$(this).next().slideDown();
    $(this).toggleClass('active')
    });
    
    //Слайдер хронологии
    var hrono_carousel_wrap_Swiper = new Swiper('.story_slider',{
    loop:false,
        grabCursor: false,
        onlyExternal : false,
        paginationClickable: false,
	slidesPerView : 'auto'
  });
    
  
//открытие закрытие всплывающей формы
    
    $('.oms_card_item .green_btn').click(function(e){
	e.preventDefault();
	yaCounter17755831.reachGoal('open_oms_form');
	$('#send_hirurg_form').show();
    });
    
    $('#send_medical_program').click(function(e){
	e.preventDefault();
	$('#send_med_prog_wrap').show();
    });
    
    $('.close_overlay_form').click(function(e){
	e.preventDefault();
	$('.overlay_form_wrapper').hide();
    });
    
    //открытие самой основной формы с кнопки в шапке сайта
    $('.header_visit_order').click(function(e){
	e.preventDefault();
	yaCounter17755831.reachGoal('open_header_visit_order');
	$('#send_header_visit_wrap').show();
    });
    
    
    
    
    //Выравнивание блоков по высоте
    function equalHeight(group) {
    var tallest = 0;
    group.each(function() {
      thisHeight = $(this).height();
      if(thisHeight > tallest) {
        tallest = thisHeight;
      }
    });
    group.height(tallest);
  }
  function equal_height_for_compare_programs()
  {
    equalHeight($(".programm_legend_item_target"));
    equalHeight($(".programm_legend_item_address"));
    equalHeight($(".programm_legend_item_specialist"));
    equalHeight($(".programm_legend_item_diagnost"));
    equalHeight($(".programm_legend_item_laborator"));
    equalHeight($(".programm_legend_item_bonus"));
  }
  
  equal_height_for_compare_programs()
  //Поиск врачей по текстам
    $('#search_doctor').keyup(function(event){
    if(event.keyCode == 13){
	
        event.preventDefault();
        $. ajax ({
      url: '/doctors/doctor_text_search',
      type: 'POST',
      data: {
        search_text:$('#search_doctor').val()
        },
      dataType: 'HTML',
      success: function(data){
      	if(data){
            $('.specialists_row .container').html(data);
        }
        else{
            $('.specialists_row .container').html('<h2>Простите, по запросу не найдено врачей</h2>');
        }
	$('body').scrollTo('.specialists_row',500);
      }
    });
    }
    });
    
    //Поиск врачей по текстам c выпадающей формы
    $('.search_scrubs#search_scrubs_overflow').click(function(event){
        event.preventDefault();
        $. ajax ({
      url: '/doctors/doctor_text_search_4_main',
      type: 'POST',
      data: {
        search_text:$('#find_scribs_overflow').val()
        },
      dataType: 'HTML',
      success: function(data){
      	if(data){
            $('.find_scrubs_wrap .search_results .search_results_data#search_results_data_scrubs .mCSB_container').html(data);
        }
        else{
            $('.find_scrubs_wrap .search_results .search_results_data#search_results_data_scrubs .mCSB_container').html('<p>Простите, по запросу не найдено врачей</p>');
        }
      }
    });
    });
    
    $('#find_scribs_overflow').keyup(function(event){
    if(event.keyCode == 13){
	$('.search_scrubs#search_scrubs_overflow').click()
    }
    })
    
    
    
    //Поиск услуг по текстам c выпадающей формы
    $('.search_scrubs#search_services_overflow').click(function(event){
        event.preventDefault();
        $. ajax ({
      url: '/services/text_search_services',
      type: 'POST',
      data: {
        search_text:$('#find_services_overflow').val()
        },
      dataType: 'HTML',
      success: function(data){
      	if(data){
            $('.find_scrubs_wrap .search_results .search_results_data#search_results_data_services .mCSB_container').html(data);
        }
        else{
            $('.find_scrubs_wrap .search_results .search_results_data#search_results_data_services .mCSB_container').html('<p>Простите, по запросу не найдено услуг</p>');
        }
      }
    });
    });
    
    $('#find_services_overflow').keyup(function(event){
    if(event.keyCode == 13){
	$('.search_scrubs#search_services_overflow').click()
    }
    })
    
    //Поиск врачей в специальности
    $('#search_doctor_speciality').keyup(function(event){
    if(event.keyCode == 13){
	
        event.preventDefault();
        $. ajax ({
      url: '/doctors/doctor_text_search_speciality/'+$('#search_doctor_speciality').attr('speciality'),
      type: 'POST',
      data: {
        search_text:$('#search_doctor_speciality').val()
        },
      dataType: 'HTML',
      success: function(data){
      	if(data){
            $('.specialists_row .container').html(data);
        }
        else{
            $('.specialists_row .container').html('<h2>Простите, по запросу не найдено врачей</h2>');
        }
      }
    });
    }
    });
    
    //Поиск услуг по странице услуг
    $('#search_all_service').keyup(function(event){
    if(event.keyCode == 13){
	
        event.preventDefault();
        $. ajax ({
      url: '/services/text_search_services_all_page',
      type: 'POST',
      data: {
        search_text:$('#search_all_service').val()
        },
      dataType: 'HTML',
      success: function(data){
      	if(data){
            $('.all_services_all_data_block').html(data);
        }
        else{
            $('.all_services_all_data_block').html('<h2>Простите, по запросу не найдено услуг</h2>');
        }
	$('body').scrollTo('.all_services_all_data_block',500);
      }
    });
    }
    });
    
    //Поиск услуг по странице прайса
    $('#search_service_by_title').keyup(function(event){
    if(event.keyCode == 13){
        event.preventDefault();
	$. ajax ({
	    url: '/info/search_service_by_title',
	    type: 'POST',
	    data: {
	      search_text:$('#search_service_by_title').val()
	      },
	    dataType: 'HTML',
	    success: function(data){
	      if(data){
		  $('.structured_data_wrap').html(data);
	      }
	      else{
		  $('.structured_data_wrap').html('<h2>Простите, по запросу не найдено услуг</h2>');
	      }
	    }
	  });
    }
    });
    
    //Поиск услуг по странице анализов
    $('#search_service_analyze').keyup(function(event){
    if(event.keyCode == 13){
        event.preventDefault();
	$. ajax ({
	    url: '/services/searchServiceByTitleAndRubric',
	    type: 'POST',
	    data: {
	      search_text:$('#search_service_analyze').val(),
	      serviceId:$('#serviceId').val()
	      },
	    dataType: 'HTML',
	    success: function(data){
	      if(data){
		  $('.structured_data_wrap').html(data);
	      }
	      else{
		  $('.structured_data_wrap').html('<h2>Простите, по запросу не найдено услуг</h2>');
	      }
	    }
	  });
    }
    });
    
    //Поиск услуг по странице анализов
    $('#search_service_go').click(function(event){
        event.preventDefault();
	$. ajax ({
	    url: '/services/searchServiceByTitleAndRubric',
	    type: 'POST',
	    data: {
	      search_text:$('#search_service_analyze').val(),
	      serviceId:$('#serviceId').val()
	      },
	    dataType: 'HTML',
	    success: function(data){
	      if(data){
		  $('.structured_data_wrap').html(data);
	      }
	      else{
		  $('.structured_data_wrap').html('<h2>Простите, по запросу не найдено услуг</h2>');
	      }
	    }
	  });
    });
    
    
    $('#page_price_go').click(function(e){
	e.preventDefault()
	$. ajax ({
	    url: '/info/search_service_by_params',
	    type: 'POST',
	    data: {
	      age:$('#age_page').val(),
	      speciality:$('#speciality_page_price').val()
	      },
	    dataType: 'HTML',
	    success: function(data){
	      if(data){
		  $('.structured_data_wrap').html(data);
	      }
	      else{
		  $('.structured_data_wrap').html('<h2>Простите, по запросу не найдено услуг</h2>');
	      }
	    }
	  });
	})

    
    
    $('#search_doctor_complicated').click(function(e){
	e.preventDefault()
	$. ajax ({
      url: '/doctors/search_doctor_complicated',
      type: 'POST',
      data: {
        age:$('#age_page').val(),
	speciality:$('#speciality_page').val(),
	clinic:$('#clinic_page').val()
        },
      dataType: 'HTML',
      success: function(data){
      	if(data){
            $('.specialists_row .container').html(data);
        }
        else{
            $('.specialists_row .container').html('<h2>Простите, по запросу не найдено врачей</h2>');
        }
      }
    });
	
	})
    
    $('.left_menu select#direction').on('select2:close', function (evt) {
	
	//alert($(this).val())
	$('.all_services_all_data_block .toggle_wrapper').hide()
	$('.all_services_all_data_block .toggle_wrapper#'+$(this).val()).show();
	if($(this).val()=='all')
	    $('.all_services_all_data_block .toggle_wrapper').show();
	
    });
    
    //Открытие самой основной формы связи на главной из широкой плашки запись на прием под картами
    $('.open_overlay_form_wrapper').click(function(e){
		e.preventDefault()
		yaCounter17755831.reachGoal('zapis_na_glavnoy');
		if (window.location.pathname == '/') ga('send', 'event', 'zvonok', 'otpravkazvonka'); // запись на приём на главной
		$('.overlay_form_wrapper#send_simple_wrap').show();
	})
    
    //Открытие формы записи к самому врачу под его фото
    $('.order_doctor_overlay_win').click(function(e){
	    yaCounter17755831.reachGoal('open_doctor_form');
	    if (window.location.pathname == '/doctors/gastroyenterolog/golovina') 
	    {
		ga('send', 'event', 'formvrach', 'otpravkazapis');
		console.log('otpravkazapis');
	    }
	    $('.overlay_form_wrapper#send_doctor_wrap').show();
	})
    
    
    $('#order_service_btn').click(function(e){
		e.preventDefault()
		yaCounter17755831.reachGoal('open_service_page_form');
		if (window.location.pathname == '/ginekolog') ga('send', 'event', 'uslugi', 'otpravkauslugi');
		$('.overlay_form_wrapper#order_service_form').show();
	});
    
    $('#order_service_btn2').click(function(e){
		e.preventDefault()
		yaCounter17755831.reachGoal('open_service_page_form');
		if (window.location.pathname == '/ginekolog') ga('send', 'event', 'uslugi', 'otpravkauslugi');
		$('.overlay_form_wrapper#order_service_form').show();
	});
    
    //открытие обратной связи с виджета
    $('#order_from_bottom_mobile').click(function(e){
	yaCounter17755831.reachGoal('fixed_desktop_callback_site');
	$('.overlay_form_wrapper#widget_wrap').show();
	})
    
    $('.more_advantages_btn').click(function(e){
	e.preventDefault()
	$('.other_adv_wrap').show(500)
	$('.more_advantages_btn').hide();
	})
    
    
    //переключение города
    $('.selector_sity a').click(function(e){
	e.preventDefault()
	Cookies.set('city_phone', $(this).attr('data-phone'));
	Cookies.set('city', $(this).text());
	$('.your_city_wrap>a').text($(this).text())
	$('.phone_header_data>a').html($(this).attr('data-phone'))
	var phone_data = $(this).attr('data-phone')
	$('.phone_header_data>a').attr('href','tel: '+phone_data.replace("<span class='callibri_phone'>",'').replace("</span>",''));
	// setTimeout(function () {
	// 	callibriInit()
	// 	}, 1000);
	})
    
    //отправка простой формы заявки с главной страницы
    $('.send_overlay_form#send_overlay_form_simple').click(function(e){
	e.preventDefault()
	if($(this).hasClass('disabled')) return;
	$('#overlay_form_phone').removeClass('failed_input');
	$('#overlay_form_name').removeClass('failed_input');
	$("#overlay_form_agree").next().removeClass('failed_label');
	if(($('#overlay_form_phone').val() != '') && ($('#overlay_form_name').val() != '')&&($("#overlay_form_agree").prop("checked")))
		{

			$('.send_overlay_form#send_overlay_form_simple').text("Идет отправка...").css('background','#00abb2')
			$('.send_overlay_form#send_overlay_form_simple').addClass('disabled')
			$.ajax({
				type: 'POST',
				url: '/post/send_message',
				data:{
				purpose:$('#overlay_form_select').val(),
				name:$('#overlay_form_name').val(),
				phone:$('#overlay_form_phone').val()
				},
				scriptCharset: "utf-8",
				success: function(data){
					$('.send_overlay_form#send_overlay_form_simple').text("Спасибо за обращение!").css('background','#00abb2')
					yaCounter17755831.reachGoal('zapis_na_glavnoy_otpravleno');
          Comagic.addOfflineRequest({name: $('#overlay_form_name').val(), phone: $('#overlay_form_phone').val(), message: $('#overlay_form_select').val()});
					if (window.location.pathname == '/ginekolog') ga('send', 'event', 'uslugi2', 'otpravkauslugi2');
					if (window.location.pathname == '/doctors/gastroyenterolog/golovina') {
						ga('send', 'event', 'formvrach2', 'otpravkazapis2');
					}
					if (window.location.pathname == '/') ga('send', 'event', 'zvonok2', 'otpravkazvonka2');
				}
				});
		}
		else
		{
		    if($('#overlay_form_phone').val() == '')	    
			$('#overlay_form_phone').addClass('failed_input');
		    if($('#overlay_form_name').val() == '')
			$('#overlay_form_name').addClass('failed_input');
		    if(!$("#overlay_form_agree").prop("checked"))
			$("#overlay_form_agree").next().addClass('failed_label');
		}
	});
    
    //отправка  формы заявки из шапки
  $('.send_overlay_form#send_overlay_form_simple_header_visit').click(function(e){
	e.preventDefault()
	if($(this).hasClass('disabled')) return;
	$('#overlay_form_phone_header_visit').removeClass('failed_input');
	$('#overlay_form_name_header_visit').removeClass('failed_input');
	$("#overlay_form_agree_header_visit").next().removeClass('failed_label');
	if(($('#overlay_form_phone_header_visit').val() != '') && ($('#overlay_form_name_header_visit').val() != '')&&($("#overlay_form_agree_header_visit").prop("checked")))
		{

			$('.send_overlay_form#send_overlay_form_simple_header_visit').text("Идет отправка...").css('background','#00abb2')
			$('.send_overlay_form#send_overlay_form_simple_header_visit').addClass('disabled')
			$.ajax({
				type: 'POST',
				url: '/post/send_message',
				data:{
				purpose:$('#overlay_form_select_header_visit').val(),
				name:$('#overlay_form_name_header_visit').val(),
				phone:$('#overlay_form_phone_header_visit').val()
				},
				scriptCharset: "utf-8",
				success: function(data){
					$('.send_overlay_form#send_overlay_form_simple_header_visit').text("Спасибо за обращение!").css('background','#00abb2')
					yaCounter17755831.reachGoal('zapis_na_glavnoy_shapka_otpravleno');
          Comagic.addOfflineRequest({name: $('#overlay_form_name_header_visit').val(), phone: $('#overlay_form_phone_header_visit, #service_subservice_purpose').val(), message: $('#overlay_form_select_header_visit').val()});
					if (window.location.pathname == '/ginekolog') ga('send', 'event', 'uslugi2', 'otpravkauslugi2');
					if (window.location.pathname == '/doctors/gastroyenterolog/golovina') {
						ga('send', 'event', 'formvrach2', 'otpravkazapis2');
					}
					if (window.location.pathname == '/') ga('send', 'event', 'zvonok2', 'otpravkazvonka2');
				}
				});
		}
		else
		{
		    if($('#overlay_form_phone_header_visit').val() == '')	    
			$('#overlay_form_phone_header_visit').addClass('failed_input');
		    if($('#overlay_form_name_header_visit').val() == '')
			$('#overlay_form_name_header_visit').addClass('failed_input');
		    if(!$("#overlay_form_agree_header_visit").prop("checked"))
			$("#overlay_form_agree_header_visit").next().addClass('failed_label');
		}
	});
    
    //отправка заявки из услуг
    $("#order_service_phone").mask('+0 (000) 000 00 00', {placeholder: "+7(___) ___ __ __",clearIfNotMatch: true});
    $('.send_overlay_form#send_order_service_form').click(function(e){
	e.preventDefault()
	if($(this).hasClass('disabled')) return;
	$('#order_service_phone').removeClass('failed_input');
	$('#order_service_name').removeClass('failed_input');
	$("#order_service_agree").next().removeClass('failed_label');
	if(($('#order_service_phone').val() != '') && ($('#order_service_name').val() != '')&&($("#order_service_agree").prop("checked")))
		{
			$('.send_overlay_form#send_order_service_form').text("Идет отправка...").css('background','#00abb2')
			$('.send_overlay_form#send_order_service_form').addClass('disabled')
			$.ajax({
				type: 'POST',
				url: '/post/send_message',
				data:{
				purpose:$('#order_service_purpose').val(),
				name:$('#order_service_name').val(),
				phone:$('#order_service_phone').val()
				},
				scriptCharset: "utf-8",
				success: function(data){
					$('.send_overlay_form#send_order_service_form').text("Спасибо за обращение!").css('background','#00abb2')
					yaCounter17755831.reachGoal('service_page_form_otpravleno');
          Comagic.addOfflineRequest({name: $('#order_service_name').val(), phone: $('#order_service_phone').val(), message: $('#order_service_purpose').val()});
					if (window.location.pathname == '/ginekolog') ga('send', 'event', 'uslugi2', 'otpravkauslugi2');
					if (window.location.pathname == '/doctors/gastroyenterolog/golovina') {
						ga('send', 'event', 'formvrach2', 'otpravkazapis2');
					}
					if (window.location.pathname == '/') ga('send', 'event', 'zvonok2', 'otpravkazvonka2');
				}
				});
		}
		else
		{
		    if($('#order_service_phone').val() == '')	    
			$('#order_service_phone').addClass('failed_input');
		    if($('#order_service_name').val() == '')
			$('#order_service_name').addClass('failed_input');
		    if(!$("#order_service_agree").prop("checked"))
			$("#order_service_agree").next().addClass('failed_label');
		}
	});
    
    
    //отправка формы записи к конкретному врачу
    $("#send_doctor_phone").mask('+0 (000) 000 00 00', {placeholder: "+7(___) ___ __ __",clearIfNotMatch: true});
    $('.send_overlay_form#send_doctor_button').click(function(e){
	e.preventDefault()
	if($(this).hasClass('disabled')) return;
	$('#send_doctor_name').removeClass('failed_input');
	$('#send_doctor_phone').removeClass('failed_input');
	$("#send_doctor_agree").next().removeClass('failed_label');
	if(($('#send_doctor_name').val() != '') && ($('#send_doctor_phone').val() != '')&&($("#send_doctor_agree").prop("checked")))
	    {

		$('.send_overlay_form#send_doctor_button').text("Идет отправка...").css('background','#00abb2')
		$('.send_overlay_form#send_doctor_button').addClass('disabled')
		$.ajax({
			type: 'POST',
			url: '/post/send_message',
			data:{
			    purpose:$('#send_doctor_purpose').val(),
			    name:$('#send_doctor_name').val(),
			    phone:$('#send_doctor_phone').val()
			    },
			    scriptCharset: "utf-8",
			    success: function(data){
				$('.send_overlay_form#send_doctor_button').text("Спасибо за обращение!").css('background','#00abb2')
				yaCounter17755831.reachGoal('doctor_form_otpravleno');
        Comagic.addOfflineRequest({name: $('#send_doctor_name').val(), phone: $('#send_doctor_phone').val(), message: $('#send_doctor_purpose').val()});
				if (window.location.pathname == '/ginekolog') ga('send', 'event', 'uslugi2', 'otpravkauslugi2');
				if (window.location.pathname == '/doctors/gastroyenterolog/golovina')
				{
				    ga('send', 'event', 'formvrach2', 'otpravkazapis2');
				}
				if (window.location.pathname == '/') ga('send', 'event', 'zvonok2', 'otpravkazvonka2');
			    }
			});
		}
		else
		{
		    if($('#send_doctor_name').val() == '')	    
			$('#send_doctor_name').addClass('failed_input');
		    if($('#send_doctor_phone').val() == '')
			$('#send_doctor_phone').addClass('failed_input');
		    if(!$("#send_doctor_agree").prop("checked"))
			$("#send_doctor_agree").next().addClass('failed_label');
		    
		}
	});
    
    //отправка формы записи на подуслуги
    
    $('.send_overlay_form#send_service_subservice_btn').click(function(e){
	e.preventDefault()
	if($(this).hasClass('disabled')) return;
	$('#subservice_service_name').removeClass('failed_input');
	$('#subservice_service_phone').removeClass('failed_input');
	$("#subservice_service_agree").next().removeClass('failed_label');
	if(($('#subservice_service_name').val() != '') && ($('#subservice_service_phone').val() != '')&&($("#subservice_service_agree").prop("checked")))
	    {
		$('.send_overlay_form#send_service_subservice_btn').text("Идет отправка...").css('background','#00abb2')
		$('.send_overlay_form#send_service_subservice_btn').addClass('disabled')
		$.ajax({
			type: 'POST',
			url: '/post/send_subservice_service',
			data:{
			    page_title:$('#page_title').val(),
			    purpose:$('#service_subservice_purpose').val(),
			    name:$('#subservice_service_name').val(),
			    phone:$('#subservice_service_phone').val()
			    },
			    scriptCharset: "utf-8",
			    success: function(data){
				$('.send_overlay_form#send_service_subservice_btn').text("Спасибо за обращение!").css('background','#00abb2')
				yaCounter17755831.reachGoal('service_page_form_otpravleno');
        Comagic.addOfflineRequest({name: $('#subservice_service_name').val(), phone: $('#subservice_service_phone').val(), message: 'Заявка из услуг'});
				if (window.location.pathname == '/ginekolog') ga('send', 'event', 'uslugi2', 'otpravkauslugi2');
				if (window.location.pathname == '/doctors/gastroyenterolog/golovina')
				{
				    ga('send', 'event', 'formvrach2', 'otpravkazapis2');
				}
				if (window.location.pathname == '/') ga('send', 'event', 'zvonok2', 'otpravkazvonka2');
			    }
			});
		}
		else
		{
		    if($('#subservice_service_name').val() == '')	    
			$('#subservice_service_name').addClass('failed_input');
		    if($('#subservice_service_phone').val() == '')
			$('#subservice_service_phone').addClass('failed_input');
		    if(!$("#subservice_service_agree").prop("checked"))
			$("#subservice_service_agree").next().addClass('failed_label');
		    
		}
	});
    
    //отправка формы c виджета
    $("#widget_phone").mask('+0 (000) 000 00 00', {placeholder: "+7(___) ___ __ __",clearIfNotMatch: true});
    $('.send_overlay_form#send_widget').click(function(e){
	e.preventDefault()
	if($(this).hasClass('disabled')) return;
	$('#widget_name').removeClass('failed_input');
	$('#widget_phone').removeClass('failed_input');
	$('#widget_agree').next().removeClass('failed_label');
	if(($('#widget_name').val() != '') && ($('#widget_phone').val() != '')&&($("#widget_agree").prop("checked")))
	    {

		$('.send_overlay_form#send_widget').text("Идет отправка...").css('background','#00abb2')
		$('.send_overlay_form#send_widget').addClass('disabled')
		$.ajax({
			type: 'POST',
			url: '/post/send_message',
			data:{
			    purpose:$('#widget_purpose').val(),
			    name:$('#widget_name').val(),
			    phone:$('#widget_phone').val()
			    },
			    scriptCharset: "utf-8",
			    success: function(data){
				$('.send_overlay_form#send_widget').text("Спасибо за обращение!").css('background','#00abb2')
				yaCounter17755831.reachGoal('widget_otpravleno');
        Comagic.addOfflineRequest({name: $('#widget_name').val(), phone: $('#widget_phone').val(), message: 'Виджет'});
				if (window.location.pathname == '/ginekolog') ga('send', 'event', 'uslugi2', 'otpravkauslugi2');
				if (window.location.pathname == '/doctors/gastroyenterolog/golovina')
				{
				    ga('send', 'event', 'formvrach2', 'otpravkazapis2');
				}
				if (window.location.pathname == '/') ga('send', 'event', 'zvonok2', 'otpravkazvonka2');
			    }
			});
		}
		else
		{
		    if($('#widget_name').val() == '')	    
			$('#widget_name').addClass('failed_input');
		    if($('#widget_phone').val() == '')
			$('#widget_phone').addClass('failed_input');
		    if(!$("#widget_agree").prop("checked"))
			$('#widget_agree').next().addClass('failed_label');
		}
	});
    
    //отправка формы заявки в подвале
    $('#footer_send_form').click(function(e){
	e.preventDefault()
	if($(this).hasClass('disabled')) return;
	$('#footer_phone').removeClass('failed_input');
	$('#footer_name').removeClass('failed_input');
	$("#footer_form_agree").next().removeClass('failed_label');
	if(($('#footer_phone').val() != '') && ($('#footer_name').val() != '') && ($("#footer_form_agree").prop("checked")))
	{

	    $('#footer_send_form').text("Идет отправка...").css('background','#00abb2')
	    $('#footer_send_form').addClass('disabled')
	     $.ajax({
			type: 'POST',
			url: '/post/send_message',
			data:{
			purpose:'Остались вопросы или нужна помощь',
			name:$('#footer_name').val(),
			phone:$('#footer_phone').val()
			},
			scriptCharset: "utf-8",
			success: function(data){
			    $('#footer_send_form').text("Готово!").css('background','#00abb2')
			    yaCounter17755831.reachGoal('overflow_footer_message_send');
          Comagic.addOfflineRequest({name: $('#footer_name').val(), phone: $('#footer_phone').val(), message: 'Футер'});
			}
			});
	}
	else
	{
	    if($('#footer_phone').val() == '')	    
		$('#footer_phone').addClass('failed_input');
	    if($('#footer_name').val() == '')
		$('#footer_name').addClass('failed_input');
	    if(!$("#footer_form_agree").prop("checked"))
		$("#footer_form_agree").next().addClass('failed_label');
	}
	})
    
    //отправка формы заявки из выпадающего окна
    $('.top_form_send').click(function(e){
	e.preventDefault()
	$btn_top_form_phone = $(this)
	if($(this).hasClass('disabled')) return;
	$(this).siblings('.top_form_phone').removeClass('failed_input');
	$(this).siblings('.top_form_name').removeClass('failed_input');
	$(this).siblings(".footer_agree").find('label').removeClass('failed_label');
	if(($(this).siblings('.top_form_name').val() != '') && ($(this).siblings('.top_form_phone').val() != '')&& ($(this).siblings(".footer_agree").find('input').prop("checked")))
	{
    
	    $btn_top_form_phone.text("Идет отправка...").css('background','#00abb2')
	    $btn_top_form_phone.addClass('disabled')
	     $.ajax({
			type: 'POST',
			url: '/post/send_message',
			data:{
			purpose:'Остались вопросы или нужна помощь',
			name:$(this).siblings('.top_form_name').val(),
			phone:$(this).siblings('.top_form_phone').val()
			},
			scriptCharset: "utf-8",
			success: function(data){
			    yaCounter17755831.reachGoal('overflow_footer_message_send');
          Comagic.addOfflineRequest({name: $('#top_form_name').val(), phone: $('#top_form_phone').val(),  message: 'Меню услуг и врачей'});
				ga('send', 'event', 'vipadform', 'otpravkaform');
			    $btn_top_form_phone.text("Готово!").css('background','#00abb2')
			}
			});
	}
	else
	{
	    if($(this).siblings('.top_form_phone').val() == '')	    
		$(this).siblings('.top_form_phone').addClass('failed_input');
	    if($(this).siblings('.top_form_name').val() == '')
		$(this).siblings('.top_form_name').addClass('failed_input');
	    if(!$(this).siblings(".footer_agree").find('input').prop("checked"))
		$(this).siblings(".footer_agree").find('label').addClass('failed_label');
	}
	})
    
    
//    jQuery("#user-city").val(ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country);
//alert(ymaps.geolocation.city);
function init2 () {
    ymaps.geolocation.get({
    // Выставляем опцию для определения положения по ip
    provider: 'yandex',
    // Автоматически геокодируем полученный результат.
    autoReverseGeocode: true
}).then(function (result) {
    // Выведем в консоль данные, полученные в результате геокодирования объекта.
    var datares = new Object(result.geoObjects.get(0).properties.get('metaDataProperty'))
    //console.log(datares.GeocoderMetaData.AddressDetails.Country.AddressLine);
    if(!Cookies.get('city'))
    {
	$('.selector_sity a[data-city="'+datares.GeocoderMetaData.AddressDetails.Country.AddressLine+'"]').click()
    }
    //console.log
    // setTimeout(function () {
		// callibriInit()
		// }, 1000);
    
});
}
ymaps.ready(init2);

//$("#overlay_form_phone").mask("+7(999) 999 99 99");
$("#overlay_form_phone").mask('+0 (000) 000 00 00', {placeholder: "+7(___) ___ __ __",clearIfNotMatch: true});
$("#footer_phone").mask('+0 (000) 000 00 00', {placeholder: "+7(___) ___ __ __",clearIfNotMatch: true});
$(".top_form_phone").mask('+0 (000) 000 00 00', {placeholder: "+7(___) ___ __ __",clearIfNotMatch: true});
$("#overlay_form_phone_order").mask('+0 (000) 000 00 00', {placeholder: "+7(___) ___ __ __",clearIfNotMatch: true});
$("#discount_phone").mask('+0 (000) 000 00 00', {placeholder: "+7(___) ___ __ __",clearIfNotMatch: true});
$("#overlay_form_phone_medprog").mask('+0 (000) 000 00 00', {placeholder: "+7(___) ___ __ __",clearIfNotMatch: true});
$("#phone_med_prog_header").mask('+0 (000) 000 00 00', {placeholder: "+7(___) ___ __ __",clearIfNotMatch: true});
$("#cps_phone").mask('+0 (000) 000 00 00', {placeholder: "+7(___) ___ __ __",clearIfNotMatch: true});
$("#review_phone").mask('+0 (000) 000 00 00', {placeholder: "+7(___) ___ __ __",clearIfNotMatch: true});
$("#vacancy_phone").mask('+0 (000) 000 00 00', {placeholder: "+7(___) ___ __ __",clearIfNotMatch: true});
$("#overlay_form_phone_hirurg").mask('+0 (000) 000 00 00', {placeholder: "+7(___) ___ __ __",clearIfNotMatch: true});
$("#doctor_service_phone").mask('+0 (000) 000 00 00', {placeholder: "+7(___) ___ __ __",clearIfNotMatch: true});
$('#overlay_form_phone_header_visit').mask('+0 (000) 000 00 00', {placeholder: "+7(___) ___ __ __",clearIfNotMatch: true});
$('#clinic_service_phone').mask('+0 (000) 000 00 00', {placeholder: "+7(___) ___ __ __",clearIfNotMatch: true});
$('#subservice_service_phone').mask('+0 (000) 000 00 00', {placeholder: "+7(___) ___ __ __",clearIfNotMatch: true});
$('#specify_phone').mask('+0 (000) 000 00 00', {placeholder: "+7(___) ___ __ __",clearIfNotMatch: true});
    
    //открытие формы заказа списка услуг
    $('#send_order_services').click(function(e){
	e.preventDefault()
	yaCounter17755831.reachGoal('open_calculator_form');
	ga('send', 'event', 'ceni', 'otpravkaprays');
	$('.overlay_form_wrapper#send_order_wrap').show()
	})
    
    //отправка формы заказа услуг
    $('.send_overlay_form#send_overlay_form_order').click(function(e){
	e.preventDefault()
	if($(this).hasClass('disabled')) return;
	$('#overlay_form_phone_order').removeClass('failed_input');
	$('#overlay_form_name_order').removeClass('failed_input');
	$("#overlay_form_agree_order").next().removeClass('failed_label');
	if(($('#overlay_form_phone_order').val() != '') && ($('#overlay_form_name_order').val() != '')&&($("#overlay_form_agree_order").prop("checked")))
	{
   
	    $('.send_overlay_form#send_overlay_form_order').text("Идет отправка...").css('background','#00abb2')
	    $('.send_overlay_form#send_overlay_form_order').addClass('disabled')
	    var order_line = '';
	    $(".service_result_data_line").each(function(indx, element){
		order_line += $(element).find(".service_result_data_title").text() + ' ' + $(element).attr('price')+' руб. \r';
	      });
	     $.ajax({
			type: 'POST',
			url: '/post/send_message',
			data:{
			purpose:$('#overlay_form_select_order').val(),
			name:$('#overlay_form_name_order').val(),
			phone:$('#overlay_form_phone_order').val(),
			order:order_line
			},
			scriptCharset: "utf-8",
			success: function(data){
			    $('.send_overlay_form#send_overlay_form_order').text("Спасибо за обращение!").css('background','#00abb2')
			    yaCounter17755831.reachGoal('calculator_form_otpravleno');
          Comagic.addOfflineRequest({name: $('#overlay_form_name_order').val(), phone: $('#overlay_form_phone_order').val(), message: $('#overlay_form_select_order').val()});
			    ga('send', 'event', 'ceni2', 'otpravkaprays2');
			}
			});
	}
	else
	{
	    if($('#overlay_form_phone_order').val() == '')	    
		$('#overlay_form_phone_order').addClass('failed_input');
	    if($('#overlay_form_name_order').val() == '')
		$('#overlay_form_name_order').addClass('failed_input');
	    if(!$("#overlay_form_agree_order").prop("checked"))
		$("#overlay_form_agree_order").next().addClass('failed_label');
	}
	})
    
    //Отправка заявки на дисконтную карту
    $('.plactic_card_btn').click(function(e){
	e.preventDefault()
	$btn_top_form_phone = $(this)
	if($(this).hasClass('disabled')) return;
	if(($('#discount_name').val() != '') && ($('#discount_phone').val() != ''))
	{
	    $('.plactic_card_btn').text("Идет отправка...").css('background','#00abb2')
	    $('.plactic_card_btn').addClass('disabled')
	     $.ajax({
			type: 'POST',
			url: '/post/send_message',
			data:{
			purpose:'Получить дисконтную карту',
			name:$('#discount_name').val(),
			phone:$('#discount_phone').val()
			},
			scriptCharset: "utf-8",
			success: function(data){
			    $('.plactic_card_btn').text("Готово!").css('background','#00abb2')
			}
			});
	}
	else
	{
	    if($('#discount_phone').val() == '')	    
		$('#discount_phone').css('border','1px solid #e94067');
	    if($('#discount_name').val() == '')
		$('#discount_name').css('border','1px solid #e94067');
	}
	})
    
    //отправка формы заявки на медпрограмму
    $('.send_overlay_form#send_med_prog').click(function(e){
	e.preventDefault()
	if($(this).hasClass('disabled')) return;
	$('#overlay_form_phone_medprog').removeClass('failed_input');
	$('#overlay_form_name_medprog').removeClass('failed_input');
	$("#overlay_form_agree_medprog").next().removeClass('failed_label');
	if(($('#overlay_form_phone_medprog').val() != '') && ($('#overlay_form_name_medprog').val() != '')&&($("#overlay_form_agree_medprog").prop("checked")))
	{
    
	    $('.send_overlay_form#send_med_prog').text("Идет отправка...").css('background','#00abb2')
	    $('.send_overlay_form#send_med_prog').addClass('disabled')
	     $.ajax({
			type: 'POST',
			url: '/post/send_message',
			data:{
			purpose:$('#send_medical_program').attr('title'),
			name:$('#overlay_form_name_medprog').val(),
			phone:$('#overlay_form_phone_medprog').val()
			},
			scriptCharset: "utf-8",
			success: function(data){
			    $('.send_overlay_form#send_med_prog').text("Спасибо за обращение!").css('background','#00abb2')
			    yaCounter17755831.reachGoal('send_med_prog_overlay');
          Comagic.addOfflineRequest({name: $('#overlay_form_name_medprog').val(), phone: $('#overlay_form_phone_medprog').val(), message: 'Заказать программу под описанием'});
			}
			});
	}
	else
	{
	    if($('#overlay_form_phone_medprog').val() == '')	    
		$('#overlay_form_phone_medprog').addClass('failed_input');
	    if($('#overlay_form_name_medprog').val() == '')
		$('#overlay_form_name_medprog').addClass('failed_input');
	    if(!$("#overlay_form_agree_medprog").prop("checked"))
		$("#overlay_form_agree_medprog").next().addClass('failed_label');
	}
	})
    
    //отправка формы заявки в хирургию
    $('.send_overlay_form#send_hirurg_prog').click(function(e){
	e.preventDefault()
	if($(this).hasClass('disabled')) return;
	$('#overlay_form_phone_hirurg').removeClass('failed_input');
	$('#overlay_form_name_hirurg').removeClass('failed_input');
	$("#overlay_form_agree_hirurg").next().removeClass('failed_label');
	if(($('#overlay_form_phone_hirurg').val() != '') && ($('#overlay_form_name_hirurg').val() != '')&&($("#overlay_form_agree_hirurg").prop("checked")))
	{
	    $('.send_overlay_form#send_hirurg_prog').text("Идет отправка...").css('background','#00abb2')
	    $('.send_overlay_form#send_hirurg_prog').addClass('disabled')
	     $.ajax({
			type: 'POST',
			url: '/post/send_hirurg',
			data:{
			purpose:'Заявка для центра хирургии',
			name:$('#overlay_form_name_hirurg').val(),
			phone:$('#overlay_form_phone_hirurg').val()
			},
			scriptCharset: "utf-8",
			success: function(data){
			    $('.send_overlay_form#send_hirurg_prog').text("Спасибо за обращение!").css('background','#00abb2')
			    yaCounter17755831.reachGoal('oms_form_otpravleno');
			}
			});
	}
	else
	{
	    if($('#overlay_form_phone_hirurg').val() == '')	    
		$('#overlay_form_phone_hirurg').addClass('failed_input');
	    if($('#overlay_form_name_hirurg').val() == '')
		$('#overlay_form_name_hirurg').addClass('failed_input');
	    if(!$("#overlay_form_agree_hirurg").prop("checked"))
		$("#overlay_form_agree_hirurg").next().addClass('failed_label');
	}
	})
    
    
    //отправка формы заявки на медпрограмму из шапки
    $('#send_med_prog_header_form').click(function(e){
	e.preventDefault()
	if($(this).hasClass('disabled')) return;
	$('#header_med_prog_agree').next().removeClass('failed_label');
	$('#phone_med_prog_header').removeClass('failed_input');
	$('#name_med_prog_header').removeClass('failed_input');
	if(($('#phone_med_prog_header').val() != '') && ($('#name_med_prog_header').val() != '')&&($("#header_med_prog_agree").prop("checked")))
	{

	    $('#send_med_prog_header_form').text("Идет отправка...").css('background','#00abb2')
	    $('#send_med_prog_header_form').addClass('disabled')
	     $.ajax({
			type: 'POST',
			url: '/post/send_message',
			data:{
			purpose:$('#send_medical_program').attr('title'),
			name:$('#name_med_prog_header').val(),
			phone:$('#phone_med_prog_header').val()
			},
			scriptCharset: "utf-8",
			success: function(data){
			    $('#send_med_prog_header_form').text("Спасибо за обращение!").css('background','#00abb2')
			    yaCounter17755831.reachGoal('send_med_prog_header_form');
          Comagic.addOfflineRequest({name: $('#name_med_prog_header').val(), phone: $('#phone_med_prog_header').val(), message: 'Заказать программу банер'});
			}
			});
	}
	else
	{
	    if($('#phone_med_prog_header').val() == '')	    
		$('#phone_med_prog_header').addClass('failed_input');
	    if($('#name_med_prog_header').val() == '')
		$('#name_med_prog_header').addClass('failed_input');
	    if(!$("#header_med_prog_agree").prop("checked"))
		$('#header_med_prog_agree').next().addClass('failed_label');
	}
	})
    
    
    //отправка отзыва с главной страницы
    $('.review_form a.big_green_btn').click(function(e){
	e.preventDefault()
	if($(this).hasClass('disabled')) return;
	$('#review_name').removeClass('failed_input');
	$('#review_phone').removeClass('failed_input');
	$("#leave_review_agree").next().removeClass('failed_label');
	if(($('#review_name').val() != '') && ($('#review_phone').val() != '') &&($('#review_clinic').val() != 'all')&&($("#leave_review_agree").prop("checked")))
	{
	    $('.review_form a').text("Идет отправка...").css('background','#00abb2')
	    $('.review_form a').addClass('disabled')
	     $.ajax({
			type: 'POST',
			url: '/reviews/add_review',
			data:{
			name:$('#review_name').val(),
			phone:$('#review_phone').val(),
			clinic:$('#review_clinic').val(),
			clinic_name:$("#review_clinic option:selected").text(),
			text:$('#review_text').val()
			},
			scriptCharset: "utf-8",
			success: function(data){
			    $('.review_form a.big_green_btn').text("Спасибо за обращение!").css('background','#00abb2')
			    //yaCounter17755831.reachGoal('send_main_form');
			}
			});
	}
	else
	{
	    if($('#review_name').val() == '')	    
		$('#review_name').addClass('failed_input');
	    if($('#review_phone').val() == '')
		$('#review_phone').addClass('failed_input');
	    if($('#review_clinic').val() == 'all')
		$('.review_form .select2-container--default .select2-selection--single').css('border','1px solid #e94067');
	    if(!$("#leave_review_agree").prop("checked"))
		$("#leave_review_agree").next().addClass('failed_label');
	}
	})
    
    
    //открытие формы заказа услуги со страницы меднаправления или подуслуги
    $('body').on('click','.subservice_from_service',function(e){
	e.preventDefault()
	$('#service_subservice_purpose').val($(this).attr('data-service'))
	yaCounter17755831.reachGoal('open_service_page_form');
	$('#send_service_subservice').show()
    });
    
    //открытие формы уточнения на странице анализов
    $('#order_specify_btn').click(function(e){
	e.preventDefault()
	yaCounter17755831.reachGoal('open_specify_form');
	$('#specify').show()
    })
    
    //отправка формы уточнения на странице анализов
    $('.send_overlay_form#specify_btn').click(function(e){
	e.preventDefault()
	if($(this).hasClass('disabled')) return;
	$('#specify_name').removeClass('failed_input');
	$('#specify_phone').removeClass('failed_input');
	$("#specify_agree").next().removeClass('failed_label');
	if(($('#specify_phone').val() != '') && ($('#specify_name').val() != '')&&($("#specify_agree").prop("checked")))
	{
	    $('.send_overlay_form#specify_btn').text("Идет отправка...").css('background','#00abb2')
	    $('.send_overlay_form#specify_btn').addClass('disabled')
	     $.ajax({
			type: 'POST',
			url: '/post/sendAnalyzeQuestion',
			data:{
			purpose:'Вопрос по анализам с сайта диалайн.рф',
			name:$('#specify_name').val(),
			phone:$('#specify_phone').val(),
			question: $('#specify_question').val()
			},
			scriptCharset: "utf-8",
			success: function(data){
			    $('.send_overlay_form#specify_btn').text("Спасибо за обращение!").css('background','#00abb2')
			    yaCounter17755831.reachGoal('send_specify_form');
			    ga('send', 'event', 'ceni2', 'otpravkaprays2');
			}
			});
	}
	else
	{
	    if($('#specify_name').val() == '')	    
		$('#specify_name').addClass('failed_input');
	    if($('#specify_phone').val() == '')
		$('#specify_phone').addClass('failed_input');
	    if(!$("#specify_agree").prop("checked"))
		$("#specify_agree").next().addClass('failed_label');
	}
	})
    
    
    //открытие формы заказа услуги врача
    $('.doctor_service_link').click(function(e){
	e.preventDefault()
	$('#doctor_service_purpose').val($(this).attr('data-service'))
	yaCounter17755831.reachGoal('open_doctor_service_form');
	$('#send_doctor_service_wrap').show()
    })
    
    //открытие формы заказа услуги с клиники
    $('.clinic_service_link').click(function(e){
	e.preventDefault();
	$('#clinic_service_purpose').val($(this).attr('data-service'));
	yaCounter17755831.reachGoal('open_clinic_service_form');
	$('#send_clinic_service').show();
    })
    
    //отправка формы заявки на услугу из врача
    $('.send_overlay_form#send_doctor_service').click(function(e){
	e.preventDefault()
	if($(this).hasClass('disabled')) return;
	$('#doctor_service_phone').removeClass('failed_input');
	$('#doctor_service_name').removeClass('failed_input');
	$("#doctor_service_agree").next().removeClass('failed_label')
	if(($('#doctor_service_phone').val() != '') && ($('#doctor_service_name').val() != '')&&($("#doctor_service_agree").prop("checked")))
	{
    
	    $('.send_overlay_form#send_doctor_service').text("Идет отправка...").css('background','#00abb2')
	    $('.send_overlay_form#send_doctor_service').addClass('disabled')
	     $.ajax({
			type: 'POST',
			url: '/post/send_doctor_service',
			data:{
			purpose:$('#doctor_service_purpose').val(),
			name:$('#doctor_service_name').val(),
			doctor_name:$('#doctor_name').val(),
			phone:$('#doctor_service_phone').val()
			},
			scriptCharset: "utf-8",
			success: function(data){
			    $('.send_overlay_form#send_doctor_service').text("Спасибо за обращение!").css('background','#00abb2')
			    yaCounter17755831.reachGoal('doctor_service_form_otpravleno');
          Comagic.addOfflineRequest({name: $('#doctor_service_name').val(), phone: $('#doctor_service_phone').val(), message: 'Запись из услуг врача'});
			}
			});
	}
	else
	{
	    if($('#doctor_service_phone').val() == '')	    
		$('#doctor_service_phone').addClass('failed_input');
	    if($('#doctor_service_name').val() == '')
		$('#doctor_service_name').addClass('failed_input');
	    if(!$("#doctor_service_agree").prop("checked"))
		$("#doctor_service_agree").next().addClass('failed_label');
	}
	})
    
    //отправка формы заявки на услугу из клиники
    $('.send_overlay_form#clinic_service_send').click(function(e){
	e.preventDefault()
	if($(this).hasClass('disabled')) return;
	$('#clinic_service_name').removeClass('failed_input');
	$('#clinic_service_phone').removeClass('failed_input');
	$("#clinic_service_agree").next().removeClass('failed_label')
	if(($('#clinic_service_phone').val() != '') && ($('#clinic_service_name').val() != '')&&($("#clinic_service_agree").prop("checked")))
	{
    
	    $('.send_overlay_form#clinic_service_send').text("Идет отправка...").css('background','#00abb2')
	    $('.send_overlay_form#clinic_service_send').addClass('disabled')
	     $.ajax({
			type: 'POST',
			url: '/post/send_clinic_service',
			data:{
			purpose:$('#clinic_service_purpose').val(),
			clinic_name:$('#clinic_name').val(),
			user_name:$('#clinic_service_name').val(),
			phone:$('#clinic_service_phone').val()
			},
			scriptCharset: "utf-8",
			success: function(data){
			    $('.send_overlay_form#clinic_service_send').text("Спасибо за обращение!").css('background','#00abb2')
			    yaCounter17755831.reachGoal('clinic_service_form_otpravleno');
          Comagic.addOfflineRequest({name: $('#clinic_service_name').val(), phone: $('#clinic_service_phone').val(), message: 'Клиники'});
			}
			});
	}
	else
	{
	    if($('#clinic_service_phone').val() == '')	    
		$('#clinic_service_phone').addClass('failed_input');
	    if($('#clinic_service_name').val() == '')
		$('#clinic_service_name').addClass('failed_input');
	    if(!$("#clinic_service_agree").prop("checked"))
		$("#clinic_service_agree").next().addClass('failed_label');
	}
	})
    
     function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
    }
    $('#send_pay_form').click(function(e){
	e.preventDefault()
	var full_form = 1;
	$('#dolg-form input[type="text"]').each(function(indx,element){
	    if($(element).val()=='')
	    {
		$(element).css('border','1px solid #e94067')
		full_form = 0;
	    }
	    else
		$(element).css('border','0px')
        });
	if(!isValidEmailAddress($('#CustEMail').val()))
	{
	    $('#CustEMail').css('border','1px solid #e94067');
	    full_form = 0;
	}
	if(!$('#clinics').val())
	{
	    $('#select2-clinics-container').css('border','1px solid #e94067');
	    full_form = 0;
	}
	if(!$("#check").prop("checked"))
	{
	    $('.input.check').css('border','1px solid #e94067');
	    full_form = 0;
	}
	else
	{
	    $('.input.check').css('border','none');
	}
	if(full_form)
	{
	    $.ajax({
			type: 'POST',
			url: '/dolg/send_data',
			data:{
			CustName:$('#CustName').val(),
			date:$('#date').val(),
			CustEMail:$('#CustEMail').val(),
			cps_phone:$('#cps_phone').val(),
			clinics:$('#clinics').val(),
			sum:$('#sum').val(),
			paymentType:$('input:radio:checked').prev().text(),
			},
			scriptCharset: "utf-8",
			success: function(data){
			    $('input[name="cps_email"]').val($('#CustEMail').val())
			    $('input[name="orderDetails"]').val('Телефон: '+$('#cps_phone').val()+', дата рождения: '+$('#date').val()+', '+$('#clinics').val())
			    $('input[name="CustomerNumber"]').val($('#CustEMail').val())
			    $('#dolg-form-send input[name="sum"]').val($('#sum').val())
			    $('#dolg-form-send input[name="paymentType"]').val($('input:radio:checked').val())
			    $('#send_pay_yandex_form').click()
			}
			});
	}
	})
    
    $('.close_mobile_menu').click(function(e){
	e.preventDefault()
	$('.main_menu_mobile').removeClass('active')
	})
    
    $('.main_menu_ul').click(function(e){
	//e.preventDefault()
	$('.main_menu_mobile').addClass('active')
	})
    
    //открытие списков вакансий
    $('.structured_data_wrap_vacancy .structured_data_table_line_border h4').click(function(e){
    e.preventDefault()
    $(this).siblings(".vacancy_description").slideToggle()
    })
    
    //отправка вакансии
    $('#send_vacancy').click(function(e){
        e.preventDefault();
	$("#vacancy_form_agree").next().removeClass('failed_label');
	$('#vacancy_name').removeClass('failed_input');
	$('#vacancy_phone').removeClass('failed_input');
        if(($('#vacancy_name').val() != '') && ($('#vacancy_phone').val() != '')&&($("#vacancy_form_agree").prop("checked")))
	{
            var order_line = '';
            $(".service_result_data_line").each(function(indx, element){
		order_line += $(element).find(".service_result_data_title").text() +' \r';
	      });
	    $('#send_vacancy').text("Идет отправка...").css('background','#00abb2')
	    $('#send_vacancy').addClass('disabled')
            $('#vacancy_list').val(order_line)
            formData = new FormData($('#send_vacancy_form').get(0)); // создаем новый экземпляр объекта и передаем ему нашу форму (*)
            $.ajax({
		    type: "POST",
                    url: "/vacancy/send_vacancy",
                    cache: false,
		    scriptCharset: "utf-8",
		    dataType: "html",
		    processData: false,
		    contentType: false,
		    data: formData,
		    success: function(data){
                            $('#send_vacancy').text("Резюме отправлено!")
			}
		});
        }
        else
        {
            if($('#vacancy_name').val() == '')	    
		$('#vacancy_name').addClass('failed_input');
	    if($('#vacancy_phone').val() == '')
		$('#vacancy_phone').addClass('failed_input');
	    if(!$("#vacancy_form_agree").prop("checked"))
		$("#vacancy_form_agree").next().addClass('failed_label');
        }
    });
    
    //Переключение на версию для слабовидящих
    $('.main_menu_selectors_wrap .numb_see').click(function(e){
	if((Cookies.get('blind_see') == 0)||(!Cookies.get('blind_see')))
	    Cookies.set('blind_see','1');
	else
	    Cookies.set('blind_see','0');
	location.reload();
	})
    
    //проверка существования программы в списке запомненных в сравнение
    function not_already_exist(value,arr_data)
    {
	console.log(arr_data.length)
	for(var i=0; i<arr_data.length; i++) 
	    if(arr_data[i] == value)
		return false;
	return true;
    }
    
    //удаление медпрограммы из сравнения
    function drop_med_prog_item(value,arr_data)
    {
	console.log(arr_data.length)
	var item_to_drop = 'not';
	for(var i=0; i<arr_data.length; i++) 
	    if(arr_data[i] == value)
	    {
		item_to_drop = i;
	    }
	if(item_to_drop != 'not')
	    arr_data.splice(item_to_drop,1);
	return arr_data;
    }
    
    //Добавление программы к сравнению
    $('body').on('click','.add_to_contest', function(e){
	    e.preventDefault()
	    var med_prog_list = new Array();
	    if($(this).hasClass('delete_from_contest'))
	    {
		med_prog_list = JSON.parse(Cookies.get('med_prog_list'));
		Cookies.set('med_prog_list',JSON.stringify(drop_med_prog_item($(this).attr('program_id'),med_prog_list)));
	    }
	    else
	    {
		if(!Cookies.get('med_prog_list'))
		    med_prog_list[0] = $(this).attr('program_id');
		else
		{
		    med_prog_list = JSON.parse(Cookies.get('med_prog_list'));
		    if(not_already_exist($(this).attr('program_id'),med_prog_list))
			med_prog_list[med_prog_list.length] = $(this).attr('program_id');
		}
		Cookies.set('med_prog_list',JSON.stringify(med_prog_list));
	    }
	    isChilds = '/adults';
	    if ($('.childs').hasClass('active')) 
		isChilds = '/kids';
	    //количество программ в сравнении
	    $.ajax({
		    type: "POST",
                    url: "/info/refresh_medical_program_count"+isChilds,
                    cache: false,
		    scriptCharset: "utf-8",
		    dataType: "html",
		    processData: false,
		    contentType: false,
		    success: function(data){
                            $('.count_contest_text_data').text(data)
			}
		});
	    //верстка списка программ
	    $.ajax({
		    type: "POST",
                    url: "/info/refresh_medical_program_list"+isChilds,
                    cache: false,
		    scriptCharset: "utf-8",
		    dataType: "html",
		    processData: false,
		    contentType: false,
		    success: function(data){
                            $('.list_programs_type_wrap').html(data)
			}
		});
	});
    
    //удаление комплексной медицинской программы на странице сравнения программ
    $('body').on('click','.programs_carousel_wrap .drop_order_program',function(e){
	e.preventDefault()
	var med_prog_list = new Array();
	med_prog_list = JSON.parse(Cookies.get('med_prog_list'));
	Cookies.set('med_prog_list',JSON.stringify(drop_med_prog_item($(this).attr('program_id'),med_prog_list)));
	$.ajax({
		    type: "POST",
                    url: "/info/refresh_medical_program_compare_list"+ $('#path_segment').val(),
                    cache: false,
		    scriptCharset: "utf-8",
		    dataType: "html",
		    processData: false,
		    contentType: false,
		    success: function(data){
                            if(med_prog_list == '')
			    {
				$('.programm_legend_wrap').remove()
				$('.programs_carousel_wrap .swiper-wrapper').height('auto')
			    }
			    $('.programs_carousel_wrap .swiper-wrapper').html(data);
			    equal_height_for_compare_programs();
			}
		});
	});
    
    //открытие окна заявки на обратный звонок с виджета
    $('.fixed_desktop_callback_site').click(function(e){
	e.preventDefault()
	yaCounter17755831.reachGoal('fixed_desktop_callback_site');
	$('.overlay_form_wrapper#widget_wrap').show();
	})
    
    //фиксирование события перехода на инфоклинику с виджета
    $('.fixed_desktop_callback_lk').click(function(e){
	    yaCounter17755831.reachGoal('fixed_desktop_callback_lk');
	});
    
    $('.callibri_phone1').click(function(e){
	    yaCounter17755831.reachGoal('click_numb_vlg');
	});
    $('.callibri_phonevlz').click(function(e){
	    yaCounter17755831.reachGoal('click_numb_volzh');
	});
    $('.callibri_phonemih').click(function(e){
	    yaCounter17755831.reachGoal('click_numb_mih');
	});
    
     $('.open-popup-link').magnificPopup({
	type:'inline',
	midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
      });
     
     $('body').on('click','.open_switch_text',function(){
	    $('.switch_text_hidden').toggleClass('rolled_up');
	    $(this).toggleClass('opened');
	});
     
});
