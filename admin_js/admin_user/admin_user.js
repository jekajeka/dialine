//отправка основной карточки пользователя
$('form#single_user').submit(function(e) {
        e.preventDefault();
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }
        if (submit)
        {
            $.ajax({
		    type: "POST",
                    url: "/admin_user/user_update",
                    cache: false,
                    data:{
                        id:$('#iser_id').val(),
                        username:$('#login-name').val(),
                        first_name:$('#first-name').val(),
                        last_name:$('#last-name').val(),
                        company:$('#company').val(),
                        phone:$('#phone').val(),
                        email:$('#email').val(),
                        active:$('#active').val()
			},
		    scriptCharset: "utf-8",
		    dataType: "html",
		    success: function(data){
                            //alert(data)
                            $('#user_final').prepend(data)
			}
		});
        }
        return false;
      });

//смена пароля
$('form#single_user_password').submit(function(e) {
        e.preventDefault();
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }
        if (submit)
        {
            $.ajax({
		    type: "POST",
                    url: "/admin_user/change_password",
                    cache: false,
                    data:{
                        id:$('#user_email').val(),
                        old_pwd:$('#old_password').val(),
                        new_pwd:$('#password2').val()
			},
		    scriptCharset: "utf-8",
		    dataType: "html",
		    success: function(data){
                            $('#user_password_final').prepend(data)
			}
		});
        }
        return false;
      });

//Добавление нового пользователя
$('form#add_single_user').submit(function(e) {
        e.preventDefault();
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }
        if (submit)
        {
            $.ajax({
		    type: "POST",
                    url: "/admin_user/add_user",
                    cache: false,
                    data:{
                        id:$('#iser_id').val(),
                        username:$('#login-name').val(),
                        first_name:$('#first-name').val(),
                        last_name:$('#last-name').val(),
                        company:$('#company').val(),
                        phone:$('#phone').val(),
                        email:$('#email').val(),
                        new_pwd:$('#password2').val()
			},
		    scriptCharset: "utf-8",
		    dataType: "html",
		    success: function(data){
                            //alert(data)
                            $('#add_user_final').prepend(data)
			}
		});
        }
        return false;
      });

//Удаление основной карточки пользователя
$('.fa-trash').click(function(e) {
        e.preventDefault();
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }
        if (submit)
        {
            $.ajax({
		    type: "POST",
                    url: "/admin_user/delete/"+$('#iser_id').val(),
                    cache: false,
                    data:{
			},
		    scriptCharset: "utf-8",
		    dataType: "html",
		    success: function(data){
                            $('#user_final').prepend(data)
			}
		});
        }
        return false;
      });