CKEDITOR.replace( 'response' );
//Редактирование новости
$('form#edit_review').submit(function(e) {
        e.preventDefault();
	for (instance in CKEDITOR.instances) {
		CKEDITOR.instances[instance].updateElement();
	    }
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }
        if (submit)
        {
	var $that = $(this),
	formData = new FormData($that.get(0)); // создаем новый экземпляр объекта и передаем ему нашу форму (*)
            $.ajax({
		    type: "POST",
                    url: "/admin_reviews/edit_review/"+$('#id').val(),
                    cache: false,
		    scriptCharset: "utf-8",
		    dataType: "html",
		    processData: false,
		    contentType: false,
		    data: formData,
		    success: function(data){
                            $('#add_user_final').prepend(data)
			}
		});
        }
        return false;
      });

$('.fa-trash').click(function(e){
    e.preventDefault()
    $.ajax({
		    type: "POST",
                    url: "/admin_reviews/delete_review/"+$('#id').val(),
                    cache: false,
		    scriptCharset: "utf-8",
		    dataType: "html",
		    processData: false,
		    contentType: false,
		    data: {},
		    success: function(data){
                            alert("Запись успешно удалена")
			}
		});
    })

$('#date_review').daterangepicker({
          singleDatePicker: true,
	  calender_style: "picker_1",
	  locale: {
	            format: 'YYYY-MM-DD'
	        }
        
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });

 $('input').iCheck({
	handle: 'checkbox',
	checkboxClass: 'icheckbox_flat-green',
	radioClass: 'iradio_flat-red'
  });
