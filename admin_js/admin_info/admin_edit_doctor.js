CKEDITOR.replace( 'text' );
//�������������� �������
$('form#edit_doctor').submit(function(e) {
        e.preventDefault();
	for (instance in CKEDITOR.instances) {
		CKEDITOR.instances[instance].updateElement();
	    }
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }
        if (submit)
        {
	$('input[name="medical_direction_real"]').val($('#medical_direction').val())
	var $that = $(this),
	formData = new FormData($that.get(0)); // ������� ����� ��������� ������� � �������� ��� ���� ����� (*)
            $.ajax({
		    type: "POST",
                    url: "/admin_doctors/edit_doctor/"+$('#id').val(),
                    cache: false,
		    scriptCharset: "utf-8",
		    dataType: "html",
		    processData: false,
		    contentType: false,
		    data: formData,
		    success: function(data){
                            $('#add_user_final').prepend(data)
			}
		});
        }
        return false;
      });

 $('input').iCheck({
	handle: 'checkbox',
	checkboxClass: 'icheckbox_flat-green',
	radioClass: 'iradio_flat-red'
  });

$('#save_doctors_services button').click(function(e){
	e.preventDefault()
	var arr = [];
	$("tbody#service_table input:checkbox:checked").each(function(indx, element){
		arr[indx] = Object();
		arr[indx].service_id = $(element).attr('med_id')
		arr[indx].doctor_id = $('#id').val()
		arr[indx].price = $('#price_'+$(element).attr('curr_id')).val()
	      });
	//alert(JSON.stringify(arr));
	$.ajax({
		    type: "POST",
                    url: "/admin_doctors/edit_doctor_services/"+$('#id').val(),
                    cache: false,
		    scriptCharset: "utf-8",
		    dataType: "html",
		    data: {doctor_services:JSON.stringify(arr)},
		    success: function(data){
                            $('#save_doctors_services').prepend(data);
			}
		});
	})

$('#save_doctors_clinic button').click(function(e){
	e.preventDefault()
	var arr = [];
	$("tbody#table_clinic input:checkbox:checked").each(function(indx, element){
		arr[indx] = Object();
		arr[indx].clinic_id = $(element).attr('clinic_id')
		arr[indx].doctor_id = $('#id').val()
	      });
	$.ajax({
		    type: "POST",
                    url: "/admin_doctors/edit_doctor_clinics/"+$('#id').val(),
                    cache: false,
		    scriptCharset: "utf-8",
		    dataType: "html",
		    data: {doctor_clinics:JSON.stringify(arr)},
		    success: function(data){
                            $('#save_doctors_clinic').prepend(data)
			}
		});
	})
        
    $('.fa-trash').click(function(e){
    e.preventDefault()
    $.ajax({
		    type: "POST",
                    url: "/admin_doctors/delete_doctor/"+$('#id').val(),
                    cache: false,
		    scriptCharset: "utf-8",
		    dataType: "html",
		    processData: false,
		    contentType: false,
		    data: {},
		    success: function(data){
                            alert("Запись успешно удалена");
			}
		});
    });

