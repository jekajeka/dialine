CKEDITOR.replace( 'text' );
//Редактирование новости
$('form#add_medical_direction').submit(function(e) {
        e.preventDefault();
        var submit = true;
	for (instance in CKEDITOR.instances) {
		CKEDITOR.instances[instance].updateElement();
	    }
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }
        if (submit)
        {
	var $that = $(this),
	formData = new FormData($that.get(0)); // создаем новый экземпляр объекта и передаем ему нашу форму (*)
            $.ajax({
		    type: "POST",
                    url: "/admin_doctors/add_medical_direction",
                    cache: false,
		    scriptCharset: "utf-8",
		    dataType: "html",
		    processData: false,
		    contentType: false,
		    data: formData,
		    success: function(data){
                            $('#add_user_final').prepend(data)
			}
		});
        }
        return false;
      });