
//Редактирование новости
$('form#edit_gallery').submit(function(e) {
        e.preventDefault();
	
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }
        if (submit)
        {
	var $that = $(this),
	formData = new FormData($that.get(0)); // создаем новый экземпляр объекта и передаем ему нашу форму (*)
            $.ajax({
		    type: "POST",
                    url: "/admin_gallery/edit_gallery/"+$('#id').val(),
                    cache: false,
		    scriptCharset: "utf-8",
		    dataType: "html",
		    processData: false,
		    contentType: false,
		    data: formData,
		    success: function(data){
                            $('#add_user_final').prepend(data)
			}
		});
        }
        return false;
      });

$('.image .fa.fa-times').click(function(e){
    e.preventDefault()
    //alert($(this).attr('image_data'))
    $.ajax({
		    type: "POST",
                    url: "/admin_gallery/delete_image/" + $('#id').val() + '/' + $(this).attr('image_data'),
                    cache: false,
		    scriptCharset: "utf-8",
		    dataType: "html",
		    processData: false,
		    contentType: false,
		    data: {},
		    success: function(data){
                            //$('#add_user_final').prepend(data)
                            if (data==1) {
                                alert('Удалено')
                            }
			}
		});
    })
 
  $('.fa-trash').click(function(e){
    e.preventDefault()
    $.ajax({
		    type: "POST",
                    url: "/admin_gallery/delete_gallery/"+$('#id').val(),
                    cache: false,
		    scriptCharset: "utf-8",
		    dataType: "html",
		    processData: false,
		    contentType: false,
		    data: {},
		    success: function(data){
                            alert("Запись успешно удалена")
			}
		});
    })