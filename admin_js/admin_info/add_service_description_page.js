CKEDITOR.replace( 'text' );
//Редактирование новости
$('form#add_service_description').submit(function(e) {
        e.preventDefault();
	for (instance in CKEDITOR.instances) {
		CKEDITOR.instances[instance].updateElement();
	    }
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }
        if (submit)
        {
	var $that = $(this),
	formData = new FormData($that.get(0)); // создаем новый экземпляр объекта и передаем ему нашу форму (*)
            $.ajax({
		    type: "POST",
                    url: "/admin_services_description/add_service_description",
                    cache: false,
		    scriptCharset: "utf-8",
		    dataType: "html",
		    processData: false,
		    contentType: false,
		    data: formData,
		    success: function(data){
                            $('#add_user_final').prepend(data)
			}
		});
        }
        return false;
      });