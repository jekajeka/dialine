
//Редактирование новости
$('form#edit_law').submit(function(e) {
        e.preventDefault();
	
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }
        if (submit)
        {
	var $that = $(this),
	formData = new FormData($that.get(0)); // создаем новый экземпляр объекта и передаем ему нашу форму (*)
            $.ajax({
		    type: "POST",
                    url: "/admin_law/edit_law/"+$('#id').val(),
                    cache: false,
		    scriptCharset: "utf-8",
		    dataType: "html",
		    processData: false,
		    contentType: false,
		    data: formData,
		    success: function(data){
                            $('#add_user_final').prepend(data)
			}
		});
        }
        return false;
      });

 
 
 $('.fa-trash').click(function(e){
    e.preventDefault()
    $.ajax({
		    type: "POST",
                    url: "/admin_law/delete_law/"+$('#id').val(),
                    cache: false,
		    scriptCharset: "utf-8",
		    dataType: "html",
		    processData: false,
		    contentType: false,
		    data: {},
		    success: function(data){
                            alert("Запись успешно удалена")
			}
		});
    })