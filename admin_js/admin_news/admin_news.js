CKEDITOR.replace( 'full_text' );
//Добавление новой новости
$('form#add_news').submit(function(e) {
        e.preventDefault();
	for (instance in CKEDITOR.instances) {
		CKEDITOR.instances[instance].updateElement();
	    }
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }
        if (submit)
        {
	var $that = $(this),
	formData = new FormData($that.get(0)); // создаем новый экземпляр объекта и передаем ему нашу форму (*)
            $.ajax({
		    type: "POST",
                    url: "/admin_news/add_news",
                    cache: false,
		    scriptCharset: "utf-8",
		    dataType: "html",
		    processData: false,
		    contentType: false,
		    data: formData,
		    success: function(data){
                            $('#add_user_final').prepend(data)
			}
		});
        }
        return false;
      });

//Редактирование новости
$('form#edit_news').submit(function(e) {
        e.preventDefault();
	for (instance in CKEDITOR.instances) {
		CKEDITOR.instances[instance].updateElement();
	    }
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }
        if (submit)
        {
	var $that = $(this),
	formData = new FormData($that.get(0)); // создаем новый экземпляр объекта и передаем ему нашу форму (*)
            $.ajax({
		    type: "POST",
                    url: "/admin_news/edit_news/"+$('#id').val(),
                    cache: false,
		    scriptCharset: "utf-8",
		    dataType: "html",
		    processData: false,
		    contentType: false,
		    data: formData,
		    success: function(data){
                            $('#add_user_final').prepend(data)
			}
		});
        }
        return false;
      });


$('.fa-trash').click(function(e){
    e.preventDefault()
    $.ajax({
		    type: "POST",
                    url: "/admin_news/delete_news/"+$('#id').val(),
                    cache: false,
		    scriptCharset: "utf-8",
		    dataType: "html",
		    processData: false,
		    contentType: false,
		    data: {},
		    success: function(data){
                            alert("Запись успешно удалена")
			}
		});
    })

//загрузка файла данных организаций на сервер


$('#my_form').on('submit', function(e){
    e.preventDefault();
    var $that = $(this),
    formData = new FormData($that.get(0)); // создаем новый экземпляр объекта и передаем ему нашу форму (*)
    $. ajax ({
      url: $that.attr('action'),
      type: $that.attr('method'),
      contentType: false, // важно - убираем форматирование данных по умолчанию
      processData: false, // важно - убираем преобразование строк по умолчанию
      data: formData,
      dataType: 'json',
      success: function(json){
      	if(json){
          $that.replaceWith(json);
        }
      }
    });
  });


$('#date_pub').daterangepicker({
          singleDatePicker: true,
	  calender_style: "picker_1",
	  locale: {
	            format: 'YYYY-MM-DD h:mm'
	        }
        
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });

$(".select2_single").select2({
          placeholder: "Выберите тип статьи",
          allowClear: true
        });
