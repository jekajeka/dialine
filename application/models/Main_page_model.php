<?
class Main_page_model extends CI_Model {
private $enterprise_query_data_list = '';

function Main_page_model()
{
    parent::__construct();
}

function get_doctors()
{
    
}

function get_rubrics()
{
    $this->db->select('title, path, medical_directions.id');
    $this->db->from('medical_directions');
    $this->db->join('published', "published.type = 'medical_directions' AND published.record_id = medical_directions.id ",'left');
    $this->db->where('(published.published=1 OR published.published IS NULL)');
    $this->db->where('medical_directions.id<>'.ANALYZE_ID);
    $this->db->order_by("title", "ASC");
    return $this->db->get()->result();    
}

//�������� ����� ����������, ���������
function get_clinics()
{
    $data = $this->db->get('clinics');
    return $data->result();
}

function get_services()
{
    $this->db->select('service_title,service_rubric_title, service_path, medical_directions.id');
    $this->db->from('medical_directions');
    $this->db->join('published', "published.type = 'medical_directions' AND published.record_id = medical_directions.id ",'left');
    $this->db->where('(published.published=1 OR published.published IS NULL)');
    $this->db->order_by("service_rubric_title", "ASC");
    return $this->db->get()->result();
}

}
