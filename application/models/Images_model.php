<?
class Images_model extends CI_Model {
private $enterprise_query_data_list = '';

function Images_model()
{
    parent::__construct();
}
function add_image($parent_id = 0,$parent_type = 'any',$path)
{
    $image_descript = array('parent_type' => $parent_type,
                            'parent_id' => $parent_id,
                            'path' => $path,
                            'date_pub' => date("Y-m-d"));
    
    $result = $this->db->insert('images', $image_descript);
    return $result;
}
function clear_all_parent_images($parent_id = NULL,$parent_type = 'any')
{
    $query = " SELECT *
            FROM images
            WHERE parent_id=$parent_id
            AND parent_type='$parent_type'";
    $data = $this->db->query($query);
    $images = $data->result();
    foreach($images as $image)
    {
        unlink('.'.$image->path);
    }
    $this->db->delete('images', array('parent_id' => $parent_id,'parent_type' => $parent_type));
}

function delete($id = NULL)
{
    $query = " SELECT *
            FROM images
            WHERE id=$id";
    $data = $this->db->query($query);
    $images = $data->result();
    foreach($images as $image)
        unlink('.'.$image->path);    
    return $this->db->delete('images', array('id' => $id));
}

}
