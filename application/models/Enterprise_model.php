<?
class Enterprise_model extends CI_Model {

    private $enterprise_query_data_list = 'enterprise.full_okved, enterprise.fio, enterprise.post,enterprise.id, enterprise.title, enterprise.full_title, enterprise.address, enterprise.coords,enterprise.size,enterprise.level, enterprise_okved.okved AS real_okved ';
    function Enterprise_model()
    {
        parent::__construct();
        $this->load->model('Enterprise_okved_model', '', TRUE);
    }
    
    function clear()
    {
        $this->db->truncate('enterprise');
    }
    
    function load_enterprise_list($enterprise_list = array())
    {
        ini_set('max_execution_time', 100);
        $this->clear();
        $this->Enterprise_okved_model->clear();
        foreach($enterprise_list as $enterprise)
        {
            if(is_numeric($enterprise[0]))
            {
                $data = array
                (
                   'title' => $enterprise[1],
                   'full_title' => $enterprise[3],
                   'address' => $enterprise[4],
                   'coords'=>'',
                   'level'=>$enterprise[32],
                   'size'=>$enterprise[11],
                   'post'=>$enterprise[6],
                   'fio'=>$enterprise[5],
                   'full_okved'=>$enterprise[8],
                );
                $this->db->insert('enterprise', $data);
                $this->Enterprise_okved_model->add_okved($this->db->insert_id(),trim($enterprise[9]));
                $this->db->insert_id();
            }
        }
    }
    
    //Выгрузка списка регионов
    function load_region_list()
    {
        $query = "SELECT
                DISTINCT region
                FROM enterprise
                WHERE region<>''
                ORDER BY region ASC";
        $data = $this->db->query($query);
        return $data->result();
    }
    
    function load_enterprise_list_unformatted($enterprise_list = array())
    {
        ini_set('max_execution_time', 1000);
        $this->clear();
        $this->Enterprise_okved_model->clear();
        foreach($enterprise_list as $enterprise)
        {
            $formatted__enterprises = array();
            if(is_numeric($enterprise[0]))
            {
                $formatted__enterprises['title'] = $enterprise[1];
                $formatted__enterprises['full_title'] = $enterprise[4];
                $formatted__enterprises['address'] = $enterprise[5];
                $formatted__enterprises['coords'] = '';
                $formatted__enterprises['post'] = $enterprise[8];
                $formatted__enterprises['fio'] = $enterprise[7];
                if(isset($enterprise[27]))
                    $formatted__enterprises['full_okved'] = $enterprise[17];
                else
                    $formatted__enterprises['full_okved'] = '';
                if(isset($enterprise[27]))
                    $formatted__enterprises['size'] = $enterprise[27];
                else
                    $formatted__enterprises['size'] = '';
                if(isset($enterprise[51]))
                    $formatted__enterprises['level'] = trim($enterprise[51]);
                else
                    $formatted__enterprises['level'] = '';
                $this->db->insert('enterprise', $formatted__enterprises);
                if(isset($enterprise[18]))
                    $this->Enterprise_okved_model->add_okved($this->db->insert_id(),trim($enterprise[18]));
                else
                    $this->Enterprise_okved_model->add_okved($this->db->insert_id(),'');
            }
        }
    }
    
    function load_enterprise_list_unformatted_fast_method($enterprise_list = array())
    {
        ini_set('max_execution_time', 1000);
        $this->clear();
        $this->Enterprise_okved_model->clear();
        $formatted__enterprises = array();
        $formatted_enterprises_okved = array();
        foreach($enterprise_list as $key=>$enterprise)
        {
            if(is_numeric($enterprise[0]))
            {
                $formatted__enterprises[$key] = array();
                //$formatted__enterprises[$key]['id'] = $enterprise[0];
                $formatted__enterprises[$key]['id'] = $key+1;
                $formatted__enterprises[$key]['title'] = $enterprise[1];
                $formatted__enterprises[$key]['full_title'] = $enterprise[4];
                $formatted__enterprises[$key]['address'] = $enterprise[5];
                $formatted__enterprises[$key]['coords'] = '';
                $formatted__enterprises[$key]['post'] = $enterprise[8];
                $formatted__enterprises[$key]['fio'] = $enterprise[7];
                if(isset($enterprise[27]))
                    $formatted__enterprises[$key]['full_okved'] = $enterprise[17];
                else
                    $formatted__enterprises[$key]['full_okved'] = '';
                if(isset($enterprise[27]))
                    $formatted__enterprises[$key]['size'] = $enterprise[27];
                else
                    $formatted__enterprises[$key]['size'] = '';
                if(isset($enterprise[51]))
                    $formatted__enterprises[$key]['region'] = trim($enterprise[51]);
                else
                    $formatted__enterprises[$key]['region'] = '';
                //формирование массива оквэд-предприятие
                //$formatted_enterprises_okved[$key]['enterprise_id'] = $enterprise[0];
                $formatted_enterprises_okved[$key]['enterprise_id'] = $key+1;
                if(isset($enterprise[18]))
                    $formatted_enterprises_okved[$key]['okved'] = $enterprise[18];
                else
                    $formatted_enterprises_okved[$key]['okved'] = '';
            }
        }
        $this->db->insert_batch('enterprise', $formatted__enterprises);
        $this->Enterprise_okved_model->add_okved_batch($formatted_enterprises_okved);
    }
    
    
    
    //Простейший список предприятий
    function get_simple_list()
    {
        $query = '
                SELECT
                '.$this->enterprise_query_data_list.'
                FROM
                enterprise,enterprise_okved
                WHERE
                enterprise_okved.enterprise_id=enterprise.id
                ';
        $query .=' ORDER BY title ASC ';
        $data = $this->db->query($query);
        return $data->result();
    }
    
    
    //Кэширование геоданных по запросу
    function get_geodata($enterprise_id)
    {
        $this->db->select('address,coords');
        $this->db->from('enterprise');
        $this->db->where('id',$enterprise_id);
        $query = $this->db->get();
        $geodata = $query->result();
        //отдача координат пользователю с еще незакэшированными геоданными
        if(!empty($geodata[0]->coords))
        {
            return $geodata[0]->coords;
        }
        else
        {
            //Получение и кэширование геоданных из яндекса
            $xml = simplexml_load_file('https://geocode-maps.yandex.ru/1.x/?geocode='.urlencode($geodata[0]->address).'&results=1');
	    $found = $xml->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found;
	    if ($found > 0)
	    {
		$coords = str_replace(' ', ',', $xml->GeoObjectCollection->featureMember->GeoObject->Point->pos);
		$coords_array = explode(',',$coords);
		$current_coords = $coords_array[1].','.$coords_array[0];
                $data = array(
                'coords' => $current_coords
                );
                $this->db->where('id', $enterprise_id);
                $this->db->update('enterprise', $data);
                return $current_coords;
	    }
            return 'Не удалось обновить данные';
        }
    }
    
    //Выборка предприятий по размеру
    function get_by_size($size='Все')
    {
        $query = '
                SELECT
                '.$this->enterprise_query_data_list.'
                FROM
                enterprise,enterprise_okved
                WHERE
                enterprise_okved.enterprise_id=enterprise.id
                ';
        
        if($size!='Все')
            $query .= " AND enterprise.size='$size' ";
        $query .=' ORDER BY title ASC ';
        $data = $this->db->query($query);
        return $data->result();
    }
    
    //Выборка предприятий по региону
    function fast_get_by_region($region='Все')
    {
        $query = '
                SELECT
                '.$this->enterprise_query_data_list.'
                FROM
                enterprise,enterprise_okved
                WHERE
                enterprise_okved.enterprise_id=enterprise.id
                ';
        if($region!='Все')
            $query .= " AND enterprise.region='$region' ";
        $query .=' ORDER BY title ASC ';
        $data = $this->db->query($query);
        return $data->result();
    }
    
    //Выборка предприятий по ОКВЭДам и другим доп данным
    //get_by_okved($_POST['okved_list'],$_POST['enterprise_size'],$_POST['enterprise_region'],$_POST['enterprise_level']);
    function get_by_okved($okved_list='',$enterprise_size='Все',$enterprise_region='Все',$enterprise_level='Все')
    {        
        $query = '
                SELECT
                '.$this->enterprise_query_data_list.'
                FROM
                enterprise,enterprise_okved
                WHERE
                enterprise_okved.enterprise_id=enterprise.id
                ';
        if(!empty($okved_list))
            $query .=' AND enterprise_okved.okved IN ('.$okved_list.') ';
        if($enterprise_size!='Все')
            $query .= " AND enterprise.size='$enterprise_size' ";
        if($enterprise_region!='Все')
            $query .= " AND enterprise.region='$enterprise_region' ";
        if($enterprise_level!='Все')
            $query .= " AND enterprise.level='$enterprise_level' ";
        $query .=' ORDER BY title ASC ';
        $data = $this->db->query($query);
        
        return $data->result();
    }
    
    //Быстрая выборка по ОКВЭД
    function fast_get_by_okved($okved_list='Все')
    {
        $query = '
                SELECT
                '.$this->enterprise_query_data_list.'
                FROM
                enterprise,enterprise_okved
                WHERE
                enterprise_okved.enterprise_id=enterprise.id
                ';
        if($okved_list!='Все')
            $query .=' AND enterprise_okved.okved IN ('.$okved_list.') ';
        $data = $this->db->query($query);
        return $data->result();
    }
    
    function search_by_name_or_okved($search_string='')
    {
        
        $query = '
                SELECT
                '.$this->enterprise_query_data_list.'
                FROM
                enterprise,enterprise_okved
                WHERE
                enterprise_okved.enterprise_id=enterprise.id
                ';
        $additional_query = '';
        if(!empty($search_string))
            $additional_query .= "AND (
                                full_title LIKE '%".$search_string."%'
                                OR enterprise_okved.okved='".$search_string."'
                                )";
        $query .= $additional_query.' ORDER BY title ASC ';
        $data = $this->db->query($query);
        return $data->result();
    }
    
    
    //Массовое получение геоданных по организациям
    function get_selected_organisation_coords($organisation_list = '')
    {
        $query = '
                SELECT
                title,address,coords,id,full_okved,enterprise.fio, enterprise.post
                FROM enterprise
                ';
        if(!empty($organisation_list))
            $query .='WHERE enterprise.id IN ('.$organisation_list.') ';
        $query .= ' ORDER BY title ASC ';
    $data = $this->db->query($query)->result();
    $coordinated_data = array();
    //print_r($data);
    $getted_yandex_data = array();
    foreach($data as $key=>$geopoint)
    {
        $coordinated_data[$key] = $geopoint;
        if(!empty($geopoint->coords))
        {
        }
        else
        {
            //Получение и кэширование геоданных из яндекса
            $xml = simplexml_load_file('https://geocode-maps.yandex.ru/1.x/?geocode='.urlencode($geopoint->address).'&results=1');
	    $found = $xml->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found;
	    if ($found > 0)
	    {
		$coords = str_replace(' ', ',', $xml->GeoObjectCollection->featureMember->GeoObject->Point->pos);
		$coords_array = explode(',',$coords);
		$current_coords = $coords_array[1].','.$coords_array[0];
                $getted_yandex_data[] = array(
                    'id'=> $geopoint->id,
                    'coords' => $current_coords
                );
                //$this->db->where('id', $geopoint->id);
                //$this->db->update('enterprise', $getted_yandex_data);
                //return $current_coords;
                $coordinated_data[$key]->coords = $current_coords;
	    }
        }
    }
    //print_r($getted_yandex_data);
    if(count($getted_yandex_data)>0)
        $this->db->update_batch('enterprise',$getted_yandex_data,'id');
    return $coordinated_data;
    }
    
}
?>