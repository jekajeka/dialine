<?
class Files_model extends CI_Model {
private $enterprise_query_data_list = '';

function Files_model()
{
    parent::__construct();
}
function add_file($parent_id = 0,$parent_type = 'any',$path)
{
    $image_descript = array('parent_type' => $parent_type,
                            'parent_id' => $parent_id,
                            'path' => $path,
                            'date_pub' => date("Y-m-d"));
    
    $result = $this->db->insert('files', $image_descript);
    return $result;
}
function clear_all_parent_files($parent_id = NULL,$parent_type = 'any')
{
    $query = " SELECT *
            FROM files
            WHERE parent_id=$parent_id
            AND parent_type='$parent_type'";
    $data = $this->db->query($query);
    $files = $data->result();
    foreach($files as $file)
    {
        unlink('.'.$file->path);
    }
    $this->db->delete('files', array('parent_id' => $parent_id,'parent_type' => $parent_type));
}
}
