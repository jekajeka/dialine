<?
class Enterprise_okved_model extends CI_Model {

    function Enterprise_okved_model()
    {
        parent::__construct();
    }
    
    function clear()
    {
        $this->db->truncate('enterprise_okved');
    }
    function add_okved($enterprise_id = NULL,$okved = 0) 
    {
        $data = array
                (
                   'enterprise_id' => $enterprise_id,
                   'okved' => $okved
                );
                $this->db->insert('enterprise_okved', $data);
    }
    
    //�������� ���������� �����
    function add_okved_batch($okved_list=array())
    {
        $this->db->insert_batch('enterprise_okved', $okved_list);
    }
}
?>