<?
class Headings_model extends CI_Model {

function Headings_model()
{
    parent::__construct();
}

function select_headings_by_type($headings_type = NULL)
{
    $query = " SELECT *
            FROM headings
            WHERE headings_type=$headings_type";
    $data = $this->db->query($query);
    return $data->result();
}

function clear_all_node_headings($node_id,$headings_type)
{
    $query = "DELETE 
            FROM headings_node
            USING headings_node,headings
            WHERE node_id=$node_id
            AND headings_id=headings.id
            AND headings_type=$headings_type";
    $data = $this->db->query($query);
    return $data;
}

function create_node_headings($node_id,$headings_list)
{
    if($headings_list != 0)
    {
        $node_heading=array('node_id'=>$node_id,'headings_id'=>$headings_list);
        $result = $this->db->insert('headings_node', $node_heading);
        return $result;
    }
    else return 'Не выбрана рубрика';
}
}
