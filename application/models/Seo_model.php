<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Seo_model extends CI_Model
{
	private $seoTableName = 'seo_meta_tags';
	
	public function readSeoRecord($table, $id){
		$query = $this->db->get_where($this->seoTableName, array('entity_table' => $table, 'entity_id' => $id));
		return $query->result_array();
	}
	
	public function createSeoRecord($table, $id, $data){
		$data = array_merge($data, ['entity_table' => $table, 'entity_id' => $id]);
		return $this->db->insert($this->seoTableName, $data);
	}
	
	public function updateSeoRecord($table, $id, $data){
		$this->db->where('entity_table', $table);
		$this->db->where('entity_id', $id);
		return $this->db->update($this->seoTableName, $data);
	}
	
	public function recordExists($table, $id){
		$sql = "SELECT * FROM {$this->seoTableName} WHERE entity_table = '$table' AND entity_id = $id";
		$query = $this->db->query($sql);
		$row = $query->row();
		return isset($row);
	}
}
