<?
class Okved_model extends CI_Model {

    function Okved_model()
    {
        parent::__construct();
    }
    
    function clear()
    {
        $this->db->empty_table('okved');
    }
    
    function load_okved_list_to_db($okved_list = array())
    {
        ini_set('max_execution_time', 100);
        foreach($okved_list as $okved)
        {
            $this->db->insert('okved', $okved);
        }
    }
    
    
    function get_simple_list()
    {
        $this->db->select('id, title, full_title, address, coords');
        $this->db->from('enterprise');
        $this->db->order_by('title', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_all_order_by($order_field='id',$direction = 'ASC')
    {
        $this->db->select();
        $this->db->from('okved');
        $this->db->order_by($order_field,$direction);
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_searched($search_string='')
    {
        $query = "SELECT * FROM okved WHERE
        okved LIKE '%".$search_string."%'
        OR
        text LIKE '%".$search_string."%'
        ORDER BY okved.okved ASC";
        $data = $this->db->query($query);
        return $data->result();
    }
    
    
}
?>