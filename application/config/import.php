<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['upload'] = realpath(FCPATH.'/../import/upload');

$config['backup'] = realpath(FCPATH.'/../import/backup');

$config['binaries'] = '/usr/local/bin';

$config['backup-tables'] = ['services', 'service_clinic', 'service_doctor'];

$config['med-tests-grpname'] = [
	'SPECID' => 165,
	'MAP' => [
		0 => 'Анализы не требующие записи',
		1 => 'Анализы по предварительной записи'
	]
];

// add keys for these fields
// when import_prices_temp table is created
$config['import_keys'] = ['KODOPER', 'SPECID', 'FILID'];

//hacks
$config['hacks'] = [
	'clinic_services' => [
		'volzhskij_hirurgija' => [
			'clinic-id' => 8,
			'sql-filter' => "(rubric IN (32) OR service_type LIKE '%операции%')"
		]
	],
	'group-order' => ["Приемы, консультации", "Диагностика", "Манипуляции", "Малые операции", "Операции"]
];
