<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'main_page';
$route['404_override'] = 'error_controller';
$route['translate_uri_dashes'] = FALSE;

$route['news/((\w+)(-?\w+)*).new'] = "news/view/$1";
$route['news'] = "news";

$route['search'] = "search";
$route['actions'] = "news/actions";
$route['dolg'] = "dolg";
$route['clinic'] = "clinic";
$route['reviews'] = "reviews";
$route['actions/(\w+\-?\w*)'] = "news/actions/page/$1";
$route['grafics'] = "news/grafics";
$route['grafics/(\w+\-?\w*)'] = "news/grafics/page/$1";
$route['novelties'] = "news/novelties";
$route['novelties/(\w+\-?\w*)'] = "news/novelties/page/$1";
$route['articles/articles_search'] = "articles/articles_search";
$route['articles/(\w+)'] = "articles/view/$1";
$route['articles'] = "articles";
$route['vacancy'] = "vacancy";
$route['smi'] = "news/smi";
$route['admin_license'] = "admin_license";
$route['info'] = "info";
$route['awards'] = "gallery/awards";
$route['gallery'] = "gallery/index";
$route['info/kontroliruyushchie-organizatsii'] = "info/kontroliruyushchie_organizatsii";
$route['komplex_programm'] = "info/komplex_programm";
$route['komplex_programm/kids'] = "info/komplex_programm/kids";
$route['komplex_programm_compare'] = "info/komplex_programm_compare";
$route['komplex_programm/((\w+)(-?\w+)*)'] = "info/single_medical_programm/$1";
$route['info/podgotovka-k-issledovaniyam'] = "info/podgotovka_k_issledovaniyam";

$route['kids'] = "children";
$route['kids/doctors'] = 'children/doctors';
$route['kids/search_doctor_complicated'] = 'children/search_doctor_complicated';
$route['kids/doctor_text_search_speciality/(\w+\-?\w*)'] = 'children/doctor_text_search_speciality/$1';
$route['kids/(\w+\-?\w*)'] = "children/specialisation/$1";
$route['kids/(\w+\-?\w*)/((\w+)(-?\w+)*)'] = "doctors/single_doctors_page/$1/$2";

$route['doctors'] = 'doctors';
$route['doctors/doctor_text_search'] = 'doctors/doctor_text_search';
$route['doctors/filter_clinics'] = 'doctors/filter_clinics';
$route['doctors/doctor_text_search_4_main'] = 'doctors/doctor_text_search_4_main';
$route['doctors/search_doctor_complicated_on_top'] = 'doctors/search_doctor_complicated_on_top';
$route['doctors/doctor_text_search_speciality/(\w+\-?\w*)'] = 'doctors/doctor_text_search_speciality/$1';
$route['doctors/search_doctor_complicated'] = 'doctors/search_doctor_complicated';
$route['doctors/(\w+\-?\w*)/((\w+)(-?\w+)*)'] = "doctors/single_doctors_page/$1/$2";
$route['doctors/(\w+\-?\w*)'] = "doctors/specialisation/$1";
$route['sitemap'] = "sitemap/index";
$route['services'] = 'services/index';
$route['medosmortry'] = 'services/additional/tsentr_profilaktiki_zdorovya';
$route['dlya_vladeltsev_polisov_dms'] = 'services/additional/dlya_vladeltsev_polisov_dms';
$route['(:any)'] = 'services/page/$1';

$route['new_page/((\w+)(-?\w+)*)'] = 'services/new_page/$1';
$route['new_page/(:any)/(:any)'] = 'services/new_page/$1/$2';
$route['clinic/(\w+\-?\w*)'] = "clinic/page/$1";
$route['ginekolog/uslugi'] = "news/view_service_article/ginekolog_uslugi";
$route['info/social_projekt'] = "news/social_projects";
$route['info/nashi_partnery'] = "info/page/nashi_partnery";
$route['info/vyzov-na-dom'] = "info/page/vyzov-na-dom";
$route['info/priem_grazhdan'] = "info/page/priem_grazhdan";
$route['info/tsentr_profilaktiki_zdorovya'] = "services/additional/tsentr_profilaktiki_zdorovya";
$route['news/(\w+)/((\w+)(-?\w+)*)/((\w+)(-?\w+)*)'] = "news/filter/$1/$2/$3";
$route['admin_user'] = "admin_user/index";
$route['info/rektoromanoskopiya'] = "404";
$route['info/sigmoidoskopiya'] = "404";
$route['info/sigmoidoskopiya'] = "404";
$route['info/men/muzhskoe_besplodie'] = "404";
$route['clinic/volzhskij_hirurgija/tehnologii/hirurgicheskie_tehnologii/laparoskopiya'] = "404";
$route['info/yendoskop'] = "404";
$route['info/videokolonoskopiya'] = "404";
$route['info/yegfs'] = "404";
//$route['(:any)/(:any)'] = 'services/page/$1/$2';

$route['uzi/(:any)'] = 'services/page/uzi/$1';
$route['hirurg/(:any)'] = 'services/page/hirurg/$1';
$route['sosudistyj_hirurg/(:any)'] = 'services/page/sosudistyj_hirurg/$1';
$route['endoskopiya/(:any)'] = 'services/page/endoskopiya/$1';
$route['ginekolog/(:any)'] = 'services/page/ginekolog/$1';
$route['oftalmolog/(:any)'] = 'services/page/oftalmolog/$1';
$route['proktolog/(:any)'] = 'services/page/proktolog/$1';
$route['dermatolog/(:any)'] = 'services/page/dermatolog/$1';
$route['funktsionalnyj-dignost/(:any)'] = 'services/page/funktsionalnyj-dignost/$1';
$route['plasticheskie-hirurgi/(:any)'] = 'services/page/plasticheskie-hirurgi/$1';
$route['travmatolog/(:any)'] = 'services/page/travmatolog/$1';
$route['dermatolog/(:any)'] = 'services/page/dermatolog/$1';
$route['triholog/(:any)'] = 'services/page/triholog/$1';
$route['fizioterapevt/(:any)'] = 'services/page/fizioterapevt/$1';
$route['transfuziolog/(:any)'] = 'services/page/transfuziolog/$1';
$route['otolarinolaringologiya/(:any)'] = 'services/page/otolarinolaringologiya/$1';
$route['spravki/(:any)'] = 'services/page/spravki/$1';
$route['info/oms'] = "404";
