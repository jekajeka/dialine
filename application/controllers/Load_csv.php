<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Load_csv extends CI_Controller {
    function Load_csv()
    {
	parent::__construct();
	$this->load->database();
	$this->load->model('Enterprise_model', '', TRUE);
	$this->load->model('Enterprise_okved_model', '', TRUE);
	$this->load->model('Okved_model', '', TRUE);
    }
    
    
    //вывод главной страницы сайта
    public function index()
    {
	$data['enterprise_data'] = $this->Enterprise_model->get_simple_list();
	$data['regions_list'] = $this->get_regions_list();
	$data['count'] = count($data['enterprise_data']);
	$data['okved_list_html'] = $this->html_okved_tree();
	$data['enterprise_data'] = $this->load->view('selected_enterprise_view',$data,TRUE);
	$this->load->view('main_page_view',$data);
    }
    
    //Полная очистка базы данных
    public function drop_base()
    {
	$this->Enterprise_okved_model->clear();
	$this->Enterprise_model->clear();
    }
    
    //загрузка файла основных данных
    public function upload_file()
    {
	ini_set("memory_limit", "256M");
	ini_set('max_execution_time', 1000);
	$map_array = array();
	$handle = fopen("short.txt", "r");
	$i=0;
	while (!feof($handle)) {
		    //echo $i;
		    $buffer = fgets($handle);
		    if((is_numeric(substr($buffer,0,1)))||(substr($buffer,0,1)=='#'))
		    {
			$map_array[$i] = $buffer;
			$i++;
		    }
		    else
		    {
			$map_array[$i-1] .= $buffer;
		    }
		}
	fclose($handle);
	$map_lines = array();
	foreach($map_array as $key=>$map_line)
	{
	    $map_lines[$key] = explode ("	",$map_line);
	}	
	array_shift($map_lines);
	array_pop($map_lines);
	array_pop($map_lines);
	print_r($map_lines[3]);
	//$this->Enterprise_model->load_enterprise_list($map_lines);
    }
    
    public function upload_file_ajax()
    {
	ini_set('max_execution_time', 1000);
	
	$error = false;
	$files = array();
     
	$uploaddir = './uploads/'; // . - текущая папка где находится submit.php
     
	// Создадим папку если её нет
     
	if( ! is_dir( $uploaddir ) ) mkdir( $uploaddir, 0777 );
     
	// переместим файлы из временной директории в указанную
	foreach( $_FILES as $file ){
	    if( move_uploaded_file( $file['tmp_name'], $uploaddir . basename($file['name']) ) ){
		$files[] = realpath( $uploaddir . $file['name'] );
		$map_array = array();
		$handle = fopen($uploaddir . $file['name'], "r");
		while (!feof($handle)) {
		    $buffer = fgets($handle);
		    $map_array[] = $buffer;
		}
		fclose($handle);
		$map_lines = array();
		foreach($map_array as $key=>$map_line)
		{
		    $map_lines[$key] = explode ("	",$map_line);
		}	
		array_shift($map_lines);
		array_pop($map_lines);
		array_pop($map_lines);
		//print_r($map_lines);
		$this->Enterprise_model->load_enterprise_list($map_lines);
	    }
	    else{
		$error = true;
	    }
	}
     
	$data = $error ? array('error' => 'Ошибка загрузки файлов.') : array('files' => $files );
     
	echo json_encode( $data );
	
	/*$config['upload_path'] = './uploads/';
	$config['allowed_types'] = 'txt';
	$this->load->library('upload', $config);
	print_r($this->upload->data());
	if ( ! $this->upload->do_upload())
	{
	    $error = array('error' => $this->upload->display_errors());		
	    print_r($error);
	}	
	else
	{
	    $data = array('upload_data' => $this->upload->data());
	    print_r($data);
	}
	$data = $error ? array('error' => 'Ошибка загрузки файлов.') : array('files' => $files );
	echo json_encode( $data );*/
	/*$map_array = array();
	$handle = fopen("map.txt", "r");
	while (!feof($handle)) {
	    $buffer = fgets($handle);
	    $map_array[] = $buffer;
	}
	fclose($handle);
	$map_lines = array();
	foreach($map_array as $key=>$map_line)
	{
	    $map_lines[$key] = explode ("	",$map_line);
	}	
	array_shift($map_lines);
	//print_r($map_lines);
	$this->Enterprise_model->load_enterprise_list($map_lines);*/
    }
    
    
    public function upload_unformatted_file_ajax()
    {
	ini_set("memory_limit", "512M");
	
	ini_set('max_execution_time', 3000);
	$error = false;
	$files = array();
	$error_msg = '';
	$uploaddir = './uploads/'; // . - текущая папка где находится submit.php
     
	// Создадим папку если её нет
     
	if( ! is_dir( $uploaddir ) ) mkdir( $uploaddir, 0777 );
	

	//echo 'index before note.class: '.memory_get_usage()/1024/1024 . '<br />';
	// переместим файлы из временной директории в указанную
	foreach( $_FILES as $file ){
	    if( move_uploaded_file( $file['tmp_name'], $uploaddir . basename($file['name']) ) ){
		$files[] = realpath( $uploaddir . $file['name'] );
		$map_array = array();
		$handle = fopen($uploaddir . $file['name'], "r");
		$i=0;
		while (!feof($handle)) {
		    $buffer = fgets($handle);
		    if((is_numeric(substr($buffer,0,1)))||(substr($buffer,0,1)=='#'))
		    {
			$map_array[$i] = $buffer;
			$i++;
		    }
		    else
		    {
			$map_array[$i-1] .= $buffer;
		    }
		}
		fclose($handle);
		//echo 'index after note.class: '.memory_get_usage()/1024/1024 . '<br />';
		$map_lines = array();
		/*foreach($map_array as $key=>$map_line)
		{
		    $map_lines[$key] = explode ("	",$map_line);
		}*/
		do
		{
		    $map_current_val = explode ("	",array_shift($map_array));
		    foreach($map_current_val as $key=>$c_val)
		    {
			if(empty($c_val))
			    $map_current_val[$key] = (bool) 0;
			//if()
		    }
		    $map_lines[] = $map_current_val;
		}
		while(count($map_array)>0);
		//echo 'index after note.class: '.memory_get_usage()/1024/1024 . '<br />';
		unset($map_array);
		array_shift($map_lines);
		//echo 'index after note.class: '.memory_get_usage()/1024/1024 . '<br />';
		//array_pop($map_lines);
		//array_pop($map_lines);
		//print_r($map_lines[0]);
		/*foreach($map_lines as $key=>$value)
		{
		    echo $key.'<br/>';
		}*/
		$this->Enterprise_model->load_enterprise_list_unformatted_fast_method($map_lines);
	    }
	    else{
		$error = true;
		$error_msg = 'move_uploaded_file';
	    }
	}
	//echo 'index after note.class: '.memory_get_usage()/1024/1024 . '<br />';
	$data = $error ? array('error' => $error_msg) : array('files' => $files );
     
	echo json_encode( $data );
	
	/*$config['upload_path'] = './uploads/';
	$config['allowed_types'] = 'txt';
	$this->load->library('upload', $config);
	print_r($this->upload->data());
	if ( ! $this->upload->do_upload())
	{
	    $error = array('error' => $this->upload->display_errors());		
	    print_r($error);
	}	
	else
	{
	    $data = array('upload_data' => $this->upload->data());
	    print_r($data);
	}
	$data = $error ? array('error' => 'Ошибка загрузки файлов.') : array('files' => $files );
	echo json_encode( $data );*/
	/*$map_array = array();
	$handle = fopen("map.txt", "r");
	while (!feof($handle)) {
	    $buffer = fgets($handle);
	    $map_array[] = $buffer;
	}
	fclose($handle);
	$map_lines = array();
	foreach($map_array as $key=>$map_line)
	{
	    $map_lines[$key] = explode ("	",$map_line);
	}	
	array_shift($map_lines);
	//print_r($map_lines);
	$this->Enterprise_model->load_enterprise_list($map_lines);*/
    }
    
    //загрузка файла оквэд
    public function upload_okved()
    {
	$okved_file_ram = file_get_contents("okved.txt");
	$exploded_okved_file_ram = explode ("\r\n",$okved_file_ram);
	//Получение правильных строк окведа
	$real_lines_count = 0;
	$real_okved_lines = array();
	foreach($exploded_okved_file_ram as $key=>$exploded_okved_file_line)
	{
	    if(is_numeric(substr($exploded_okved_file_line,0,1)))
	    {
		if($key==0)
		    $real_okved_lines[$key] = $exploded_okved_file_line;
		else
		{
		    $real_lines_count++;
		    $real_okved_lines[$real_lines_count] = $exploded_okved_file_line;
		}
	    }
	    else
		$real_okved_lines[$real_lines_count] .= "\r\n".$exploded_okved_file_line;
	}
	//Отделение данных окведа от идентификаторов и создание указателей на родительские пункты
	$separated_okved = array();
	foreach($real_okved_lines as $key=>$real_okved_line)
	{
	    $separated_okved[$key]['okved'] = trim(substr($real_okved_line,0,strpos($real_okved_line, ' ')+1));
	    if(strrpos($separated_okved[$key]['okved'],'.'))
		$separated_okved[$key]['parent'] = trim(substr($separated_okved[$key]['okved'],0,strrpos($separated_okved[$key]['okved'],'.')));
	    else
		$separated_okved[$key]['parent'] = 0;
	    $separated_okved[$key]['text'] = trim(substr($real_okved_line,strpos($real_okved_line, ' ')+1));
	}
	//print_r($separated_okved);
	$this->Okved_model->clear();
	$this->Okved_model->load_okved_list_to_db($separated_okved);
    }
    
    //формирование дерева оквэдов
    public function html_okved_tree()
    {
	$sorted_by_parent_okved = $this->Okved_model->get_all_order_by('parent');	
	$html = '';
	foreach($sorted_by_parent_okved as $okved)
	{
	    if($okved->parent == '0')
	    {
		$html .= '<div class="okved_top_range">';
		$html .='
			<div class="okved_top_range_title">
                            <input type="checkbox" id="'.$okved->okved.'" /><label for="'.$okved->okved.'">'.$okved->okved.' '.$okved->text.'</label>
                        </div>';
		$html .='<div class="okved_data">';
		$html .= $this->recursion_okved_tree_html($sorted_by_parent_okved,$okved->okved);
		$html .='</div>';
		$html .='</div>';
	    }
	}
	return $html;
    }
    
    public function search_okved()
    {
	//если пустая строка то возвращаем традиционное дерево окведов
	if($this->input->post('search_okved')=='')
	    echo $this->html_okved_tree();
	else
	{
	    $sorted_by_parent_okved = $this->Okved_model->get_searched($this->input->post('search_okved'));
	    $html = '<div class="okved_top_range">';
	    $html .= '<div class="okved_data">';
	    foreach($sorted_by_parent_okved as $okved_line)
	    {
		$html .= '
		<div class="okved_top_range_title">
                    <input type="checkbox" id="'.$okved_line->okved.'" /><label for="'.$okved_line->okved.'">'.$okved_line->okved.' '.$okved_line->text.'</label>
                </div>';
	    }
	    $html .='</div>';
	    $html .='</div>';
	    echo $html;
	}
    }
    
    //рекурсивное построение дерева ОКВЭДов
    function recursion_okved_tree_html($all_okved = array(),$parent_okved = '0')
    {
	$html='';
	foreach($all_okved as $okved_line)
	{
	    if(!strcmp($okved_line->parent,$parent_okved))
	    {
		$html .= '
		<div class="okved_top_range_title">
                    <input type="checkbox" id="'.$okved_line->okved.'" /><label for="'.$okved_line->okved.'">'.$okved_line->okved.' '.$okved_line->text.'</label>
                </div>';
		$inner_data = $this->recursion_okved_tree_html($all_okved,$okved_line->okved);
		if(!empty($inner_data))
		{
		    $html .= '<div class="okved_data">';
		    $html .= $inner_data;
		    $html .= '</div>';
		}
	    }
	}
	return $html;
    }
    
    //Подгрузка и кэширование расположения организации
    function load_geodata($enterprise_id)
    {
	print_r($this->Enterprise_model->get_geodata($enterprise_id));
    }
    
    //фильтрация по размеру предприятия
    function select_by_size()
    {
	$data['enterprise_data'] = $this->Enterprise_model->get_by_size($_POST['size']);
	$respond = array(
			'view'=>$this->load->view('selected_enterprise_view',$data,TRUE),
			'count'=>count($data['enterprise_data'])
			);
	echo json_encode( $respond );
    }
    
    //Быстрая выборка по ОКВЭД
    function fast_select_by_okved()
    {
	$data['enterprise_data'] = $this->Enterprise_model->fast_get_by_okved($this->input->post('okved'));
	$respond = array(
			'view'=>$this->load->view('selected_enterprise_view',$data,TRUE),
			'count'=>count($data['enterprise_data'])
			);
	echo json_encode( $respond );
    }
    
    
    //Быстрая выборка по регионам
    function fast_select_by_region()
    {
	$data['enterprise_data'] = $this->Enterprise_model->fast_get_by_region($this->input->post('region'));
	$respond = array(
			'view'=>$this->load->view('selected_enterprise_view',$data,TRUE),
			'count'=>count($data['enterprise_data'])
			);
	echo json_encode( $respond );
    }
    
    //выборка всех данных
    function select_all()
    {
	$data['enterprise_data'] = $this->Enterprise_model->get_simple_list();
	$respond = array(
			'view'=>$this->load->view('selected_enterprise_view',$data,TRUE),
			'count'=>count($data['enterprise_data'])
			);
	echo json_encode( $respond );
    }
    
    //Выборка данных по оквэдам
    function select_by_okved()
    {
	$data['enterprise_data'] = $this->Enterprise_model->get_by_okved($_POST['okved_list'],$_POST['enterprise_size'],$_POST['enterprise_region'],$_POST['enterprise_level']);
	$respond = array(
			'view'=>$this->load->view('selected_enterprise_view',$data,TRUE),
			'count'=>count($data['enterprise_data'])
			);
	echo json_encode( $respond );
    }
    
    //Поиск организаций по названию
    function search_by_name_or_okved()
    {
	$data['enterprise_data'] = $this->Enterprise_model->search_by_name_or_okved($this->input->post('search_string'));
	$respond = array(
			'view'=>$this->load->view('selected_enterprise_view',$data,TRUE),
			'count'=>count($data['enterprise_data'])
			);
	echo json_encode( $respond );
    }
    
    //Выборка данных координат по организациям
    function get_selected_organisation_coords()
    {
	ini_set('max_execution_time', 2000);
	$respond = $this->Enterprise_model->get_selected_organisation_coords($this->input->post('organisation_list'));
	echo json_encode( $respond );
    }
    
    
    //формирование html списка регионов
    function get_regions_list()
    {
	$data['regions'] = $this->Enterprise_model->load_region_list();
	return $this->load->view('regions_list_view',$data,TRUE);
    }
}
