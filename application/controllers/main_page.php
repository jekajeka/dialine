<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_page extends CI_Controller  {
function __construct()
{
    parent::__construct();
    //$this->load->model('news_frontend_model', '', TRUE);
    $this->title = 'Диалайн';
}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
public function index()
{
    $this->data['title'] = 'Диалайн';
    //$this->data['news_objects'] = $this->news_frontend_model->get_all_news();
    $this->load->view('main_page_view',$this->data);
}


}
