<?php
class SeoService
{
    private $ciDb;
    private $defaults = [
        'title' => '',
        'description' => '',
        'keywords' => ''
    ];
    
    public function __construct(\Seo_model $model){
        $this->ciDb = $model;
    }
    
    public function read($entityTable, $entityId){
        $result = $this->getResultFor($entityTable, $entityId);
        $record = $this->updateDataWithDefaults($result);
        return $record;
    }
    
        private function getResultFor($entityTable, $entityId){
            try{
                $result = $this->ciDb->readSeoRecord($entityTable, $entityId);
            } catch (\Exception $ex) {
                $result = [];
            }
            return $result;
        }
    
        private function updateDataWithDefaults($result){
            $newData = isset($result[0]) ? $result[0] : [];
            $record = array_replace($this->defaults, array_intersect_key($newData, $this->defaults));
            return $record;
        }
    
    public function save($table, $id, array $data){
        $data = $this->truncateUnallowed($data);
        try{
            if($this->ciDb->recordExists($table, $id)){
                $this->ciDb->updateSeoRecord($table, $id, $data);
            }
            else {
                $data = $this->updateDataWithDefaults([$data]);
                $this->ciDb->createSeoRecord($table, $id, $data);
            }
        } catch (\Exception $ex) {

        }
    }
    
        private function truncateUnallowed($data){
            return array_intersect_key($data, $this->defaults);
        }
}
