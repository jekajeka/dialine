<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is the abstaction layer for access to
 * services, prices, etc.
 * Made to unify access method and eliminate duplication
 * throughout models (they are target clients of this class).
 */
class ServicesLibrary
{	
	private $ci;
	private $db;
	private $usa;
	private $bookabilitySample = 'Анализы не требующие записи';
	private $bookableOnMatch = false;
		
	public function __construct(){
		$this->ci =& get_instance();
		$this->ci->load->database('default');
		$this->ci->config->load('import', true);
		$this->db = $this->ci->db;
		$groupOrder = isset($this->ci->config->item('import')['hacks']['group-order']) ? $this->ci->config->item('import')['hacks']['group-order'] : [];
		$this->usa = new \Dialine\UnitedServiceAccess($this->db->conn_id, $groupOrder);
	}
	
	public function getServicesForClinic($clinicId) {
		$filter = $this->loadVolzhskiyFilter($clinicId);
		if ($filter) $this->usa->addCustomWhereCondition($filter);
		$this->usa->filterByClinic($clinicId)
				  ->hackBookabilityBy($this->bookabilitySample, $this->bookableOnMatch);
		return $this->usa->getServices();
	}
	
		private function loadVolzhskiyFilter($clinicId) {
			$importConfig = $this->ci->config->item('import');
			$filterAvailable = isset($importConfig['hacks']['clinic_services']['volzhskij_hirurgija']) &&
							   $importConfig['hacks']['clinic_services']['volzhskij_hirurgija']['clinic-id'] == $clinicId;
			if ($filterAvailable) {
				return $importConfig['hacks']['clinic_services']['volzhskij_hirurgija']['sql-filter'];
			} 
			return '';
		}
	
// 	public function getGroupNamesForServicesInClinic($clinicId) {
// 		$filter = $this->loadVolzhskiyFilter($clinicId);
// 		if ($filter) $filter = ' AND ('.$filter.') ';
// 		$query = 'SELECT services.service_type, services.rubric FROM services '
// 				.'INNER JOIN service_clinic ON service_clinic.service_id = services.med_id '
// 				.'INNER JOIN clinics ON service_clinic.clinic_id = clinics.id '
// 				.'WHERE service_clinic.clinic_id = '.$clinicId.' '.$filter
// 				.'GROUP BY services.service_type, services.rubric '
// 				.'ORDER BY services.id ASC';
// 		$data = $this->db->query($query);
// 		return $data->result();
// 	}
	
// 	public function getMedicalDirectionsForClinic($clinicId, $useHacks = false) {
// 		if ($useHacks) {
// 			$filter = $this->loadVolzhskiyFilter($clinicId);
// 			if ($filter) $filter = ' AND ('.$filter.') ';
// 		} else {
// 			$filter = '';
// 		}
// 		$query = 'SELECT count( * ) as services_qty, medical_directions.service_rubric_title, rubric FROM services '
// 				.'INNER JOIN service_clinic ON service_clinic.service_id = services.med_id '
// 				.'INNER JOIN clinics ON service_clinic.clinic_id = clinics.id '
// 				.'INNER JOIN medical_directions ON services.rubric = medical_directions.id '
// 				.'WHERE service_clinic.clinic_id = '.$clinicId.' '
// 				.$filter
// 				.'GROUP BY rubric '
// 				.'ORDER BY service_rubric_title ASC';
// 		$data = $this->db->query($query);
// 		return $data->result();
// 	}
	
// 	public function getMedicalDirectionsForDoctor($doctorId) {
// 		$query = 'SELECT count( * ) as services_qty , service_type FROM services '
// 				.'INNER JOIN service_doctor ON services.med_id = service_doctor.service_id '
// 				.'WHERE service_doctor.doctor_id = '.$doctorId. ' '
// 				.'GROUP BY service_type '
// 				.'ORDER BY services.id ASC';
// 		$data = $this->db->query($query);
// 		return $data->result();
// 	}
	
	public function getServicesForSpecialization($rubricId) {
		$data = $this->usa->filterBySpecialization($rubricId)
						  ->setGroupAsTopLevel()
						  ->hackBookabilityBy($this->bookabilitySample, $this->bookableOnMatch)
					      ->getServices();
		return $data;
	}
	
	public function getServicesForDoctor($doctorId) {
		$this->usa->filterByDoctor($doctorId);
		$this->usa->setGroupAsTopLevel()
				  ->hackBookabilityBy($this->bookabilitySample, $this->bookableOnMatch);
		$data = $this->usa->getServices();
		return $data;
	}
	
	public function getAllServices() {
		$data = $this->usa->hackBookabilityBy($this->bookabilitySample, $this->bookableOnMatch)
						  ->getServices();
		return $data;
	}
}
