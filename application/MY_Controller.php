<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

protected $type = '';/** Тип объектов с которым работает контроллер в данный момент */
protected $title = '';
protected $view_type = 1;
function __construct()
{
    parent::__construct();
    $this->get_type();
    $this->load->library('session');
    $this->load->helper('url');
    $this->load->helper('form');
    $this->load->database();
}

function _output($data)
{
    $this->data['content'] = $this->output->get_output();
    echo $this->load->view('template/main_page_view',$this->data,TRUE);
}

function autoload_model()
{
//$this->load->model($this->type.'_model','', TRUE);
}

/*!
@brief Определяет имя текущего контроллера и имя модели для контроллера
@details Антон Вопилов 19.10.2012
@param[in] 
@return Устанавливает имя таблицы, доступной текущему дочернему контроллеру напрямую
*/
private function get_type()
{
$class = get_class($this);
$controller_name = $class;
$this->type = strtolower($controller_name);
}

}

class Admin_Controller extends MY_Controller {

function __construct()
{
    parent::__construct();
    $this->autoload_model();
    $this->load->library('ion_auth');
    if ( !$this->ion_auth->logged_in() )
        redirect("/auth/login");
}
function _output($data)
{
if($this->view_type==1)
{
    $this->data['content'] = $this->output->get_output();
    //$this->data['title'] = $this->title;
    //$this->data['main_menu'] = $this->load->view('admin/main_menu_view','',TRUE);
    echo $this->load->view('template/main_page_view',$this->data,TRUE);
}
else echo $this->output->get_output();
}


}

class Frontend_Controller extends CI_Controller {
    protected $view_type = 1;
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('cookie');
        $this->load->model('main_page_model', '', TRUE);
        if(get_cookie('city'))
            $this->data['cookie_city'] = get_cookie('city');
        else
            $this->data['cookie_city'] = 'Волгоград';
        if(get_cookie('city_phone'))        
            $this->data['cookie_city_phone'] = get_cookie('city_phone');
        else
            $this->data['cookie_city_phone'] = '+7 (8442) <span class="callibri_phone">220 - 220</span>';
    }
    
    function _output($data)
    {
    if($this->view_type==1)
    {
        $this->data['content'] = $this->output->get_output();
        $this->data['doctors'] = $this->main_page_model->get_doctors();
        $this->data['rubrics'] = $this->main_page_model->get_rubrics();
        $this->data['clinics'] = $this->main_page_model->get_clinics();
        $this->data['services_top'] = $this->main_page_model->get_services();
        echo $this->load->view('main_page/main_page_view',$this->data,TRUE);
    }
    else echo $this->output->get_output();
    }
    
}
