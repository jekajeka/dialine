<?php
class MY_Model extends CI_Model {

protected $db_table = '';

function MY_Model()
{
    parent::__construct();
}

/*
 *������� ���� ������ �� ������� ������� ��� ����� � ������� ���������
 */
function get_all()
{
    $this->db->select('*');
    $this->db->from($this->db_table);
    $this->db->order_by("id", "DESC");
    return $this->db->get()->result();
}

function get_all_ordered($field = "id", $order = "ASC")
{
    $this->db->select('*');
    $this->db->from($this->db_table);
    $this->db->order_by($field, $order);
    return $this->db->get()->result();
}

function edit_node($id = NULL, $additional_data = array())
{
    $this->db->where('id', $id);
    return $this->db->update($this->db_table, $additional_data);
}

function delete_node($id = NULL)
{
    $this->db->where('id', $id);
    $this->db->delete($this->db_table);
}

/*
 *������� ����� ���� ��� ����� � ������� ���������
 */
function get_node($id = NULL)
{
    $this->db->select('*');
    $this->db->from($this->db_table);
    $this->db->where("id", $id);
    return $this->db->get()->result();
}

/*
 *���������� ����� ���� ��� ����� � ������� ���������
 *return ������������� ����������� ���� � ��
 */
function add_node($additional_data = array())
{
    $insert_result = array();
    $insert_result['success'] = $this->db->insert($this->db_table, $additional_data);
    $insert_result['insert_id'] = $this->db->insert_id();
    return $insert_result;
}

} ?>