<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Exceptions extends CI_Exceptions {

    public function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
    }
    
    public function show_404($page = '', $log_error = TRUE){
        header("HTTP/1.1 404 Not Found");
        $this->CI->load->model('main_page_model', '', TRUE);
        $this->data['title'] = "Простите, того, что Вы искали, здесь нет...";
        $this->data['content'] = $this->CI->load->view('main_page/error_404_view',$this->data,TRUE);
        $this->data['doctors'] = $this->CI->main_page_model->get_doctors();
        $this->data['rubrics'] = $this->CI->main_page_model->get_rubrics();
        //$this->data['clinics'] = $this->CI->main_page_model->get_clinics();
        $this->data['services_top'] = $this->CI->main_page_model->get_services();
        echo $this->CI->load->view('main_page/main_page_view',$this->data,TRUE);
        exit(4);
    }
}