<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

protected $type = '';/** Тип объектов с которым работает контроллер в данный момент */
protected $title = '';
protected $view_type = 1;
function __construct()
{
    parent::__construct();
    $this->get_type();
    $this->load->library('session');
    $this->load->helper('url');
    $this->load->helper('form');
    $this->load->database();
    $this->load->library('seo');
}

function _output($data)
{
    $this->data['content'] = $this->output->get_output();
    echo $this->load->view('template/main_page_view',$this->data,TRUE);
}

function autoload_model()
{
//$this->load->model($this->type.'_model','', TRUE);
}

/*!
@brief Определяет имя текущего контроллера и имя модели для контроллера
@details Антон Вопилов 19.10.2012
@param[in] 
@return Устанавливает имя таблицы, доступной текущему дочернему контроллеру напрямую
*/
private function get_type()
{
$class = get_class($this);
$controller_name = $class;
$this->type = strtolower($controller_name);
}

}

class Admin_Controller extends MY_Controller {
	protected $logs_data_type;//тип данных для записи в лог
	function __construct()
	{
		parent::__construct();
		$this->autoload_model();
		$this->load->library('ion_auth');
		$this->load->model('admin_logs/admin_logs_model', '', TRUE);
		$this->load->model('admin_doctors/admin_doctors_model', '', TRUE);
		$this->load->model('admin_subservices/admin_subservices_model', '', TRUE);
		if ( !$this->ion_auth->logged_in() )
		    redirect("/auth/login");
	}
	
	function _output($data)
	{
		if($this->view_type==1)
		{
			$this->data['content'] = $this->output->get_output();
			//$this->data['title'] = $this->title;
			//$this->data['main_menu'] = $this->load->view('admin/main_menu_view','',TRUE);
			$this->data['medical_directions_in_menu'] = $this->admin_doctors_model->get_all_medical_directions_list();
			$this->data['medical_subservice'] = $this->admin_subservices_model->get_all_ordered("title");
			$this->data['user'] = $this->ion_auth->get_current_user();
			echo $this->load->view('template/main_page_view',$this->data,TRUE);
		}
		else
		{
			echo $this->output->get_output();
		}
	}

	protected function getSeo($table, $id){
		$seo = $this->seo->read($table, $id);
		$this->data['seo'] = $seo;
	}
	
	/**
	 * To be used on pages with forms to save data with inputs as stated below.
	*/
	protected function saveSeo($table, $id){
		$seo = [
			'title' => $this->input->post('seo_title'),
			'description' => $this->input->post('seo_description'),
			'keywords' => $this->input->post('seo_keywords'),
		];
		$this->seo->save($table, $id, $seo);
	}

	protected function readSeoFromForm(){
		$seo = [
			'title' => $this->input->post('seo_title'),
			'description' => $this->input->post('seo_description'),
			'keywords' => $this->input->post('seo_keywords'),
		];
		return $seo;
	}
}

class Frontend_Controller extends CI_Controller {
    protected $view_type = 1;
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('cookie');
        $this->load->model('main_page_model', '', TRUE);
        if(get_cookie('city'))
            $this->data['cookie_city'] = get_cookie('city');
        else
            $this->data['cookie_city'] = 'Волгоград';
        if(get_cookie('city_phone'))        
            $this->data['cookie_city_phone'] = get_cookie('city_phone');
        else
            $this->data['cookie_city_phone'] = '+7 (8442) <span>220 - 220</span>';
            
	$this->data['description'] = '';
	$this->load->library('seo');
	//$this->output->enable_profiler(TRUE);
	//Проверка обращений к несуществующим контроллерам и вывод 404 если контроллер не найден
	if(empty($this->router->fetch_class()))
	    show_404('page');
    }
    
    function _output($data)
    {
	if($this->view_type==1)
	{
	    $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
	    $blind_see = '';
	    if((get_cookie('blind_see'))&&(get_cookie('blind_see') != 0))
		$blind_see = '.blind_see';
	    else
		$blind_see = '';
	    //$cache_page_name = str_replace('/','_',$_SERVER['REQUEST_URI']).$blind_see;
	    //if ( ! $cache_page_output = $this->cache->get($cache_page_name))
	    {
		$this->data['content'] = $this->output->get_output();
		$this->data['doctors'] = $this->main_page_model->get_doctors();
		$this->data['rubrics'] = $this->main_page_model->get_rubrics();
	    	//$this->data['clinics'] = $this->main_page_model->get_clinics();
		$this->data['services_top'] = $this->main_page_model->get_services();
		$cache_page_output = $this->load->view('main_page/main_page_view',$this->data,TRUE);
		//$this->cache->save($cache_page_name, $cache_page_output, 3600);
	    }        
	    echo $cache_page_output;
	}
	else echo $this->output->get_output();
    }
    
    protected function getSeo($table, $id){
		$seo = $this->seo->read($table, $id);
		if(!empty($seo['title'])){
			$this->data['title'] = $seo['title'];
		}
		$this->data['description'] = $seo['description'];
		$this->data['keywords'] = $seo['keywords'];
	}
    
}
