<div class="item form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_title">Meta -title-</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input id="meta_title" class="form-control col-md-7 col-xs-12" value="<?=$seo['title']?>"  name="seo_title" placeholder="Тег title" type="text">
	</div>
</div>
<div class="item form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_description">Meta -description-</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input id="meta_description" class="form-control col-md-7 col-xs-12" value="<?=$seo['description']?>"  name="seo_description" placeholder="Мета-тег description" type="text">
	</div>
</div>
<div class="item form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="keywords">Meta -keywords-</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<textarea name="seo_keywords" placeholder="Мета-тег keywords" rows="3" style="width: 100%; height: 90px;resize: none" id="keywords"><?=$seo['keywords']?></textarea>
	</div>
</div>
