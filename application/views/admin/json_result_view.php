<?if($this->data['result']==1){?>
<div class="alert alert-success alert-dismissible fade in" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
    Запись успешно сохранена!
</div>
<?}
else{?>
<div role="alert" class="alert alert-danger alert-dismissible fade in">
    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>
    <?=$this->data['result']?>
</div>
<?}?>