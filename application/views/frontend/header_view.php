<meta name="description" content="<?=(isset($description)) ? $description : ''?>">
<meta name="keywords" content="<?=(isset($keywords)) ? $keywords : ''?>">
<meta name="cmsmagazine" content="959532022e908aa92ad16c2a7661ebdb" />
<meta name="yandex-verification" content="ba66a1ff41ae3b82" />
<link rel="icon" href="/favicon.ico">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="/frontend/js/device.min.js"></script>
<script>
    if (device.landscape()) {
        document.write('<meta name="viewport" content="width=1200" >')
    }
    else document.write('<meta name="viewport" content="width=640" >')
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<?php

$l = mb_strlen($_SERVER["REQUEST_URI"], 'utf8');
$s = str_replace('/','', $_SERVER["REQUEST_URI"]);
$e = mb_strlen($s, 'utf8');

if($l-$e >= 3){ ?>

    <link rel="canonical" href="http://<?=$_SERVER["SERVER_NAME"]?><?= str_replace('/kids/','/doctors/', $_SERVER["REQUEST_URI"]) ?>" >

<?php } ?>