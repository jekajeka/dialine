<div class="row super_slider">
        <div class="swiper-wrapper">
            <div class="swiper-slide" style="background: url(/frontend/images/banner/chekup.jpg) no-repeat center;background-size: cover">
                <div class="container">
                    <div class="swiper_table">
                        <div class="swiper_table_child">
                            <div class="green_field">
                                <div class="title">КОМПЛЕКСНЫЕ ПРОГРАММЫ ОБСЛЕДОВАНИЯ "ЧЕК-АП" С ВЫГОДОЙ ДО 25%</div>
                                <p>
                                    Чтобы организм функционировал исправно и для своевременного обнаружения и устранения проблем.
                                </p>
                                <a href="/news/14289.new">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide" style="background: url(/frontend/images/banner/hg.jpg) no-repeat center;background-size: cover">
                <div class="container">
                    <div class="swiper_table">
                        <div class="swiper_table_child">
                            <div class="green_field">
                                <div class="title">ВЫЕЗДНАЯ СЛУЖБА ДИАЛАЙН "ВРАЧ НА ДОМ"</div>
                                <p>
                                    Выезд в любые районы Волгограда и Волжского. Возможен выезд за пределы города.
                                </p>
                                <a href="/inhome">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide" style="background: url(/frontend/images/banner/pk.jpg) no-repeat center;background-size: cover">
                <div class="container">
                    <div class="swiper_table">
                        <div class="swiper_table_child">
                            <div class="green_field">
                                <div class="title">ПЕРЕДВИЖНАЯ КЛИНИКА ДИАЛАЙН</div>
                                <p>
                                    Проведения мероприятий по предупреждению и своевременному выявлению различных заболеваний. Данный центр работает как с физическими, так и с юридическими лицами.
                                </p>
                                <a href="/services/additional/uslugi_predpriyatiyam">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="switches"></div>
    </div>
    <div class="row row_advantages">
        <div class="container">
            <div class="advantage_wrap" style="background: url(/frontend/images/specialists.png) no-repeat 190px 170px">
                <div class="advatage_icon" style="background: url(/frontend/images/specialists_min.png) no-repeat bottom left"></div>
                <div class="advantage_title">Опытные<br/>специалисты</div>
                <div class="advantage_overlay">
                    <div class="advantage_overlay_title">Опытные специалисты</div>
                    <div class="advantage_overlay_text">
                        <p>
                            Мы  тщательно подходим к подбору персонала. Наши врачи регулярно проходят обучение, сдают аттестацию, повышают  квалификацию.
                        </p>
                    </div>
                </div>
            </div>
            <div class="advantage_wrap" style="background: url(/frontend/images/adv/comfort.png) no-repeat 140px 160px ">
                <div class="advatage_icon" style="background: url(/frontend/images/adv/comfort_g.png) no-repeat bottom left;width: 160px"></div>
                <div class="advantage_title">Комфортные условия</div>
                <div class="advantage_overlay">
                    <div class="advantage_overlay_title">Комфортные условия</div>
                    <div class="advantage_overlay_text">
                        <p>
                            Работаем без выходных, собственный колл-центр, территориальная доступность, обслуживание на дому.
                        </p>
                    </div>
                </div>
            </div>
            <div class="advantage_wrap" style="background: url(/frontend/images/profil.png) no-repeat 180px 170px">
                <div class="advatage_icon" style="background: url(/frontend/images/profil_min.png) no-repeat bottom left"></div>
                <div class="advantage_title">Многопрофильность</div>
                <div class="advantage_overlay">
                    <div class="advantage_overlay_title">Многопрофильность</div>
                    <div class="advantage_overlay_text">
                        <p>
                            Максимальное количество услуг в рамках одной клиники для экономии времени пациента.
                        </p>
                    </div>
                </div>
            </div>
            <div class="advantage_wrap" style="background: url(/frontend/images/sovr.png) no-repeat 160px 170px">
                <div class="advatage_icon" style="background: url(/frontend/images/sovr_min.png) no-repeat bottom left"></div>
                <div class="advantage_title">Современное оборудование</div>
                <div class="advantage_overlay">
                    <div class="advantage_overlay_title">Современное оборудование</div>
                    <div class="advantage_overlay_text">
                        <p>
                            Минихолтеры, высокоточные аппараты УЗИ, ультратонкое видеоэндоскопическое оборудование, рентген и многое другое, что позволяет выявить заболевание на ранней стадии.
                        </p>
                    </div>
                </div>
            </div>
            <div class="other_adv_wrap" style="display: none">
                <div class="advantage_wrap" style="background: url(/frontend/images/adv/hirurg.png) no-repeat 190px 160px">
                        <div class="advatage_icon" style="background: url(/frontend/images/adv/hirurg_g.png) no-repeat bottom left"></div>
                        <div class="advantage_title">Собственная хирургия и лаборатория </div>
                        <div class="advantage_overlay">
                            <div class="advantage_overlay_title">Собственная хирургия и лаборатория </div>
                            <div class="advantage_overlay_text">
                                <p>
                                    позволяют контролировать полный цикл диагностики и лечения.
                                </p>
                            </div>
                        </div>
                </div>
                <div class="advantage_wrap" style="background: url(/frontend/images/adv/info.png) no-repeat 150px 160px ">
                        <div class="advatage_icon" style="background: url(/frontend/images/adv/info_g.png) no-repeat bottom left"></div>
                        <div class="advantage_title">Единая инфосистема</div>
                        <div class="advantage_overlay">
                            <div class="advantage_overlay_title">Единая инфосистема</div>
                            <div class="advantage_overlay_text">
                                <p>
                                    позволяет быстро найти  электронную карту пациента, а врачу оперативно проследить историю болезней и назначений из любой клиники Диалайн. Вам не придется расшифровывать почерк врача – все назначения в печатном виде.
                                </p>
                            </div>
                        </div>
                </div>
                <div class="advantage_wrap" style="background: url(/frontend/images/adv/stabil.png) no-repeat 180px 155px">
                        <div class="advatage_icon" style="background: url(/frontend/images/adv/stabil_g.png) no-repeat bottom left"></div>
                        <div class="advantage_title">Стабильность</div>
                        <div class="advantage_overlay">
                            <div class="advantage_overlay_title">Стабильность</div>
                            <div class="advantage_overlay_text">
                                <p>
                                     Диалайн существует с 2001 г., трудоустраивает более 700 человек, ежегодно открывает новые клиники.
                                </p>
                            </div>
                        </div>
                </div>
                <div class="advantage_wrap" style="background: url(/frontend/images/adv/gibkost.png) no-repeat 160px 155px">
                        <div class="advatage_icon" style="background: url(/frontend/images/adv/gibkost_g.png) no-repeat bottom left"></div>
                        <div class="advantage_title">Гибкие условия</div>
                        <div class="advantage_overlay">
                            <div class="advantage_overlay_title">Гибкие условия</div>
                            <div class="advantage_overlay_text">
                                <p>
                                    работаем по программам ОМС и ДМС, предлагаем систему скидок для постоянных клиентов, работаем с корпоративными клиентами на индивидуальных условиях по профосмотрам, вакцинации, выезжаем на территорию предприятия.
                                </p>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <div class="container more_advantages">
            <a class="more_advantages_btn">Ещё</a>
        </div>
    </div>
    <div class="row row_title" id="find_clinic_block">
        <div class="container">Найти ближайшую к Вам клинику</div>
    </div>
    <div class="row">
        <div class="container">
            <div class="map_operate_wrap">
                <div class="search_input_wrap">
                    <input type="text" placeholder="Введите свой адрес" />
                </div>
                <div class="addresses_wrap">
                <?foreach($clinics as $key=>$clinic){?>
                    <div class="single_address_wrap" data-trg="<?=$key?>">
                        <div class="single_addr_title"><?=$clinic->title?> <?=$clinic->district?></div>
                        <div class="single_addr_data_wrap">
                                <?
                                $w_times = explode ("\n",$clinic->work_time);
                                ?>
                        <div class="time_data_wrap">
                            <div class="phone_data"><a href="tel:<?=$clinic->phones?>"><?=$clinic->phones?></a></div>
                        </div>
                            <div class="time_data_wrap">
                                <?foreach($w_times as $w_tm){?>
                                <div class="time_data"><?=$w_tm?></div>
                                <?}?>
                            </div>
                        </div>
                    </div>
                <?}?>
                </div>
            </div>
            <div class="" id="map"></div>
        </div>
    </div>
    <div class="row plancs">
        <div class="container">
            <div class="planc_wrap">
                <div class="plank_text_wrap">
                    <a class="plank_title">Запись на прием</a>
                    <div class="plank_text">
                        Заполните форму обратной связи, и наши операторы свяжутся с Вами
                    </div>
                    <a class="plank_link plank_link_order open_overlay_form_wrapper"  return false" href="">Записаться</a>
                    <div class="plank_overlay"></div>
                </div>
                <div class="plank_img_wrap" style="background: url(/frontend/images/plank_img_wrap.jpg) no-repeat center;background-size: cover"></div>
            </div>
            <div class="planc_wrap">
                <div class="plank_text_wrap">
                    <a class="plank_title">Оплата услуг</a>
                    <div class="plank_text">
                        Здесь Вы можете оплатить уже оказанные клиниками ДИАЛАЙН услуги, по которым имеется задолженность
                    </div>
                    <a class="plank_link plank_link_pay" href="/dolg">Перейти к оплате</a>
                    <div class="plank_overlay"></div>
                </div>
                <div class="plank_img_wrap" style="background: url(/frontend/images/plank_img_wrap2.jpg) no-repeat center;background-size: cover"></div>
            </div>
        </div>
    </div>
    <div class="row row_video">
        <div class="container">
            <div class="block6">
                <div class="video_text_wrap">
                    <div class="video_title">Открытие новой клиники ДИАЛАЙН в Дзержинском районе</div>
                    <div class="video_text">
                        Компания Диалайн существует на волгоградском рынке с 2001 года. За это время открыто семь многопрофильных клиник Диалайн, которые объединяет единый четкий стиль работы. Он основан на принципах доказательной медицины, преемственности амбулаторного и стационарного звеньев медицинской помощи, оптимальном сочетании классических и современных технологий, в том числе малоинвазивных хирургических и пр.
                    </div>
                </div>
            </div>
            <div class="block6">
                <div class="video_vrap">
                    <iframe width="100%" height="290" src="https://www.youtube.com/embed/r5gF2IZook0" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div class="container all_video_link">
            <a target="_blank" href="https://www.youtube.com/channel/UCARtFzDwn-05UVzFnOUR4Xg">Смотреть больше видео о клинике Диалайн</a>
        </div>
    </div>
    <div class="row row_reviews">
        <div class="container">
            <div class="leave_review_wrap">
                <div class="leave_review_title">Написать главному врачу</div>
                <div class="review_form">
                    <input type="text" id="review_name" placeholder="Ваше имя" />
                    <input type="text" id="review_phone" placeholder="Телефон" />
                    <select style="width: 263px" id="review_clinic">
                        <option value="all">Выберите клинику</option>
                        <?foreach($clinics as $clinic){?>
                        <option value="<?=$clinic->id?>"><?=$clinic->title?></option>
                        <?}?>
                    </select>
                    <textarea id="review_text" placeholder="Ваш отзыв"></textarea>
                    <a class="big_green_btn">Отправить</a>
                </div>
            </div>
            <div class="reviews_title">Последние отзывы</div>
            <div class="reviews_container">
                <div class="swiper-wrapper">
                        <?foreach($reviews as $review){?>
                        <div class="swiper-slide">
                            <div class="single_review_wrap">
                                <div class="single_review_title_wrap">
                                    <?=$review->name?>
                                    <span><?=date("d.m.Y", strtotime($review->date_review))?></span>
                                </div>
                                <div class="addr_review"><?=$review->title?><br/> <?=$review->district?></div>
                                <div class="text_review">
                                    <p>
                                        <?=substr(strip_tags($review->text),0,320).'...'?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <?}?>
                </div>
                <div class="reviews_control">
                    <a class="reviews_left"></a>
                    <a class="reviews_right"></a>
                </div>
                <a class="reviews_all_link" href="/reviews">Все отзывы</a>
            </div>
        </div>
    </div>
    <div class="row row_title">
        <div class="container">Последние новости</div>
    </div>
    <div class="row row_news">
        <div class="container">
                <?foreach($news_objects as $news){?>
            <div class="news_item_wrap">
                <a class="news_img_link" title="<?=$news->title?>" href="/news/<?=$news->path?>.new"><img alt="<?=$news->title?>" src="<?=$news->image_path?>" /></a>
                <div class="news_date"><?=$news->news_date_pub?></div>
                <h2><?=$news->title?></h2>
                <p>
                    <?=$news->preview_text?>
                </p>
                <a class="news_full_link" title="<?=$news->title?>" href="/news/<?=$news->path?>.new">Подробнее</a>
            </div>
            <?}?>            
        </div>
        <div class="container wrap_big_link">
            <a href="/news" class="big_green_btn">Все новости</a>
        </div>
    </div>
    <script>
    function init () {
        myMap = new ymaps.Map ("map", {
                                    center: [48.758625,44.818625],
                                    zoom: 16
                                });
        var myPlacemark = new Array;
        <?foreach($clinics as $key=>$clinic) {?>
        myPlacemark[<?=$key?>] = new ymaps.Placemark([<?=$clinic->coordinates?>], {
            balloonContent: '<?=$clinic->title?><br/><a href="/clinic/<?=$clinic->path?>">Подробнее...</a>',
            iconCaption: '<?=$clinic->title?>'
        }, {
            preset: 'islands#hospitalIcon',
	    iconColor: '#00abb2'
        });
        <?}?>
        <?foreach($clinics as $key=>$clinic) {?>
        myMap.geoObjects.add(myPlacemark[<?=$key?>]);
        <?}?>
$('.single_address_wrap').click(function(e){
                                        e.preventDefault();
                                        var opts = Object;
                                        opts.duration = 2000;
                                        myMap.panTo(myPlacemark[$(this).attr('trg')].geometry.getBounds(),opts);
                                    });
    
myMap.behaviors.disable('scrollZoom');


    
}

$(document).ready(function(){
    ymaps.ready(init);
    
    
});
</script>