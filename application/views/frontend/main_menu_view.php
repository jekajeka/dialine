<header>
    <div class="row top_green_line"></div>
    <div class="row top_data_row">
        <div class="container">
            <div class="portrait_container">
                <a class="top_logo" href="/" title="Диалайн - многопрофильные клиники в Волгограде, Волжском"></a>
                <div class="slogan">Если нужен хороший врач</div>
            </div>
            <div class="portrait_container">
                <div class="phone_header_data">
                    <div class="phone_header_data_text">Информационный центр клиники:</div>
                    <a class="callibri_phone"
                       href="tel:+<?= strip_tags(preg_replace('~[^0-9]+~', '', $cookie_city_phone)) ?>"><?= $cookie_city_phone ?></a>

                </div>
                <div class="geotarget_wrap">
                    <a class="header_visit_order">Записаться на прием</a>
                    <a href="/#find_clinic_block" class="geotargeting">Подобрать клинику</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row main_menu">
        <div class="container">
            <ul class="main_menu_ul">
                <li>
                    <a>О компании</a>
                    <ul>
                        <li><a href="/info">О компании</a></li>
                        <li><a href="/info/patsientu">Справочная</a></li>
                        <li><a href="/info/pravovaya_informaciya">Правовая информация</a></li>
                        <li><a href="/gallery">Фотогалерея</a></li>
                        <li><a href="/smi">СМИ о нас</a></li>
                        <li><a href="/awards">Награды</a></li>
                        <li><a href="/info/social_projekt">Социальные проекты</a></li>
                        <li><a href="/info/nashi_partnery">Партнеры</a></li>
                        <li><a href="/reviews">Отзывы</a></li>
                        <li><a href="/vacancy">Вакансии</a></li>
                        <li><a href="/info/page/sitemap">Карта сайта</a></li>
                    </ul>
                </li>
                <li><a id="services_menu" href="/services">Услуги</a></li>
                <li><a href="/doctors" id="vrachi_menu">Врачи</a></li>
                <li><a href="/kids" id="vrachi_menu">Для детей</a><span class="child_icon_butterfly"></span></li>
                <li><a href="/actions">Акции</a></li>
<!--                <li><a target="_blank" href="/uploads/files/%D0%9F%D1%80%D0%B0%D0%B9%D1%81_%D0%BE%D1%82_24_05_2018.pdf">Стоимость</a></li>-->
                <li><a target="_blank" rel="nofollow" href="https://lk.dialine.org/login">Личный кабинет</a></li>
            </ul>
            <div class="main_menu_selectors_wrap">
                <a title="Версия для слабовидящих" class="numb_see"></a>
                <form class="search-form" action="/search" method="POST">
                    <input name="search_string" placeholder="Поиск..." type="text"/>
                </form>
                <div class="your_city_wrap">
                    <span>Ваш город: </span>
                    <a href=""><?= $cookie_city ?></a>
                    <div class="selector_sity">
                        <a data-city="Волгоград" data-phone="+7 (8442) <span class='callibri_phone'>220 - 220</span>">Волгоград</a>
                        <a data-city="Волжский" data-phone="+7 (8443) <span>450 - 450</span>">Волжский</a>
                        <a data-city="Михайловка" data-phone="+7 (961) <span>68 - 68 - 222</span>">Михайловка</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>