<div class="overlay_form_wrapper" id="send_simple_wrap">
        <div class="overlay_form">
            <a class="close_overlay_form"></a>
            <div class="overlay_form_inner">
                <div class="overlay_form_title">Записаться на прием</div>
                <input type="hidden" id="overlay_form_select" value="Заказать звонок" />
                <input type="text" id="overlay_form_name" placeholder="Имя" />
                <input type="text" id="overlay_form_phone" placeholder="Телефон" />
                <div class="wrap_agree_checkbox">
                        <input type="checkbox"  id="overlay_form_agree"><label>Оставляя свои персональные данные, Вы даете добровольное согласие на их обработку. <a class="open-popup-link" target="_blank" href="#test-popup">Подробнее</a></label>
                </div>
                <a class="send_overlay_form" id="send_overlay_form_simple">Отправить</a>
                <p>*С Вами свяжутся в ближайшее время и запишут на удобное время к выбранному специалисту</p>
            </div>
        </div>
</div>

<!-- Форма обратной связи из шапки сайта -->
<div class="overlay_form_wrapper" id="send_header_visit_wrap">
        <div class="overlay_form">
            <a class="close_overlay_form"></a>
            <div class="overlay_form_inner">
                <div class="overlay_form_title">Записаться на прием</div>
                <input type="hidden" id="overlay_form_select_header_visit" value="Заказать звонок из шапки" />
                <input type="text" id="overlay_form_name_header_visit" placeholder="Имя" />
                <input type="text" id="overlay_form_phone_header_visit" placeholder="Телефон" />
                <div class="wrap_agree_checkbox">
                        <input type="checkbox"  id="overlay_form_agree_header_visit"><label>Оставляя свои персональные данные, Вы даете добровольное согласие на их обработку. <a class="open-popup-link" target="_blank" href="#test-popup">Подробнее</a></label>
                </div>
                <a class="send_overlay_form" id="send_overlay_form_simple_header_visit">Отправить</a>
                <p>*С Вами свяжутся в ближайшее время и запишут на удобное время к выбранному специалисту</p>
            </div>
        </div>
</div>

<!-- Форма виджета обратной связи -->
<div class="overlay_form_wrapper" id="widget_wrap">
        <div class="overlay_form">
            <a class="close_overlay_form"></a>
            <div class="overlay_form_inner">
                <div class="overlay_form_title">Записаться на прием</div>
                <input type="hidden" id="widget_purpose" value="Заказать звонок с виджета" />
                <input type="text" id="widget_name" placeholder="Имя" />
                <input type="text" id="widget_phone" placeholder="Телефон" />
                <div class="wrap_agree_checkbox">
                        <input type="checkbox"  id="widget_agree"><label>Оставляя свои персональные данные, Вы даете добровольное согласие на их обработку. <a class="open-popup-link" target="_blank" href="#test-popup">Подробнее</a></label>
                </div>
                <a class="send_overlay_form" id="send_widget">Отправить</a>
                <p>*С Вами свяжутся в ближайшее время и запишут на удобное время к выбранному специалисту</p>
            </div>
        </div>
</div>

<!-- Форма записи к врачу на странице врача -->
<div class="overlay_form_wrapper" id="send_doctor_wrap">
        <div class="overlay_form">
            <a class="close_overlay_form"></a>
            <div class="overlay_form_inner">
                <div class="overlay_form_title">Записаться на прием</div>
                <input type="hidden" id="send_doctor_purpose" value="Запись из карточки врача под фото" />
                <input type="text" id="send_doctor_name" placeholder="Имя" />
                <input type="text" id="send_doctor_phone" placeholder="Телефон" />
                <div class="wrap_agree_checkbox">
                        <input type="checkbox"  id="send_doctor_agree"><label>Оставляя свои персональные данные, Вы даете добровольное согласие на их обработку. <a class="open-popup-link" target="_blank" href="#test-popup">Подробнее</a></label>
                </div>
                <a class="send_overlay_form" id="send_doctor_button">Отправить</a>
                <p>*С Вами свяжутся в ближайшее время и запишут на удобное время к выбранному специалисту</p>
            </div>
        </div>
</div>

<!-- Форма записи к врачу на странице детского врача -->
<div class="overlay_form_wrapper" id="send_doctor_wrap_kids">
    <div class="overlay_form">
        <a class="close_overlay_form"></a>
        <div class="overlay_form_inner">
            <div class="overlay_form_title">Записаться на прием</div>
            <input type="hidden" id="send_doctor_purpose" value="Запись из карточки врача под фото" />
            <input type="text" id="send_doctor_name" placeholder="Имя" />
            <input type="text" id="send_doctor_phone" placeholder="Телефон" />
            <div class="wrap_agree_checkbox">
                <input type="checkbox"  id="send_doctor_agree"><label>Оставляя свои персональные данные, Вы даете добровольное согласие на их обработку. <a class="open-popup-link" target="_blank" href="#test-popup">Подробнее</a></label>
            </div>
            <a class="send_overlay_form" id="send_doctor_button_kids">Отправить</a>
            <p>*С Вами свяжутся в ближайшее время и запишут на удобное время к выбранному специалисту</p>
        </div>
    </div>
</div>

<div class="overlay_form_wrapper" id="send_order_wrap">
        <div class="overlay_form">
            <a class="close_overlay_form"></a>
            <div class="overlay_form_inner">
                <div class="overlay_form_title">Записаться на прием</div>
                <input type="hidden" id="overlay_form_select_order" value="Заказать услуги из прайса" />
                <input type="text" id="overlay_form_name_order" placeholder="Имя" />
                <input type="text" id="overlay_form_phone_order" placeholder="Телефон" />
                <div class="wrap_agree_checkbox">
                        <input type="checkbox"   id="overlay_form_agree_order"><label>Оставляя свои персональные данные, Вы даете добровольное согласие на их обработку. <a target="_blank" href="/uploads/files/Politika_obrabotki_i_zashiti_personalnih_dannih1.pdf">Подробнее</a></label>
                </div>
                <a class="send_overlay_form" id="send_overlay_form_order">Отправить</a>
                <p>*С Вами свяжутся в ближайшее время и запишут на удобное время к выбранному специалисту</p>
            </div>
        </div>
    </div>

<div class="overlay_form_wrapper" id="send_med_prog_wrap">
        <div class="overlay_form">
            <a class="close_overlay_form"></a>
            <div class="overlay_form_inner">
                <div class="overlay_form_title">Заказать программу</div>
                <input type="hidden" id="overlay_form_select_prog" value="Заказать программу" />
                <input type="text" id="overlay_form_name_medprog" placeholder="Имя" />
                <input type="text" id="overlay_form_phone_medprog" placeholder="Телефон" />
                <div class="wrap_agree_checkbox">
                        <input type="checkbox"   id="overlay_form_agree_medprog"><label>Оставляя свои персональные данные, Вы даете добровольное согласие на их обработку. <a target="_blank" href="/uploads/files/Politika_obrabotki_i_zashiti_personalnih_dannih1.pdf">Подробнее</a></label>
                </div>
                <a class="send_overlay_form" id="send_med_prog">Отправить</a>
                <p>*С Вами свяжутся в ближайшее время и запишут на удобное время к выбранному специалисту</p>
            </div>
        </div>
</div>
<div class="overlay_form_wrapper" id="send_hirurg_form">
        <div class="overlay_form">
            <a class="close_overlay_form"></a>
            <div class="overlay_form_inner">
                <div class="overlay_form_title">Записаться на прием</div>
                <input type="hidden" id="overlay_form_select_hirurg" value="Записаться" />
                <input type="text" id="overlay_form_name_hirurg" placeholder="Имя" />
                <input type="text" id="overlay_form_phone_hirurg" placeholder="Телефон" />
                <div class="wrap_agree_checkbox">
                        <input type="checkbox"   id="overlay_form_agree_hirurg"><label>Оставляя свои персональные данные, Вы даете добровольное согласие на их обработку. <a target="_blank" href="/uploads/files/Politika_obrabotki_i_zashiti_personalnih_dannih1.pdf">Подробнее</a></label>
                </div>
                <a class="send_overlay_form" id="send_hirurg_prog">Отправить</a>
                <p>*С Вами свяжутся в ближайшее время и запишут на удобное время к выбранному специалисту</p>
            </div>
        </div>
</div>
<div class="fixed_desktop_callback">
        <a href="https://lk.dialine.org/" target="_blank" class="fixed_desktop_callback_lk">Записаться On-Line</a>
        <a href="" target="_blank" class="fixed_desktop_callback_site">Оставить заявку</a>
</div>

<!-- Форма заказа услуг со страницы услуг или с вставляемой кнопки -->
<div class="overlay_form_wrapper" id="order_service_form">
        <div class="overlay_form">
            <a class="close_overlay_form"></a>
            <div class="overlay_form_inner">
                <div class="overlay_form_title">Записаться на прием</div>
                <input type="hidden" id="order_service_purpose" value="Заявка из услуг" />
                <input type="text" id="order_service_name" placeholder="Имя" />
                <input type="text" id="order_service_phone" placeholder="Телефон" />
                <div class="wrap_agree_checkbox">
                        <input type="checkbox"  id="order_service_agree"><label>Оставляя свои персональные данные, Вы даете добровольное согласие на их обработку. <a class="open-popup-link" target="_blank" href="#test-popup">Подробнее</a></label>
                </div>
                <a class="send_overlay_form" id="send_order_service_form">Отправить</a>
                <p>*С Вами свяжутся в ближайшее время и запишут на удобное время к выбранному специалисту</p>
            </div>
        </div>
</div>

<!-- Форма заказа услуг со страницы услуг для детей -->
<div class="overlay_form_wrapper" id="order_service_form_kids">
    <div class="overlay_form">
        <a class="close_overlay_form"></a>
        <div class="overlay_form_inner">
            <div class="overlay_form_title">Записаться на прием</div>
            <input type="hidden" id="order_service_purpose" value="Заявка из услуг" />
            <input type="text" id="order_service_name" placeholder="Имя" />
            <input type="text" id="order_service_phone" placeholder="Телефон" />
            <div class="wrap_agree_checkbox">
                <input type="checkbox" id="order_service_agree"><label>Оставляя свои персональные данные, Вы даете добровольное согласие на их обработку. <a class="open-popup-link" target="_blank" href="#test-popup">Подробнее</a></label>
            </div>
            <a class="send_overlay_form" id="send_order_service_form_kids">Отправить</a>
            <p>*С Вами свяжутся в ближайшее время и запишут на удобное время к выбранному специалисту</p>
        </div>
    </div>
</div>


<div id="test-popup" class="white-popup mfp-hide">
  <p>
        Оставляя свои персональные данные, Вы даёте добровольное согласие на обработку своих персональных данных. Под персональными данными понимается любая информация, относящаяся к Вам, как субъекту персональных данных (ФИО, дата рождения, город проживания, адрес, контактный номер телефона, адрес электронной почты, род занятости и пр.). Ваше Согласие распространяется на осуществление ООО «MMЦ «ДИАЛАЙН» любых действий в отношении Ваших персональных данных, которые могут понадобиться для сбора, систематизации, хранения, уточнения (обновления, изменения), а обработки (например отправки писем или совершения звонков) и т.п. с учетом действующего законодательства. Согласие на обработку персональных данных дается без ограничения срока, но может быть отозвано Вами. Достаточно сообщить об этом в ООО «MMЦ «ДИАЛАЙН». Пересылая в ООО «MMЦ «ДИАЛАЙН» свои персональные данные, Вы подтверждаете, что с правами и обязанностями в соответствии с Федеральным Законом «О персональных данных» ознакомлены. Ознакомиться с политикой обработки и защиты персональных данных можно по <a target="_blank" href="/uploads/files/Politika_obrabotki_i_zashiti_personalnih_dannih1.pdf">ссылке</a>
  </p>
</div>