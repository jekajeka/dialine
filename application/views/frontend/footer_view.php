<div class="row row_have_a_question bukashka">
        <div class="container">
                <div class="have_a_question_title">Остались вопросы<br/>или нужна помощь?</div>
                <!--noindex-->
                <div class="have_a_question_form">
                        <input id="footer_name" type="text" placeholder="Ваше имя" />
                        <input id="footer_phone" type="text" placeholder="Ваш телефон" />
                        <a id="footer_send_form" class="send_question_form">Отправить</a>
                        <div class="footer_agree">
                                <input type="checkbox" id="footer_form_agree"><label>Добровольно согласен(а) на обработку своих персональных данных. <a target="_blank" href="https://dialine.org/uploads/files/Politika_obrabotki_i_zashiti_personalnih_dannih1.pdf">Подробнее</a></label>
                        </div>
                </div>
                <!--/noindex-->
            <div class="child_icon_beetle"></div>
        </div>
    </div>
    <div class="row row_footer">
        <div class="container">
            <div class="footer_links_wrap">
                <ul>
                    <li><a title="Подробно о клинике Диалайн" href="/info">О компании</a></li>
                    <li><a title="Вакансии в клинике Диалайн" href="/vacancy">Вакансии</a></li>
                    <li><a title="Партнеры Диалайн" href="/info/nashi_partnery">Партнеры</a></li>
                    <li><a title="Фотографии Диалайн" href="/gallery">Фотогалерея</a></li>
                    <!--<li><a href="">Отзывы</a></li>-->
                    <!--<li><a href="/info">Лицензии</a></li>-->
                </ul>
                <ul>
                    <li><a title="Полезные статьи о медицине"  href="/articles">Полезные статьи</a></li>
                    <li><a title="Социальные проекты клиники Диалайн" href="/info/social_projekt">Социальные проекты</a></li>
                    <li><a title="СМИ о клинике Диалайн" href="/smi">СМИ о нас</a></li>
                    <li><a title="Награды клиники Диалайн" href="/awards">Награды</a></li>
                </ul>
                <ul>
                    
                    <!--<li><a href="">Подобрать специалиста по симптомам</a></li>
                    <li><a href="">Найти клинику</a></li>-->

                    <li><!--noindex--><a target="_blank" rel="nofollow" href="http://диалайн-хирургия.рф/">Центр хирургии</a><!--/noindex--></li>
                    <li><a title="Диалайн, контакты" href="/info/page/contacts">Контакты</a></li>
                    <li><a title="Карта сайта" href="/info/page/sitemap">Карта сайта</a></li>
                    <li><a target="_blank" href="/uploads/files/%D0%9F%D1%80%D0%B0%D0%B9%D1%81_%D0%BE%D1%82_24_05_2018.pdf">Стоимость</a></li>
                    <!--<li><a href="">Центр флебологии</a></li>
                    <li><a href="">Центр психосоматики</a></li>-->
                </ul>
            </div>
            <div class="footer_data_wrap" itemscope itemtype="http://schema.org/Organization">
                <div class="org-name-hidden" itemprop="name">Диалайн</div>
                <div class="org-name-hidden" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                    <span itemprop="addressLocality">Волгоград</span>
                    <span itemprop="addressLocality">Волжский</span>
                    <span itemprop="addressLocality">Михайловка</span>
                </div>
                <div class="work_data">
                    <div class="work_data_title">Режим работы колл-центра:</div>
                    <div class="work_data_text">
                        <span>Пн-Вс:</span>  с 7-00 до 22-00<br/>
                    </div>
                </div>
                <div class="work_data">
                    <div class="work_data_title">Информационный центр:</div>
                    <div class="work_data_text">
                        <a class="callibri_phone" href="tel:+78442220220" itemprop="telephone"><span>+7 </span>  (8442) <span>220 - 220</span></a><br/>
                        <a href="tel:+78443450450" itemprop="telephone"><span>+7 </span>  (8443) 450 - 450</a><br/>
                        <a href="tel:+79616868222" itemprop="telephone"><span>+7 </span>  (961) 68 - 68 - 222</a>
                    </div>
                </div>
                <div class="social_wrap">
                    <div class="social_data_title">Присоединяйтесь к нам</div>
                    <div class="social_links_wrap">
                        <div class="social_links_only">
                            <!--noindex-->
                                <a target="_blank" rel="nofollow" class="fb" href="https://www.facebook.com/dialineclinics"></a>
                                <a target="_blank" rel="nofollow" class="vk" href="https://vk.com/dialinemed"></a>
                                <a target="_blank" rel="nofollow" class="utube" href="https://www.youtube.com/channel/UCARtFzDwn-05UVzFnOUR4Xg"></a>
                                <a target="_blank" rel="nofollow" class="inst" href="https://www.instagram.com/dialinemed/"></a>
                                <a target="_blank" rel="nofollow" class="ok" href="https://ok.ru/dialinemed"></a>
                            <!--/noindex-->
                        </div>
                        <!--noindex--><a class="artel" rel="nofollow" title="Разработка сайтов в Волгограде. Веб-студия Артель" href="https://artel-tm.ru"></a><!--/noindex-->
                    </div>
                </div>
            </div>
            <div class="disclaimer">ИМЕЮТСЯ ПРОТИВОПОКАЗАНИЯ, НЕОБХОДИМА КОНСУЛЬТАЦИЯ СПЕЦИАЛИСТА. МАТЕРИАЛЫ НА САЙТЕ НОСЯТ ИНФОРМАЦИОННЫЙ ХАРАКТЕР И НЕ ЯВЛЯЮТСЯ МЕДИЦИНСКОЙ КОНСУЛЬТАЦИЕЙ</div>
            <div class="copyright">Сеть многопрофильных клиник Диалайн © 2016</div>
            <div class="footer-phone">+7 (8442) 459-658</div>
            <!--noindex-->   
            <!--LiveInternet counter--><script><!--
document.write("<a style='height:1px;display:block;float:left' href='//www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t14.12;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet: показано число просмотров за 24"+
" часа, посетителей за 24 часа и за сегодня' "+
"border='0' width='1' height='1'><\/a>")
//--></script><!--/LiveInternet-->
            <!--/noindex-->
        </div>
    </div>