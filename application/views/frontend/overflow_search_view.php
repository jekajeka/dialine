<div class="row row_search" id="vrachi_data">
        <div class="row row_search_wrap">
            <div class="container">
                <div class="find_scrubs_wrap">
                    <div class="find_scrubs_title">Найдите подходящего врача</div>
                    <!--noindex-->
                    <div class="search_body">
                        <div class="search_field">
                            <input id="find_scribs_overflow" type="text" placeholder="начните поиск, например, хирург, или Иванов..." />
                            <a id="search_scrubs_overflow" class="search_scrubs"></a>
                        </div>
                        <div class="search_results">
                            <div class="search_results_title">Результаты поиска</div>
                            <div class="search_results_data" id="search_results_data_scrubs"></div>
                        </div>
                    </div>
                    <!--/noindex-->
                    <a href="/doctors" class="look_all_scrubs">Посмотреть всех</a>
                </div>
                <div class="find_special_wrap">
                    <a class="row_search_close"></a>
                    <div class="find_special_wrap_content">
                        <div class="search_specialist_row">
                            <select id="speciality">
                                <option value="all">Все направления</option>
                                <?foreach($rubrics as $rubric){?>
                                <option value="<?=$rubric->id?>"><?=$rubric->title?></option>
                                <?}?>
                            </select>
                            <select id="age">
                                <option value="all">Все возрасты</option>
                                <option value="2">Детям</option>
                                <option value="1">Взрослым</option>
                            </select>
                            <select id="clinic">
                                <option value="all">Все клиники</option>
                                <?foreach($clinics as $clinic){?>
                                <option value="<?=$clinic->id?>"><?=$clinic->title?> <?=$clinic->district?></option>
                                <?}?>
                            </select>
                        </div>
                        <div class="search_results">
                            <div class="search_results_title">Поиск по направлениям</div>
                            <div class="search_results_data">
                                <?=$this->load->view('doctors/directions_list_overflow_view',$this->data,true);?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row row_row_have_a_question_title">
            <div class="container">
                <div class="have_a_question_title">Есть вопросы<br/>Закажите звонок</div>
                <div class="have_a_question_form">
                    <input class="top_form_name" type="text" placeholder="Ваше имя" />
                    <input class="top_form_phone" type="text" placeholder="Ваш телефон" />
                    <a class="send_question_form top_form_send">Отправить</a>
                    <div class="footer_agree">
                        <input type="checkbox" id="vrachi_form_agree"><label>Добровольно согласен(а) на обработку своих персональных данных. <a target="_blank" href="/uploads/files/Politika_obrabotki_i_zashiti_personalnih_dannih1.pdf">Подробнее</a></label>
                    </div>
                </div>
                <div class="send_replay_wrap">
                    <div class="send_replay">
                        <a class="send_replay_link">Оператор Вам перезвонит</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


<div class="row row_search" id="services_data">
        <div class="row row_search_wrap">
            <div class="container">
                <div class="find_scrubs_wrap">
                    <div class="find_scrubs_title">Найдите подходящую услугу</div>
                    <div class="search_body">
                        <div class="search_field">
                            <input id="find_services_overflow" type="text" placeholder="начните поиск, например, хирургия..." />
                            <a id="search_services_overflow" class="search_scrubs"></a>
                        </div>
                        <div class="search_results">
                            <div class="search_results_title">Результаты поиска</div>
                            <div class="search_results_data" id="search_results_data_services"></div>
                        </div>
                    </div>
                    <a href="/services" class="look_all_scrubs">Показать все услуги</a>
                </div>
                <div class="find_special_wrap">
                    <a class="row_search_close"></a>
                    <div class="find_special_wrap_content">
                        <div class="search_specialist_row">
                            <a title="Направления" href="/services" id="directions_overlay" class="green_btn box_shadow ">Направления</a>
                            <a title="Анализы" href="/services/additional/analizy" id="analyses_overlay" class="green_btn box_shadow ">Анализы</a>
                            <a title="Комплексные медицинские программы" href="/services" id="program_overlay" class="green_btn box_shadow">Программы</a>
                            <a title="Медосмотры" href="/medosmortry" id="medical_review_overlay" class="green_btn box_shadow ">Медосмотры</a>
                            <a title="ДМС" href="/dlya_vladeltsev_polisov_dms" id="dmc_overlay" class="green_btn box_shadow ">ДМС</a>
                        </div>
                        <div class="search_results">
                            <div class="search_results_title">Поиск по направлениям</div>
                            <div class="search_results_data">
                                <?=$this->load->view('services/services_list_overflow_view',$this->data,true);?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row row_row_have_a_question_title">
            <div class="container">
                <div class="have_a_question_title">Есть вопросы<br/>Закажите звонок</div>
                <div class="have_a_question_form">
                    <input class="top_form_name" type="text" placeholder="Ваше имя" />
                    <input class="top_form_phone" type="text" placeholder="Ваш телефон" />
                    <a class="send_question_form top_form_send">Отправить</a>
                    <div class="footer_agree">
                        <input type="checkbox" id="services_form_agree"><label>Добровольно согласен(а) на обработку своих персональных данных. <a target="_blank" href="/uploads/files/Politika_obrabotki_i_zashiti_personalnih_dannih1.pdf">Подробнее</a></label>
                    </div>
                </div>
                <div class="send_replay_wrap">
                    <div class="send_replay">
                        <a class="send_replay_link">Оператор Вам перезвонит</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
