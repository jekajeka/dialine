<div class="row main_menu_mobile">
    <a class="close_mobile_menu"></a>
    <div class="container">
        <div class="mobile_wrap_menu">
            <ul class="main_menu_ul">
                <li><a href="/services">Услуги</a></li>
                <li><a href="/doctors">Врачи</a></li>
                <li><a href="/kids" id="vrachi_menu">Для детей</a><span class="child_icon_butterfly_mbl"></span></li>
                <li><a target="_blank" href="/uploads/files/%D0%9F%D1%80%D0%B0%D0%B9%D1%81_%D0%BE%D1%82_24_05_2018.pdf">Стоимость</a></li>
                <li><a href="/komplex_programm">Комплексные программы</a></li>
                <li><a href="/services/additional/tsentr_profilaktiki_zdorovya">Корпоративным клиентам</a></li>
                <li><a href="/actions">Акции</a></li>
                <li><a href="/news">Новости</a></li>
                <li><a href="/info">О компании</a></li>
                <li><a href="/info/patsientu">Справочная</a></li>
                <li><a href="/info/pravovaya_informaciya">Правовая информация</a></li>
                <li><a href="/gallery">Фотогалерея</a></li>
                <li><a href="/reviews">Отзывы</a></li>
                <li><a href="/vacancy">Вакансии</a></li>
                <li><a href="/info/page/contacts">Контакты</a>
                <li><a target="_blank" href="https://lk.dialine.org/login">Личный кабинет</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row back_call_widget">
    <div class="container">
        <div class="call_mobile_btn">Позвонить
            <div class="back_call_phones">
                <a class="callibri_phone1" data-city="Волгоград" href="tel:+7(8442)220-220" data-phone="+7 (8442) 220 - 220">Волгоград</a>
                <a  class="callibri_phonevlz" data-city="Волжский" href="tel:+7(8443)450-450" data-phone="+7 (8443) <span>450 - 450</span>">Волжский</a>
                <a  class="callibri_phonemih" data-city="Михайловка" href="tel:+7(961)68-68-222" data-phone="+7 (961) <span>68 - 68 - 222</span>">Михайловка</a>
            </div>
        </div>
        <a class="" id="order_from_bottom_mobile" >Записаться</a>

        <?php if(strpos($_SERVER["REQUEST_URI"], '/kids/') !== false || strpos($_SERVER["REQUEST_URI"], '/kids') !== false) { ?>
            <div class="child_icon_bird_bottom_mbl"></div>
        <?php } ?>
    </div>
</div>