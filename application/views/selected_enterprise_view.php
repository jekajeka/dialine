<? foreach($enterprise_data as $enterprise){?>
    <div class="sigle_organisation" id="sigle_organisation_<?=$enterprise->id?>">
    <?
    $okved_image = '/images/org.jpg';
    switch ($enterprise->real_okved) {
    case '01.23':
        $okved_image = '/images/okved/svin.png';
        break;
    case '01.24':
        $okved_image = '/images/okved/ptic.png';
        break;
    case '01.22.2':
        $okved_image = '/images/okved/kon.png';
        break;
    case '01.21':
        $okved_image = '/images/okved/skot.png';
        break;
    case '01.22.1':
        $okved_image = '/images/okved/koz.png';
        break;
    case '01.25.2':
        $okved_image = '/images/okved/krol.png';
        break;
    case '01.13.2':
        $okved_image = '/images/okved/frukt.png';
        break;
    case '01.13':
        $okved_image = '/images/okved/frukt.png';
        break;
    case '01.13':
        $okved_image = '/images/okved/frukt.png';
        break;
    case '01.11.2':
        $okved_image = '/images/okved/ovo.png';
        break;
    case '01.11.8':
        $okved_image = '/images/okved/ovo.png';
        break;
    case '01.12':
        $okved_image = '/images/okved/ovo.png';
        break;
    case '01.12.1':
        $okved_image = '/images/okved/ovo.png';
        break;
    case '01.1':
        $okved_image = '/images/okved/rast.png';
        break;
    case '01.11.6':
        $okved_image = '/images/okved/rast.png';
        break;
    case '01.11.7':
        $okved_image = '/images/okved/rast.png';
        break;
    case '01.11.8':
        $okved_image = '/images/okved/rast.png';
        break;
    case '01.12.2':
        $okved_image = '/images/okved/rast.png';
        break;
    case '01.11.6':
        $okved_image = '/images/okved/rast.png';
        break;
    }
    ?>
        <div class="organisation_image"><img src="<?=$okved_image?>" /></div>
        <h2><?=$enterprise->title?></h2>
        <?if(!empty($enterprise->size)){?>
        <div class="full_name_label full_name_label_size"><?=$enterprise->size?></div>
        <?}?>
        <div class="full_name_label">Полное наименование:</div>
        <div class="full_name"><?=$enterprise->full_title?></div>
        <div class="full_name_label">Руководитель:</div>
        <div class="addr"><?=$enterprise->post.' '.$enterprise->fio?></div>
        <div class="full_name_label">Деятельность:</div>
        <div class="addr addr_work"><?=$enterprise->full_okved?></div>
        <div class="full_name_label">Адрес:</div>
        <div class="addr addr_field" ><?=$enterprise->address?></div>        
        <a enterprise_id="<?=$enterprise->id?>" class="show_on_map" href="<?=$enterprise->coords?>">Показать на карте</a>
    </div>
<?}?>