<div class="mainInfo">

	<div class="pageTitle"><h2>Авторизация</h2></div>
	<p>Пожалуйста, авторизуйтесь, введя логин или email ниже.</p>
	
	<div id="infoMessage"><?=$message;?></div>
	
    <?=form_open("auth/login");?>
    	<dl class="authorisation">
		<dd><label for="identity">Логин/Email:</label></dd>
		<dt><?=form_input($identity);?></dt>
	
		<dd><label for="password">Пароль:</label></dd>
		<dt><?=form_input($password);?></dt>
      
		<dd><label for="remember">Запомнить:</label></dd>
		<dt><?=form_checkbox('remember', '1', FALSE);?></dt>
		<dd> </dd><dt><?=form_submit('submit', 'Войти');?></dt>
	
	</dl>
      
    <?=form_close();?>

</div>