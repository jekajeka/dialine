<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$title?></title>
<?=$this->load->view('frontend/header_view','',TRUE);?>
</head>
<body>
    <?=$this->load->view('frontend/main_menu_view','',TRUE);?>
    <section>
    <div class="row super_slider">
        <div class="swiper-wrapper">
            <div class="swiper-slide" style="background: url(/frontend/images/big_image.jpg) no-repeat center;background-size: cover">
                <div class="container">
                    <div class="swiper_table">
                        <div class="swiper_table_child">
                            <div class="green_field">
                                <div class="title">СПЕЦИАЛЬНЫЕ УСЛОВИЯ ПОСЕЩЕНИЯ АЛЛЕРГОЛОГА НА ВСЕ ЛЕТО</div>
                                <p>
                                    Мы понимаем, как сложно жить с аллергией и поэтому предлагаем первое обследование бесплатно!
                                </p>
                                <a href="">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide" style="background: url(/frontend/images/big_image.jpg) no-repeat center;background-size: cover">
            </div>
        </div>
        <div class="switches"></div>
    </div>
    <div class="row row_advantages">
        <div class="container">
            <div class="advantage_wrap" style="background: url(/frontend/images/specialists.png) no-repeat 190px 170px">
                <div class="advatage_icon" style="background: url(/frontend/images/specialists_min.png) no-repeat bottom left"></div>
                <div class="advantage_title">Опытные<br/>специалисты</div>
                <div class="advantage_overlay">
                    <div class="advantage_overlay_title">Опытные специалисты</div>
                    <div class="advantage_overlay_text">
                        <p>
                            Мы работаем без перерывов и выходных с максимальной оперативностью, в клиниках предусмотрена удобная зона ожидания для пациентов, в том числе и для малышей, просторная парковка. Наш колл центр на связи с 7 до 22:00.
                        </p>
                    </div>
                </div>
            </div>
            <div class="advantage_wrap" style="background: url(/frontend/images/specialists.png) no-repeat 190px 170px ">
                <div class="advatage_icon" style="background: url(/frontend/images/specialists_min.png) no-repeat bottom left"></div>
                <div class="advantage_title">Комфортные условия</div>
                <div class="advantage_overlay">
                    <div class="advantage_overlay_title">Комфортные условия</div>
                    <div class="advantage_overlay_text">
                        <p>
                            Мы работаем без перерывов и выходных с максимальной оперативностью, в клиниках предусмотрена удобная зона ожидания для пациентов, в том числе и для малышей, просторная парковка. Наш колл центр на связи с 7 до 22:00.
                        </p>
                    </div>
                </div>
            </div>
            <div class="advantage_wrap" style="background: url(/frontend/images/profil.png) no-repeat 180px 170px">
                <div class="advatage_icon" style="background: url(/frontend/images/profil_min.png) no-repeat bottom left"></div>
                <div class="advantage_title">Многопрофильность</div>
                <div class="advantage_overlay">
                    <div class="advantage_overlay_title">Многопрофильность</div>
                    <div class="advantage_overlay_text">
                        <p>
                            Мы работаем без перерывов и выходных с максимальной оперативностью, в клиниках предусмотрена удобная зона ожидания для пациентов, в том числе и для малышей, просторная парковка. Наш колл центр на связи с 7 до 22:00.
                        </p>
                    </div>
                </div>
            </div>
            <div class="advantage_wrap" style="background: url(/frontend/images/sovr.png) no-repeat 160px 170px">
                <div class="advatage_icon" style="background: url(/frontend/images/sovr_min.png) no-repeat bottom left"></div>
                <div class="advantage_title">Современное оборудование</div>
                <div class="advantage_overlay">
                    <div class="advantage_overlay_title">Современное оборудование</div>
                    <div class="advantage_overlay_text">
                        <p>
                            Мы работаем без перерывов и выходных с максимальной оперативностью, в клиниках предусмотрена удобная зона ожидания для пациентов, в том числе и для малышей, просторная парковка. Наш колл центр на связи с 7 до 22:00.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container more_advantages">
            <a class="more_advantages_btn">Ещё</a>
        </div>
    </div>
    <div class="row row_title">
        <div class="container">Найти ближайшую к Вам клинику</div>
    </div>
    <div class="row">
        <div class="container">
            <div class="map_operate_wrap">
                <div class="search_input_wrap">
                    <input type="text" placeholder="Введите свой адрес" />
                </div>
                <div class="addresses_wrap">
                    <div class="single_address_wrap">
                        <div class="single_addr_title">Клиника Диалайн на ул. 50-тия Октября, 27 (Красноармейский район)</div>
                        <div class="single_addr_data_wrap">
                            <div class="phone_data"><a href="tel:">+7 (8442) 300–700</a></div>
                            <div class="time_data">07:00–16:00</div>
                        </div>
                    </div>
                    <div class="single_address_wrap">
                        <div class="single_addr_title">Клиника Диалайн на ул. 50-тия Октября, 27 (Красноармейский район)</div>
                        <div class="single_addr_data_wrap">
                            <div class="phone_data"><a href="tel:">+7 (8442) 300–700</a></div>
                            <div class="time_data_wrap">
                                <div class="time_data">Пн-пт: 07:00–16:00</div>
                                <div class="time_data">Сб: 07:00–16:00</div>
                                <div class="time_data">Вс: 07:00–16:00</div>
                            </div>
                        </div>
                    </div>
                    <div class="single_address_wrap">
                        <div class="single_addr_title">Клиника Диалайн на ул. 50-тия Октября, 27 (Красноармейский район)</div>
                        <div class="single_addr_data_wrap">
                            <div class="phone_data"><a href="tel:">+7 (8442) 300–700</a></div>
                            <div class="time_data">07:00–16:00</div>
                        </div>
                    </div>
                    <div class="single_address_wrap">
                        <div class="single_addr_title">Клиника Диалайн на ул. 50-тия Октября, 27 (Красноармейский район)</div>
                        <div class="single_addr_data_wrap">
                            <div class="phone_data"><a href="tel:">+7 (8442) 300–700</a></div>
                            <div class="time_data">07:00–16:00</div>
                        </div>
                    </div>
                    <div class="single_address_wrap">
                        <div class="single_addr_title">Клиника Диалайн на ул. 50-тия Октября, 27 (Красноармейский район)</div>
                        <div class="single_addr_data_wrap">
                            <div class="phone_data"><a href="tel:">+7 (8442) 300–700</a></div>
                            <div class="time_data">07:00–16:00</div>
                        </div>
                    </div>
                    <div class="single_address_wrap">
                        <div class="single_addr_title">Клиника Диалайн на ул. 50-тия Октября, 27 (Красноармейский район)</div>
                        <div class="single_addr_data_wrap">
                            <div class="phone_data"><a href="tel:">+7 (8442) 300–700</a></div>
                            <div class="time_data">07:00–16:00</div>
                        </div>
                    </div>
                    <div class="single_address_wrap">
                        <div class="single_addr_title">Клиника Диалайн на ул. 50-тия Октября, 27 (Красноармейский район)</div>
                        <div class="single_addr_data_wrap">
                            <div class="phone_data"><a href="tel:">+7 (8442) 300–700</a></div>
                            <div class="time_data">07:00–16:00</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="" id="map"></div>
        </div>
    </div>
    <div class="row plancs">
        <div class="container">
            <div class="planc_wrap">
                <div class="plank_text_wrap">
                    <a class="plank_title">Запись на прием</a>
                    <div class="plank_text">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    </div>
                    <a class="plank_link plank_link_order" href="">Запись на прием</a>
                    <div class="plank_overlay"></div>
                </div>
                <div class="plank_img_wrap" style="background: url(/frontend/images/plank_img_wrap.jpg) no-repeat center;background-size: cover"></div>
            </div>
            <div class="planc_wrap">
                <div class="plank_text_wrap">
                    <a class="plank_title">Запись на прием</a>
                    <div class="plank_text">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    </div>
                    <a class="plank_link plank_link_pay" href="">Запись на прием</a>
                    <div class="plank_overlay"></div>
                </div>
                <div class="plank_img_wrap" style="background: url(/frontend/images/plank_img_wrap2.jpg) no-repeat center;background-size: cover"></div>
            </div>
        </div>
    </div>
    <div class="row row_video">
        <div class="container">
            <div class="block6">
                <div class="video_text_wrap">
                    <div class="video_title">Открытие новой клиники ДИАЛАЙН в Дзержинском районе</div>
                    <div class="video_text">
                        Компания Диалайн существует на волгоградском рынке с 2001 года. За это время открыто семь многопрофильных клиник Диалайн, которые объединяет единый четкий стиль работы. Он основан на принципах доказательной медицины, преемственности амбулаторного и стационарного звеньев медицинской помощи, оптимальном сочетании классических и современных технологий, в том числе малоинвазивных хирургических и пр.
                    </div>
                </div>
            </div>
            <div class="block6">
                <div class="video_vrap">
                    <iframe width="100%" height="290" src="https://www.youtube.com/embed/r5gF2IZook0" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div class="container all_video_link">
            <a href="">Смотреть больше видео о клинике Диалайн</a>
        </div>
    </div>
    <div class="row row_reviews">
        <div class="container">
            <div class="leave_review_wrap">
                <div class="leave_review_title">Оставить отзыв</div>
                <div class="review_form">
                    <input type="text" id="review_name" placeholder="Ваше имя" />
                    <input type="text" id="review_email" placeholder="E-mail" />
                    <textarea id="review_text" placeholder="Ваш отзыв"></textarea>
                    <a class="big_green_btn">Отправить</a>
                </div>
            </div>
            <div class="reviews_title">Последние отзывы</div>
            <div class="reviews_container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="single_review_wrap">
                            <div class="single_review_title_wrap">
                                Татьяна
                                <span>06 июля 2016</span>
                            </div>
                            <div class="addr_review">Клиника Диалайн на ул. 50-лет Октября, 27<br/>Врач Чернявский В. И</div>
                            <div class="text_review">
                                <p>
                                    Очень понравилось, обслуживание, очень приятный доктор, в регистратуре девочки приветливые, доброжелательные, оформили, рассказали как пройти к кабинету, добродушные такие!! Осталась очень довольна вашим медицинским центром!!
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="single_review_wrap">
                            <div class="single_review_title_wrap">
                                Ксения
                                <span>16 января 2016</span>
                            </div>
                            <div class="addr_review">Клиника Диалайн на ул. Кирова, 19Б (г. Волжский)<br/>Врач Егорова Анастасия Сергеевна</div>
                            <div class="text_review">
                                <p>
                                    Здравствуйте. Давно решила для себя,что лучше обратиться в вашу клинику чем в обычную поликлинику.Пока меня не разочаровало отношение ваших специалистов к пациентам.У вас работают замечательные специалисты такие 
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="reviews_control">
                    <a class="reviews_left"></a>
                    <a class="reviews_right"></a>
                </div>
                <a class="reviews_all_link" href="">Все отзывы</a>
            </div>
        </div>
    </div>
    <div class="row row_title">
        <div class="container">Последние новости</div>
    </div>
    <div class="row row_news">
        <div class="container">
            <div class="news_item_wrap">
                <a class="news_img_link" href=""><img src="/frontend/images/news_image.jpg" /></a>
                <div class="news_date">06.07.2016</div>
                <h2>ДИАЛАЙН – это лидер частной медицины Волгограда</h2>
                <p>
                    Компания ДИАЛАЙН впервые заявила о себе в 2001 году с открытием первой в Волгограде независимой лаборатории ДИАЛАЙН, где
                </p>
                <a class="news_full_link" href="">Подробнее</a>
            </div>
            <div class="news_item_wrap">
                <a class="news_img_link" href=""><img src="/frontend/images/news_image.jpg" /></a>
                <div class="news_date">06.07.2016</div>
                <h2>ДИАЛАЙН – это лидер частной медицины Волгограда</h2>
                <p>
                    Компания ДИАЛАЙН впервые заявила о себе в 2001 году с открытием первой в Волгограде независимой лаборатории ДИАЛАЙН, где
                </p>
                <a class="news_full_link" href="">Подробнее</a>
            </div>
            <div class="news_item_wrap">
                <a class="news_img_link" href=""><img src="/frontend/images/news_image.jpg" /></a>
                <div class="news_date">06.07.2016</div>
                <h2>ДИАЛАЙН – это лидер частной медицины Волгограда</h2>
                <p>
                    Компания ДИАЛАЙН впервые заявила о себе в 2001 году с открытием первой в Волгограде независимой лаборатории ДИАЛАЙН, где
                </p>
                <a class="news_full_link" href="">Подробнее</a>
            </div>
            <div class="news_item_wrap">
                <a class="news_img_link" href=""><img src="/frontend/images/news_image.jpg" /></a>
                <div class="news_date">06.07.2016</div>
                <h2>ДИАЛАЙН – это лидер частной медицины Волгограда</h2>
                <p>
                    Компания ДИАЛАЙН впервые заявила о себе в 2001 году с открытием первой в Волгограде независимой лаборатории ДИАЛАЙН, где
                </p>
                <a class="news_full_link" href="">Подробнее</a>
            </div>
        </div>
        <div class="container wrap_big_link">
            <a href="" class="big_green_btn">Все новости</a>
        </div>
    </div>
    <?=$this->load->view('frontend/footer_view','',TRUE);?>
    </section>
    <?=$this->load->view('frontend/overflow_search_view','',TRUE);?>
</body>
<script src="/frontend/js/jquery-1.10.1.min.js"></script>
<script src="/frontend/js/jquery.mobile.custom.min.js"></script>
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="/frontend/js/idangerous.swiper-2.1.min.js"></script>
<script src="/frontend/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="/frontend/js/jquery.pajinate.min.js"></script>
<script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="/frontend/js/jquery.waypoints.min.js"></script>
<script src="/frontend/js/index.js"></script>
<script src="/frontend/js/main.js"></script>
<script src="/frontend/js/contacts.js"></script>
<script type="text/javascript" src="/frontend/js/jquery.inputmask.min.js"></script>
<link href="http://allfont.ru/allfont.css?fonts=franklin-gothic-book" rel="stylesheet" type="text/css" />
<link href="http://allfont.ru/allfont.css?fonts=franklin-gothic-medium" rel="stylesheet" type="text/css" />
<link href="http://allfont.ru/allfont.css?fonts=franklin-gothic-demi-cond" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" media="screen" href="frontend/css/idangerous.swiper.css" />
<link rel="stylesheet" href="frontend/css/jquery.mCustomScrollbar.css">
	
<link href="/frontend/css/dialine.css" rel="stylesheet" />
<link href="/frontend/css/dialine_portrait.css" rel="stylesheet" media="screen and (orientation:portrait)" />
</html>
